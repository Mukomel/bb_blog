const CronJob = require('cron').CronJob;
const Round = require('../models/Round');

Round.getActiveRound()
    .then(round => {
        if (!round) Round.create({});
    });

const startNewRound = new CronJob({
    cronTime: '0 0 0 * * 1',
    onTick: () => {
        Round.update({is_active: true}, {$set: { is_active: false } }, {multi: true })
            .then(() => {
                Round.create({});
            })
    }
});

module.exports = startNewRound;