const config = require("./config");

const stripe = require('stripe')(config[process.env.NODE_ENV].STRIPE_SECRET_KEY);

module.exports = stripe;