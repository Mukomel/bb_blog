const THIRTY_DAYS_IN_MS = 2592000000;

db.subscriptions.find().forEach((subscription) => {
    const user = db.users.findOne({_id: subscription.user});
    if (!user.visible_content_periods || (user.visible_content_periods.length === 0)) {
        user.visible_content_periods = [
            {
                start: new Date(subscription.created_at.getTime() - THIRTY_DAYS_IN_MS),
                end: new Date(subscription.created_at.getTime() + THIRTY_DAYS_IN_MS),
                type: 'SUBSCRIPTION'
            }
        ];
        db.users.save(user);
    }
});