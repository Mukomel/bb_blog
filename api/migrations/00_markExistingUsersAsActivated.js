db.users.find().forEach((user) => {
    if (!user.activation_token) {
        user.is_activated = true;
        db.users.save(user)
    }
});