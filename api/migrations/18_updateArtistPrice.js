db.artists.find().forEach((artist) => {
    if (artist.subscription_price_cents !== undefined) {
        artist.subscription_price_cents_first_30_days = artist.subscription_price_cents;
        artist.subscription_price_cents_recurring_30_days = artist.subscription_price_cents;
        delete artist.subscription_price_cents;

        db.artists.save(artist);
    }
});
