db.artists.find().forEach((artist) => {
    if (!artist.first_post_date) {
        const firstPost = db.posts.findOne({artist: artist._id});
        artist.first_post_date = firstPost.created_at;
        db.artists.save(artist);
    }
});