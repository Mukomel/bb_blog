db.users.find().forEach((user) => {
    if (user.receive_emails === undefined) {
        user.receive_emails = true;
        db.users.save(user);
    }
});
