db.artists.find().forEach((artist) => {
    if (artist.wishlist === undefined) {
        const wishlistInsertResult = db.groupwishlists.insertOne({
            title: "",
            description: "",
            link: "",
            is_enabled: false
        });

        artist.wishlist = wishlistInsertResult.insertedId;
        db.artists.save(artist);
    }
});
