db.users.find({role: 'AUTHOR'}).forEach((author) => {
    if (author.subscribers === undefined) {
        author.subscribers = [];
        db.users.save(author);
    }
});