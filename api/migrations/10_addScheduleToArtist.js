db.artists.find().forEach((artist) => {
    if (artist.schedule === undefined) {
        const defaultDay = {
            description: ''
        };
        const scheduleInsertResult = db.schedules.insertOne({
            monday: defaultDay,
            tuesday: defaultDay,
            wednesday: defaultDay,
            thursday: defaultDay,
            friday: defaultDay,
            saturday: defaultDay,
            sunday: defaultDay
        });

        artist.schedule = scheduleInsertResult.insertedId;
        db.artists.save(artist);
    }
});