const THIRD_OF_AUGUST_DATE = 1533254401000;

db.users.find().forEach((user) => {
    const subscription = db.subscriptions.findOne({user: user._id, is_canceled: false});
    if (subscription && subscription.stripe_discount) {
        user.visible_content_periods = user.visible_content_periods.map(period => {
            return Object.assign({}, period, {end: new Date(THIRD_OF_AUGUST_DATE)})
        });
        user.has_monthly_subscription = true;
    } else {
        user.has_monthly_subscription = false;
    }
    db.users.save(user);
});