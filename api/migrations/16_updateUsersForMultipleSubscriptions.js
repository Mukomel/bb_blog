var SUBSCRIBED_ARTIST_ID = "";

if (SUBSCRIBED_ARTIST_ID) {
    db.users.find().forEach((user) => {
        if (user.has_monthly_subscription !== undefined) {
            if (user.has_monthly_subscription) {
                user.artists_subscriptions = [ObjectId(SUBSCRIBED_ARTIST_ID)];
            } else {
                user.artists_subscriptions = [];
            }
            delete user.has_monthly_subscription;
            db.users.save(user);
        }
    });

    db.segpaysubscriptions.find().forEach((subscription) => {
        if (subscription.artist === undefined) {
            subscription.artist = ObjectId(SUBSCRIBED_ARTIST_ID);
            db.segpaysubscriptions.save(subscription);
        }
    });
} else {
    print("Please modifiy migration file to insert Subscription artist id");
}

