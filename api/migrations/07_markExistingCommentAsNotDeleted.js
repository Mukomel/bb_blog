db.comments.find().forEach((comment) => {
    if (comment.is_user_deleted === undefined) {
        comment.is_user_deleted = false;
        db.comments.save(comment)
    }
});