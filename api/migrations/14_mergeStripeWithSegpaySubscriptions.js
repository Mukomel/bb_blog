db.subscriptions.find().forEach((subscription) => {
    db.segpaysubscriptions.insertOne({
        price: String(subscription.subscription_price/100),
        created_at: subscription.created_at,
        is_canceled: Boolean(subscription.stripe_discount) ? subscription.is_canceled : true,
        is_pending_cancellation: false,
        user: subscription.user,
        is_old_stripe_subscription: true,
        with_stripe_coupon: Boolean(subscription.stripe_discount)
    })
});
