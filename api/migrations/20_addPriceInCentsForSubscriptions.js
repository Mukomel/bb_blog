var stringPriceToCents = (stringPrice) => {
    if (isNaN(Number(stringPrice))) {
        return 0;
    } else {
        return Number(stringPrice) * 100;
    }
};


db.segpaysubscriptions.find().forEach((subscription) => {
    if (subscription.price) {
        subscription.amount = stringPriceToCents(subscription.price);
        db.segpaysubscriptions.save(subscription);
    }
});