db.comments.find().forEach((comment) => {
    let isUpdated = false;

    if (comment.type === undefined) {
        isUpdated = true;
        comment.type = 'COMMENT';
    }

    if (comment.replies === undefined) {
        isUpdated = true;
        comment.replies = [];
    }

    if (isUpdated) {
        db.comments.save(comment);
    }
});