db.artists.find().forEach((artist) => {
    if (artist.tip_jar_enabled === undefined) {
        artist.tip_jar_enabled = true;
        db.artists.save(artist);
    }
});