db.artists.find().forEach((artist) => {
    var postsCount = 0;
    var postsViewsTotal = 0;
    db.posts.find({artist: artist._id}).forEach(post => {
        postsCount++;
        if (post.views) {
            postsViewsTotal = postsViewsTotal + post.views;
        }
    });

    var isUpdated = false;

    if (artist.views_count === undefined) {
        artist.views_count = postsViewsTotal;
        isUpdated = true;
    }

    if (artist.posts_count === undefined) {
        artist.posts_count = postsCount;
        isUpdated = true;
    }

    if (isUpdated) {
        db.artists.save(artist);
    }
});
