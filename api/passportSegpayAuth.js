const passport = require("passport");
const passportHttp = require("passport-http");
const config = require('./config');
const segpayPostbackResponses = require('./constants/segpayPostbackResponses');

const BasicStrategy = passportHttp.BasicStrategy;

const strategy = new BasicStrategy(function(username, password, done) {
    if ((username === config[process.env.NODE_ENV].SEGPAY_POSTBACK_AUTH_USERNAME)
        && (password === config[process.env.NODE_ENV].SEGPAY_POSTBACK_AUTH_PASSWORD)) {
        return done(null, true);
    } else {
        return done('Unathorized', false);
    }
});

passport.use(strategy);

const authenticate = (req, res, next) => {
    passport.authenticate('basic', {session: false}, function (err, user, info) {

        if (err || !user) {
            return res.send(segpayPostbackResponses.NOT_SUCCESSFUL)
        }

        return next();

    })(req, res, next);
};

module.exports = {
    authenticate
};

