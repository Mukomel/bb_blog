const config = require('../config');
const roundTo = require('./roundTo');

const usdCentsToPoints = (usdCents) => {
    return roundTo(usdCents * config.USD_CENTS_TO_POINTS_RATE, 2);
};

module.exports = usdCentsToPoints;