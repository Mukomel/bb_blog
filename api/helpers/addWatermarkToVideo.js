const path = require('path');
const fs = require('fs-extra');
const ffmpeg = require('fluent-ffmpeg');
const changeFileExtension = require('./changeFileExtension');

const addWatermarkToVideo = ({
    watermarkPath,
    fileStorageFolderPath,
    originalFileName,
    onSuccess
}) => {
    const watermarkedFileName = changeFileExtension(originalFileName, 'mp4');
    const newOriginalFileName = 'temp-' + originalFileName;
    const center = 290;
    const left = 370;
    const right = 10;
    return fs.rename(
        path.resolve(fileStorageFolderPath, originalFileName),
        path.resolve(fileStorageFolderPath, newOriginalFileName)
    )
        .then(() => {

            ffmpeg()
                .input(path.resolve(fileStorageFolderPath, newOriginalFileName))
                .input(watermarkPath)
                .inputOptions('-itsoffset 10')
                .videoCodec('libx264')
                .complexFilter([
                    "[0:v]scale=-1:-1[bg];[bg][1:v]overlay=W-w-"+right+":H-h-10"
                ])
                .save(path.resolve(fileStorageFolderPath, watermarkedFileName))
                .on('end', function() {
                    fs.remove(path.resolve(fileStorageFolderPath, newOriginalFileName))
                        .then(() => onSuccess({watermarkedFileName, type: 'video'}))
                });
        })
};

module.exports = addWatermarkToVideo;
