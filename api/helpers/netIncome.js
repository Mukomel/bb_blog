const config = require('../config');
const roundTo = require('../helpers/roundTo');

const calculateFee = income => {
    const segpayFeeRate = 1 / config.SEGPAY_FEE_PERCENT;

    return roundTo(income * segpayFeeRate, 0);
};

const calculateNetIncome = (income, expense) => {

    return roundTo(income - calculateFee(income) - expense, 0);
};

module.exports = {
    calculateFee,
    calculateNetIncome
};