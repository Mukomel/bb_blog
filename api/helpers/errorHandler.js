const errorHandler = (error, res) => {
    res.status(error.status).send({message: error.message});
};

module.exports = errorHandler;