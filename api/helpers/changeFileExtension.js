const changeFileExtension = (fileName, extension) => {
    const fileNameArray = fileName.split('.');
    fileNameArray.pop();
    fileNameArray.push(extension);
    return fileNameArray.join('.');
};

module.exports = changeFileExtension;