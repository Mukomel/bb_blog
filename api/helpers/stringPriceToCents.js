// Converts string price (e.g. 2.99) to amount in cents

const stringPriceToCents = (stringPrice) => {
    if (isNaN(Number(stringPrice))) {
        return 0;
    } else {
        return Number(stringPrice) * 100;
    }
};

module.exports = stringPriceToCents;