const emails = require('./emails');
const config = require('../config');
const User = require('../models/User');
const Post = require('../models/Post');
const Artist = require('../models/Artist');
const Notification = require('../models/Notification');

const sendNewPostNotifications = (postId) => {
    Post.getById(postId)
        .then(post => {
            return Promise.all([
                Promise.resolve(post),
                Artist.getArtistById(post.artist),
                User.getById(post.user)
                    .then(postAuthor => {
                        return User.populate(postAuthor, {path: 'subscribers', select: '_id email username'})
                    })
            ])
        })
        .then(([post, artist, postAuthor]) => {
            const messagesList = [];
            postAuthor.subscribers.forEach(user => {
                if (user) {
                    const artistLink = config[process.env.NODE_ENV].SERVER_DOMAIN + '/artists/' + artist.slug;
                    if (user.receive_emails) {
                        messagesList.push({
                            to: user.email,
                            subject: 'New post from ' + artist.name,
                            html: 'Hey there, ' + user.username + ', ' +
                            '<br/> I just made a new post on Brandtsboys. Click the link below to check it out!' +
                            '<br/>' + artistLink +
                            '<br/> Yours truly,' +
                            '<br/>' + artist.name
                        });
                    }
                    Notification.create({
                        user: user._id,
                        title: 'New post from ' + artist.name,
                        message: post.title
                    })
                }
            });
            emails.sendMultipleDifferentEmails(messagesList)
        })
};

module.exports = sendNewPostNotifications;
