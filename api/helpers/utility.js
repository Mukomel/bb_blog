const updatePropIfExists = (object, prop, key) => {
    if (prop !== undefined) {
        object[key] = prop;
    }
};

module.exports = {
    updatePropIfExists
};