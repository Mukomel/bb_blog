const qs = require('qs');
const axios = require('axios');
const config = require('../config');

const BASE_URL = 'http://srs.segpay.com/ADM.asmx';
const authQuery = {
    Userid: config[process.env.NODE_ENV].SEGPAY_USER_ID,
    UserAccessKey: config[process.env.NODE_ENV].SEGPAY_USER_ACCESS_KEY
};

const request = ({url, query, baseURL = BASE_URL}) => {
    const resultQueryObject = {
        ...query,
        ...authQuery
    };

    return axios({
        url,
        method: 'get',
        params: resultQueryObject,
        baseURL
    })
};

const cancelSubcription = (purchaseId) => {
    return request({
        url: '/CancelMembership',
        query: {PurchaseID: purchaseId, CancelReason: 'Cancellation'}
    })
};

const expireSubscription = (purchaseId, expireReason) => {
    return request({
        url: '/ExpireMembership',
        query: {PurchaseID: purchaseId, CancelReason: expireReason || 'Cancellation'}
    })
};

const getAmountHash = (amountString) => {
    return request({
        baseURL: 'https://srs.segpay.com',
        url: '/PricingHash/PricingHash.svc/GetDynamicTrans',
        query: {value: amountString}
    })
};

module.exports = {
    cancelSubcription,
    expireSubscription,
    getAmountHash
};
