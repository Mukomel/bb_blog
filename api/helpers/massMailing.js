const sanitizeHtml = require('sanitize-html');
const htmlToText = require('html-to-text');

const prepareEmailHtml = (html) => {
    return sanitizeHtml(html, {
        allowedTags: [ 'b', 'i', 'em', 'strong', 'del', 'a', 'br', 'p', 'ul', 'ol', 'li'],
        allowedAttributes: {
            'a': [ 'href' ]
        }
    });
};

const prepareNotificationText = (html) => {
    return htmlToText.fromString(html, {
        wordwrap: false,
        singleNewLineParagraphs: true,
        format: {
            anchor: (elem, fn, options) => {
                const linkText = fn(elem.children, options);
                return `<a href="${elem.attribs.href}">${linkText}</a>`;
            }
        }
    });
};

module.exports = {
    prepareEmailHtml,
    prepareNotificationText
};