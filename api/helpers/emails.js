const sgMail = require('@sendgrid/mail');
const config = require('../config');

sgMail.setApiKey(config[process.env.NODE_ENV].EMAIL_PASS);
const EMAILS_PER_REQUEST_LIMIT = 800;

const getEmailMessage = (options) => ({
    from: config[process.env.NODE_ENV].EMAIL_USER,
    ...options
});

const getChunksOfEmails = (emailsList) => {
    const result = [];
    let restEmailsList = emailsList.slice();
    while (restEmailsList.length > 0) {
        const emailsChunk = restEmailsList.slice(0, EMAILS_PER_REQUEST_LIMIT);
        restEmailsList = restEmailsList.slice(EMAILS_PER_REQUEST_LIMIT, restEmailsList.length);
        result.push(emailsChunk);
    }
    return result;
};

const sendEmail = (options) => {
    return sgMail.send(getEmailMessage(options));
};

const sendMultipleEmails = (options, emailsList) => {
    const emailsChunks = getChunksOfEmails(emailsList);
    return Promise.all(emailsChunks.map(emails => {
        return sgMail.send(getEmailMessage({
            ...options,
            to: emails,
            isMultiple: true
        }))
    }))
};

const sendMultipleDifferentEmails = (messages) => {
    const emailsChunks = getChunksOfEmails(messages);
    return Promise.all(emailsChunks.map(messagesChunk => {
        return sgMail.send(messagesChunk.map(message => getEmailMessage(message)))
    }))
};

module.exports = {
    sendEmail,
    sendMultipleEmails,
    sendMultipleDifferentEmails
};

