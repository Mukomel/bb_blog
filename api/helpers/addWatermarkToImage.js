const path = require('path');
const fs = require('fs-extra');
const Jimp = require("jimp");

const WATERMARK_SIZE = 0.11; // Image width or height multiplied by this number
const WATERMARK_OFFSET = 0.25; // Watermark width multiplied by this number. Offset from bottom and right corners of image

const addWatermarkToImage = ({
    watermarkPath,
    fileStorageFolderPath,
    originalFileName,
    onSuccess
}) => {
    return Promise.all([
        Jimp.read(path.resolve(fileStorageFolderPath, originalFileName)),
        Jimp.read(watermarkPath)
    ]).then(([image, watermark]) => {
        const {width: imageWidth, height: imageHeight} = image.bitmap;
        const resizedWatermark = (imageWidth > imageHeight)
            ? watermark.resize(imageWidth * WATERMARK_SIZE, Jimp.AUTO)
            : watermark.resize(Jimp.AUTO, imageHeight * WATERMARK_SIZE);
        const {width: resizedWatermarkWidth, height: resizedWatermarkHeight} = resizedWatermark.bitmap;
        image.composite(
            resizedWatermark,
            imageWidth - resizedWatermarkWidth - resizedWatermarkWidth * WATERMARK_OFFSET,
            imageHeight - resizedWatermarkHeight - resizedWatermarkWidth * WATERMARK_OFFSET
        )
            .write(path.resolve(fileStorageFolderPath, originalFileName));

        onSuccess({watermarkedFileName: originalFileName, type: 'image'});
    });
};

module.exports = addWatermarkToImage;