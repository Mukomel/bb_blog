const userStatuses = require('../constants/membersStatisticUserTypes');

const getUserStatusAndPromoCode = ({segpay_subscriptions}, artistId) => {
    const activeSubscription = segpay_subscriptions.find((subscription) => {
        return subscription.artist.equals(artistId) && !subscription.is_canceled && subscription.promo_code;
    }) || segpay_subscriptions.find((subscription) => {
        return subscription.artist.equals(artistId) && !subscription.is_canceled;
    });
    const canceledSubscription = segpay_subscriptions.find((subscription) => {
        return subscription.artist.equals(artistId) && subscription.is_canceled;
    });

    if (!activeSubscription && !canceledSubscription) return {status: userStatuses.REGISTERED, promoCode: ''};
    if (activeSubscription) return {status: userStatuses.ACTIVE, promoCode: activeSubscription.promo_code};
    if (canceledSubscription && !activeSubscription) return {
        status: userStatuses.CANCELED,
        promoCode: canceledSubscription.promo_code,
        canceledAt: canceledSubscription.canceled_at
    };
};

module.exports = {
    getUserStatusAndPromoCode
};
