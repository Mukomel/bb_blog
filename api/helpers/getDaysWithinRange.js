const getDaysWithinRange = (startDate, endDate, returnMomentDates = true) => {
    const now = startDate.clone(), dates = [];

    while (now.isSameOrBefore(endDate)) {
        dates.push(returnMomentDates ? now.clone() : now.format('D/M/YYYY'));
        now.add(1, 'days');
    }
    return dates;
};

module.exports = getDaysWithinRange;