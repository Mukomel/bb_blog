const passport = require("passport");
const passportJWT = require("passport-jwt");
const httpStatus = require('http-status');
const User = require("./models/User");
const config = require("./config");
const userRoles = require('./constants/userRoles');

const ExtractJwt = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.JWT_SECRET
};

const strategy = new JwtStrategy(jwtOptions, function(jwt_payload, next) {
    User.getById(jwt_payload.id)
        .then((user) => {
            if (user) {
                next(null, user);
            } else {
                next(null, false);
            }
        })
        .catch(() => {
            next(null, false);
        });
});

passport.use(strategy);

const authenticateFactory = ({allowedRoles = [], allowGuest = false}) => (req, res, next) => {
    passport.authenticate('jwt', { session: false }, function(err, user, info) {
        if (err) { return next(err); }

        if (allowGuest || (user && (allowedRoles.indexOf(user.role) > -1))) {
            req.user = user;
            return next();
        } else {
            res.status(httpStatus.UNAUTHORIZED);
            return res.json({error: 'Unauthorized'});
        }
    })(req, res, next);
};

const authenticateUser = authenticateFactory({allowedRoles: [userRoles.USER, userRoles.AUTHOR, userRoles.ADMIN]});

const authenticateAuthor = authenticateFactory({allowedRoles: [userRoles.AUTHOR, userRoles.ADMIN]});

const authenticateAdmin = authenticateFactory({allowedRoles: [userRoles.ADMIN]});

const authenticateButAllowGuest = authenticateFactory({allowGuest: true});

const maintenanceModeAuthenticate = (req, res, next) => {
    passport.authenticate('jwt', { session: false }, function(err, user, info) {
        if (err) { return next(err); }

        const maintenanceUserIds = process.env.MAINTENANCE_USERS ? process.env.MAINTENANCE_USERS.split(',') : [];

        if (user && ((user.role === userRoles.ADMIN) || maintenanceUserIds.indexOf(user._id.toString()) > -1)) {
            req.user = user;
        }
        return next();
    })(req, res, next);
};

module.exports = {
    passport,
    authenticateUser,
    authenticateAuthor,
    authenticateAdmin,
    authenticateButAllowGuest,
    maintenanceModeAuthenticate
};
