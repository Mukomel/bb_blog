const express = require('express');
const authenticate = require('../passport').authenticate;

const subscriptionsRoutes = require('./subscriptionsRoutes');
const donationRoutes = require('./donationRoutes');
const pointEarningsRoutes = require('./pointEarningsRoutes');
const roundsRoutes = require('./roundsRoutes');
const usersRoutes = require('./usersRoutes');
const assetsRoutes = require('./assetsRoutes');
const postsRoutes = require('./postsRoutes');
const subscriptionPriceRoute = require('./subscriptionPriceRoute');
const artistsRoutes = require('./artistsRoutes');
const commentsRoutes = require('./commentsRoutes');
const supportRoutes = require('./supportRoutes');
const notificationsRoutes = require('./notificationsRoutes');
const massMailingRoutes = require('./massMailingRoutes');
const segpayRoutes = require('./segpayRoutes');
const errorHandlingRoutes = require('./errorHandlingRoutes');
const statisticsRoutes = require('./statisticsRoutes');

const router = express.Router();

router.use('/subscriptions', subscriptionsRoutes);
router.use('/donations', donationRoutes);
router.use('/earnings', pointEarningsRoutes);
router.use('/rounds', roundsRoutes);
router.use('/subscription-price', subscriptionPriceRoute);
router.use('/users', usersRoutes);
router.use('/assets', assetsRoutes);
router.use('/posts', postsRoutes);
router.use('/artists', artistsRoutes);
router.use('/comments', commentsRoutes);
router.use('/support', supportRoutes);
router.use('/notifications', notificationsRoutes);
router.use('/mass-mailing', massMailingRoutes);
router.use('/segpay', segpayRoutes);
router.use('/logs', errorHandlingRoutes);
router.use('/statistics', statisticsRoutes);

module.exports = router;
