const express = require('express');
const promiseRouter = require('express-promise-router');
const { body } = require('express-validator/check');

const validationMiddleware = require('../validation/validationMiddleware');
const roundsController = require('../controllers/roundsController');

const router = promiseRouter();

router.route('/current')
    .get(roundsController.getActiveRound);

module.exports = router;