const express = require('express');
const promiseRouter = require('express-promise-router');
const { body } = require('express-validator/check');

const passport = require('../passport');
const subscriptionTypes = require('../constants/subscriptionTypes');
const validationMiddleware = require('../validation/validationMiddleware');
const subscriptionsController = require('../controllers/subscriptionsController');

const router = promiseRouter();

router.route('/active')
    .get(
        passport.authenticateAuthor,
        subscriptionsController.getActiveSubscriptions
    );

router.route('/expired')
    .get(
        passport.authenticateAuthor,
        subscriptionsController.getExpiredSubscriptions
    );

router.route('/archived')
    .get(
        passport.authenticateAuthor,
        subscriptionsController.getArchivedSubscriptions
    );

router.route('/mark_added')
    .post(
        passport.authenticateAuthor,
        [
            body('user_id').exists()
        ],
        validationMiddleware,
        subscriptionsController.markAdded
    );

router.route('/mark_archived')
    .post(
        passport.authenticateAuthor,
        [
            body('user_id').exists()
        ],
        validationMiddleware,
        subscriptionsController.markArchived
    );

router.route('/reset_anchor_now')
    .post(
        subscriptionsController.resetAnchorNow
    );

module.exports = router;
