const express = require('express');
const promiseRouter = require('express-promise-router');
const { body, param } = require('express-validator/check');

const passport = require('../passport');
const validationMiddleware = require('../validation/validationMiddleware');
const postsController = require('../controllers/postsController');

const router = promiseRouter();

router.route('/')
    .post(
        passport.authenticateAuthor,
        [
            body('title').exists()
        ],
        validationMiddleware,
        postsController.createPost
    );

router.route('/my-artist')
    .get(
        passport.authenticateAuthor,
        postsController.getUserArtistPosts
    );

router.route('/:post_id')
    .delete(
        passport.authenticateAuthor,
        [
            param('post_id').isMongoId()
        ],
        validationMiddleware,
        postsController.deletePost
    );

router.route('/:post_id/edit')
    .put(
        passport.authenticateAuthor,
        [
            param('post_id').isMongoId()
        ],
        validationMiddleware,
        postsController.editPost
    );

router.route('/:post_id/like')
    .post(
        passport.authenticateUser,
        postsController.likePost
    );

router.route('/:post_id/view')
    .post(
        postsController.viewPostVideo
    );

router.route('/:post_id/comments')
    .get(
        [
            param('post_id').isMongoId()
        ],
        validationMiddleware,
        postsController.getComments
    );

router.route('/:post_id/pin')
    .post(
        passport.authenticateAuthor,
        [
            param('post_id').isMongoId()
        ],
        validationMiddleware,
        postsController.pinPost
    );

router.route('/:post_id/unpin')
    .post(
        passport.authenticateAuthor,
        [
            param('post_id').isMongoId()
        ],
        validationMiddleware,
        postsController.unpinPost
    );

module.exports = router;
