const express = require('express');
const promiseRouter = require('express-promise-router');
const { body, param } = require('express-validator/check');
const massMailingTargets = require('../constants/massMailingTargets');

const validationMiddleware = require('../validation/validationMiddleware');
const passport = require('../passport');
const massMailingController = require('../controllers/massMailingController');
const membersStatisticUserTypes = require('../constants/membersStatisticUserTypes');

const router = promiseRouter();

router.route('/')
    .post(
        passport.authenticateAdmin,
        [
            body('send_emails')
                .custom((value, {req}) => ((typeof value === 'boolean') && value) || req.body.send_notifications)
                .withMessage('Either `send_emails` or `send_notifications` should be `true`'),
            body('send_notifications')
                .custom((value, {req}) => ((typeof value === 'boolean') && value) || req.body.send_emails)
                .withMessage('Either `send_emails` or `send_notifications` should be `true`'),
            body('target').isIn([massMailingTargets.ALL_FANS, massMailingTargets.ACTIVE_SUBSCRIBERS, massMailingTargets.CANCELED_SUBSCRIBERS, massMailingTargets.REGISTERED_USERS]),
            body('with_promocode').optional().isBoolean(),
            body('without_promocode').optional().isBoolean(),
            body('title').exists(),
            body('html').exists()
        ],
        validationMiddleware,
        massMailingController.createMailing
    );

router.route('/members-stats-selection')
    .post(
        passport.authenticateAdmin,
        [
            body('period_first_day').optional().isISO8601(),
            body('period_last_day').optional().isISO8601(),
            body('artist').optional().isMongoId(),
            body('users_type').optional().isIn([membersStatisticUserTypes.ACTIVE, membersStatisticUserTypes.CANCELED, membersStatisticUserTypes.REGISTERED]),
            body('promo_code').optional(),
            body('send_emails')
                .custom((value, {req}) => ((typeof value === 'boolean') && value) || req.body.send_notifications)
                .withMessage('Either `send_emails` or `send_notifications` should be `true`'),
            body('send_notifications')
                .custom((value, {req}) => ((typeof value === 'boolean') && value) || req.body.send_emails)
                .withMessage('Either `send_emails` or `send_notifications` should be `true`'),
            body('title').exists(),
            body('html').exists()
        ],
        validationMiddleware,
        massMailingController.createMailingOnMembersStatsSelection
    );

module.exports = router;
