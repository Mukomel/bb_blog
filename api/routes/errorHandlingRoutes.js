const express = require('express');
const promiseRouter = require('express-promise-router');
const { body, param } = require('express-validator/check');
const errorHandlingController = require('../controllers/errorHandlingController');

const passport = require('../passport');
const validationMiddleware = require('../validation/validationMiddleware');

const router = promiseRouter();

router.route('/')
    .post(
        passport.authenticateButAllowGuest,
        validationMiddleware,
        errorHandlingController.logError
    );

module.exports = router;