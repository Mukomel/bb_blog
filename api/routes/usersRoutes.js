const express = require('express');
const promiseRouter = require('express-promise-router');
const { body } = require('express-validator/check');
const passport = require('../passport');

const validationMiddleware = require('../validation/validationMiddleware');
const usernameValidationMiddleware = require('../validation/usernameValidationMiddleware');
const usersController = require('../controllers/usersController');

const router = promiseRouter();

router.route('/')
    .post(
        [
            body('email').isEmail(),
            body('password').isLength({ min: 6 }),
            body('username').exists(),
            body('receive_emails').isBoolean(),
        ],
        validationMiddleware,
        usernameValidationMiddleware,
        usersController.createUser);

router.route('/login')
    .post(
        [
            body('email').isEmail(),
            body('password').exists()
        ],
        validationMiddleware,
        usersController.login
    );

router.route('/me')
    .get(
        passport.authenticateUser,
        usersController.getUser
    );

router.route('/me')
    .put(
        passport.authenticateUser,
        [
            body('email').isEmail(),
            body('username').exists(),
            body('snapchat_username').optional(),
            body('receive_emails').isBoolean()
        ],
        validationMiddleware,
        usersController.updateUser
    );

router.route('/me')
    .delete(
        passport.authenticateUser,
        usersController.deleteUser
    );

router.route('/buy-period')
    .post(
        passport.authenticateUser,
        [
            body('stripe_token').exists(),
            body('period_end_date').isISO8601(),
            body('artist_slug').exists()

        ],
        validationMiddleware,
        usersController.buyPeriod
    );

router.route('/forgot-password')
    .post(
        [
            body('email').isEmail(),

        ],
        validationMiddleware,
        usersController.forgotPassword);

router.route('/reset-password')
    .post(
        [
            body('password').isLength({ min: 6 }),
            body('password_reset_token').exists(),
        ],
        validationMiddleware,
        usersController.resetPassword);

router.route('/activate-account')
    .post(
        [
            body('activation_token').exists(),
        ],
        validationMiddleware,
        usersController.activateAccount
    );

router.route('/ban')
    .post(
        passport.authenticateAdmin,
        [
            body('user_id').isMongoId(),

        ],
        validationMiddleware,
        usersController.banUser
    );

router.route('/banned')
    .get(
        passport.authenticateAuthor,
        usersController.getBannedUsers
    );

router.route('/subscribe')
    .post(
        passport.authenticateUser,
        [
            body('user_id').isMongoId(),

        ],
        validationMiddleware,
        usersController.subscribeToUser
    );

router.route('/unsubscribe')
    .post(
        passport.authenticateUser,
        [
            body('user_id').isMongoId(),

        ],
        validationMiddleware,
        usersController.unsubscribeFromUser
    );

router.route('/resend-activation-link')
    .post(
        passport.authenticateUser,
        usersController.resendActivationLink
    );

module.exports = router;
