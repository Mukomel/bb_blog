const express = require('express');
const promiseRouter = require('express-promise-router');
const { body, param } = require('express-validator/check');

const validationMiddleware = require('../validation/validationMiddleware');
const passport = require('../passport');
const supportController = require('../controllers/supportController');

const router = promiseRouter();

router.route('/contact-us')
    .post(
        [
            body('username').exists(),
            body('email').exists(),
            body('message').exists()
        ],
        validationMiddleware,
        supportController.contactUs
    );

module.exports = router;