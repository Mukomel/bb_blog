const promiseRouter = require('express-promise-router');
const { body } = require('express-validator/check');

const validationMiddleware = require('../validation/validationMiddleware');
const notificationsController = require('../controllers/notificationsController');
const passport = require('../passport');

const router = promiseRouter();

router.route('/count')
    .get(
        passport.authenticateUser,
        notificationsController.getNotificationsCount
    );

router.route('/')
    .get(
        passport.authenticateUser,
        notificationsController.getNotifications
    );

module.exports = router;