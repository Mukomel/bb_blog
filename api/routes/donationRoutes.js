const express = require('express');
const promiseRouter = require('express-promise-router');
const { body, param } = require('express-validator/check');

const passport = require('../passport');
const validationMiddleware = require('../validation/validationMiddleware');
const donationsController = require('../controllers/donationsController');
const donationTargets = require('../constants/donationTargets');
const periods = require('../constants/periods');

const router = promiseRouter();

const MIN_DONATION_AMOUNT_CENTS = 100;

router.route('/segpay_link_params/:amount_string')
    .get(
        passport.authenticateUser,
        [
            param('amount_string').isFloat({ min: 2.95, max: 100.00 }),
        ],
        validationMiddleware,
        donationsController.getSegpayLinkParams);

router.route('/')
    .post(
        passport.authenticateUser,
        [
            body('amount').isInt({min: MIN_DONATION_AMOUNT_CENTS}),
            body('target').optional().isIn([donationTargets.BRANDT, donationTargets.NASH, donationTargets.DREW]),
            body('stripe_token').exists(),
            body('post_id').optional().isMongoId()
        ],
        validationMiddleware,
        donationsController.createDonation);

router.route('/tip_jar')
    .post(
        passport.authenticateUser,
        [
            body('amount').isInt({min: MIN_DONATION_AMOUNT_CENTS}),
            //body('stripe_token').exists(),
            body('tip_jar_id').isMongoId()
        ],
        validationMiddleware,
        donationsController.createJarDonation
);

router.route('/by-posts')
    .get(
        passport.authenticateAuthor,
        donationsController.getDonationsByPosts
    );

router.route('/by-authors/:period')
    .get(
        passport.authenticateAuthor,
        [
            param('period').isIn([periods.WEEKLY, periods.MONTHLY, periods.ALL_TIME])
        ],
        validationMiddleware,
        donationsController.getDonationsByAuthors
    );

router.route('/by-fans/:period')
    .get(
        passport.authenticateAuthor,
        [
            param('period').isIn([periods.WEEKLY, periods.MONTHLY, periods.ALL_TIME])
        ],
        validationMiddleware,
        donationsController.getDonationsByFans
    );

router.route('/by-fans/:period/details/:user_id')
    .get(
        passport.authenticateAuthor,
        [
            param('period').isIn([periods.WEEKLY, periods.MONTHLY, periods.ALL_TIME]),
            param('user_id').isMongoId(),
        ],
        validationMiddleware,
        donationsController.getDonationsDetailsByUser
    );

module.exports = router;
