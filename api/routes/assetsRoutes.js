const express = require('express');
const promiseRouter = require('express-promise-router');
const multer  = require('multer');
const path = require('path');
const shortid = require('shortid');

const passport = require('../passport');
const assetsController = require('../controllers/assetsController');
const acceptedMIMETypes = require('../constants/acceptedMIMETypes');

const storage = multer.diskStorage({
    destination: path.resolve(__dirname, '../uploadedAssets'),
    filename: function (req, file, cb) {
        const fileExtnension = file.originalname.split('.').pop();
        cb(null, shortid.generate() + '_' + Date.now() + '.' + fileExtnension);
    }
});

const fileFilter = mimeTypes => (req, file, cb) => {
    if (mimeTypes.indexOf(file.mimetype) > -1) {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

const uploadMedia = multer({storage, fileFilter: fileFilter(acceptedMIMETypes.postMediaMIMETypes)});
const uploadImage = multer({storage, fileFilter: fileFilter(acceptedMIMETypes.imageMIMETypes)});

const router = promiseRouter();

router.route('/post-media')
    .post(
        passport.authenticateAuthor,
        uploadMedia.single('file'),
        assetsController.uploadPostMedia
    );

router.route('/image')
    .post(
        passport.authenticateAuthor,
        uploadImage.single('file'),
        assetsController.uploadImage
    );

module.exports = router;
