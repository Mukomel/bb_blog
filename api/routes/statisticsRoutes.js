const express = require('express');
const promiseRouter = require('express-promise-router');
const { query, body } = require('express-validator/check');

const validationMiddleware = require('../validation/validationMiddleware');
const passport = require('../passport');
const statisticsController = require('../controllers/statisticsController');
const segpayTransactionSources = require('../constants/segpayTransactionSources');
const membersStatisticUserTypes = require('../constants/membersStatisticUserTypes');

const router = promiseRouter();

router.route('/transactions')
    .get(
        passport.authenticateAuthor,
        [
            query('period_first_day').isISO8601(),
            query('period_last_day').isISO8601(),
            query('from_day').optional().isISO8601(),
            query('author').optional().isMongoId(),
            query('source').optional().isIn([segpayTransactionSources.SUBSCRIPTION, segpayTransactionSources.TIP]),
            query('forecast').optional().isBoolean()
        ],
        validationMiddleware,
        statisticsController.getTransactionStatistics
    );

router.route('/members')
    .get(
        passport.authenticateAuthor,
        [
            query('page').isInt({min: 1}),
            query('period_first_day').optional().isISO8601(),
            query('period_last_day').optional().isISO8601(),
            query('artist').optional().isMongoId(),
            query('users_type').optional().isIn([membersStatisticUserTypes.ACTIVE, membersStatisticUserTypes.CANCELED, membersStatisticUserTypes.REGISTERED]),
            query('promo_code').optional(),
            query('show_total_users_count').optional().isBoolean()
        ],
        validationMiddleware,
        statisticsController.getMembersStatistics
    );

module.exports = router;
