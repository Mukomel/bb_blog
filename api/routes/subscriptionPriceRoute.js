const httpStatus = require('http-status');
const express = require('express');
const promiseRouter = require('express-promise-router');
const stripe = require('../stripe');
const config = require('../config');

const router = promiseRouter();

router.route('/')
    .get((req, res, next) => {
        return stripe.plans.retrieve(config[process.env.NODE_ENV].STRIPE_PLAN_ID)
            .then(plan => {
                res.send({
                    monthly: plan.amount,
                    lifetime: config.LIFETIME_SUBSCRIPTION_PRICE_CENTS
                })
            })
            .catch(error => {
                res.status(httpStatus.INTERNAL_SERVER_ERROR);
                res.send({error});
            })
    });

module.exports = router;