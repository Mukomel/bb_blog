const express = require('express');
const promiseRouter = require('express-promise-router');
const { body, param } = require('express-validator/check');

const passport = require('../passport');
const validationMiddleware = require('../validation/validationMiddleware');
const commentsController = require('../controllers/commentsController');

const router = promiseRouter();

router.route('/')
    .post(
        passport.authenticateUser,
        [
            body('post_id').isMongoId(),
            body('comment').exists()
        ],
        validationMiddleware,
        commentsController.createComment
    );

router.route('/:comment_id')
    .delete(
        passport.authenticateUser,
        [
            param('comment_id').isMongoId()
        ],
        validationMiddleware,
        commentsController.softDeleteComment
    );

router.route('/:comment_id')
    .put(
        passport.authenticateUser,
        [
            param('comment_id').isMongoId(),
            body('comment').exists()
        ],
        validationMiddleware,
        commentsController.editComment
    );

router.route('/:comment_id/like')
    .post(
        passport.authenticateUser,
        [
            param('comment_id').isMongoId()
        ],
        validationMiddleware,
        commentsController.likeComment
    );

router.route('/:comment_id/reply')
    .post(
        passport.authenticateUser,
        [
            param('comment_id').isMongoId(),
            body('comment').exists()
        ],
        validationMiddleware,
        commentsController.createCommentReply
    );

module.exports = router;