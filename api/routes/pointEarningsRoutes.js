const express = require('express');
const promiseRouter = require('express-promise-router');


const validationMiddleware = require('../validation/validationMiddleware');
const pointEarningsController = require('../controllers/pointEarningsController');

const router = promiseRouter();

router.route('/top_fans/by_periods')
    .get(pointEarningsController.getTopEarningsByPeriods);

router.route('/top_fans/all_time')
    .get(pointEarningsController.getTopEarningsAllTime);

router.route('/top_fans/weekly')
    .get(pointEarningsController.getTopEarningsWeekly);

router.route('/top_fans/monthly')
    .get(pointEarningsController.getTopEarningsMonthly);

module.exports = router;
