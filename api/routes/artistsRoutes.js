const express = require('express');
const promiseRouter = require('express-promise-router');
const { body, param } = require('express-validator/check');

const validationMiddleware = require('../validation/validationMiddleware');
const passport = require('../passport');
const artistsController = require('../controllers/artistsController');

const router = promiseRouter();

router.route('/temp-endpoint-to-create-artist')
    .post(
        passport.authenticateAdmin,
        [
            body('slug').exists(),
            body('name').exists(),
            body('segpay_package_id').exists(),
            body('segpay_pricepoint_id').exists(),
            body('subscription_price_cents_first_30_days').isInt(),
            body('subscription_price_cents_recurring_30_days').isInt()
        ],
        validationMiddleware,
        artistsController.tempEndpointToCreateArtist
    );

router.route('/')
    .get(
        passport.authenticateAdmin,
        artistsController.getArtists
    );

router.route('/:slug')
    .get(
        artistsController.getArtistBySlug
    );

router.route('/:slug')
    .put(
        passport.authenticateAuthor,
        artistsController.updateArtistBySlug
    );

router.route('/:slug/posts/:count')
    .get(
        passport.authenticateButAllowGuest,
        [
            param('count').isInt()
        ],
        validationMiddleware,
        artistsController.getArtistPostsBySlugWithPagination
    );

router.route('/:slug/pinned-post')
    .get(
        passport.authenticateButAllowGuest,
        artistsController.getArtistPinnedPost
    );

router.route('/:slug/authors')
    .get(
        passport.authenticateButAllowGuest,
        artistsController.getArtistAuthors
    );

router.route('/:slug/schedule')
    .put(
        passport.authenticateAuthor,
        ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'].map(day => (
            body(day + '.description').exists()
        )),
        validationMiddleware,
        artistsController.updateArtistSchedule
    );

router.route('/:slug/posts-by-period/:period_start_date/:period_end_date')
    .get(
        [
            param('period_start_date').isISO8601(),
            param('period_end_date').isISO8601()
        ],
        validationMiddleware,
        artistsController.getArtistPostsBySlugWithinPeriod
    );

router.route('/:slug/tip_jar')
    .post(
        passport.authenticateAuthor,
        [
            body('title').exists(),
            body('description').exists(),
            body('goal_amount').isInt(),
            body('image').optional(),
        ],
        validationMiddleware,
        artistsController.createTipJar
    );

router.route('/:slug/tip_jar')
    .get(
        artistsController.getTipJar
    );

router.route('/:slug/tip_jar/close')
    .post(
        passport.authenticateAuthor,
        artistsController.closeTipJar
    );

router.route('/:slug/wishlist')
    .put(
        passport.authenticateAuthor,
        [
            body('is_enabled').isBoolean(),
            body('title').exists(),
            body('description').optional(),
            body('link').exists()
        ],
        validationMiddleware,
        artistsController.updateWishlist
    );

module.exports = router;
