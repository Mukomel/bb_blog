const express = require('express');
const promiseRouter = require('express-promise-router');
const { body, param, query } = require('express-validator/check');
const passport = require('../passport');
const passportSegpayAuth = require('../passportSegpayAuth');
const validationMiddleware = require('../validation/validationMiddleware');
const segpayController = require('../controllers/segpayController');

const router = promiseRouter();

router.route('/inquiry')
    .all(
        passportSegpayAuth.authenticate,
        segpayController.inquiry
    );

router.route('/enable')
    .all(
        passportSegpayAuth.authenticate,
        segpayController.enable
    );

router.route('/cancellation')
    .all(
        passportSegpayAuth.authenticate,
        segpayController.cancellation
    );

router.route('/disable')
    .all(
        passportSegpayAuth.authenticate,
        segpayController.disable
    );

router.route('/transaction')
    .all(
        passportSegpayAuth.authenticate,
        segpayController.transaction
    );

router.route('/transaction-tip')
    .all(
        passportSegpayAuth.authenticate,
        segpayController.transactionTip
    );

router.route('/my')
    .get(
        passport.authenticateUser,
        segpayController.getUserSubscriptions
    );

router.route('/subscriptions/active')
    .get(
        passport.authenticateAuthor,
        segpayController.getActiveSubscriptions
    );

router.route('/subscriptions/cancel')
    .post(
        passport.authenticateUser,
        [
            body('subscription_id').exists()
        ],
        validationMiddleware,
        segpayController.cancelSubscription
    );

router.route('/promocodes')
    .get(
        passport.authenticateAuthor,
        [
            query('all_artists').optional().isBoolean()
        ],
        validationMiddleware,
        segpayController.getPromoCodes
    );

router.route('/promocodes')
    .post(
        passport.authenticateAuthor,
        [
            body('code').exists(),
            body('segpay_pricepoint_id').exists(),
            body('subscription_price_cents_first_30_days').isInt(),
            body('subscription_price_cents_recurring_30_days').isInt()
        ],
        validationMiddleware,
        segpayController.createPromoCode
    );

router.route('/promocodes/:promocode_id')
    .put(
        passport.authenticateAuthor,
        [
            param('promocode_id').isMongoId(),
            body('is_active').isBoolean()
        ],
        validationMiddleware,
        segpayController.updatePromoCode
    );

router.route('/check_promocode')
    .post(
        passport.authenticateUser,
        [
            body('code').exists(),
            body('artist_id').isMongoId()
        ],
        validationMiddleware,
        segpayController.checkPromoCode
    );

module.exports = router;
