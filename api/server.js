const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const mongoose = require('mongoose');
const httpStatus = require('http-status');
const config = require('./config');
const router = require('./routes/index');
const startNewRoundJob = require('./schedules/startNewRound');
const passport = require('./passport');

const app = express();
const http = require('http').Server(app);

mongoose.Promise = Promise;
mongoose.connect(config[process.env.NODE_ENV].MONGODB_URL);

app.use(cors());
app.use(bodyParser.json({limit: '50mb'}));

if (process.env.MAINTANANCE_MODE) {
    app.use(passport.maintenanceModeAuthenticate);
    const segpayRouter = require('./routes/segpayRoutes');
    app.use('/api/segpay', segpayRouter);
    app.use((req, res, next) => {
        if (req.user) {
            next();
        } else {
            res.send('We will be back online in one hour. Come back soon!');
        }
    });
}

app.use('/api', router);
app.use('/uploads', express.static(path.join(__dirname, './uploadedAssets')));
app.use('/', express.static(path.join(__dirname, '../app/build')));
app.get('*', function (request, response) {
    response.sendFile(path.resolve(__dirname, '../app/build/index.html'));
});

app.use((req, res, next) => {
    res.status(httpStatus.NOT_FOUND);
    res.send({error: 'Not found'});
    return next();
});

app.use((err, req, res, next) => {
    res.status(httpStatus.INTERNAL_SERVER_ERROR).send({error: {
        message: err && err.message,
        stack: err && err.stack
    }});
});

startNewRoundJob.start();

http.listen(config[process.env.NODE_ENV].PORT);
