const moment = require('moment');
const mongoose = require('mongoose');
const httpStatus = require('http-status');
const SegPayTransactionRecord = require('./../models/SegPayTransactionRecord');
const SegPaySubscription = require('./../models/SegPaySubscription');
const User = require('./../models/User');
const SegPayPromoCodes = require('./../models/SegPayPromoCodes');
const netIncome = require('../helpers/netIncome');
const getDaysWithinRange = require('../helpers/getDaysWithinRange');
const { INCOME, EXPENSE } = require('../constants/segpayTransactionRecordTypes');
const { TIP, SUBSCRIPTION } = require('../constants/segpayTransactionSources');
const statistics = require('../helpers/statistics');
const userRoles = require('../constants/userRoles');

const TRANSACTION_STATISTICS_DAYS_PER_PAGE = 50;
const MEMBERS_STATISTICS_USERS_PER_PAGE = 50;
const SEGPAY_SUBSCRIPTION_CYCLE_DAYS = 30;

const getTransactionStatistics = (req, res, next) => {

    const today = moment();
    const periodLastDay = moment(req.query.period_last_day);
    const periodFirstDay = moment(req.query.period_first_day);

    const shouldForecast = (req.query.forecast === 'true') && today.isBefore(periodLastDay, 'day');
    const shouldCalculateTransactions = today.isSameOrAfter(periodFirstDay, 'day');

    const transactionCalculationPromises = shouldCalculateTransactions
        ? [
            SegPayTransactionRecord.getTotalPerPeriod({...req.query, artist: req.user.artist, type: INCOME}),
            SegPayTransactionRecord.getTotalPerPeriod({...req.query, artist: req.user.artist, type: EXPENSE}),
            SegPayTransactionRecord.getRecordsByDayPerPeriod({
                ...req.query,
                period_first_day: req.query.from_day
                    ? moment(req.query.from_day).add(1, 'day').toISOString()
                    : req.query.period_first_day,
                artist: req.user.artist,
                type: INCOME,
                limit: TRANSACTION_STATISTICS_DAYS_PER_PAGE
            })
        ]
        : [];

    const forecastPromises = [
        SegPaySubscription.getArtistActiveSubscriptionsSummaryByDay(req.user.artist)
    ];

    return Promise.all([
            Promise.all(transactionCalculationPromises),
            Promise.all(forecastPromises)
        ])
        .then(([transactionCalculationPromisesResults, forecastPromisesResult]) => {
            let response = {};
            if (shouldCalculateTransactions) {
                const [totalIncome, totalExpense, groupedTransactionsByDate] = transactionCalculationPromisesResults;
                response.days = groupedTransactionsByDate.map(groupedTransactions => ({
                    day: groupedTransactions.day,
                    income: groupedTransactions.income,
                    expense: groupedTransactions.expense,
                    fee: netIncome.calculateFee(groupedTransactions.income),
                    net: netIncome.calculateNetIncome(groupedTransactions.income, groupedTransactions.expense)
                }));
                response.total = {
                    income: totalIncome,
                    expense: totalExpense,
                    fee: netIncome.calculateFee(totalIncome),
                    net: netIncome.calculateNetIncome(totalIncome, totalExpense)
                }
            } else {
                response.days = [];
                response.total = {
                    income: 0,
                    expense: 0,
                    fee: 0,
                    net: 0
                };
            }

            if (shouldForecast) {
                const [subscriptionsSummariesByDay] = forecastPromisesResult;
                const datesToForecast = getDaysWithinRange(
                    today.isSameOrAfter(periodFirstDay) ? today.add(1, 'day') : periodFirstDay,
                    periodLastDay
                );

                const forecastedTotal = {
                    income: 0,
                    expense: 0,
                    fee: 0,
                    net: 0
                };
                const forecastedDays = datesToForecast.map(momentDate => {
                    let dayAmountSum = 0;
                    subscriptionsSummariesByDay.forEach(daySummary => {
                        if ((momentDate.diff(moment(daySummary.created_at), 'days') % SEGPAY_SUBSCRIPTION_CYCLE_DAYS) === 0) {
                            dayAmountSum = dayAmountSum + daySummary.amount;
                        }
                    });

                    const dayIncome = dayAmountSum;
                    const dayExpense = 0;
                    const dayFee = netIncome.calculateFee(dayIncome);
                    const dayNet = netIncome.calculateNetIncome(dayAmountSum, dayExpense);

                    forecastedTotal.income = forecastedTotal.income + dayIncome;
                    forecastedTotal.expense = forecastedTotal.expense + dayExpense;
                    forecastedTotal.fee = forecastedTotal.fee + dayFee;
                    forecastedTotal.net = forecastedTotal.net + dayNet;
                    return {
                        day: momentDate.format('DD-MM-YYYY'),
                        income: dayIncome,
                        expense: dayExpense,
                        fee: dayFee,
                        net: dayNet
                    }
                });

                response.forecast = {
                    days: forecastedDays,
                    total: forecastedTotal
                };
            }

            res.send(response)
        });
};

const getMembersStatistics = (req, res, next) => {
    if (req.query.artist && !(req.user.role === userRoles.ADMIN)) {
        res.status(httpStatus.FORBIDDEN).send({message: 'You are not allowed to choose artist'});
    }

    const artistId = mongoose.Types.ObjectId(req.query.artist || req.user.artist);

    let startPromise;
    if (req.query.promo_code){
        if (req.query.promo_code !== 'no_code') {
            startPromise = SegPayPromoCodes.getById(req.query.promo_code)
                .then(promoCode => {
                    if (!promoCode) {
                        throw {status: httpStatus.NOT_FOUND, message: 'Promo code not found'}
                    } else {
                        return promoCode.code;
                    }
                });
        } else {
            startPromise = Promise.resolve({$exists: false});
        }

    } else {
        startPromise = Promise.resolve();
    }

    return startPromise
        .then(promoCodeValue => {
            const params = {
                ...req.query,
                promo_code: promoCodeValue,
                artist: artistId,
                limit: MEMBERS_STATISTICS_USERS_PER_PAGE,
            };

            const userCountQuery = (req.query.promo_code || req.query.users_type || req.query.artist || (req.query.period_first_day && req.query.period_last_day))
                ? User.getMembersStatistics({...params, show_total_user_count: true})
                : User.count();
            return Promise.all([
                (req.query.show_total_users_count === 'true')
                    ? userCountQuery
                    : Promise.resolve(),
                User.getMembersStatistics({...params, show_total_user_count: false}),
            ])
        })
        .then(([totalUsersCount, usersList]) => {
            if ((req.query.show_total_users_count === 'true') && !Number.isInteger(totalUsersCount)) {
                totalUsersCount = totalUsersCount[0] ? totalUsersCount[0].total_users : 0;
            }
            const preparedUsers = usersList.map(user => {
                const {status, promoCode, canceledAt} = statistics.getUserStatusAndPromoCode(user, artistId);
                return {
                    total_tips_income: user.segpay_transaction_records.reduce((acc, record) => {
                        if ((record.type === INCOME) && (record.source === TIP)) {
                            return acc + record.amount
                        } else {
                            return acc;
                        }
                    }, 0),
                    total_subscription_income: user.segpay_transaction_records.reduce((acc, record) => {
                        if ((record.type === INCOME) && (record.source === SUBSCRIPTION)) {
                            return acc + record.amount
                        } else {
                            return acc;
                        }
                    }, 0),
                    status: status,
                    promo_code: promoCode || '',
                    canceled_at: canceledAt || '',
                    _id: user._id,
                    email: user.email,
                    username: user.username,
                    snapchat_username: user.snapchat_username,
                    created_at: user.created_at
                }
            });

            const responseJson = {
                users: preparedUsers,
                current_page: Number(req.query.page),
            };
            if (req.query.show_total_users_count === 'true') {
                responseJson.total = totalUsersCount;
                responseJson.total_pages = Math.ceil(totalUsersCount / MEMBERS_STATISTICS_USERS_PER_PAGE);
            }

            res.send(responseJson);
        })
        .catch(error => {
            res.status(error.status).send({message: error.message})
        })
};

module.exports = {
    getTransactionStatistics,
    getMembersStatistics
};
