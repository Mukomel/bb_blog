const emails = require('../helpers/emails');

const contactUs = (req, res, next) => {
    emails.sendEmail({
        to: 'support@brandtsboys.com',
        subject: 'Contact Us Request from ' + req.body.email,
        html: '' +
        '<br/> Username: ' + req.body.username +
        '<br/> Email: ' + req.body.email +
        '<br/> Message: ' + req.body.message
    });
    res.send({message: 'Email is sent'});
};


module.exports = {
    contactUs
};
