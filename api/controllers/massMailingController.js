const httpStatus = require('http-status');
const mongoose = require('mongoose');
const emails = require('../helpers/emails');
const User = require('../models/User');
const SegPaySubscription = require('../models/SegPaySubscription');
const SegPayPromoCodes = require('../models/SegPayPromoCodes');
const Notification = require('../models/Notification');
const MassMailing = require('../models/MassMailing');
const massMailingTargets = require('../constants/massMailingTargets');
const userRoles = require('../constants/userRoles');
const massMailingHelpers = require('../helpers/massMailing');

const createMailing = (req, res, next) => {
    if (!req.user.artist) {
        res.status(httpStatus.CONFLICT).send({message: "You are not related to any artist"})
    }

    return User.getUsersForMassMailing(req.body.target, req.user.artist)
        .then(users => {
            return Promise.all(users.map(user => {
                return SegPaySubscription.getUserSubscriptionToArtist(user._id, req.user.artist)
                    .then(subscription => ({
                        user,
                        subscription
                    }))
            }))
        })
        .then(list => list.filter(item => {
            switch (req.body.target) {
                case massMailingTargets.ALL_FANS:
                    return true;

                case massMailingTargets.ACTIVE_SUBSCRIBERS:
                    return true;

                case massMailingTargets.REGISTERED_USERS:
                    return !Boolean(item.subscription);

                case massMailingTargets.CANCELED_SUBSCRIBERS:
                    return Boolean(item.subscription);
            }
        }))
        .then(list => list.filter(item => {
            if (req.body.august_promocoders) {
                return Boolean(item.subscription && (item.subscription.with_stripe_coupon));
            } else if ((!req.body.with_promocode && !req.body.without_promocode)
                || (req.body.with_promocode && req.body.without_promocode)) {
                return true;
            } else if (req.body.with_promocode) {
                return Boolean(item.subscription && (item.subscription.with_stripe_coupon || item.subscription.promo_code));
            } else if (req.body.without_promocode) {
                return Boolean(item.subscription
                    ? (!item.subscription.with_stripe_coupon && !item.subscription.promo_code)
                    : true);
            }
        }))
        .then(list => {
            const emailsList = [];
            const notificationUsersList = [];
            list.forEach(item => {
                if (req.body.send_emails && item.user && item.user.receive_emails && item.user.is_activated && !item.user.is_banned) {
                    emailsList.push(item.user.email);
                }
                if (req.body.send_notifications && item.user) {
                    notificationUsersList.push(item.user._id);
                }
            });
            const emailHtml = massMailingHelpers.prepareEmailHtml(req.body.html);
            const notificationText = massMailingHelpers.prepareNotificationText(emailHtml);

            return Promise.all([
                emails.sendMultipleEmails({
                    subject: req.body.title,
                    html: emailHtml
                }, emailsList),
                Notification.insertMany(notificationUsersList.map(userId => ({
                    user: userId,
                    title: req.body.title,
                    message: notificationText
                })))
            ])
                .then(() => MassMailing.create({
                    user: req.user._id,
                    emails_list: emailsList,
                    notification_users_list: notificationUsersList,
                    email_text: emailHtml,
                    notification_text: notificationText,
                    options: {
                        target: req.body.target,
                        send_emails: Boolean(req.body.send_emails),
                        send_notifications: Boolean(req.body.send_notifications),
                        with_promocode: Boolean(req.body.with_promocode),
                        without_promocode: Boolean(req.body.without_promocode)
                    }
                }))
        })
        .then(mailing => res.send(mailing))
};

const createMailingOnMembersStatsSelection = (req, res, next) => {
    const artistId = mongoose.Types.ObjectId(req.body.artist || req.user.artist);

    let startPromise;
    if (req.query.promo_code) {
        if (req.query.promo_code !== 'no_code') {
            SegPayPromoCodes.getById(req.body.promo_code)
                .then(promoCode => {
                    if (!promoCode) {
                        throw {status: httpStatus.NOT_FOUND, message: 'Promo code not found'}
                    } else {
                        return promoCode.code;
                    }
                })
        } else {
            startPromise = Promise.resolve({$exists: false});
        }
    } else {
            startPromise = Promise.resolve();
        }

    return startPromise
        .then(promoCodeValue => {
            return User.getMembersStatistics({
                ...req.body,
                promo_code: promoCodeValue,
                artist: artistId,
                get_users_for_mailing: true
            })
                .then(users => {
                    const emailsList = [];
                    const notificationUsersList = [];
                    users.forEach(user => {
                        if (req.body.send_emails && user && user.receive_emails && user.is_activated && !user.is_banned) {
                            emailsList.push(user.email);
                        }
                        if (req.body.send_notifications && user) {
                            notificationUsersList.push(user._id);
                        }
                    });
                    const emailHtml = massMailingHelpers.prepareEmailHtml(req.body.html);
                    const notificationText = massMailingHelpers.prepareNotificationText(emailHtml);

                    return Promise.all([
                        emails.sendMultipleEmails({
                            subject: req.body.title,
                            html: emailHtml
                        }, emailsList),
                        Notification.insertMany(notificationUsersList.map(userId => ({
                            user: userId,
                            title: req.body.title,
                            message: notificationText
                        })))
                    ])
                        .then(() => {
                            const memberStatsSelectionOptions = {
                                artist: artistId,
                                users_type: req.body.users_type,
                                promo_code: promoCodeValue ? promoCodeValue : undefined,
                            };
                            if (req.body.period_first_day && req.body.period_last_day) {
                                memberStatsSelectionOptions.period_first_day = new Date(req.body.period_first_day);
                                memberStatsSelectionOptions.period_last_day = new Date(req.body.period_last_day);
                            }

                            return MassMailing.create({
                                user: req.user._id,
                                emails_list: emailsList,
                                notification_users_list: notificationUsersList,
                                email_text: emailHtml,
                                notification_text: notificationText,
                                member_stats_selection_options: memberStatsSelectionOptions,
                                promo_code: memberStatsSelectionOptions,
                                options: {
                                    send_emails: Boolean(req.body.send_emails),
                                    send_notifications: Boolean(req.body.send_notifications),
                                }
                            })
                        })
                        .then(() => res.send({message: "Mailing created"}))
                });
        })
};

module.exports = {
    createMailing,
    createMailingOnMembersStatsSelection
};
