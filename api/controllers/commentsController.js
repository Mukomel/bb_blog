const httpStatus = require('http-status');
const Comment = require('../models/Comment');
const Post = require('../models/Post');
const User = require('../models/User');
const Notification = require('../models/Notification');
const userRoles = require('../constants/userRoles');
const commentTypes = require('../constants/commentTypes');

const createComment = (req, res, next) => {
    return Post.getById(req.body.post_id)
        .then(post => {
            if (!(req.user.artists_subscriptions.indexOf(post.artist) > -1)
                && (req.user.role !== userRoles.ADMIN) && (req.user.role !== userRoles.AUTHOR)) {
                return res.status(httpStatus.FORBIDDEN).send({message: 'Only for subscribed users or admins/authors'});
            } else {
                return Comment.create({
                    type: commentTypes.COMMENT,
                    comment: req.body.comment,
                    user: req.user._id,
                    post: req.body.post_id,
                    replies: []
                })
                    .then(comment => {
                        return User.populate(comment, {path: 'user', select: 'username avatar'})
                    })
                    .then(comment => comment.toJSON())
                    .then(comment => {
                        return Comment.prepareComments([comment], req.user.id)
                    })
                    .then(([comment]) => res.send(comment))
            }
        })
};

const createCommentReply = (req, res, next) => {
    return Comment.getById(req.params.comment_id)
        .then(comment => {
            if (!comment) {
                throw {status: httpStatus.NOT_FOUND, message: 'Comment not found'}
            } else {
                return Promise.all([
                    Post.getById(comment.post),
                    Promise.resolve(comment)
                ]);
            }})
        .then(([post, comment]) => {
            if (!(req.user.artists_subscriptions.indexOf(post.artist) > -1)
                && (req.user.role !== userRoles.ADMIN) && (req.user.role !== userRoles.AUTHOR)) {
                throw {status: httpStatus.FORBIDDEN, message: 'Only for subscribed users or admins/authors'}
            } else {
                return Notification.create({
                    user: comment.user,
                    title: req.user.username + ' replied to your comment!',
                    message: `Your comment was replied under ${post.title} post`
                });
            }})
        .then(() => {
            return Comment.create({
                type: commentTypes.REPLY,
                comment: req.body.comment,
                user: req.user._id,
                parent_comment: req.params.comment_id
            })})
        .then(comment => {
            return User.populate(comment, {path: 'user', select: 'username avatar'})
        })
        .then(comment => {
            return Comment.updateOne(
                {_id: req.params.comment_id},
                {$push: {
                        replies: {
                            $each: [comment._id],
                            $position: 0
                        }
                    }}
            )
                .then(() => comment.toJSON())
        })
        .then(comment => {
            return Comment.prepareComments([comment], req.user.id)
        })
        .then(comment => {
            res.send(comment);
        })
        .catch(error => {
            res.status(error.status).send({message: error.message});
        });
};

const softDeleteComment = (req, res, next) => {
    return Comment.getById(req.params.comment_id)
        .then(comment => {
            if (!comment) {
                throw {status: httpStatus.NOT_FOUND, message: 'Comment not found'}
            }

            if ((req.user.role !== userRoles.AUTHOR) && (req.user.role !== userRoles.ADMIN) && (!comment.user.equals(req.user._id))) {
                throw {status: httpStatus.FORBIDDEN, message: 'You are not allowed to delete this comment'}
            }

            return Comment.update(
                {_id: req.params.comment_id},
                {$set: {
                    is_deleted: true
                }}
            );
        })
        .then(() => {
            res.send({message: 'Comment deleted'});
        })
        .catch(error => {
            res.status(error.status).send({message: error.message})
        });
};

const editComment = (req, res, next) => {
    return Comment.getById(req.params.comment_id)
        .then(comment => {
            if (!comment) {
                throw {status: httpStatus.NOT_FOUND, message: 'Comment not found'}
            }

            if (!comment.user.equals(req.user._id)) {
                throw {status: httpStatus.FORBIDDEN, message: 'You are not allowed to edit this comment'}
            }

            return Comment.update(
                {_id: req.params.comment_id},
                {$set: {
                    comment: req.body.comment
                }}
            );
        })
        .then(() => {
            res.send({message: 'Comment edited'});
        })
        .catch(error => {
            res.status(error.status).send({message: error.message})
        });
};

const likeComment = (req, res, next) => {
    return Comment.getById(req.params.comment_id)
        .then(comment => {
            if (!comment) {
                throw {status: httpStatus.NOT_FOUND, message: 'Comment not found'}
            } else if (comment.user.equals(req.user.id)) {
                throw {status: httpStatus.FORBIDDEN, message: 'You can\'t like your own comments'}
            } else {
                return Comment.findOneAndUpdate(
                    {
                        _id: req.params.comment_id,
                        likes: {$ne: req.user._id}
                    },
                    {
                        $push: {
                            likes: req.user.id
                        }
                    }
                )
            }
        })
        .then(comment => {
            if (comment) {
                Post.getById(comment.post)
                    .then(post => {
                        Notification.create({
                            user: comment.user,
                            title: req.user.username + ' liked your comment!',
                            message: `Your comment was liked under ${post.title} post`
                        });
                    });
                res.send({message: 'Success'})
            } else {
                throw {status: httpStatus.CONFLICT, message: 'You have already liked this comment'}
            }
        })
        .catch(error => {
            res.status(error.status).send({message: error.message})
        })

};

module.exports = {
    createComment,
    createCommentReply,
    softDeleteComment,
    editComment,
    likeComment
};
