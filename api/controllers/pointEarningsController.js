const httpStatus = require('http-status');
const PointEarning = require('../models/PointEarning');

const getTopEarningsByPeriods = (req, res, next) => {
    return Promise.all([
        PointEarning.getTopEarningsAllTime(),
        PointEarning.getTopEarningsMonthly(),
        PointEarning.getTopEarningsWeekly()
    ])
        .then(([allTimeTopEarnings, monthlyTopEarnings, weeklyTopEarnings]) => {
            res.send({
                all_time: allTimeTopEarnings,
                monthly: monthlyTopEarnings,
                weekly: weeklyTopEarnings
            })
        })
};

const getTopEarningsAllTime = (req, res, next) => {
    return PointEarning.getTopEarningsAllTime()
        .then(earnings => {
            res.send(earnings);
        });
};

const getTopEarningsWeekly = (req, res, next) => {
    return PointEarning.getTopEarningsWeekly()
        .then(earnings => {
            res.send(earnings);
        });
};

const getTopEarningsMonthly = (req, res, next) => {
    return PointEarning.getTopEarningsMonthly()
        .then(earnings => {
            res.send(earnings);
        });
};

module.exports = {
    getTopEarningsByPeriods,
    getTopEarningsAllTime,
    getTopEarningsWeekly,
    getTopEarningsMonthly
};
