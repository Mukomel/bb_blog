const httpStatus = require('http-status');
const Round = require('../models/Round');
const donationTargets = require('../constants/donationTargets');
const usdCentsToPoints = require('../helpers/usdCentsToPoints');

const getActiveRound = (req, res, next) => {
    return Round.getActiveRound()
        .then(round => {
            res.send({
                [donationTargets.BRANDT]: usdCentsToPoints(round[donationTargets.BRANDT]),
                [donationTargets.DREW]: usdCentsToPoints(round[donationTargets.DREW]),
                [donationTargets.NASH]: usdCentsToPoints(round[donationTargets.NASH])
            });
        })
};

module.exports = {
    getActiveRound,
};