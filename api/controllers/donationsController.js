const httpStatus = require('http-status');
const xml2js = require('xml2js');
const Donation = require('../models/Donation');
const PointEarning = require('../models/PointEarning');
const SegPaySubscription = require('../models/SegPaySubscription');
const SegPayDonation = require('../models/SegPayDonation');
const Round = require('../models/Round');
const Artist = require('../models/Artist');
const TipJar = require('../models/TipJar');
const Post = require('../models/Post');
const User = require('../models/User');
const stripe = require('../stripe');
const config = require('../config');
const usdCentsToPoints = require('../helpers/usdCentsToPoints');
const pointEarningSources = require('../constants/pointEarningSources');
const errorHandler = require('../helpers/errorHandler');
const segpayHelpers = require('../helpers/segpayHelpers');

const xmlParser = new xml2js.Parser();

const getSegpayLinkParams = (req, res, next) => {

    return Promise.all([
        segpayHelpers.getAmountHash(req.params.amount_string),
        SegPaySubscription.getUserSubscriptionWithOCToken(req.user._id)
    ])
        .then(([segpayResponse, subscription]) => {
            xmlParser.parseString(segpayResponse.data, (err, result) => {
                res.send({
                    amount_hash: result.string._,
                    oc_token: subscription ? subscription.purchase_id : null
                });
            });
        })
};

const createDonation = (req, res, next) => {
    if (!req.body.target && !req.body.post_id) {
        return res.status(httpStatus.UNPROCESSABLE_ENTITY).send({error: {message: "Either `target` or `post_id` is required"}})
    }

    let startPromise;
    if (req.body.post_id) {
        startPromise = Post.getById(req.body.post_id)
            .then(post => {
                if (post) {
                    return post;
                } else {
                    throw {message: "Post is not found"}
                }
            })
            .then(post => User.populate(post, {path: 'user', select: 'donation_target'}))
            .then(post => {
                if (post.user.donation_target) {
                    return post;
                } else {
                    throw {message: "Donation target for post's author is not set"}
                }
            })
    } else {
        startPromise = Promise.resolve();
    }

    startPromise
        .then(post => {
            const target = post ? post.user.donation_target : req.body.target;

            return stripe.charges.create({
                amount: req.body.amount,
                currency: "usd",
                description: "Donation to " + target,
                source: req.body.stripe_token,
            })
                .then(charge => {
                    return Promise.all([
                        Round.findOne({is_active: true})
                            .then(round => {
                                round[target] = round[target] + req.body.amount;
                                round.save();
                            }),
                        Donation.create({
                            charge_id: charge.id,
                            amount: req.body.amount,
                            target: target,
                            stripe_creation_date: charge.created,
                            user: req.user._id,
                            post: req.body.post_id
                        })
                            .then(donation => {
                                return PointEarning.create({
                                    points: usdCentsToPoints(donation.amount),
                                    user: req.user._id,
                                    source: {
                                        type: pointEarningSources.DONATION,
                                        donation_id: donation._id,
                                    }
                                })
                            })
                    ])
                })
                .then(() => res.send({message: 'Success'}))
        })
        .catch(error => {
            res.status(httpStatus.UNPROCESSABLE_ENTITY);
            res.send({error});
        })
};

const createJarDonation = (req, res, next) => {
    return TipJar.getById(req.body.tip_jar_id, true)
        .then(jar => {
            if (!jar) {
                throw {status: httpStatus.NOT_FOUND, message: 'Active jar not found'}
            } else if (!jar.artist.tip_jar_enabled) {
                throw {status: httpStatus.FORBIDDEN, message: 'Tip jar is disabled'}
            } else {
                return Promise.resolve({id: 'charge_id', created: 'charge_creation_date'});
                /*return stripe.charges.create({
                    amount: req.body.amount,
                    currency: "usd",
                    description: "Donation to " + req.body.target,
                    source: req.body.stripe_token,
                 })*/
            }
        })
        .then(charge => {
            return Promise.all([
                TipJar.updateOne(
                    {_id: req.body.tip_jar_id},
                    {$addToSet: {bakers: req.user._id}, $inc: {current_amount: req.body.amount}}
                ),
                Donation.create({
                    charge_id: charge.id,
                    amount: req.body.amount,
                    tip_jar: req.body.tip_jar_id,
                    stripe_creation_date: charge.created,
                    user: req.user._id
                })
            ])
        })
        .then(() => res.send({message: 'Success'}))
        .catch(error => errorHandler(error, res))
};

const getDonationsByPosts = (req, res, next) => {
    return Promise.all([
        SegPayDonation.getPostsDonations(),
        Post.getUserArtistPosts(req.user.artist)
    ])
        .then(([donations, posts]) => {
            const postsWithDonationsAmount = posts.map(post => {
                const postDonations = donations.filter(donation => donation.post.equals(post._id));
                const postDonationsAmount = postDonations.reduce((acc, donation) => {
                    return acc + donation.amount;
                }, 0);

                return {
                    ...post.toObject(),
                    donations_amount: postDonationsAmount
                }
            });
            res.send(postsWithDonationsAmount);
        })
};

const getDonationsByAuthors = (req, res, next) => {
    return Promise.all([
        SegPayDonation.getGroupedDonationsByTime(req.params.period),
        Artist.getArtistById(req.user.artist, false)
            .then(artist => {
                return User.populate(artist, {path: 'users', select: 'username'})
            })
            .then(artist => {
                return artist.users
            })
    ])
        .then(([donations, users]) => {
            const donationsByAuthor = users.map(user => {
                const authorDonations = donations.find(donation => user._id.equals(donation.target));
                return {
                    ...user.toObject(),
                    donations_amount: authorDonations ? authorDonations.amount : 0
                }
            });

            res.send(donationsByAuthor)
        })
};

const getDonationsByFans = (req, res, next) => {
    return Promise.all([
        SegPayDonation.getDonationsToTargetsByTime(req.params.period),
        Artist.getArtistById(req.user.artist, false)
            .then(artist => {
                return User.populate(artist, {path: 'users', select: 'username'})
            })
            .then(artist => {
                return artist.users
            })
    ])
        .then(([donations, authors]) => {
            donations = donations.filter(donation => donation.user && donation.target);
            Promise.all(donations.map(donation => {
                return User.populate(donation, {path: 'user', select: 'username email snapchat_username'})
            }))
                .then(donations => {
                    const usersById = {};
                    donations.forEach(donation => {
                        if (!donation.user || !authors.find(author => author._id.equals(donation.target))) return;

                        if (usersById[donation.user._id]) {
                            usersById[donation.user._id].donations[donation.target] = donation.amount;
                            usersById[donation.user._id].donations_amount_total = usersById[donation.user._id].donations_amount_total + donation.amount;
                        } else {
                            usersById[donation.user._id] = {
                                ...donation.user.toObject(),
                                donations: {[donation.target]: donation.amount},
                                donations_amount_total: donation.amount
                            }
                        }
                    });
                    res.send({
                        users: Object.values(usersById).sort((a, b) => {
                            if (a.donations_amount_total < b.donations_amount_total) return 1;
                            if (a.donations_amount_total > b.donations_amount_total) return -1;
                            return 0;
                        }),
                        authors
                    });
                })
        })
};

const getDonationsDetailsByUser = (req, res, next) => {
    return SegPayDonation.getDonationsByUserByTime(req.params.period, req.params.user_id)
        .then(donations => {
            return donations.filter(donation => {
                return donation.target.artist.equals(req.user.artist)
            })
        })
        .then(donations => res.send(donations))
};

module.exports = {
    getSegpayLinkParams,
    createDonation,
    createJarDonation,
    getDonationsByPosts,
    getDonationsByAuthors,
    getDonationsByFans,
    getDonationsDetailsByUser
};