const httpStatus = require('http-status');
const mongoose = require('mongoose');
const Artist = require('../models/Artist');
const Schedule = require('../models/Schedule');
const User = require('../models/User');
const Post = require('../models/Post');
const TipJar = require('../models/TipJar');
const GroupWishlist = require('../models/GroupWishlist');
const userRoles = require('../constants/userRoles');
const errorHandler = require('../helpers/errorHandler');

const checkArtistAndAuthor = (artist, req) => {
    if (!artist) {
        throw {status: httpStatus.NOT_FOUNT, message: 'Artist not found'}
    } else if (!req.user.artist || (!artist._id.equals(req.user.artist))) {
        throw {status: httpStatus.FORBIDDEN, message: 'You are not allowed to update this artist'}
    } else {
        return artist
    }
};

const tempEndpointToCreateArtist = (req, res, next) => {
    return Artist.create({
        slug: req.body.slug,
        name: req.body.name,
        biography: req.body.biography,
        avatar: req.body.avatar,
        cover: req.body.cover,
        users: req.body.users,
        segpay_package_id: req.body.segpay_package_id,
        segpay_pricepoint_id: req.body.segpay_pricepoint_id,
        subscription_price_cents_first_30_days: req.body.subscription_price_cents_first_30_days,
        subscription_price_cents_recurring_30_days: req.body.subscription_price_cents_recurring_30_days,
    })
        .then(artist => {
            User.update(
                {_id: {$in: req.body.users}},
                {$set: {role: userRoles.AUTHOR, artist: artist._id}},
                {multi: true}
            )
                .then(() => res.send(artist))
        })
};

const getArtists = (req, res, next) => {
    return Artist.getArtists()
        .then(artists => res.send(artists));
};

const getArtistBySlug = (req, res, next) => {
    return Artist.getArtistBySlug(req.params.slug)
        .then(artist => {
            if (artist) {
                res.send(artist);
            } else {
                res.status(httpStatus.NOT_FOUND).send({message: 'Artist not found'});
            }
        })
};

const updateArtistBySlug = (req, res, next) => {
    return Artist.getArtistBySlug(req.params.slug)
        .then(artist => checkArtistAndAuthor(artist, req))
        .then(artist => {

            const artistUpdate = {};
            if ('name' in req.body) artistUpdate.name = req.body.name;
            if ('biography' in req.body) artistUpdate.biography = req.body.biography;
            if ('avatar' in req.body) artistUpdate.avatar = req.body.avatar;
            if ('cover' in req.body) artistUpdate.cover = req.body.cover;
            if ('tip_jar_enabled' in req.body) artistUpdate.tip_jar_enabled = req.body.tip_jar_enabled;
            if (req.body.snapchat_link !== undefined) artistUpdate.snapchat_link = req.body.snapchat_link;
            if (req.body.instagram_link !== undefined) artistUpdate.instagram_link = req.body.instagram_link;
            if (req.body.tumblr_link !== undefined ) artistUpdate.tumblr_link = req.body.tumblr_link;
            if (req.body.twitter_link !== undefined) artistUpdate.twitter_link = req.body.twitter_link;


            return Artist.findOneAndUpdate(
                {slug: req.params.slug},
                {$set: artistUpdate},
                {new: true}
            )
        })
        .then(artist => {
            res.send(artist);
        })
        .catch(error => errorHandler(error, res))
};

const getArtistPostsBySlugWithPagination = (req, res, next) => {
    return Artist.getArtistBySlug(req.params.slug)
        .then(artist => {
            if (artist) {
                return artist;
            } else {
                res.status(httpStatus.NOT_FOUND).send({message: 'Artist not found'});
            }
        })
        .then(artist => {
            const postsByAuthor = 'author_id' in req.query;
            const authorId = req.query.author_id;
            if (postsByAuthor && !mongoose.Types.ObjectId.isValid(authorId)) {
                return [];
            }

            if (req.user && ((req.user.artists_subscriptions.indexOf(artist._id) > -1) || ((req.user.role === userRoles.AUTHOR) && (req.user.artist.equals(artist._id))) || (req.user.role === userRoles.ADMIN))) {
                return Post.getAllPosts(artist._id, req.user && req.user._id, true, Number(req.params.count), req.query.last_post_id, postsByAuthor, authorId);
            } else {
                return Post.getAllPosts(artist._id, req.user && req.user._id, false, Number(req.params.count), req.query.last_post_id, postsByAuthor, authorId);
            }
        })
        .then(posts => res.send(posts))
};

const getArtistPostsBySlugWithinPeriod = (req, res, next) => {
    return Artist.getArtistBySlug(req.params.slug)
        .then(artist => {
            if (artist) {
                return artist;
            } else {
                res.status(httpStatus.NOT_FOUND).send({message: 'Artist not found'});
            }
        })
        .then(artist => {
            const postsByAuthor = 'author_id' in req.query;
            const authorId = req.query.author_id;
            if (postsByAuthor && !mongoose.Types.ObjectId.isValid(authorId)) {
                return [];
            }

            return Post.getPostsWithinPeriodWithoutMedia(artist._id, req.user && req.user._id,req.params.period_start_date, req.params.period_end_date, postsByAuthor, authorId)
        })
        .then(posts => res.send(posts))
};

const getArtistPinnedPost = (req, res, next) => {
    return Artist.getArtistBySlug(req.params.slug)
        .then(artist => {
            if (artist) {
                return artist;
            } else {
                res.status(httpStatus.NOT_FOUND).send({message: 'Artist not found'});
            }
        })
        .then(artist => {
            const postByAuthor = 'author_id' in req.query;
            const authorId = req.query.author_id;
            if (postByAuthor && !mongoose.Types.ObjectId.isValid(authorId)) {
                return null;
            }

            return Post.getPinnedPost(artist._id, req.user && req.user._id, postByAuthor, authorId)
        })
        .then(post => res.send({pinned_post: post}))
};

const getArtistAuthors = (req, res, next) => {
    return Artist.getArtistBySlug(req.params.slug)
        .then(artist => {
            if (!artist) {
                throw {status: httpStatus.NOT_FOUND, message: 'Artist not found'}
            } else {
                return Artist.getArtistAuthorsBySlug(req.params.slug, req.user && req.user._id)
            }
        })
        .then(authors => res.send(authors))
        .catch(error => {
            res.status(error.status).send({message: error.message});
        })
};

const updateArtistSchedule = (req, res, next) => {
    return Artist.getArtistBySlug(req.params.slug)
        .then(artist => checkArtistAndAuthor(artist, req))
        .then(artist => {
            const scheduleUpdate = {};
            const days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
            days.forEach(day => {
                if (req.body[day]) {
                    scheduleUpdate[day] = {};
                    scheduleUpdate[day].description = req.body[day].description || '';
                }
            });
            return Schedule.findOneAndUpdate(
                {_id: artist.schedule._id},
                {$set: scheduleUpdate},
                {new: true}
            );
        })
        .then(schedule => {
            res.send(schedule);
        })
        .catch(error => {
            res.status(error.status).send({message: error.message});
        })
};

const createTipJar = (req, res, next) => {
    return Artist.getArtistBySlug(req.params.slug)
        .then(artist => checkArtistAndAuthor(artist, req))
        .then(artist => {
            if (!artist.tip_jar_enabled) {
                throw {status: httpStatus.FORBIDDEN, message: 'Tip jar is disabled'}
            }
            return TipJar.update(
                {artist: artist._id, is_active: true},
                {$set: {is_active: false}},
                {multi: true}
            )
                .then(() => {
                    return TipJar.create({
                        artist: artist._id,
                        title: req.body.title,
                        description: req.body.description,
                        image: req.body.image,
                        goal_amount: req.body.goal_amount
                    })
                })
        })
        .then(jar => {
            res.send(jar);
        })
        .catch(error => errorHandler(error, res))
};

const getTipJar = (req, res, next) => {
    return Artist.getArtistBySlug(req.params.slug)
        .then(artist => {
            if (!artist) {
                throw {status: httpStatus.NOT_FOUNT, message: 'Artist not found'}
            } else if (!artist.tip_jar_enabled) {
                throw {status: httpStatus.FORBIDDEN, message: 'Tip jar is disabled'}
            } else {
                return TipJar.getActiveArtistJar(artist._id)

            }
        })
        .then(jar => {
            if (!jar) {
                throw {status: httpStatus.NOT_FOUND, message: 'Active jar not found'}
            } else {
                res.send(jar);
            }
        })
        .catch(error => errorHandler(error, res))
};

const closeTipJar = (req, res, next) => {
    return Artist.getArtistBySlug(req.params.slug)
        .then(artist => checkArtistAndAuthor(artist, req))
        .then(artist => {
            return TipJar.update(
                {artist: artist._id, is_active: true},
                {$set: {is_active: false}},
                {multi: true}
            )
        })
        .then(() => {
            res.send({message: 'Success'});
        })
        .catch(error => errorHandler(error, res))
};

const updateWishlist = (req, res, next) => {
    return Artist.getArtistBySlug(req.params.slug)
        .then(artist => checkArtistAndAuthor(artist, req))
        .then(artist => {
            return GroupWishlist.findOneAndUpdate(
                {_id: artist.wishlist},
                {$set: {
                        title: req.body.title,
                        description: req.body.description,
                        link: req.body.link,
                        is_enabled: req.body.is_enabled
                    }
                },
                {new: true}
            )
        })
        .then(wishlist => res.send(wishlist))
};

module.exports = {
    tempEndpointToCreateArtist,
    getArtists,
    getArtistBySlug,
    updateArtistBySlug,
    getArtistPostsBySlugWithPagination,
    getArtistPostsBySlugWithinPeriod,
    getArtistPinnedPost,
    getArtistAuthors,
    updateArtistSchedule,
    getTipJar,
    createTipJar,
    closeTipJar,
    updateWishlist
};
