const httpStatus = require('http-status');
const jwt = require('jsonwebtoken');
const moment = require('moment');
const uuidv4 = require('uuid/v4');
const stripe = require('../stripe');
const userRoles = require('../constants/userRoles');
const config = require('../config');
const User = require('../models/User');
const Artist = require('../models/Artist');
const SegPaySubscription = require('../models/SegPaySubscription');
const Comment = require('../models/Comment');
const PointEarning = require('../models/PointEarning');
const pointEarningSources = require('../constants/pointEarningSources');
const contentPeriodTypes = require('../constants/contentPeriodTypes');
const emails = require('../helpers/emails');
const subscriptionTypes = require('../constants/subscriptionTypes');
const segpayHelpers = require('../helpers/segpayHelpers');

const createUser = (req, res, next) => {
    const activationToken = uuidv4();
    const emailLowercased = req.body.email.toLocaleLowerCase();

    return User.getActivatedUsersBySnapchatOrUsername(req.body.username, req.body.snapchat_username)
        .then(users => {
            let errors = {};
            users.forEach(user => {
                if (req.body.snapchat_username && (user.snapchat_username === req.body.snapchat_username)) {
                    errors.snapchat_username = {message: 'This snapchat name is already used'}
                }

                if (user.username === req.body.username) {
                    errors.username = {message: 'This username name is already used'}
                }
            });

            if (errors.snapchat_username || errors.username) {
                throw {errors}
            } else {
                return;
            }
        })
        .then(() => {
            return User.create({
                email: emailLowercased,
                snapchat_username: req.body.snapchat_username,
                password: req.body.password,
                username: req.body.username,
                receive_emails: req.body.receive_emails,
                is_activated: false,
                activation_token: activationToken
            })
        })
        .then(user => {
            const activationLink = config[process.env.NODE_ENV].SERVER_DOMAIN + '/activate-account/' + activationToken;

            emails.sendEmail({
                to: emailLowercased,
                subject: 'Email confirmation',
                html: 'Hi ' + user.username + ', ' +
                '<br/> to finish your registration please <a href=\"' + activationLink + '\">click here.</a>' +
                '<br/> If this link doesn\'t work, copy paste this URL - ' + activationLink + ' into your browser.' +
                '<br/> Regards,' +
                '<br/> Brandts Boys'
            });

            PointEarning.create({
                user: user._id,
                points: 10,
                source: {type: pointEarningSources.REGISTRATION}
            })
                .then(() => {
                    const token = jwt.sign({ id: user._id }, config.JWT_SECRET, {
                        expiresIn: '7d'
                    });
                    res.send({token});
                })
        })
        .catch(err => {
            res.status(httpStatus.UNPROCESSABLE_ENTITY);
            res.send(err);
        })
};

const login = (req, res, next) => {
    return User.getUserByEmailAndPassword(req.body.email, req.body.password)
        .then(user => {
            const token = jwt.sign({ id: user._id }, config.JWT_SECRET, {
                expiresIn: '7d'
            });
            res.send({token});
        })
        .catch(err => {
            res.status(httpStatus.UNPROCESSABLE_ENTITY).send({message: 'Email/password pair is not valid'});
        })
};

const getUser = (req, res, next) => {
    let user = req.user;
    let userPromise;
    if (user.artist) {
        userPromise = User.getById(user._id, true);
    } else {
        userPromise = Promise.resolve(user);
    }

    return userPromise
        .then((user) => {
            return PointEarning.getTopEarningsAllTimeByUser(user._id)
                .then(earnings => {
                    user.points = earnings[0] ? earnings[0].points : 0;
                    res.send(user);
                });
        });
};

const deleteUser = (req, res, next) => {
    if ((req.user.role === userRoles.ADMIN) || (req.user.role === userRoles.AUTHOR)) {
        return res.status(httpStatus.FORBIDDEN).send({message: 'Admin or author can\'t delete account'});
    }

    return Promise.all([
        User.remove({_id: req.user._id}),
        Comment.update(
            {user: req.user._id},
            {
                $unset: {
                    user: 1,
                    comment: 1
                },
                $set: {
                    is_user_deleted: true
                }
            },
            {multi: true}
        ),
        PointEarning.remove({user: req.user._id}),
        SegPaySubscription.getUserOngoingSubscriptions(req.user._id)
            .then(subscriptions => {
                return Promise.all(subscriptions.map(subscription => {
                    return Promise.all([
                        (subscription && !subscription.is_old_stripe_subscription)
                            ? segpayHelpers.expireSubscription(subscription.purchase_id, 'Account deleted')
                            : Promise.resolve(),
                        SegPaySubscription.remove({user: req.user._id})
                    ])
                }));
            })
    ])
        .then(() => {
            res.send({message: 'User deleted'});
        })
};

const buyPeriod = (req, res, next) => {
    return Artist.getArtistBySlug(req.body.artist_slug)
        .then(artist => {
            if (artist && artist.first_post_date) {
                return artist;
            } else {
                res.status(httpStatus.NOT_FOUND).send({message: 'Artist not found'});
            }
        })
        .then(artist => {
            if (moment(artist.first_post_date).isAfter(req.body.period_end_date)) {
                res.status(httpStatus.BAD_REQUEST).send({message: 'period start date is invalid'})
            } else {
                return artist;
            }
        })
        .then(artist => {
            return stripe.charges.create({
                amount: config.PERIOD_UNLOCK_PRICE_CENTS,
                currency: "usd",
                description: "Unlock 30 days period till " + req.body.period_end_date,
                source: req.body.stripe_token,
            })
                .then(() => {
                    const periodStart = moment.max(
                        moment(artist.first_post_date),
                        moment(req.body.period_end_date).add({days: -30})
                    );

                    return User.update(
                        {_id: req.user._id},
                        {$push: {visible_content_periods: {
                            end: req.body.period_end_date,
                            start: periodStart.toISOString(),
                            type: contentPeriodTypes.PURCHASE
                        }}}
                    )
                })
        })
        .then(() => res.send({message: 'Period bought'}))
        .catch(error => {
            res.status(httpStatus.UNPROCESSABLE_ENTITY);
            res.send({error});
        })
};

const forgotPassword = (req, res, next) => {
    const emailLowercased = req.body.email.toLocaleLowerCase();
    const passwordResetToken = uuidv4();

    return User.findOneAndUpdate(
        {email: emailLowercased},
        {$set: {
            password_reset_token: passwordResetToken
        }}
    )
        .then(user => {
            if (user) {
                const resetPasswordLink = config[process.env.NODE_ENV].SERVER_DOMAIN + '/reset-password/' + passwordResetToken;

                emails.sendEmail({
                    to: emailLowercased,
                    subject: 'Reset a password',
                    html: 'Hi ' + user.username + ', ' +
                        '<br/> We\'ve received a Password Reset request. If you didn\'t request it, please ignore this email.' +
                        '<br/> To reset a password, <a href=\"' + resetPasswordLink + '\">click here.</a>' +
                        '<br/> If this link doesn\'t work, copy paste this URL - ' + resetPasswordLink + ' into your browser.' +
                        '<br/> Regards,' +
                        '<br/> Brandts Boys'
                });
            }
            res.send({message: 'Email sent'})
        })
};

const resetPassword = (req, res, next) => {
    return User.getByPasswordResetToken(req.body.password_reset_token)
        .then(user => {
            if (user) {
                user.password = req.body.password;
                user.password_reset_token = undefined;
                user.save()
                    .then(() => res.send({message: 'Password changed'}));
            } else {
                res.status(httpStatus.NOT_FOUND).send({message: 'Token is not valid'})
            }
        })
};

const activateAccount = (req, res, next) => {
    return User.getUserByActivationToken(req.body.activation_token)
        .then(notActivatedUser => {
            if (!notActivatedUser) {
                throw {status: httpStatus.NOT_FOUND, message: 'Activation code is not valid'}
            } else {
                return notActivatedUser;
            }
        })
        .then(notActivatedUser => {
            return User.getActivatedUsersBySnapchatOrUsername(notActivatedUser.username, notActivatedUser.snapchat_username)
                .then(users => {
                    if (users.length > 0) {
                        throw {status: httpStatus.CONFLICT, message: 'User with this username or snapchat name is already exists'}
                    } else {
                        return;
                    }
                })
        })
        .then(() => {
            return User.findOneAndUpdate(
                {activation_token: req.body.activation_token},
                {$set: {
                    is_activated: true,
                    activation_token: undefined
                }}
            )
        })
        .then(() => {
            res.send({message: 'User activated'});
        })
        .catch(error => {
            res.status(error.status).send({message: error.message})
        });
};

const banUser = (req, res, next) => {

    return SegPaySubscription.getUserOngoingSubscriptions(req.body.user_id)
        .then(subscriptions => {
            return Promise.all(subscriptions.map(subscription => {
                if (subscription && !subscription.is_old_stripe_subscription) {
                    return segpayHelpers.expireSubscription(subscription.purchase_id, 'Banned');
                } else {
                    return;
                }
            }));
        })
        .then(() => {
            return User.findOneAndUpdate(
                {_id: req.body.user_id},
                {$set: {
                    is_banned: true,
                    has_monthly_subscription: false
                }}
            )
        })
        .then(user => {
            if (user) {
                res.send({message: 'User banned'});
            } else {
                res.status(httpStatus.NOT_FOUND).send({message: 'User not found'});
            }
        })
};

const getBannedUsers = (req, res, next) => {
    return User.getBannedUsers()
        .then(users => res.send(users));
};

const subscribeToUser = (req, res, next) => {
    return User.getById(req.body.user_id)
        .then(user => {
            if (!user) {
                throw {status: httpStatus.NOT_FOUND, message: 'User not found'}
            } else if ((user.role !== userRoles.AUTHOR) && (user.role !== userRoles.ADMIN)) {
                throw {status: httpStatus.CONFLICT, message: 'User is not author'}
            } else {
                return User.findOneAndUpdate(
                    {_id: req.body.user_id, subscribers: {$ne: req.user._id}},
                    {$push: {
                        subscribers: req.user.id
                    }}
                )
            }
        })
        .then(author => {
            if (!author) {
                throw {status: httpStatus.CONFLICT, message: 'You have already subscribed to this author'}
            } else {
                res.send({message: 'Success'});
            }
        })
        .catch(error => {
            res.status(error.status).send({message: error.message});
        })
};

const unsubscribeFromUser = (req, res, next) => {
    return User.getById(req.body.user_id)
        .then(user => {
            if (!user) {
                throw {status: httpStatus.NOT_FOUND, message: 'User not found'}
            } else if ((user.role !== userRoles.AUTHOR) && (user.role !== userRoles.ADMIN)) {
                throw {status: httpStatus.CONFLICT, message: 'User is not author'}
            } else {
                return User.updateOne(
                    {_id: req.body.user_id},
                    {$pull: {
                        subscribers: req.user.id
                    }}
                )
            }
        })
        .then(() => {
            res.send({message: 'Success'});
        })
        .catch(error => {
            res.status(error.status).send({message: error.message});
        })
};

const updateUser = (req, res, next) => {
    const emailLowercased = req.body.email.toLocaleLowerCase();

    return User.getActivatedUsersBySnapchatOrUsername(req.body.username, req.body.snapchat_username)
        .then(users => {
            let errors = {};
            users.forEach(user => {
                if (user._id.equals(req.user._id)) return;

                if (req.body.snapchat_username && (user.snapchat_username === req.body.snapchat_username)) {
                    errors.snapchat_username = {message: 'This snapchat name is already used'}
                }

                if (user.username === req.body.username) {
                    errors.username = {message: 'This username name is already used'}
                }
            });

            if (errors.snapchat_username || errors.username) {
                throw {errors}
            } else {
                return;
            }
        })
        .then(() => {
            const userUpdate = {
                email: emailLowercased,
                username: req.body.username,
                snapchat_username: req.body.snapchat_username,
                receive_emails: req.body.receive_emails
            };
            if (req.body.avatar && ((req.user.role === userRoles.AUTHOR) || (req.user.role === userRoles.ADMIN))) {
                userUpdate.avatar = req.body.avatar;
            }
            if (req.body.biography && ((req.user.role === userRoles.AUTHOR) || (req.user.role === userRoles.ADMIN))) {
                userUpdate.biography = req.body.biography;
            }
            if (req.body.wishlist_link && ((req.user.role === userRoles.AUTHOR) || (req.user.role === userRoles.ADMIN))) {
                userUpdate.wishlist_link = req.body.wishlist_link;
            }
            return User.updateOne(
                {_id: req.user._id},
                {$set: userUpdate}
            )
        })
        .then(() => {
            return User.getById(req.user._id, Boolean(req.user.artist))
        })
        .then(user => {
            return PointEarning.getTopEarningsAllTimeByUser(user._id)
                .then(earnings => {
                    user.points = earnings[0] ? earnings[0].points : 0;
                    res.send(user);
                });
        })
        .catch(err => {
            res.status(httpStatus.UNPROCESSABLE_ENTITY);
            res.send(err);
        })
};

const resendActivationLink = (req, res, next) => {
    if (req.user.is_activated) {
        return res.status(httpStatus.CONFLICT).send({message: 'Account is already activated'});
    }

    return User.getById(req.user._id, false, false)
        .then(user => {
            const activationLink = config[process.env.NODE_ENV].SERVER_DOMAIN + '/activate-account/' + user.activation_token;

            emails.sendEmail({
                to: user.email,
                subject: 'Email confirmation',
                html: 'Hi ' + user.username + ', ' +
                '<br/> to finish your registration please <a href=\"' + activationLink + '\">click here.</a>' +
                '<br/> If this link doesn\'t work, copy paste this URL - ' + activationLink + ' into your browser.' +
                '<br/> Regards,' +
                '<br/> Brandts Boys'
            });

            res.send({message: 'Activation link was sent'});
        });
};

module.exports = {
    createUser,
    login,
    getUser,
    deleteUser,
    buyPeriod,
    forgotPassword,
    resetPassword,
    banUser,
    getBannedUsers,
    activateAccount,
    subscribeToUser,
    unsubscribeFromUser,
    updateUser,
    resendActivationLink
};
