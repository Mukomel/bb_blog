const httpStatus = require('http-status');
const User = require('../models/User');
const Artist = require('../models/Artist');
const segpayPostbackResponses = require('../constants/segpayPostbackResponses');
const SegPaySubscription = require('./../models/SegPaySubscription');
const SegPayPromoCodes = require('./../models/SegPayPromoCodes');
const SegPayDonation = require('./../models/SegPayDonation');
const SegPayTransactionRecord = require('./../models/SegPayTransactionRecord');
const PointEarning = require('./../models/PointEarning');
const TipJar = require('./../models/TipJar');
const segpayHelpers = require('../helpers/segpayHelpers');
const userRoles = require('../constants/userRoles');
const usdCentsToPoints = require('../helpers/usdCentsToPoints');
const stringPriceToCents = require('../helpers/stringPriceToCents');
const pointEarningSources = require('../constants/pointEarningSources');
const segpayTransactionSources = require('../constants/segpayTransactionSources');
const segpayTransactionRecordTypes = require('../constants/segpayTransactionRecordTypes');

const inquiry = (req, res, next) => {
    const userId = req.query.password;

    return User.getById(userId)
        .then(user => {
            if (!user) {
                res.send(segpayPostbackResponses.NOT_SUCCESSFUL);
            } else {
                res.send(segpayPostbackResponses.OK);
            }
        });
};

const enable = (req, res, next) => {
    const userId = req.query.password;
    const packageId = req.query.eticketid.split(':').shift();

    return Artist.getArtistBySegpayPackageId(packageId)
        .then(artist => {
            if (!artist) return res.send(segpayPostbackResponses.NOT_SUCCESSFUL);

            return Promise.all([
                SegPaySubscription.create({
                    price: req.query.price,
                    amount: stringPriceToCents(req.query.price),
                    approved: req.query.approved,
                    name: req.query.billname,
                    email: req.query.billemail,
                    trans_guid: req.query.transguid,
                    tran_id: req.query.tranid,
                    trans_time: req.query.transtime,
                    description: req.query.packagedescription,
                    purchase_id: req.query.purchaseid,
                    url_id: req.query.urlid,
                    country: req.query.billcntry,
                    user: userId,
                    artist: artist._id,
                    promo_code: req.query.ref1
                }),
                User.findOneAndUpdate(
                    {_id: userId},
                    {$push: {
                        artists_subscriptions: artist._id
                    }}
                )
                    .then(user => {
                        if (!user) {
                            res.send(segpayPostbackResponses.NOT_SUCCESSFUL);
                        } else {
                            res.send(segpayPostbackResponses.OK);
                        }
                    })
            ]);
        })
};

const cancellation = (req, res, next) => {
    return SegPaySubscription.findOneAndUpdate(
        {purchase_id: req.query.purchaseid},
        {$set: {
            canceled_at: new Date().toISOString(),
            is_canceled: true,
            is_pending_cancellation: false
        }}
    )
        .then((subscription) => {
            if(subscription) {
                res.send(segpayPostbackResponses.OK)
            } else {
                res.send(segpayPostbackResponses.NOT_SUCCESSFUL)
            }
        })
};

const disable = (req, res, next) => {
    const purchaseId = req.query.purchaseid;

    return SegPaySubscription.getByPurchaseId(purchaseId)
        .then(subscription => {
            if (!subscription) return res.send(segpayPostbackResponses.NOT_SUCCESSFUL);

            return User.updateOne(
                {_id: subscription.user},
                {$pull: {
                        artists_subscriptions: subscription.artist
                    }}
            )
                .then(() => res.send(segpayPostbackResponses.OK))
        });
};

const transaction = (req, res, next) => {
    const purchaseId = req.query.purchaseid;
    const transactionType = req.query.trantype;
    const userId = req.query.password;
    const packageId = req.query.eticketid.split(':').shift();

    Artist.getArtistBySegpayPackageId(packageId)
        .then(artist => {
            const params = {
                source: segpayTransactionSources.SUBSCRIPTION,
                price: req.query.price,
                amount: stringPriceToCents(req.query.price),
                approved: req.query.approved,
                name: req.query.billname,
                email: req.query.billemail,
                trans_guid: req.query.transguid,
                tran_id: req.query.tranid,
                trans_time: req.query.transtime,
                description: req.query.packagedescription,
                purchase_id: req.query.purchaseid,
                url_id: req.query.urlid,
                country: req.query.billcntry,
                user: userId,
                artist: artist._id,
            };

            if ((transactionType === 'Sale') && req.query.approved === 'Yes') {
                SegPayTransactionRecord.create({...params, type: segpayTransactionRecordTypes.INCOME});
            } else if ((transactionType === 'charge') || (transactionType === 'credit')) {
                SegPayTransactionRecord.create({...params, type: segpayTransactionRecordTypes.EXPENSE});
            }
        });

    if ((transactionType === 'charge') || (transactionType === 'credit')) {
        return SegPaySubscription.findOneAndUpdate(
            {purchase_id: purchaseId},
            {$set: {
                    is_canceled: true,
                    is_pending_cancellation: false
                }}
        )
            .then(subscription => {
                if (!subscription) return res.send(segpayPostbackResponses.NOT_SUCCESSFUL);

                return User.updateOne(
                    {_id: subscription.user},
                    {$pull: {
                            artists_subscriptions: subscription.artist
                        }}
                )
            })
            .then(() => res.send(segpayPostbackResponses.OK))
    } else {
        res.send(segpayPostbackResponses.OK);
    }
};

const transactionTip = (req, res, next) => {
    const transactionType = req.query.trantype;
    const userId = req.query.password;
    const artistId = req.query.ref5;

    Artist.getArtistById(artistId)
        .then(artist => {
            let tipParams = {};
            if (req.query.ref2 && req.query.ref3) {
                tipParams.post = req.query.ref2;
                tipParams.target = req.query.ref3;
            } else if (req.query.ref4) {
                tipParams.tip_jar = req.query.ref4;
            }

            const params = {
                source: segpayTransactionSources.TIP,
                price: req.query.price,
                amount: stringPriceToCents(req.query.price),
                approved: req.query.approved,
                name: req.query.billname,
                email: req.query.billemail,
                trans_guid: req.query.transguid,
                tran_id: req.query.tranid,
                trans_time: req.query.transtime,
                description: req.query.packagedescription,
                purchase_id: req.query.purchaseid,
                url_id: req.query.urlid,
                country: req.query.billcntry,
                user: userId,
                artist: artist._id,
                ...tipParams
            };

            if ((transactionType === 'Sale') && req.query.approved === 'Yes') {
                SegPayTransactionRecord.create({...params, type: segpayTransactionRecordTypes.INCOME});
            } else if ((transactionType === 'charge') || (transactionType === 'credit')) {
                SegPayTransactionRecord.create({...params, type: segpayTransactionRecordTypes.EXPENSE});
            }
        });

    if ((transactionType === 'Sale') && req.query.approved === 'Yes') {
        //Tip to author on post
        if (req.query.ref2 && req.query.ref3) {
            return SegPayDonation.create({
                price: req.query.price,
                amount: stringPriceToCents(req.query.price),
                approved: req.query.approved,
                name: req.query.billname,
                email: req.query.billemail,
                trans_guid: req.query.transguid,
                tran_id: req.query.tranid,
                trans_time: req.query.transtime,
                description: req.query.packagedescription,
                purchase_id: req.query.purchaseid,
                url_id: req.query.urlid,
                country: req.query.billcntry,
                user: userId,
                post: req.query.ref2,
                target: req.query.ref3
            })
                .then(segpayDonation => {
                    return PointEarning.create({
                        points: usdCentsToPoints(stringPriceToCents(segpayDonation.price)),
                        user: userId,
                        source: {
                            type: pointEarningSources.DONATION,
                            donation_id: segpayDonation._id,
                        }
                    })
                })
                .then(() => res.send(segpayPostbackResponses.OK))
        }

        //Tip to jar
        if (req.query.ref4) {
            return SegPayDonation.create({
                price: req.query.price,
                amount: stringPriceToCents(req.query.price),
                approved: req.query.approved,
                name: req.query.billname,
                email: req.query.billemail,
                trans_guid: req.query.transguid,
                tran_id: req.query.tranid,
                trans_time: req.query.transtime,
                description: req.query.packagedescription,
                purchase_id: req.query.purchaseid,
                url_id: req.query.urlid,
                country: req.query.billcntry,
                user: userId,
                tip_jar: req.query.ref4
            })
                .then(segpayDonation => {
                    return Promise.all([
                        TipJar.updateOne(
                            {_id: segpayDonation.tip_jar},
                            {$addToSet: {bakers: segpayDonation.user}, $inc: {current_amount: segpayDonation.amount}}
                        ),
                        PointEarning.create({
                            points: usdCentsToPoints(stringPriceToCents(segpayDonation.price)),
                            user: userId,
                            source: {
                                type: pointEarningSources.DONATION,
                                donation_id: segpayDonation._id,
                            }
                        })
                    ])
                })
                .then(() => res.send(segpayPostbackResponses.OK))
        }
    } else {
        res.send(segpayPostbackResponses.OK);
    }
};

const getUserSubscriptions = (req, res, next) => {
    return SegPaySubscription.getUserSubscriptions(req.user._id)
        .then(subscriptions => res.send(subscriptions));
};

const getActiveSubscriptions = (req, res, next) => {
    return SegPaySubscription.getActiveSubscriptions((req.user.role === userRoles.ADMIN) ? null : req.user.artist)
        .then(subscriptions => {
            return User.populate(subscriptions, {path: '_id.user', select: 'snapchat_username username password_reset_token email is_banned'})
        })
        .then(subscriptions => {
            return Artist.populate(subscriptions, {path: 'subscriptions.artist', select: 'name'})
        })
        .then(subscriptions => {
            return subscriptions.map(subscription => ({
                    subscriptions: subscription.subscriptions,
                    user: subscription._id.user
                })
            )
        })
        .then(subscriptions => {
            return subscriptions.filter(subscription => (subscription.user && !subscription.user.is_banned));
        })
        .then(subscriptions => res.send(subscriptions));
};


const cancelSubscription = (req, res, next) => {
    return SegPaySubscription.getById(req.body.subscription_id)
        .then(subscription => {
            if (subscription.user._id.equals(req.user._id)) {
                segpayHelpers.cancelSubcription(subscription.purchase_id)
                    .then(() => {
                        return SegPaySubscription.update({_id: subscription._id}, {$set: {is_pending_cancellation: true}});
                    })
                    .then(() => {
                        res.send({message: 'Subscription canceled'});
                    })
            } else {
                res.status(httpStatus.FORBIDDEN).send({message: 'forbidden'});
            }
        })
        .catch(error => {
            res.status(httpStatus.INTERNAL_SERVER_ERROR).send(error);
        })
};

const getPromoCodes = (req, res, next) => {
    if ((req.query.all_artists === 'true') && (req.user.role !== userRoles.ADMIN)) {
        return res.status(httpStatus.FORBIDDEN).send({message: 'all_artists param is allowed only for admins'})
    }

    let startPromise = (req.query.all_artists === 'true')
        ? SegPayPromoCodes.getPromoCodes()
        : SegPayPromoCodes.getByArtistId(req.user.artist);

    return startPromise
        .then(promoCodes => res.send(promoCodes))
};

const createPromoCode = (req, res, next) => {
    return SegPayPromoCodes.create({
        code: req.body.code,
        segpay_pricepoint_id: req.body.segpay_pricepoint_id,
        artist: req.user.artist,
        subscription_price_cents_first_30_days: req.body.subscription_price_cents_first_30_days,
        subscription_price_cents_recurring_30_days: req.body.subscription_price_cents_recurring_30_days
    })
        .then(promoCode => res.send(promoCode))
        .catch(err => {
            res.status(httpStatus.UNPROCESSABLE_ENTITY);
            res.send(err);
        })
};

const updatePromoCode = (req, res, next) => {
    return SegPayPromoCodes.getById(req.params.promocode_id)
        .then(promoCode => {
            if (!promoCode) {
                throw {status: httpStatus.NOT_FOUND, message: 'Promo code is not found'};
            } else if (!promoCode.artist.equals(req.user.artist)) {
                throw {status: httpStatus.CONFLICT, message: 'This promo code does not belong to your artist'}
            }

            return SegPayPromoCodes.findOneAndUpdate(
                {_id: promoCode._id},
                {$set: {
                    is_active: req.body.is_active
                }},
                {new: true}
            )
        })
        .then((promoCode) => res.send(promoCode))
        .catch(error => {
            res.status(error.status).send({message: error.message});
        })
};

const checkPromoCode = (req, res, next) => {
    return SegPayPromoCodes.getByCode(req.body.code)
        .then(promoCode => {
            console.log(promoCode);
            if (promoCode && promoCode.is_active && promoCode.artist.equals(req.body.artist_id)) {
                res.send(promoCode);
            } else {
                res.status(httpStatus.NOT_FOUND).send({message: "Promo code not found"})
            }
        })
};

module.exports = {
    inquiry,
    enable,
    cancellation,
    disable,
    transaction,
    transactionTip,
    getUserSubscriptions,
    getActiveSubscriptions,
    cancelSubscription,
    getPromoCodes,
    createPromoCode,
    updatePromoCode,
    checkPromoCode
};
