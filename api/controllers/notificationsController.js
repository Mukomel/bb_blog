const httpStatus = require('http-status');
const Notification = require('../models/Notification');

const getNotificationsCount = (req, res, next) => {
    return Notification.getUserNotificationsCount(req.user._id)
        .then(count => res.send({count}))
};

const getNotifications = (req, res, next) => {
    return Notification.getUserNotifications(req.user._id)
        .then(notifications => {
            Notification.update(
                {user: req.user._id, is_viewed: false},
                {$set: {is_viewed: true}},
                {multi: true}
            )
                .then(res.send(notifications))
        })
};

module.exports = {
    getNotificationsCount,
    getNotifications
};