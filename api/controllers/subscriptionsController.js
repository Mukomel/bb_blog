const httpStatus = require('http-status');
const moment = require('moment');
const Subscription = require('./../models/Subscription');
const User = require('./../models/User');
const stripe = require('../stripe');
const subscriptionTypes = require('../constants/subscriptionTypes');
const contentPeriodTypes = require('../constants/contentPeriodTypes');

const getActiveSubscriptions = (req, res, next) => {
    return Subscription.getActiveSubscriptions()
        .then(subscriptions => res.send(subscriptions));
};

const getExpiredSubscriptions = (req, res, next) => {
    res.send([]);
};

const getArchivedSubscriptions = (req, res, next) => {
    return Subscription.getArchivedSubscriptions()
        .then(subscriptions => res.send(subscriptions));
};

const markAdded = (req, res, next) => {
    return Subscription.update({_id: req.body.user_id}, {$set: {is_added: true}})
        .then(() => res.send('User marked as added'));
};

const markArchived = (req, res, next) => {
    return Subscription.update({_id: req.body.user_id}, {$set: {is_archived: true}})
        .then(() => res.send('User marked as archived'));
};

const resetAnchorNow = (req, res, next) => {
    const thirdOfMay = 1525305601;
    const thirdOfApril = 1522713601;
    return Subscription.getOngoingSubscriptions(subscriptionTypes.MONTHLY)
        .then(subscriptions => {
            return Promise.all(subscriptions.map((subscription, index) => {
                if (thirdOfMay > subscription.stripe_current_period_end && index === 0) {
                    return stripe.subscriptions.update(subscription.subscription_id, {
                        trial_end: thirdOfMay,
                        prorate: true
                    })
                        .then(() => {
                            return Promise.all([
                                Subscription.update(
                                    {_id: subscription._id},
                                    {$set: {
                                            stripe_creation_date: String(thirdOfApril),
                                            stripe_current_period_end: String(thirdOfMay)
                                        }}
                                ),
                                User.update(
                                    {_id: subscription.user, "visible_content_periods.type": contentPeriodTypes.SUBSCRIPTION},
                                    {$set: {"visible_content_periods.$": {
                                                type: contentPeriodTypes.SUBSCRIPTION,
                                                start: moment(Number(thirdOfApril + '000')).add({days: -30}).toISOString(),
                                                end: moment(Number(thirdOfMay + '000')).toISOString()
                                            }}}

                                )
                            ])
                        })
                }

            }))
        })
        .then(() => res.send({message: 'Success'}))
        .catch((error) => res.status(httpStatus.INTERNAL_SERVER_ERROR).send(error))
};

module.exports = {
    getActiveSubscriptions,
    getExpiredSubscriptions,
    getArchivedSubscriptions,
    markAdded,
    markArchived,
    resetAnchorNow
};
