const logger = require('../config/winston');

const logError = (req, res, next) => {
    const { user_id, message, stack } = req.body;

    logger.info({
        user_id,
        created_at: Date.now(),
        message,
        stack
    });
    res.send({});
};

module.exports = { logError };