const httpStatus = require('http-status');
const fs = require('fs-extra');
const path = require('path');
const config = require('../config');
const Post = require('../models/Post');
const User = require('../models/User');
const Artist = require('../models/Artist');
const PointEarning = require('../models/PointEarning');
const Comment = require('../models/Comment');
const pointEarningSources = require('../constants/pointEarningSources');
const {updatePropIfExists} = require('../helpers/utility');
const sendNewPostNotifications = require('../helpers/sendNewPostNotifications');

const createPost = (req, res, next) => {
    return Post.create({
        title: req.body.title,
        body: req.body.body,
        media: req.body.media,
        user: req.user._id,
        artist: req.user.artist,
        is_published: req.body.is_published
    })
        .then(post => {
            if (post.is_published) {
                sendNewPostNotifications(post._id)
            }
            Artist.getArtistById(req.user.artist)
                .then(artist => {
                    if (!artist.first_post_date) {
                        artist.first_post_date = post.created_at;
                        return artist.save();
                    } else {
                        return;
                    }
                })
                .then(() => {
                    Artist.update(
                        {_id: post.artist},
                        {
                            $inc: { posts_count: 1 }
                        })
                        .then(() => res.send(post))
                })
        });
};

const deletePost = (req, res, next) => {
    return Post.findOneAndRemove({_id: req.params.post_id})
        .then(post => {
            if (post) {
                if (post.media.path) {
                    const fileName = post.media.path.split('/').pop();
                    fs.remove(path.resolve(__dirname, '../uploadedAssets/' + fileName));
                }
                Artist.update(
                    {_id: post.artist},
                    {
                        $inc: { posts_count: -1 }
                    })
                    .then(() => res.send({message: "Post deleted"}))
            } else {
                res.status(httpStatus.NOT_FOUND).send({message: 'Post not found'});
            }
        })
};

const editPost = (req, res, next) => {
    let newPost = {};
    const {title, body, media, is_published} = req.body;

    updatePropIfExists(newPost, title, 'title');
    updatePropIfExists(newPost, body, 'body');
    updatePropIfExists(newPost, media, 'media');
    updatePropIfExists(newPost, is_published, 'is_published');
    return Post.findOneAndUpdate(
        {_id: req.params.post_id},
        {
            $set: newPost
        },
        {new: true}
    )
        .then(post => {
            if (post) {
                return User.populate(post, {path: 'user', select: 'username'})
                    .then(post => res.send(post));
            } else {
                res.status(httpStatus.NOT_FOUND).send({message: 'Post not found'});
            }
        })
};

const getUserArtistPosts = (req, res, next) => {
    return Post.getUserArtistPosts(req.user.artist)
        .then(posts => res.send(posts));
};

const likePost = (req, res, next) => {
    return Post.findOneAndUpdate(
        {
            _id: req.params.post_id,
            likes: {$ne: req.user._id}
        },
        {
            $push: {
                likes: req.user.id
            }
        }
    )
        .then(post => {
            if (!post) {
                res.status(httpStatus.NOT_FOUND).send({message: "Post not found or already liked by this user"})
            } else {
                Promise.all([
                    Artist.update(
                        {_id: post.artist},
                        {
                            $inc: {
                                likes_count: 1
                            }
                        }
                    ),
                    PointEarning.create({
                        points: config.POINTS_FOR_LIKE,
                        user: req.user._id,
                        source: {
                            type: pointEarningSources.LIKE,
                        }
                    })
                ])
                    .then(() => res.send({message: 'Success'}))
            }
        })
};

const viewPostVideo = (req, res, next) => {
    return Post.findOneAndUpdate(
        {
            _id: req.params.post_id
        },
        {$inc: {views: 1}}
    )
        .then(post => {
            if (!post) {
                res.status(httpStatus.NOT_FOUND).send({message: "Post not found"})
            } else {
                Artist.update(
                    {_id: post.artist},
                    {
                        $inc: {views_count: 1}
                    }
                ).then(() => res.send({message: 'Success'}))
            }
        })
};

const pinPost = (req, res, next) => {
    if (!req.user.artist) {
        return res.status(httpStatus.CONFLICT).send({message: 'User is not associated with artist'})
    }

    return Post.update(
        {
            artist: req.user.artist,
            is_pinned: true
        },
        {
            $unset: {
                is_pinned: 1
            }
        },
        {multi: true}
    )
        .then(() => {
            Post.findOneAndUpdate(
                {_id: req.params.post_id},
                {
                    $set: {
                        is_pinned: true
                    }
                }
            )
                .then(post => {
                    if (post) {
                        res.send({message: 'Post pinned'});
                    } else {
                        res.status(httpStatus.NOT_FOUND).send({message: 'Post not found'});
                    }
                })
        })
};

const unpinPost = (req, res, next) => {
    return Post.findOneAndUpdate(
        {
            _id: req.params.post_id,
            artist: req.user.artist
        },
        {
            $unset: {
                is_pinned: 1
            }
        }
    )
        .then(post => {
            if (post) {
                res.send({message: 'Post unpinned'})
            } else {
                res.status(httpStatus.NOT_FOUND).send({message: 'Post with id ' + req.params.post_id + ' is not found for your artist'});
            }
        })
};

const getComments = (req, res, next) => {
    return Comment.getPostComments(req.params.post_id, req.user && req.user.id)
        .then(comments => res.send(comments))
};

module.exports = {
    createPost,
    deletePost,
    editPost,
    likePost,
    pinPost,
    unpinPost,
    getUserArtistPosts,
    getComments,
    viewPostVideo
};