const httpStatus = require('http-status');
const path = require('path');
const config = require('../config');
const addWatermarkToImage = require('../helpers/addWatermarkToImage');
const addWatermarkToVideo = require('../helpers/addWatermarkToVideo');
const Post = require('../models/Post');
const sendNewPostNotifications = require('../helpers/sendNewPostNotifications');

const uploadPostMedia = (req, res, next) => {
    if (req.file) {
        return new Promise((resolve) => {
            const fileType = req.file.mimetype.split('/').shift();

            const options = {
                watermarkPath: path.resolve(__dirname, '../assets/watermark.png'),
                fileStorageFolderPath: path.resolve(__dirname, '../uploadedAssets'),
                originalFileName: req.file.filename,
                onSuccess: resolve
            };
            Post.updateOne(
                {_id: req.body.postId},
                {
                    $set: {
                        is_watermarking: true
                    }
                }).then(post => {
                    if (!post) {
                        res.status(httpStatus.NOT_FOUND).send({message: 'Post not found'});
                    }
                });

            if (fileType === 'image') {
                addWatermarkToImage(options)

            } else if (fileType === 'video') {
                addWatermarkToVideo(options)
            }
        })
            .then(({watermarkedFileName, type}) => {

                if (req.body.postId) {
                    Post.findOneAndUpdate(
                        {_id: req.body.postId},
                        {
                            $set: {
                                'media.path': config[process.env.NODE_ENV].SERVER_DOMAIN + '/uploads/' + watermarkedFileName,
                                'media.type': type,
                                is_published: true,
                                is_watermarking: false
                            }
                        }
                    ).then(post => {
                        if (!post) {
                            res.status(httpStatus.NOT_FOUND).send({message: 'Post not found'});
                        } else {
                            sendNewPostNotifications(post._id)
                        }
                    });
                }
            })
    } else {
        res.status(httpStatus.UNPROCESSABLE_ENTITY).send({message: "Please provide valid file"})
    }
};

const uploadImage = (req, res, next) => {
    if (req.file) {
        res.send({path: config[process.env.NODE_ENV].SERVER_DOMAIN + '/uploads/' + req.file.filename})
    } else {
        res.status(httpStatus.UNPROCESSABLE_ENTITY).send({message: "Please provide valid file"})
    }
};

module.exports = {
    uploadPostMedia,
    uploadImage
};