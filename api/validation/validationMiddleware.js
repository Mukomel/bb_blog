const { validationResult } = require('express-validator/check');
const httpStatus = require('http-status');

function validationMiddleware(req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(httpStatus.UNPROCESSABLE_ENTITY).json({ errors: errors.mapped() });
    }

    next();
}

module.exports = validationMiddleware;
