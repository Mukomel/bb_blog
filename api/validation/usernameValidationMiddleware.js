const httpStatus = require('http-status');

const usernameValidationMiddleware = (req, res, next) => {
    if ((req.body.username === req.body.email) || (req.body.username === req.body.snapchat_username)) {
        res.status(httpStatus.UNPROCESSABLE_ENTITY);
        res.send({errors: {username: {msg: 'Username cannot be same as email/snapchat username'}}});
    } else {
        next();
    }
};

module.exports = usernameValidationMiddleware;