const INCOME = 'INCOME';
const EXPENSE = 'EXPENSE';

module.exports = {
    INCOME,
    EXPENSE
};