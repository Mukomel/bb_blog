const ACTIVE = 'ACTIVE';
const CANCELED = 'CANCELED';
const REGISTERED = 'REGISTERED';

module.exports = {
    ACTIVE,
    CANCELED,
    REGISTERED
};