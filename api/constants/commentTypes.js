const COMMENT = 'COMMENT';
const REPLY = 'REPLY';

module.exports = {
    COMMENT,
    REPLY
};