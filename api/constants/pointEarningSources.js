const REGISTRATION = 'REGISTRATION';
const DONATION = 'DONATION';
const LIKE = 'LIKE';

module.exports = {
    REGISTRATION,
    DONATION,
    LIKE
};