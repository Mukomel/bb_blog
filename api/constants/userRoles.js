const USER = 'USER';
const AUTHOR = 'AUTHOR';
const ADMIN = 'ADMIN';

module.exports = {
    USER,
    AUTHOR,
    ADMIN
};