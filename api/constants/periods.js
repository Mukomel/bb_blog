const MONTHLY = 'monthly';
const WEEKLY = 'weekly';
const ALL_TIME = 'all_time';

module.exports = {
    MONTHLY, WEEKLY, ALL_TIME
};