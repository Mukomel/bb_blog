const BRANDT = 'BRANDT';
const NASH = 'NASH';
const DREW = 'DREW';

module.exports = {
    BRANDT,
    NASH,
    DREW
};