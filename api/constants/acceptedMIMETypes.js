const imageMIMETypes = ['image/png', 'image/jpeg', 'image/gif'];
const videoMIMETypes = ['video/x-msvideo', 'video/avi', 'video/mp4', 'video/quicktime'];

const postMediaMIMETypes = [...imageMIMETypes, ...videoMIMETypes];

module.exports = {
    imageMIMETypes,
    videoMIMETypes,
    postMediaMIMETypes
};