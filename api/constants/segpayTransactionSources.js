const SUBSCRIPTION = 'SUBSCRIPTION';
const TIP = 'TIP';

module.exports = {
    SUBSCRIPTION,
    TIP
};