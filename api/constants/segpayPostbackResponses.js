const OK = 'OK';
const NOT_SUCCESSFUL = 'NOT_SUCCESSFUL';

module.exports = {
    OK,
    NOT_SUCCESSFUL
};
