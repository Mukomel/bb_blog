const MONTHLY = 'MONTHLY';
const LIFETIME = 'LIFETIME';

module.exports = {
    MONTHLY,
    LIFETIME
};