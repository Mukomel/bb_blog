const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const subscriptionSchema = new Schema({
    created_at: {type: Date, required: true, default: Date.now},
    subscription_id: String,
    expiration_date: String,
    stripe_creation_date: String,
    stripe_expiration_data: String,
    stripe_current_period_end: String,
    stripe_discount: String,
    subscription_price: Number,
    subscription_type: String,
    charge_id: String,
    customer_id: String,
    is_added: {type: Boolean, required: true, default: false},
    is_canceled: {type: Boolean, required: true, default: false},
    is_archived: {type: Boolean, required: true, default: false},
    user: { type: Schema.Types.ObjectId, ref: 'User' },
});

subscriptionSchema.statics = {
    getBySubscriptionId(subscriptionId) {
        return this.findOne({subscription_id: subscriptionId})
            .populate({path: 'user'})
            .exec()
            .then(subscription => subscription)
    },

    getUserOngoingSubscription(userId, subscriptionType) {
        return this.findOne({
            is_canceled: false,
            user: userId,
            subscription_type: subscriptionType
        })
            .exec()
            .then(subscription => subscription)
    },

    getOngoingSubscriptions(subscriptionType) {
        return this.find({
            is_canceled: false,
            subscription_type: subscriptionType
        })
            .exec()
            .then(subscriptions => subscriptions)
    },

    getUpdatedSubscriptions() {
        return this.find({$or: [
                {is_added: false},
                {is_archived: false, is_canceled: true}
            ]})
            .populate({ path: 'user', select: 'snapchat_username username password_reset_token email is_banned'})
            .exec()
            .then(subscriptions => subscriptions.filter(subscription => (subscription.user && !subscription.user.is_banned)))
    },

    getActiveSubscriptions() {
        return this.find({is_added: true})
            .populate({ path: 'user', select: 'snapchat_username email'})
            .exec()
            .then(subscriptions => subscriptions)
    },

    getArchivedSubscriptions() {
        return this.find({is_archived: true})
            .populate({ path: 'user', select: 'snapchat_username email'})
            .exec()
            .then(subscriptions => subscriptions)
    },

    getUserSubscriptions(userId) {
        return this.find({user: userId})
            .populate({path: 'user', select: 'snapchat_username'})
            .exec()
            .then(subscriptions => subscriptions)
    }
};

module.exports = mongoose.model('Subscription', subscriptionSchema);