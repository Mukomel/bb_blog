const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const massMailingSchema = new Schema({
    created_at: {type: Date, required: true, default: Date.now},
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    emails_list: [{type: String}],
    email_text: {type: String},
    notification_users_list: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    notification_text: {type: String},
    member_stats_selection_options: {
        period_first_day: {type: Date},
        period_last_day: {type: Date},
        artist: {type: Schema.Types.ObjectId, ref: 'Artist'},
        users_type: {type: String},
        promo_code: {type: String},
    },
    options: {
        target: {type: String},
        send_emails: {type: Boolean},
        send_notifications: {type: Boolean},
        with_promocode: {type: Boolean},
        without_promocode: {type: Boolean}
    }
});

massMailingSchema.statics = {};

module.exports = mongoose.model('MassMailing', massMailingSchema);