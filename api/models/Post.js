const Comment = require('./Comment');

const mongoose = require('mongoose');
const moment = require('moment');

const Schema = mongoose.Schema;

const postSchema = new Schema({
    created_at: {type: Date, required: true, default: Date.now},
    title: {type: String, required: true},
    body: {type: String, required: false},
    media: {
        type: {type: String, required: false},
        path: {type: String, required: false}
    },
    is_pinned: {type: Boolean},
    is_published: {type: Boolean, required: true, default: true},
    is_watermarking: {type: Boolean, default: false},
    views: {type: Number},
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    artist: { type: Schema.Types.ObjectId, ref: 'Artist' },
    likes: [{type: Schema.Types.ObjectId, ref: 'User'}],
});

const VISIBLE_COMMENTS_NUMBER = 5;


const preparePosts = ((posts, userId) => {
    return Promise.all(posts.map(({likes = [], ...fields}) => {
        return Promise.all([
            Comment.getPostComments(fields._id, userId, VISIBLE_COMMENTS_NUMBER),
            Comment.getPostCommentsCount(fields._id)
        ])
            .then(([comments, count]) => ({
                ...fields,
                likes_count: likes.length,
                is_liked: Boolean(likes.find(id => id.equals(userId))),
                comments,
                has_more_comments: count > VISIBLE_COMMENTS_NUMBER,
                comments_count: count
            }))
    }));
});

postSchema.statics = {
    getById(id) {
        return this.findOne({_id: id})
            .exec()
            .then(post => post)
    },

    getUserArtistPosts(artistId) { // Admin Panel posts
        return this.find({artist: artistId})
            .sort({created_at: 'desc'})
            .populate({ path: 'user', select: 'username'})
            .exec()
            .then(posts => posts)
    },

    getAllPosts(artistId, userId, keepMedia, postsPerPage, lastPostId, postsByAuthor, authorId) { // all posts for guests and admins
        const query = lastPostId
            ? {artist: artistId, is_pinned: {$ne: true}, _id: {$lt: lastPostId}, is_published: true, user: postsByAuthor ? authorId : {$exists: true}}
            : {artist: artistId, is_pinned: {$ne: true}, is_published: true, user: postsByAuthor ? authorId : {$exists: true}};

        return this.find(query)
            .sort({created_at: 'desc'})
            .limit(postsPerPage)
            .populate({ path: 'user', select: 'username avatar donation_target'})
            .lean()
            .exec()
            .then(posts => posts.map(post => {
                if (post.media && !keepMedia) {
                    return {...post, media: {...post.media, path: null}};
                } else {
                    return post
                }
            }))
            .then(posts => preparePosts(posts, userId))
    },

    getPostsWithinPeriodsAndNewest(artistId, periods, userId, postsPerPage, lastPostId, postsByAuthor, authorId) { // for subscribers only
        let latestPeriodEndDate;
        let createdAtQuery = [];
        periods.forEach(period => {
            createdAtQuery.push({
                created_at: {
                    $lte: period.end,
                    $gte: period.start
                },
                is_published: true
            });
            if (!latestPeriodEndDate) {
                latestPeriodEndDate = period.end
            } else if (moment(latestPeriodEndDate).isBefore(period.end)) {
                latestPeriodEndDate = period.end
            }
        });
        createdAtQuery.push({
            created_at: {
                $gte: latestPeriodEndDate
            },
            is_published: true
        });

        const query = lastPostId
            ? {$and: [
                {artist: artistId},
                {user: postsByAuthor ? authorId : {$exists: true}},
                {_id: {$lt: lastPostId}},
                {is_pinned: {$ne: true}},
                {$or: createdAtQuery}
            ]}
            : {$and: [
                {artist: artistId},
                {user: postsByAuthor ? authorId : {$exists: true}},
                {is_pinned: {$ne: true}},
                {$or: createdAtQuery}
            ]};

        return this.find(query)
            .sort({created_at: 'desc'})
            .limit(postsPerPage)
            .populate({ path: 'user', select: 'username avatar donation_target'})
            .lean()
            .exec()
            .then(posts => posts.map(post => {
                if (post.media && moment(post.created_at).isAfter(latestPeriodEndDate)) {
                    return {...post, media: {...post.media, path: null}};
                } else {
                    return post
                }
            }))
            .then(posts => preparePosts(posts, userId))
    },

    getPostsWithinPeriodWithoutMedia(artistsId, userId, periodStartDate, periodEndDate, postsByAuthor, authorId) { //for subscribers only for given date
        return this.find({
            artist: artistsId,
            user: postsByAuthor ? authorId : {$exists: true},
            is_pinned: {$ne: true},
            is_published: true,
            created_at: {
                $lte: periodEndDate,
                $gte: periodStartDate
            }
        })
            .sort({created_at: 'desc'})
            .populate({ path: 'user', select: 'username avatar donation_target'})
            .lean()
            .exec()
            .then(posts => posts.map(post => {
                if (post.media) {
                    return {...post, media: {...post.media, path: null}};
                } else {
                    return post
                }
            }))
            .then(posts => preparePosts(posts, userId))
    },

    getPinnedPost(artistId, userId, postByAuthor, authorId) {
        return this.findOne({
            artist: artistId,
            is_pinned: true,
            is_published: true,
            user: postByAuthor ? authorId : {$exists: true}
        })
            .populate({ path: 'user', select: 'username avatar donation_target'})
            .lean()
            .exec()
            .then(post => {
                if (!post) {
                    throw {}
                } else {
                    return post;
                }
            })
            .then(post => preparePosts([post], userId))
            .then(([post]) => post)
            .catch(() => null)
    }
};

module.exports = mongoose.model('Post', postSchema);
