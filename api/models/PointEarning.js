const mongoose = require('mongoose');
const moment = require('moment');
const User = require('./User');
const userRoles = require('../constants/userRoles');

const Schema = mongoose.Schema;

const pointEarningSchema = new Schema({
    created_at: {type: Date, required: true, default: Date.now},
    source: {
        type: {type: String, required: true},
        donation_id: Schema.Types.ObjectId
    },
    points: {type: Number, required: true},
    user: { type: Schema.Types.ObjectId, ref: 'User' }
});

pointEarningSchema.statics = {
    getTopEarnings(matchCreatedAt, userId = {$exists: true}) {

        return this.aggregate([
            {$match:
                {
                    created_at: matchCreatedAt,
                    user: userId
                }
            },
            {$group: {_id: '$user', points: {$sum: '$points'}}}
        ])
            .sort({points: 'desc'})
            .limit(20)
            .exec()
            .then(earnings => {
                return User.populate(earnings, {path: '_id', select: 'username role'})
            })
            .then(earnings => earnings.filter(earning => (earning._id.role !== userRoles.ADMIN) && (earning._id.role !== userRoles.AUTHOR)))
            .then(earnings => earnings.slice(0, 10))
            .then(earnings => earnings.map(earning => ({
                user: earning._id,
                points: earning.points
            })))
    },

    getTopEarningsAllTimeByUser(userId) {
        return this.getTopEarnings({$exists: true}, userId);
    },
    getTopEarningsAllTime() {
        return this.getTopEarnings({$exists: true});
    },

    getTopEarningsWeekly() {
        return this.getTopEarnings({$gte: new Date(moment().startOf('isoWeek').toISOString())});
    },

    getTopEarningsMonthly() {
        return this.getTopEarnings({$gte: new Date(moment().startOf('month').toISOString())});
    }
};

module.exports = mongoose.model('PointEarning', pointEarningSchema);