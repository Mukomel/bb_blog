const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const Schema = mongoose.Schema;

const segPayPromoCodeSchema = new Schema({
    created_at: {type: Date, required: true, default: Date.now},
    code: {type: String, required: true,  unique: 'This promocode is already exists'},
    segpay_pricepoint_id: {type: String, required: true},
    is_active: {type: Boolean, required: true, default: false},
    artist: {type: Schema.Types.ObjectId, ref: 'Artist'},
    subscription_price_cents_first_30_days: {type: Number, required: true},
    subscription_price_cents_recurring_30_days: {type: Number, required: true}
});

segPayPromoCodeSchema.plugin(uniqueValidator);

segPayPromoCodeSchema.statics = {
    getById(id) {
        return this.findOne({_id: id})
            .exec()
            .then(promoCode => promoCode)
    },

    getPromoCodes() {
        return this.find()
            .exec()
    },

    getByArtistId(artistId) {
        return this.find({artist: artistId})
            .sort({created_at: 'desc'})
            .exec()
            .then(promocodes => promocodes)
    },

    getByCode(code) {
        return this.findOne({code})
            .exec()
            .then(promoCode => promoCode)
    }
};

module.exports = mongoose.model('SegPayPromoCode', segPayPromoCodeSchema);
