const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const moment = require('moment');
const uniqueValidator = require('mongoose-unique-validator');
const userRoles = require('../constants/userRoles');
const massMailingTargets = require('../constants/massMailingTargets');
const membersStatisticUserTypes = require('../constants/membersStatisticUserTypes');

const PASSWORD_SALT_ROUNDS = 10;

const Schema = mongoose.Schema;

const userSchema = new Schema({
    email: {type: String, required: true, unique: 'This email is already used'},
    snapchat_username: {type: String},
    created_at: {type: Date, required: true, default: Date.now},
    username: {type: String, required: true},
    avatar: {type: String},
    donation_target: {type: String}, //especially for Brandt, Nash, Drew users
    biography: {type: String},
    wishlist_link: {type: String},
    password: {type: String, required: true},
    role: {type: String, required: true, default: userRoles.USER},
    artists_subscriptions: [{type: Schema.Types.ObjectId, ref: 'Artist'}],
    password_reset_token: {type: String},
    points: {type: Number},
    visible_content_periods: [
        {
            start: {type: Date, required: true},
            end: {type: Date, required: true},
            type: {type: String, required: true}
        }
    ],
    is_banned: {type: Boolean},
    is_activated: {type: Boolean, required: true},
    activation_token: {type: String},
    artist: {type: Schema.Types.ObjectId, ref: 'Artist'},
    subscribers: [{type: Schema.Types.ObjectId, ref: 'User'}],
    receive_emails: {type: Boolean, required: true}
});

userSchema.plugin(uniqueValidator);

userSchema.pre('save', function (next) {
    bcrypt.hash(this.password, PASSWORD_SALT_ROUNDS)
        .then(hash => {
            this.password = hash;
            next();
        })
        .catch(err => {
            next(err);
        });
});

const userSchemaProjection = {password: 0, password_reset_token: 0, activation_token: 0};

userSchema.statics = {
    getById(id, populateArtist = false, withProjection = true) {
        return this.findOne({_id: id})
            .select(withProjection ? userSchemaProjection : {})
            .populate(populateArtist ? { path: 'artist', select: 'slug'} : '')
            .exec()
            .then(user => user)
    },

    getByPasswordResetToken(passwordResetToken) {
        return this.findOne({password_reset_token: passwordResetToken})
            .exec()
            .then(user => user)
    },

    getUserByEmailAndPassword(email, password) {
        const emailLowerCased = email.toLowerCase();
        return this.findOne({email: emailLowerCased})
            .exec()
            .then(user => {
                return new Promise((resolve, reject) => {
                    bcrypt.compare(password, user.password, (err, isPasswordMatch) => {
                        if (isPasswordMatch) {
                            resolve(user)
                        } else {
                            reject({});
                        }
                    })
                })
            })
    },

    getBannedUsers() {
        return this.find({is_banned: true})
            .select(userSchemaProjection)
            .exec()
            .then(users => users)
    },

    getActivatedUsersBySnapchatOrUsername(username, snapchat_username) {
        return this.find({
            $and: [
                {is_activated: true},
                {$or: snapchat_username
                    ? [
                        {username},
                        {snapchat_username}
                    ]
                    : [
                        {username}
                    ]
                }
            ]
        })
            .exec()
            .then(users => users)
    },

    getUserByActivationToken(activation_token) {
        return this.findOne({activation_token})
            .exec()
            .then(user => user)
    },

    getUsersForMassMailing(target, artist) {
        let query = {};

        switch (target) {
            case massMailingTargets.ALL_FANS:
                break;

            case massMailingTargets.ACTIVE_SUBSCRIBERS:
                query = {
                    artists_subscriptions: artist
                };
                break;

            case massMailingTargets.CANCELED_SUBSCRIBERS:
            case massMailingTargets.REGISTERED_USERS:
                query = {
                    artists_subscriptions: {$ne: artist}
                };
                break;
        }

        return this.find(query)
            .exec()
            .then(users => users)
    },

    getDonationTargetUsers() {
        return this.find({donation_target: {$exists: true}})
            .select(userSchemaProjection)
            .exec()
            .then(users => users)
    },

    getUsers() {
        return this.find({role: userRoles.USER})
            .select({username: 1, email: 1})
    },

    getMembersStatistics({period_first_day, period_last_day, users_type, promo_code, artist, limit, page,
                             show_total_user_count, get_users_for_mailing}) {
        let query, filteringQuery, userCreatedAtQuery;

        if (period_first_day && period_last_day && (users_type !== membersStatisticUserTypes.CANCELED)) {
            userCreatedAtQuery = {
                created_at: {
                    $lte: new Date(moment(period_last_day).endOf('day').toISOString()),
                    $gte: new Date(moment(period_first_day).startOf('day').toISOString())
                }
            }
        }

        const promoCodeQueryPart = promo_code
            ? {
                promo_code
            }
            : {
                $or: [
                    {promo_code: {$exists: true}},
                    {promo_code: {$exists: false}}
                ]
            };
        switch (users_type) {
            case membersStatisticUserTypes.ACTIVE:
                filteringQuery = {
                    artists_subscriptions: artist
                };
                query = {
                    segpay_subscriptions: {
                        $elemMatch: {
                            is_canceled: false,
                            artist,
                            ...promoCodeQueryPart
                        }
                    }
                };
                break;

            case membersStatisticUserTypes.CANCELED:
                const canceledAtQueryPart = (period_first_day && period_last_day)
                    ? {
                        canceled_at: {
                            $lte: new Date(moment(period_last_day).endOf('day').toISOString()),
                            $gte: new Date(moment(period_first_day).startOf('day').toISOString())
                        }
                    }
                    : {
                        $or: [
                            {canceled_at: {$exists: true}},
                            {canceled_at: {$exists: false}}
                        ]
                    };
                query = {
                    segpay_subscriptions: {
                        $not: {
                            $elemMatch: {
                                is_canceled: false,
                                artist
                            }
                        },
                        $all: [
                            {
                                $elemMatch: {
                                    is_canceled: true,
                                    artist,
                                    ...canceledAtQueryPart,
                                    ...promoCodeQueryPart,
                                }
                            }
                        ]
                    }
                };
                break;

            case membersStatisticUserTypes.REGISTERED:
                filteringQuery = {
                    artists_subscriptions: {$ne: artist}
                };
                query = {
                    segpay_subscriptions: {
                        $not: {
                            $elemMatch: {
                                artist
                            }
                        }
                    }
                };
                break;

            default:
                query = {};
                break;
        }

        let aggregationRules = [];
        if (get_users_for_mailing) {
            aggregationRules = [
                userCreatedAtQuery && {
                    $match: userCreatedAtQuery
                },
                filteringQuery && {
                    $match: filteringQuery
                },
                {
                    $lookup: {
                        from: "segpaysubscriptions",
                        localField: "_id",
                        foreignField: "user",
                        as: "segpay_subscriptions"
                    }
                },
                {
                    $match: query
                },
                {
                    $project: {
                        _id: 1,
                        email: 1,
                        receive_emails: 1,
                        is_activated: 1,
                        is_banned: 1
                    }
                }
            ];
        } else if (show_total_user_count) {
            aggregationRules = [
                userCreatedAtQuery && {
                    $match: userCreatedAtQuery
                },
                filteringQuery && {
                    $match: filteringQuery
                },
                {
                    $lookup: {
                        from: "segpaysubscriptions",
                        localField: "_id",
                        foreignField: "user",
                        as: "segpay_subscriptions"
                    }
                },
                {
                    $match: query
                },
                {
                    $count: 'total_users'
                }
            ];
        } else {
            aggregationRules = [
                userCreatedAtQuery && {
                    $match: userCreatedAtQuery
                },
                filteringQuery && {
                    $match: filteringQuery
                },
                {
                    $sort : { created_at: -1 }
                },
                {
                    $lookup: {
                        from: "segpaysubscriptions",
                        localField: "_id",
                        foreignField: "user",
                        as: "segpay_subscriptions"
                    }
                },
                {
                    $match: query
                },
                {
                    $skip: (page - 1) * limit
                },
                {
                    $limit: limit
                },
                {
                    $lookup: {
                        from: "segpaytransactionrecords",
                        localField: "_id",
                        foreignField: "user",
                        as: "segpay_transaction_records"
                    }
                }
            ];
        }

        return this.aggregate(aggregationRules.filter(rule => Boolean(rule)))
            .exec()
    }
};

module.exports = mongoose.model('User', userSchema);
