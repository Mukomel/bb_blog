const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const Schema = mongoose.Schema;

const artistSchema = new Schema({
    slug: {type: String, required: true, unique: 'This slug is already used'},
    created_at: {type: Date, required: true, default: Date.now},
    name: {type: String, required: true},
    biography: {type: String},
    avatar: {type: String},
    cover: {type: String},
    snapchat_link: {type: String},
    instagram_link: {type: String},
    tumblr_link: {type: String},
    twitter_link: {type: String},
    likes_count: {type: Number, required: true, default: 0},
    views_count: {type: Number, required: true, default: 0},
    posts_count: {type: Number, required: true, default: 0},
    users: [{type: Schema.Types.ObjectId, ref: 'User'}],
    first_post_date: {type: Date},
    schedule: {type: Schema.Types.ObjectId, ref: 'Schedule'},
    wishlist: {type: Schema.Types.ObjectId, ref: 'GroupWishlist'},
    tip_jar_enabled: {type: Boolean, required: true, default: true},
    segpay_package_id: {type: String, required: true},
    segpay_pricepoint_id: {type: String, required: true},
    subscription_price_cents_first_30_days: {type: Number, required: true},
    subscription_price_cents_recurring_30_days: {type: Number, required: true}
});

artistSchema.plugin(uniqueValidator);

const artistSchemaProjection = {users: 0};

const prepareAuthors = (authors, userId) => authors.map(({_id, username, avatar, subscribers, donation_target, biography, wishlist_link}) => ({
    _id,
    username,
    avatar,
    donation_target,
    biography,
    wishlist_link,
    is_subscribed: Boolean(subscribers.find(id => id.equals(userId))),
}));

artistSchema.statics = {
    getArtists() {
        return this.find()
            .exec()
    },

    getArtistBySlug(slug) {
        return this.findOne({slug})
            .populate({ path: 'schedule'})
            .populate({ path: 'wishlist'})
            .select(artistSchemaProjection)
            .exec()
            .then(artist => artist)
    },

    getArtistById(id, withProjection = true) {
        return this.findOne({_id: id})
            .select(withProjection ? artistSchemaProjection : {})
            .exec()
            .then(artist => artist)
    },

    getArtistBySegpayPackageId(packageId) {
        return this.findOne({segpay_package_id: packageId})
            .select(artistSchemaProjection)
            .exec()
            .then(artist => artist)
    },

    getArtistAuthorsBySlug(slug, userId) {
        return this.findOne({slug})
            .populate({ path: 'users', select: 'username avatar subscribers donation_target biography wishlist_link'})
            .lean()
            .exec()
            .then(artist => {
                if (!artist) {
                    throw [];
                } else {
                    return artist;
                }
            })
            .then(artist => prepareAuthors(artist.users, userId))
    }
};

module.exports = mongoose.model('Artist', artistSchema);
