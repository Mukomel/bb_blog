const mongoose = require('mongoose');
const donationTargets = require('../constants/donationTargets');

const Schema = mongoose.Schema;

const roundSchema = new Schema({
    is_active: Boolean,
    created_at: Date,
    [donationTargets.BRANDT]: Number,
    [donationTargets.NASH]: Number,
    [donationTargets.DREW]: Number
});

roundSchema.pre('save', function (next) {
    if (!this.created_at) {
        this.is_active = true;
        this.created_at = new Date;
        this[donationTargets.BRANDT] = 0;
        this[donationTargets.NASH] = 0;
        this[donationTargets.DREW] = 0;
    }
    next();
});

roundSchema.statics = {
    getActiveRound() {
        return this.findOne({is_active: true})
            .exec()
            .then(round => round)
    }
};

module.exports = mongoose.model('Round', roundSchema);