const mongoose = require('mongoose');
const moment = require('moment');

const Schema = mongoose.Schema;

const segpayTransactionRecordSchema = new Schema({
    created_at: Date,
    type: String,
    source: String,
    target: { type: Schema.Types.ObjectId, ref: 'User' },
    tip_jar: { type: Schema.Types.ObjectId, ref: 'TipJar' },
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    post: { type: Schema.Types.ObjectId, ref: 'Post' },
    artist: { type: Schema.Types.ObjectId, ref: 'Artist' },
    amount: Number,
    price: String,
    approved: String,
    name: String,
    email: String,
    trans_guid: String,
    tran_id: String,
    trans_time: String,
    description: String,
    purchase_id: String,
    url_id: String,
    country: String,
});

segpayTransactionRecordSchema.pre('save', function (next) {
    this.created_at = new Date;
    next();
});

segpayTransactionRecordSchema.statics = {
    getTotalPerPeriod({period_first_day, period_last_day, type, artist, author, source}) {
        const query = {
            created_at: {
                $lte: new Date(moment(period_last_day).endOf('day').toISOString()),
                $gte: new Date(moment(period_first_day).startOf('day').toISOString())
            },
            artist,
            type
        };
        if (author) query.target = mongoose.Types.ObjectId(author);
        if (source) query.source = source;

        return this.aggregate([
            {$match: query},
            {$group: {_id: null, amount: {$sum: '$amount'}}}
        ])
            .exec()
            .then(array => array[0] ? array[0].amount : 0)
    },

    getRecordsByDayPerPeriod({period_first_day, period_last_day, type, artist, author, source, limit}) {
        const query = {
            created_at: {
                $lte: new Date(moment(period_last_day).endOf('day').toISOString()),
                $gte: new Date(moment(period_first_day).startOf('day').toISOString())
            },
            artist
        };
        if (author) query.target = mongoose.Types.ObjectId(author);
        if (source) query.source = source;

        return this.aggregate([
            {$match: query},
            {$project: {
                    yearMonthDay: { $dateToString: { format: "%d-%m-%Y", date: "$created_at" } },
                    amount: "$amount",
                    type: '$type',
                    created_at: '$created_at'
            }},
            {$group: {
                    _id: {day: '$yearMonthDay'},
                    date: {$first: "$created_at"},
                    income: {$sum: { $cond: [
                                {$eq: ['$type', 'INCOME']},
                                '$amount',
                                0
                            ]}},
                    expense: {$sum: { $cond: [
                                {$eq: ['$type', 'EXPENSE']},
                                '$amount',
                                0
                            ]}},
            }}
        ])
            .sort({date: 'desc'})
            .limit(limit)
            .exec()
            .then(listOfGroupedTransactions => listOfGroupedTransactions.map(groupedTransactions => ({
                day: groupedTransactions._id.day,
                income: groupedTransactions.income,
                expense: groupedTransactions.expense
            })))
    }
};

module.exports = mongoose.model('SegPayTransactionRecord', segpayTransactionRecordSchema);