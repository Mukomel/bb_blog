const mongoose = require('mongoose');
const mongooseHidden = require('mongoose-hidden')();

const Schema = mongoose.Schema;

const schemaOptions = {
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true
    }
};

const tipJarSchema = new Schema({
    created_at: {type: Date, required: true, default: Date.now},
    artist: {type: Schema.Types.ObjectId, ref: 'Artist'},
    title: {type: Schema.Types.String, required: true},
    description: {type: Schema.Types.String, required: true},
    image: {type: String},
    goal_amount: {type: Number, required: true},
    current_amount: {type: Number, required: true, default: 0},
    is_active: {type: Boolean, required: true, default: true},
    bakers: {type: [{type: Schema.Types.ObjectId, ref: 'User'}], hide: true}
}, schemaOptions);

tipJarSchema.virtual('bakers_count').get(function () {
    return this.bakers.length;
});

tipJarSchema.plugin(mongooseHidden);

tipJarSchema.statics = {
    getById(id, populateArtist = false) {
        return this.findOne({
            _id: id,
            is_active: true
        })
            .populate(populateArtist ? { path: 'artist' } : {})
            .exec()
            .then(jar => jar)
    },

    getActiveArtistJar(artist) {
        return this.findOne({
            artist,
            is_active: true
        })
            .exec()
            .then(jar => jar)
    }
};

module.exports = mongoose.model('TipJar', tipJarSchema);