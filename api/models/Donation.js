const mongoose = require('mongoose');
const moment = require('moment');
const periods = require('../constants/periods');

const Schema = mongoose.Schema;

const donationSchema = new Schema({
    created_at: Date,
    stripe_creation_date: {type: String, required: true},
    charge_id: {type: String, required: true},
    amount: {type: Number, required: true},
    target: {type: String},
    tip_jar: { type: Schema.Types.ObjectId, ref: 'TipJar' },
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    post: { type: Schema.Types.ObjectId, ref: 'Post' }
});

donationSchema.pre('save', function (next) {
    this.created_at = new Date;
    next();
});

donationSchema.statics = {
    getPostsDonations() {
        return this.find({post: {$exists: true}})
            .exec()
            .then(donations => donations)
    },

    getGroupedDonationsByTime(period) {
        let createdAtQuery;
        switch(period) {
            case periods.WEEKLY:
                createdAtQuery = {$gte: new Date(moment().startOf('isoWeek').toISOString())};
                break;

            case periods.MONTHLY:
                createdAtQuery = {$gte: new Date(moment().startOf('month').toISOString())};
                break;

            case periods.ALL_TIME:
                createdAtQuery = {$exists: true};
                break;
        }

        return this.aggregate([
            {$match:
                {
                    created_at: createdAtQuery,
                }
            },
            {$group: {_id: '$target', amount: {$sum: '$amount'}}}
        ])
            .sort({amount: 'desc'})
            .exec()
            .then(donations => donations.map(donation => ({
                target: donation._id,
                amount: donation.amount
            })))
    },

    getDonationsToTargetsByTime(period) {
        let createdAtQuery;
        switch(period) {
            case periods.WEEKLY:
                createdAtQuery = {$gte: new Date(moment().startOf('isoWeek').toISOString())};
                break;

            case periods.MONTHLY:
                createdAtQuery = {$gte: new Date(moment().startOf('month').toISOString())};
                break;

            case periods.ALL_TIME:
                createdAtQuery = {$exists: true};
                break;
        }

        return this.aggregate([
            {$match:
                {
                    created_at: createdAtQuery,
                }
            },
            {$group: {_id: {user: '$user', target: '$target'}, amount: {$sum: '$amount'}}}
        ])
            .sort({amount: 'desc'})
            .exec()
            .then(donations => donations.map(donation => ({
                target: donation._id.target,
                user: donation._id.user,
                amount: donation.amount
            })))
    }
};

module.exports = mongoose.model('Donation', donationSchema);