const mongoose = require('mongoose');
const User = require('./User');

const Schema = mongoose.Schema;

const notificationSchema = new Schema({
    created_at: {type: Date, required: true, default: Date.now},
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    title: {type: String, required: true},
    message: {type: String, required: true},
    is_viewed: {type: Boolean, required: true, default: false}
});

notificationSchema.statics = {
    getUserNotificationsCount(userId) {
        return this.count({user: userId, is_viewed: false})
            .then(count => count)
    },

    getUserNotifications(userId) {
        return this.find({user: userId})
            .exec()
            .then(notifications => notifications)
    }
};

module.exports = mongoose.model('Notification', notificationSchema);