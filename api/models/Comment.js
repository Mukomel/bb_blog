const mongoose = require('mongoose');
const commentTypes = require('../constants/commentTypes');

const Schema = mongoose.Schema;

const commentSchema = new Schema({
    created_at: {type: Date, required: true, default: Date.now},
    type: {type: String, required: true},
    user: {type: Schema.Types.ObjectId, ref: 'User'},
    post: {type: Schema.Types.ObjectId, ref: 'Post'},
    comment: {type: String},
    is_deleted: {type: Boolean, required: true, default: false},
    is_user_deleted: {type: Boolean, required: true, default: false},
    likes: [{type: Schema.Types.ObjectId, ref: 'User'}],
    replies: [{type: Schema.Types.ObjectId, ref: 'Comment'}],
    parent_comment: {type: Schema.Types.ObjectId, ref: 'Comment'}
});

commentSchema.statics = {
    prepareComments(comments, userId) {
        return comments.map(({likes = [], replies, type, ...fields}) => {
            const filteredReplies = replies.filter(reply => !reply.is_deleted && !reply.is_user_deleted);
            return {
                ...fields,
                type,
                likes_count: likes.length,
                is_liked: Boolean(likes.find(id => id.equals(userId))),
                replies: (type === commentTypes.COMMENT) ? this.prepareComments(filteredReplies, userId) : filteredReplies
            }
        })
    },

    getById(commentId) {
        return this.findOne({_id: commentId})
            .exec()
            .then(comment => comment)
    },

    getPostComments(postId, userId, commentsLimit = 0) {
        return this.find({
            $and: [
                {post: postId},
                {type: commentTypes.COMMENT},
                {$or: [
                    {is_deleted: false, is_user_deleted: false},
                    {"replies.0": { $exists: true }}
                ]}
            ]
        })
            .limit(commentsLimit)
            .sort({created_at: 'desc'})
            .populate({ path: 'user', select: 'username avatar'})
            .populate({ path: 'replies', populate: { path: 'user', model: 'User', select: 'username avatar'}})
            .lean()
            .exec()
            .then(comments => this.prepareComments(comments, userId))
    },

    getPostCommentsCount(postId) {
        return this.count({
            $and: [
                {post: postId},
                {type: commentTypes.COMMENT},
                {$or: [
                    {is_deleted: false, is_user_deleted: false},
                    {"replies.0": { $exists: true }}
                ]}
            ]
        })
            .then(count => count)
    }
};

module.exports = mongoose.model('Comment', commentSchema);