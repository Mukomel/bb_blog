const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const dayFields = {
    description: {type: String, required: true, default: ''}
};

const scheduleSchema = new Schema({
    monday: dayFields,
    tuesday: dayFields,
    wednesday: dayFields,
    thursday: dayFields,
    friday: dayFields,
    saturday: dayFields,
    sunday: dayFields
});

scheduleSchema.statics = {};

module.exports = mongoose.model('Schedule', scheduleSchema);