const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const groupWishlistSchema = new Schema({
    title: {type: String, required: true},
    description: {type: String},
    link: {type: String, required: true},
    is_enabled: {type: Boolean, required: true}
});

groupWishlistSchema.statics = {};

module.exports = mongoose.model('GroupWishlist', groupWishlistSchema);
