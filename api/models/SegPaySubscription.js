const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const segPaySubscriptionSchema = new Schema({
    created_at: {type: Date, required: true, default: Date.now},
    canceled_at: {type: Date},
    price: String,
    is_canceled: {type: Boolean, required: true, default: false},
    is_pending_cancellation: {type: Boolean, required: true, default: false},
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    approved: String,
    name: String,
    email: String,
    trans_guid: String,
    tran_id: String,
    trans_time: String,
    description: String,
    purchase_id: String,
    url_id: String,
    country: String,
    is_old_stripe_subscription: {type: Boolean, required: true, default: false},
    with_stripe_coupon: {type: Boolean, required: true, default: false},
    promo_code: String,
    artist: {type: Schema.Types.ObjectId, ref: 'Artist'}
});

segPaySubscriptionSchema.statics = {
    getById(id) {
        return this.findOne({_id: id})
            .populate({path: 'user'})
            .exec()
            .then(subscription => subscription)
    },

    getByPurchaseId(purchaseId) {
        return this.findOne({purchase_id: purchaseId})
            .exec()
            .then(subscription => subscription)
    },

    getUserSubscriptions(userId) {
        return this.find({user: userId})
            .populate({path: 'user', select: 'snapchat_username'})
            .exec()
            .then(subscriptions => subscriptions)
    },

    getUserSubscriptionToArtist(userId, artistId) {
        return this.findOne({user: userId, artist: artistId})
            .sort({created_at: -1})
            .exec()
            .then(subscription => subscription)
    },

    getActiveSubscriptions(artistId) {
        return this.aggregate([
            {$match:
                    {
                        is_canceled: false,
                        artist: artistId ? artistId : {$exists: true}
                    }
            },
            {$group: {
                        _id: {user: "$user"},
                        subscriptions: {$push: {
                            artist: "$artist",
                            created_at: "$created_at",
                            with_stripe_coupon: "$with_stripe_coupon",
                            is_old_stripe_subscription: "$is_old_stripe_subscription"
                        }}
                    }
            }
        ])
            .exec()
            .then(subscriptions => subscriptions)
    },

    getUserOngoingSubscriptions(userId) {
        return this.find({
            is_canceled: false,
            user: userId
        })
            .exec()
            .then(subscriptions => subscriptions)
    },

    getUserSubscriptionWithOCToken(userId) {
        return this.findOne({
            user: userId,
            purchase_id: {$exists: true}
        })
            .sort({created_at: -1})
            .exec()
            .then(subscription => subscription)
    },

    getArtistActiveSubscriptionsSummaryByDay(artist) {
        return this.aggregate([
            {
                $match: {
                    artist,
                    is_canceled: false,
                    is_old_stripe_subscription: false,
                    amount: {$exists: true}
                }
            },
            {
                $project: {
                    yearMonthDay: { $dateToString: { format: "%d-%m-%Y", date: "$created_at" } },
                    amount: "$amount",
                    created_at: '$created_at'
                }
            },
            {
                $group: {
                    _id: {day: '$yearMonthDay'},
                    date: {$first: "$created_at"},
                    amount: {$sum: '$amount'}
                }
            }
        ])
            .exec()
            .then(listOfGroupedSummaries => listOfGroupedSummaries.map(groupedSummaries => ({
                created_at: groupedSummaries.date,
                amount: groupedSummaries.amount
            })))
    }
};

module.exports = mongoose.model('SegPaySubscription', segPaySubscriptionSchema);
