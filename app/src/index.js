import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import ErrorBoundary from './base-components/ErrorBoundary/ErrorBoundary';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import store from './store';
import ensureEnvVariablesExists from './helpers/ensureEnvVariablesExists';
import config from './config';

//global styles
import 'normalize.css/normalize.css';
import './index.scss';

ensureEnvVariablesExists();

ReactDOM.render(
    <Provider store={store}>
        <ErrorBoundary>
            <Router>
                <App/>
            </Router>
        </ErrorBoundary>
    </Provider>,
    document.getElementById('root'));
registerServiceWorker();
// window.Intercom("boot", {
//     app_id: "vtmwj4v9"
// });
