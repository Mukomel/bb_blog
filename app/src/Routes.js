import React from 'react';
import AuthenticatedUserRoute from './base-components/AuthenticatedUserRoute/AuthenticatedUserRoute';
import {Route, Switch, Redirect} from 'react-router-dom';
import ErrorBoundary from './base-components/ErrorBoundary/ErrorBoundary';
import LandingPage from './features/landing/containers/LandingPage/LandingPage';
import SigninComponent from './features/auth/containers/SignInModal/SignInModal';
import SignupComponent from './features/auth/containers/SignUpModal/SignUpModal';
import ResetPasswordModal from './features/auth/containers/ResetPasswordModal/ResetPasswordModal';
import ActivateAccountModal from './features/auth/containers/ActivateAccountModal/ActivateAccountModal';
import SubscribersPage from './features/subscribe/containers/SubscribersPage/SubscribersPage';
import UserSubscriptionsPage from './features/user/containers/UserProfilePage/UserProfilePage';
import ArtistDashboard from './features/dashboard/containers/ArtistDashboard/ArtistDashboard';
import ArtistProfilePage from './features/artist/containers/ArtistProfilePage/ArtistProfilePage';
import SupportPage from './features/support/containers/SupportPage/SupportPage';
import TermsOfServicePage from './features/termsOfService/containers/TermsOfServicePage/TermsOfServicePage';
import CopyrightPolicyPage from './features/copyrightPolicy/containers/CopyrightPolicyPage/CopyrightPolicyPage';
import PrivacyPolicyPage from './features/privacyPolicy/containers/PrivacyPolicyPage/PrivacyPolicyPage';
import ArtistScheduleMobilePage from './features/artist/containers/ArtistScheduleMobilePage/ArtistScheduleMobilePage'

export default ({childProps}) => (
    <ErrorBoundary>
        <Switch>
            <Route exact path='/' component={LandingPage}>
                {/*<Route exact path='signin' component={SigninComponent} />*/}
            </Route>
            <Route exact path='/copyright-policy' component={CopyrightPolicyPage}/>
            <Route exact path='/privacy-policy' component={PrivacyPolicyPage}/>
            <Route exact path='/terms-of-service' component={TermsOfServicePage}/>
            <Route exact path='/signin' component={SigninComponent}/>
            <Route exact path='/signup' component={SignupComponent}/>
            <Route exact path='/reset-password/:reset_password_token' component={ResetPasswordModal}/>
            <Route exact path='/activate-account/:activation_token' component={ActivateAccountModal}/>
            <AuthenticatedUserRoute exact path='/dashboard/subscriptions' component={UserSubscriptionsPage} componentProps={childProps}/>
            <Route path='/dashboard' render={routeProps => <ArtistDashboard {...routeProps} childProps={childProps}/>}/>
            <Route exact path='/:slug' component={ArtistProfilePage}/>
            <Redirect from='/artists/brandts-boys' to='/brandtandnash'/>
            <Route exact path='/:slug/schedule' component={ArtistScheduleMobilePage}/>
        </Switch>
    </ErrorBoundary>
)
