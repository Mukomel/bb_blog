import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import debounce from 'debounce';
import Routes from './Routes';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import ErrorBoundary from './base-components/ErrorBoundary/ErrorBoundary';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import {yellow, red} from '@material-ui/core/colors'
import GlobalModalContainer from './features/ui/containers/GlobalModalContainer/GlobalModalContainer';
import MobileSidebar from './features/ui/containers/MobileSidebar/MobileSidebar';
import SuccessSnackbar from './base-components/SuccessSnackbar/SuccessSnackbar'
import {checkAuth} from './features/auth/actions';
import {windowSizeChanged} from './features/ui/actions';
import {getCookie, setCookie} from './helpers/auth';
import 'react-date-range/dist/styles.css';
import 'react-date-range/dist/theme/default.css';

import * as styles from './App.module.scss';
const muiTheme = createMuiTheme({
    palette: {
        primary: {
            light: '#00caff',
            dark: '#008cb1',
            main: '#00C1F3',
            contrastText: '#ffffff',
        },
        accent: yellow,
        error: red,
        type: 'light'
    }
});

const WINDOW_RESIZE_DEBOUNCE_TIME = 300;

class App extends Component {

    componentWillMount() {
        this.props.checkAuth();
    }

    componentDidMount() {
        this.handleWindowResize();
        window.addEventListener('resize', debounce(this.handleWindowResize, WINDOW_RESIZE_DEBOUNCE_TIME));
    }

    componentDidUpdate(prevProps) {
        if (this.props.location !== prevProps.location) {
            window.scrollTo(0, 0)
        }
    }

    handleWindowResize = () => {
        this.props.windowSizeChanged(window.innerWidth);
    };

    onAgree = () => {
        setCookie('agree', true);
        window.location.reload();
    };

    render() {
        const {isLoggedIn, isFetchingUser, isAuthChecked, user} = this.props;
        const childProps = {isLoggedIn, isFetchingUser, user};

        if (!isAuthChecked) {
            return null;
        }
        const isAgreed = getCookie('agree');

        return (
            <MuiThemeProvider theme={muiTheme}>

                <div className={styles.routeContainer}>
                    <Routes childProps={childProps}/>
                </div>
                <ErrorBoundary>
                    <GlobalModalContainer />
                </ErrorBoundary>
                <MobileSidebar />
                <SuccessSnackbar />

            </MuiThemeProvider>
        )
    }
}

export default withRouter(connect(
    (state) => ({
        isLoggedIn: state.auth.isLoggedIn,
        user: state.auth.user,
        isFetchingUser: state.auth.isFetchingUser,
        isAuthChecked: state.auth.isAuthChecked
    }),
    (dispatch) => ({
        checkAuth: () => dispatch(checkAuth()),
        windowSizeChanged: (windowWidth) => dispatch(windowSizeChanged(windowWidth))
    })
)(App));
