import {createStore, applyMiddleware, combineReducers, compose} from 'redux'
import createSagaMiddleware from 'redux-saga'
import { fork } from 'redux-saga/effects'
import { all } from 'redux-saga/effects'
import { reducer as formReducer } from 'redux-form';
import uiReducer from './features/ui/reducer';
import subscribeReducer from './features/subscribe/reducer';
import landingReducer from './features/landing/reducer';
import donationReducer from './features/donation/reducer';
import postsReducer from './features/posts/reducer';
import authReducer from './features/auth/reducer';
import artistReducer from './features/artist/reducer';
import subscribeSagas from './features/subscribe/sagas';
import landingSagas from './features/landing/sagas';
import authSagas from './features/auth/sagas';
import donationSagas from './features/donation/sagas';
import postSagas from './features/posts/sagas';
import artistSagas from './features/artist/sagas';
const sagaMiddleware = createSagaMiddleware();

let initialState = {};

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const Store = createStore(
    combineReducers({
        form: formReducer,
        ui: uiReducer,
        subscribe: subscribeReducer,
        landing: landingReducer,
        donation: donationReducer,
        auth: authReducer,
        posts: postsReducer,
        artist: artistReducer
    }),
    initialState,
    composeEnhancers(
        applyMiddleware(sagaMiddleware)
    )
);

function* sagas() {
    yield all([fork(subscribeSagas), fork(landingSagas), fork(authSagas), fork(donationSagas), fork(postSagas),
        fork(artistSagas)]);
}
sagaMiddleware.run(sagas).done.catch(error => {
    throw error;
});

export default Store;

