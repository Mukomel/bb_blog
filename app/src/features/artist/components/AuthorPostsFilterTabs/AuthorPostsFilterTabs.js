import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import styles from './AuthorPostsFilterTabs.module.scss';

const AuthorPostsFilterTabs = ({tabs, activeTab, onChange}) => {
    return (
        <div className={styles.container}>
            {
                tabs.map(({value, label}) => (
                    <div
                        key={value}
                        className={cn(styles.tab, {[styles.active]: value === activeTab, [styles.noValue]: !value})}
                        onClick={() => onChange(value)}
                    >
                        {label}
                    </div>
                ))
            }
        </div>
    )
};

AuthorPostsFilterTabs.propTypes = {
    tabs: PropTypes.arrayOf(PropTypes.shape({
        value: PropTypes.string,
        label: PropTypes.string
    })),
    activeTab: PropTypes.string,
    onChange: PropTypes.func.isRequired
};

export default AuthorPostsFilterTabs;
