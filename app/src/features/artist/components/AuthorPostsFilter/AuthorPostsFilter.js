import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import {donationTargets} from '../../../donation/constants';
import AuthorPostsFilterTabs from '../AuthorPostsFilterTabs/AuthorPostsFilterTabs';
import giftImage from '../../../../assets/images/gift_white.svg';
import arrowsImage from '../../../../assets/images/arrows.svg';

import styles from './AuthorPostsFilter.module.scss';

export default class AuthorPostsFilter extends React.Component {
    state = { isOpen: false };
    handleTabChange = (value) => {
        const {artistAuthors, onChange}  = this.props;

        onChange({
            value,
            ...artistAuthors.find(author => author._id === value)
        })
    };

    handleArrowButtonClick = () => this.setState(prevState => ({ isOpen: !prevState.isOpen }))

    render() {

        const arrowImageClasses = cn(styles.arrowsImage, { [styles.active]: this.state.isOpen });
        const authorClasses = cn(styles.authorBlock, { [styles.active]: this.state.isOpen });
        const {filter: {value, avatar, biography, wishlist_link}, className, artistAuthors} = this.props;

        const tabs = [
            {value: null, label: 'All Posts'}
        ];

        artistAuthors.forEach(author => {
            tabs.push({value: author._id, label: author.username});
        });

        return (
            <div className={cn(styles.container, className)}>
                <AuthorPostsFilterTabs tabs={tabs} activeTab={value} onChange={this.handleTabChange}/>
                {
                    value
                    && <div className={authorClasses}>
                        <a href={wishlist_link} target='_blank' className={styles.wishlistLink}>
                            Wishlist <img src={giftImage} className={styles.giftImage}/>
                        </a>
                        <img src={avatar} className={styles.avatar}/>
                        <div className={styles.biography}>{biography}</div>
                        <img
                            src={arrowsImage}
                            className={arrowImageClasses}
                            onClick={this.handleArrowButtonClick}/>
                    </div>
                }
            </div>
        )
    }
}

AuthorPostsFilter.propTypes = {
    className: PropTypes.string,
    artistAuthors: PropTypes.array,
    filter: PropTypes.shape({
        value: PropTypes.string
    }),
    onChange: PropTypes.func.isRequired
};
