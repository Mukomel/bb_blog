import React from 'react';
import {array, bool, number, func} from 'prop-types';
import moment from 'moment';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper/Paper';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop'
import {roundToExactly} from '../../../../helpers/roundTo'

import * as styles from './ArtistStatsMembersTable.module.scss'

const columnData = [
    {id: 'email', numeric: false, label: 'Email'},
    {id: 'username', numeric: true, label: 'Username'},
    {id: 'status', numeric: true, label: 'Status'},
    {id: 'snapchat', numeric: true, label: 'Snapchat'},
    {id: 'tips', numeric: true, label: 'Tips, $'},
    {id: 'subs', numeric: true, label: 'Subs, $'},
    {id: 'promo_code', numeric: true, label: 'Promocode'},
    {id: 'date', numeric: true, label: 'Reg. Date'},
    {id: 'cancelDate', numeric: true, label: 'Cancel Date'},
];

export default class ArtistStatsMembersTable extends React.Component {
    static propTypes = {
        data: array.isRequired,
        changePage: func.isRequired,
        isFetchingStats: bool.isRequired,
        total: number,
    };

    state = {
        rowsPerPage: 50
    };

    handleChangePage = (event, page) => {
        this.setState({page});
        this.props.changePage(page + 1);
    };

    render() {
        const {data, isFetchingStats, total, currentPage} = this.props;
        const {rowsPerPage} = this.state;

        return (
            <div className={styles.membersContainer}>
                <Paper>
                    {
                        isFetchingStats && <SpinnerWithBackdrop/>
                    }
                    <div>
                        <Table aria-labelledby="tableTitle">
                            <TableHead className={styles.tableHead}>
                                <TableRow>
                                    {
                                        columnData.map(column => (
                                            <TableCell
                                                key={column.id}
                                                numeric={column.numeric}
                                                className={styles.tableHeadCell}
                                            >
                                                {column.label}
                                            </TableCell>
                                        ))
                                    }
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    data && data.map(item => {
                                        return (
                                            <TableRow
                                                hover
                                                tabIndex={-1}
                                                key={item._id}
                                            >
                                                <TableCell component="th" scope="row" padding="none">
                                                    {item.email}
                                                </TableCell>
                                                <TableCell numeric>{item.username}</TableCell>
                                                <TableCell numeric>{item.status}</TableCell>
                                                <TableCell numeric>
                                                    {item.snapchat_username ? item.snapchat_username : '-'}
                                                </TableCell>
                                                <TableCell numeric>
                                                    {roundToExactly(item.total_tips_income / 100, 2)} $
                                                </TableCell>
                                                <TableCell numeric>
                                                    {roundToExactly(item.total_subscription_income / 100, 2)} $
                                                </TableCell>
                                                <TableCell numeric>{item.promo_code}</TableCell>
                                                <TableCell numeric>{moment(item.created_at).format('L')}</TableCell>
                                                <TableCell numeric>
                                                    {item.canceled_at ? moment(item.canceled_at).format('L') : null}
                                                </TableCell>
                                            </TableRow>
                                        );
                                    })}
                            </TableBody>
                        </Table>
                    </div>
                    <TablePagination
                        rowsPerPageOptions={[50]}
                        component="div"
                        count={total}
                        rowsPerPage={rowsPerPage}
                        page={currentPage - 1}
                        backIconButtonProps={{
                            'aria-label': 'Previous Page',
                        }}
                        nextIconButtonProps={{
                            'aria-label': 'Next Page',
                        }}
                        onChangePage={this.handleChangePage}
                    />
                </Paper>
            </div>
        )
    }
}
