import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import ArtistPostComment from '../ArtistPostComments/ArtistPostComment';
import AddPostCommentForm from '../../../posts/components/AddPostCommentForm/AddPostCommentForm';
import ErrorBoundary from '../../../../base-components/ErrorBoundary/ErrorBoundary';
import Switch from '../../../../base-components/Switch/Switch'
import {mediaTypes, videoViewDuration} from '../../../posts/constants';
import {layoutTypes} from "../../../ui/constants";
import lockImg from '../../../../assets/images/lock-white.svg';
import PostImage from '../../../../base-components/PostImage/PostImage';
import PostVideo from '../../../../base-components/PostVideo/PostVideo';
import PostHeader from '../../../../base-components/PostHeader/PostHeader';
import PostContent from '../../../../base-components/PostContent/PostContent';
import {userRoles} from '../../../auth/constants';

import styles from './ArtistPost.module.scss';

export default class ArtistPost extends React.Component {
    state = {
        isAddCommentFormVisible: false,
        isVideoPostTitleVisible: false
    };

    isMediaTypeVideo = null;
    isMediaTypeImage = null;

    handlePlayVideo = () => {
        this.setState({
            isVideoPostTitleVisible: true
        })
    };

    handlePauseVideo = () => {
        this.setState({
            isVideoPostTitleVisible: false
        })
    };
    renderMedia = (media, title) => {
        const {
            onSubscribeClick,
            onUnlockClick,
            isBlockedInsidePeriod,
            blockedLabel,
            onImageClick,
            topFansList,
            artistProfile
        } = this.props;
        // set types for later use in PostHeader
        this.isMediaTypeImage = media.path && (media.type === mediaTypes.IMAGE);
        this.isMediaTypeVideo = media.path && (media.type === mediaTypes.VIDEO);

        if (this.isMediaTypeImage) {
            return (
                <PostImage imagePath={media.path} onImageClick={() => onImageClick(media.path)}/>
            )
        }

        if (this.isMediaTypeVideo) {
            return (
                <PostVideo
                    videoPath={media.path}
                    onPlayVideo={this.handlePlayVideo}
                    onPauseVideo={this.handlePauseVideo}
                    title={title}
                    isTitleActive={this.state.isVideoPostTitleVisible}
                    getRef={node => this.videoNode = node}
                />
            );
        }

        if (!media.path) {
            if (isBlockedInsidePeriod) {
                return (
                    <div className={styles.subscribeContainer}>
                        <img src={lockImg} className={styles.lockImage} />
                        <div className={styles.onlyForSubscribers}>{blockedLabel}</div>
                        <button className={styles.subscribeButton} onClick={onUnlockClick}>BUY</button>
                    </div>
                )
            } else {
                return (
                    <div className={styles.subscribeContainer}>
                        <img src={lockImg} className={styles.lockImage} />
                        <button className={styles.subscribeButton} onClick={onSubscribeClick}>Subscribe (${artistProfile.subscription_price_cents_first_30_days/100}/<small>mo</small>)</button>
                    </div>
                )
            }
        }
    };

    componentDidMount = () => {
        if (this.videoNode) {
            this.videoNode.addEventListener('contextmenu', this.handleVideoContextMenu);
            this.videoNode.addEventListener('timeupdate', this.handleTimeUpdate);
        }
    };

    componentWillUnmount = () => {
        if (this.videoNode) {
            this.videoNode.removeEventListener('contextmenu', this.handleVideoContextMenu);
            this.videoNode.removeEventListener('timeupdate', this.handleTimeUpdate);
        }
    };

    handleVideoContextMenu = (e) => {
        e.preventDefault();
        return false;
    };

    handleTimeUpdate = (e) => {
        const {post: {_id} } = this.props;
        if (e.target.currentTime > videoViewDuration) {
            this.props.trackVideoView(_id);
            this.videoNode.removeEventListener('timeupdate', this.handleTimeUpdate);
        }
    };

    handleTip = (donationAmount) => {
        const {onTip, post: {user, _id}} = this.props;
        onTip({
            donationAmount,
            donationTargetId: user._id,
            donationTargetName: user.username,
            postId: _id
        });
    };

    handleClickAddCommentButton = () => {
        this.setState({
            isAddCommentFormVisible: true
        })
    };

    handleClickLike = () => {
        const { postLike, isLoggedIn, post: {is_liked, _id} } = this.props;
        if (isLoggedIn && !is_liked) {
            postLike(_id);
        }
    };

    handleShowMoreCommentsClick = () => {
        const {onShowMoreCommentsClick, post} = this.props;
        this.setState({
            isAllCommentsVisible: true
        });
        onShowMoreCommentsClick(post._id);
    };

    saveFormData = (value, reset) => {
        const { createPostComment, post: { _id } } = this.props;
        if (value.comment && value.comment.trim().length !== 0) {
            createPostComment(value.comment, _id);
        }
        this.setState({
            isAddCommentFormVisible: false
        });
        reset();
    };

    onDeleteComment = (id, postId, parentCommentId) => {
        const { deleteComment } = this.props;
        const result = window.confirm('Are you sure you want to delete this comment?');
        if (result) {
            deleteComment(id, postId, parentCommentId);
        }
    };

    checkIsUserSubscribe() {
        const { artistAuthors, post: {user} } = this.props;
        const artistSubscriber = artistAuthors.find(author => author._id === user._id);
        if (artistSubscriber) {
            return artistSubscriber.is_subscribed;
        }
        return false
    }

    handleSwitch = (isChecked) => {
        const { subscribeArtist, unsubscribeArtist, post: { user }} = this.props;
        if (isChecked) {
            subscribeArtist(user._id)
        } else {
            unsubscribeArtist(user._id)
        }
    };

    handleHideCommentsClick = () => {
        this.setState({
            isAllCommentsVisible: false
        })
    };

    render() {
        const {
            currentUser,
            isLoggedIn,
            onSubscribeClick,
            className,
            commentLike,
            replyComment,
            editComment,
            layoutType,
            topFansList,
            post: {
                _id,
                title,
                body,
                media,
                user,
                created_at,
                likes_count,
                views,
                comments_count,
                has_more_comments,
                comments,
                is_liked,
                artist
            }
        } = this.props;

        const isAdminOrAuthor = isLoggedIn && currentUser && ((currentUser.role === userRoles.ADMIN) || (currentUser.role === userRoles.AUTHOR));
        const isUserCanAddComment = isAdminOrAuthor || (currentUser && currentUser.artists_subscriptions.indexOf(artist) > -1);
        const mobileOrTabletLayout = (layoutType === layoutTypes.MOBILE) || (layoutType === layoutTypes.TABLET);
        const {isAllCommentsVisible} = this.state;

        return (
            <div className={cn(styles.container, className)}>
                {
                    media && this.renderMedia(media, title)
                }
                <PostHeader
                    title={!this.isMediaTypeVideo ? title : ''}
                    username={user.username}
                    avatar={user.avatar}
                    onTip={this.handleTip}>
                    {
                        isLoggedIn && currentUser ?
                            <div className={styles.switchNotification}>
                                <Switch
                                    onSwitch={this.handleSwitch}
                                    defaultChecked={this.checkIsUserSubscribe()}/>
                            </div> : null
                    }
                </PostHeader>
                <PostContent
                    postDate={created_at}
                    body={body}
                    onLike={this.handleClickLike}
                    isPostLiked={is_liked}
                    likesCount={likes_count}
                    views={views}
                >
                    {
                        !mobileOrTabletLayout && has_more_comments
                        && <div
                            className={styles.showCommentsLink}
                            onClick={this.handleShowMoreCommentsClick}
                        >
                            Show {comments_count} comments
                        </div>
                    }
                    {
                        <ErrorBoundary>
                            <React.Fragment>
                                {
                                    mobileOrTabletLayout &&
                                    <React.Fragment>
                                        {
                                            isAllCommentsVisible ?
                                                <div
                                                    className={styles.showCommentsLink}
                                                    onClick={this.handleHideCommentsClick}>
                                                    Hide comments
                                                </div> :
                                                <div
                                                    className={styles.showCommentsLink}
                                                    onClick={this.handleShowMoreCommentsClick}>
                                                    Show {comments_count} {comments_count <= 1 ? 'comment' : 'comments'}
                                                </div>
                                        }
                                    </React.Fragment>
                                }
                                {
                                    (!mobileOrTabletLayout || isAllCommentsVisible) &&
                                    <React.Fragment>
                                        <div className={styles.commentsContainer}>
                                            {
                                                comments.map(item => {
                                                        const isUserOrCommentDeleted = item.is_user_deleted || item.is_deleted;
                                                        const showDeleteButton = (currentUser && item.user && (item.user._id === currentUser._id) && !isUserOrCommentDeleted) || (currentUser && currentUser.role === userRoles.ADMIN);
                                                        const showEditButton = (currentUser && item.user && (item.user._id === currentUser._id) && !isUserOrCommentDeleted);
                                                        return (
                                                            <ArtistPostComment
                                                                type={item.type}
                                                                isUserOrCommentDeleted={
                                                                    isUserOrCommentDeleted
                                                                }
                                                                isLoggedIn={isLoggedIn}
                                                                likesCount={item.likes_count}
                                                                key={item._id}
                                                                id={item._id}
                                                                isLiked={item.is_liked}
                                                                postId={item.post}
                                                                comment={item.comment}
                                                                commentLike={commentLike}
                                                                createdData={item.created_at}
                                                                {...item.user}
                                                                deleteComment={this.onDeleteComment}
                                                                editComment={editComment}
                                                                showDeleteBtn={showDeleteButton}
                                                                showEditBtn={showEditButton}
                                                                showReply={isUserCanAddComment}
                                                                replyComment={replyComment}
                                                                topFansList={topFansList}
                                                            >
                                                                {
                                                                    item.replies.length > 0 ?
                                                                        item.replies.map(reply => {
                                                                            const isUserOrCommentDeleted = reply.is_user_deleted || reply.is_deleted;
                                                                            const showDeleteButton = (currentUser && reply.user && (reply.user._id === currentUser._id) && !isUserOrCommentDeleted) || (currentUser && currentUser.role === userRoles.ADMIN);
                                                                            const showEditButton = (currentUser && reply.user && (reply.user._id === currentUser._id) && !isUserOrCommentDeleted);
                                                                            return (
                                                                                <ArtistPostComment
                                                                                    isLoggedIn={isLoggedIn}
                                                                                    postId={item.post}
                                                                                    type={reply.type}
                                                                                    likesCount={reply.likes_count}
                                                                                    key={reply._id}
                                                                                    id={reply._id}
                                                                                    isLiked={reply.is_liked}
                                                                                    comment={reply.comment}
                                                                                    parentCommentId={reply.parent_comment}
                                                                                    commentLike={commentLike}
                                                                                    createdData={reply.created_at}
                                                                                    {...reply.user}
                                                                                    deleteComment={this.onDeleteComment}
                                                                                    editComment={editComment}
                                                                                    showDeleteBtn={showDeleteButton}
                                                                                    showEditBtn={showEditButton}
                                                                                    replyComment={replyComment}
                                                                                    topFansList={topFansList}
                                                                                />
                                                                            )
                                                                        }) : null
                                                                }
                                                            </ArtistPostComment>
                                                        )
                                                    }
                                                )
                                            }
                                            {
                                                isUserCanAddComment ?
                                                    this.state.isAddCommentFormVisible ?
                                                        <AddPostCommentForm
                                                            form={"AddPostCommentForm" + _id}
                                                            saveData={this.saveFormData}
                                                        />
                                                        : <div className={cn([styles.addCommentButtonContainer, {
                                                            [styles.addCommentButtonContainerBorder]: comments_count > 0
                                                        }])}
                                                               onClick={this.handleClickAddCommentButton}>
                                                            <button className={styles.addCommentButton}>
                                                                Add comment
                                                            </button>
                                                        </div>
                                                    : <div className={styles.subscribeComment}><span onClick={onSubscribeClick}>Subscribe</span> to comment </div>
                                            }
                                        </div>
                                    </React.Fragment>
                                }
                            </React.Fragment>
                        </ErrorBoundary>
                    }
                </PostContent>
            </div>
        )
    }
}

ArtistPost.propTypes = {
    className: PropTypes.string,
    post: PropTypes.shape({
        title: PropTypes.string.isRequired,
        body: PropTypes.string,
        created_at: PropTypes.string,
        media: PropTypes.shape({
            path: PropTypes.string,
            type: PropTypes.oneOf([mediaTypes.IMAGE, mediaTypes.VIDEO])
        }),
        user: PropTypes.shape({
            username: PropTypes.string,
            avatar: PropTypes.string,
            donation_target: PropTypes.string
        }),
        likes_count: PropTypes.number,
        is_liked: PropTypes.bool,
        comments: PropTypes.array,
        comment_count: PropTypes.number,
        has_more_comments: PropTypes.bool,
    }),
    isBlockedInsidePeriod: PropTypes.bool,
    isLoggedIn: PropTypes.bool,
    blockedLabel: PropTypes.string,
    onUnlockClick: PropTypes.func,
    onSubscribeClick: PropTypes.func,
    onShowMoreCommentsClick: PropTypes.func,
    onTip: PropTypes.func.isRequired,
    onImageClick: PropTypes.func.isRequired,
    createPostComment: PropTypes.func.isRequired,
    deleteComment: PropTypes.func.isRequired,
    editComment: PropTypes.func.isRequired,
    postLike: PropTypes.func.isRequired,
    artistAuthors: PropTypes.array.isRequired,
    replyComment: PropTypes.func.isRequired,
    commentLike: PropTypes.func.isRequired
};
