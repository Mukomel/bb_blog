import React from 'react';
import PropTypes from 'prop-types';
import {Field, reduxForm} from 'redux-form';
import {TextField, RadioGroup} from 'redux-form-material-ui';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import RadioButton from '@material-ui/core/Radio';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import RichHtmlEditor from '../../../../base-components/RichHtmlEditor/RichHtmlEditor';

import * as styles from './NotificationsForm.module.scss';

class NotificationsForm extends React.Component {
    static propTypes = {
        onSubmit: PropTypes.func.isRequired
    };

    onChange = (value) => { this.setState({value}) };

    handleFormSubmit = (values) => {
        const {onSubmit, reset} = this.props;
        onSubmit(values);
        reset();
    };

    renderCheckbox = ({input, label, meta: { error, valid }}) => (
        <React.Fragment>
            {
                !valid ? <p className={styles.error}>{ error} </p> : null
            }
            <FormControlLabel
                control={
                    <Checkbox
                        checked={!!input.value}
                        onChange={input.onChange}
                        color="primary"
                    />
                }
                label={label}
            />
        </React.Fragment>
    );

    render() {
        const {handleSubmit} = this.props;
        return (
            <form className={styles.form} onSubmit={handleSubmit(this.handleFormSubmit)}>
                <div>
                    <Field
                        name="send_emails"
                        component={this.renderCheckbox}
                        label="Emails"/>
                </div>
                <div>
                    <Field
                        name="send_notifications"
                        component={this.renderCheckbox}
                        label="Notifications"/>
                </div>
                <Field
                    name='title'
                    label='Subject'
                    fullWidth
                    component={TextField}
                    margin='dense'
                    helperText=' '
                />
                <Field
                    name="html"
                    component={RichHtmlEditor}
                    placeholder="Message"
                    errorClassName={styles.error}
                    editorClassName={styles.formEditor}
                />
                <Field name="target" component={RadioGroup} className={styles.radioGroup}>
                    <FormControlLabel
                        value="ALL_FANS"
                        control={<RadioButton color="primary"/>}
                        label="All fans"/>
                    <FormControlLabel
                        value="ACTIVE_SUBSCRIBERS"
                        control={<RadioButton color="primary"/>}
                        label="Active Subscribers"/>
                    <FormControlLabel
                        value="CANCELED_SUBSCRIBERS"
                        control={<RadioButton color="primary"/>}
                        label="Canceled Subscribers"/>
                    <FormControlLabel
                        value="REGISTERED_USERS"
                        control={<RadioButton color="primary"/>}
                        label="Registered"/>
                </Field>
                <div>
                    <Field
                        name="with_promocode"
                        component={this.renderCheckbox}
                        label="With Promo Code Only"/>
                </div>
                <div>
                    <Field
                        name="august_promocoders"
                        component={this.renderCheckbox}
                        label="August Promocoders"/>
                </div>
                <div>
                    <Field
                        name="without_promocode"
                        component={this.renderCheckbox}
                        label="Without Promo Code Only"/>
                </div>
                <Button
                    variant="raised"
                    color="primary"
                    type="submit"
                >
                    Send
                </Button>
            </form>
        )
    }
}

NotificationsForm.propTypes = {
    onSubmit: PropTypes.func.isRequired
};

export default reduxForm({
    form: 'NotificationForm',
    enableReinitialize: true,
    validate: (values) => {
        const errors = {};

        if (!values.send_emails && !values.send_notifications) {
            errors.send_emails = 'At least one checkbox should be selected';
        }

        if (!values.title) {
            errors.title = 'Required';
        }
        if (!values.html) {
            errors.html = 'Required';
        }

        return errors;
    }
})(NotificationsForm)
