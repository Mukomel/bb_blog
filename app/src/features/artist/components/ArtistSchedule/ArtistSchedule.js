import React, {Component} from 'react';
import {shape, string, func} from 'prop-types';
import cn from 'classnames';
import calendarIcon from '../../../../assets/images/Calendar.svg'
import newIcon from '../../../../assets/images/New Icon.svg'
import styles from './ArtistSchedule.module.scss'
import ArtistScheduleList from '../ArtistScheduleList/ArtistScheduleList';

export default class ArtistSchedule extends Component {
    static propTypes = {
        className: string,
        schedule: shape({
            day: shape({
                description: string.isRequired
            })
        }),
        onClick: func
    };

    render() {
        const {className, schedule, onClick} = this.props;
        return (
            <div className={cn(className, styles.schedule)}>
                <div className={styles.caption} onClick={onClick}>
                    <h3>This week plans</h3>
                    <img src={calendarIcon} alt=""/>
                    <img src={newIcon} alt=""/>
                </div>
                <ArtistScheduleList schedule={schedule} className={styles.scheduleList} />
            </div>
        )
    }
}
