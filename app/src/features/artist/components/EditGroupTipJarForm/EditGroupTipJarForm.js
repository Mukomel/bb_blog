import React from 'react';
import { func, bool } from 'prop-types';
import {Field, reduxForm} from 'redux-form';
import {TextField} from 'redux-form-material-ui';
import Button from '@material-ui/core/Button';
import config from '../../../../config';
import FileInput from '../../../../base-components/FileInput/FileInput';

import * as styles from './EditGroupTipJarForm.module.scss';

class EditGroupTipJarForm extends React.Component {
    static propTypes = {
        onSubmit: func.isRequired,
        onTipJarComplete: func.isRequired,
        isTipJarCompleted: bool.isRequired,
    };

    handleFormSubmit = (values) => {
        const {onSubmit, reset} = this.props;
        onSubmit(values);
        reset();
    };

    render() {
        const { handleSubmit, onTipJarComplete, isTipJarCompleted } = this.props;
        return (
            <form onSubmit={handleSubmit(this.handleFormSubmit)}>
                <Field
                    name="title"
                    label="Title"
                    component={TextField}
                    fullWidth
                    margin='dense'
                    helperText=' '
                />
                <Field
                    name="description"
                    label="Description"
                    component={TextField}
                    multiline
                    rowsMax="4"
                    rows={2}
                    fullWidth
                    margin='dense'
                    helperText=' '
                />
                <Field
                    name="goal_amount"
                    label="Amount in dollars"
                    component={TextField}
                    margin='dense'
                    helperText=' '
                />
                <div className={styles.selectPhotoContainer}>
                    <Field
                        name='image'
                        component={FileInput}
                        accept={config.acceptedImageMIMETypes}
                        selectFileLabel='Select photo'
                        changeFileLabel='Change photo'
                    />
                </div>
                {
                    isTipJarCompleted ?
                        <Button
                            variant="raised"
                            color="primary"
                            type="submit"
                        >
                            Set new goal
                        </Button> :
                        <Button
                            variant="raised"
                            onClick={onTipJarComplete}
                            color="secondary">
                            Complete Current Goal
                        </Button>
                }
            </form>
        )
    }
}

export default reduxForm({
    form: 'editGroupJipJarForm',
    enableReinitialize: true,
    validate: (values) => {
        const errors = {};

        if (!values.title) {
            errors.title = 'Required Field';
        }

        if (!values.description) {
            errors.description = 'Required Field';
        }
        if (!values.goal_amount) {
            errors.goal_amount = 'Required Field';
        }

        if (/[a-zA-Z]/g.test(values.goal_amount)) {
            errors.goal_amount = 'Amount should be a number';
        }

        return errors;
    }
})(EditGroupTipJarForm)