import React from 'react';
import PropTypes from 'prop-types';
import {Field, reduxForm} from 'redux-form';
import {TextField} from 'redux-form-material-ui';
import Button from '@material-ui/core/Button';
import { EDIT_SCHEDULE_FIELDS_FORM } from '../../constants';

import * as styles from './EditScheduleForm.module.scss';

class EditSheduleForm extends React.Component {

    handleFormSubmit = (values) => {
        const {onSubmit, reset} = this.props;
        onSubmit(values);
        reset();
    };

    render() {
        const {handleSubmit} = this.props;
        return (
            <form className={styles.form} onSubmit={handleSubmit(this.handleFormSubmit)}>
                {
                    EDIT_SCHEDULE_FIELDS_FORM.map(({name, id, day}) => (
                        <Field
                            multiline
                            rows={2}
                            key={id}
                            name={name}
                            label={day}
                            component={TextField}
                            fullWidth
                            margin='dense'
                            helperText=' '
                        />
                    ))
                }
                <Button
                    variant="raised"
                    color="primary"
                    type="submit"
                >
                    Update Schedule
                </Button>
            </form>
        )
    }
}

EditSheduleForm.propTypes = {
    onSubmit: PropTypes.func.isRequired
};

export default reduxForm({
    form: 'edit-schedule-form',
    enableReinitialize: true
})(EditSheduleForm)