import React from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import giftImage from '../../../../assets/images/gift_blue.svg';
import closeImage from '../../../../assets/images/close_grey.svg';
import arrowImage from '../../../../assets/images/arrow.svg';
import Button from '../../../../base-components/Button/Button';

import styles from './GroupWIshlistModal.module.scss'

const backdropProps = {
    classes: {
        root: styles.backdrop
    }
};

const dialogClasses = {
    paper: styles.paper
};

const GroupWishlistModal = ({isOpen, onHide, title, description, link}) => {
    return (
        <Dialog
            open={isOpen}
            onClose={onHide}
            classes={dialogClasses}
            fullWidth={true}
            BackdropProps={backdropProps}
        >
            <div className={styles.container}>
                <Button className={styles.closeButton} onClick={onHide}>
                    <img src={closeImage} className={styles.closeImage} />
                </Button>
                <img src={giftImage} className={styles.giftImage} />
                <div className={styles.title}>{title}</div>
                <div className={styles.description}>{description}</div>
                <a href={link} target='_blank' className={styles.link}>
                    Our wishlist
                    <img src={arrowImage} className={styles.arrowImage} />
                </a>
            </div>
        </Dialog>
    )
};

export default GroupWishlistModal;
