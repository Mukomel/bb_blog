import React from 'react';
import {array, bool, number, func} from 'prop-types';
import moment from 'moment';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import TableHead from '@material-ui/core/TableHead';
import {roundToExactly} from '../../../../helpers/roundTo'

import * as styles from './UserDonationsDetailsTable.module.scss'

const columnData = [
    {id: 'amount', numeric: true, label: 'Amount'},
    {id: 'date', numeric: true, label: 'Date'},
    {id: 'author', numeric: false, label: 'Author'},
];

export default class UserDonationsDetailsTable extends React.Component {
    static propTypes = {
        donations: array.isRequired,
    };

    render() {
        const {donations} = this.props;

        return (
            <Table aria-labelledby="tableTitle">
                <TableHead className={styles.tableHead}>
                    <TableRow>
                        {
                            columnData.map(column => (
                                <TableCell
                                    key={column.id}
                                    numeric={column.numeric}
                                    className={styles.tableHeadCell}
                                >
                                    {column.label}
                                </TableCell>
                            ))
                        }
                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                        donations.map(donation => {
                            return (
                                <TableRow
                                    hover
                                    tabIndex={-1}
                                    key={donation._id}
                                >
                                    <TableCell numeric>
                                        {roundToExactly(donation.amount / 100, 2)} $
                                    </TableCell>
                                    <TableCell numeric>{moment(donation.created_at).format('D MMM YY')}</TableCell>
                                    <TableCell>{donation.target.username}</TableCell>
                                </TableRow>
                            );
                        })}
                </TableBody>
            </Table>
        )
    }
}
