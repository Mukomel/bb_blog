import React from 'react';
import PropTypes from 'prop-types';
import {Field, reduxForm} from 'redux-form';
import FormControlLabel  from '@material-ui/core/FormControlLabel'
import {TextField, Checkbox} from 'redux-form-material-ui';
import Button from '@material-ui/core/Button';

import * as styles from './EditArtistWishlistForm.module.scss';

class EditArtistWishlistForm extends React.Component {

    handleFormSubmit = (values) => {
        const {onSubmit} = this.props;
        return onSubmit(values);
    };

    render() {
        const {handleSubmit, submitting, valid} = this.props;
        return (
            <form className={styles.form} onSubmit={handleSubmit(this.handleFormSubmit)}>
                <Field
                    name='title'
                    label='Title'
                    component={TextField}
                    fullWidth
                    margin='dense'
                    helperText=' '
                />
                <Field
                    name='description'
                    label='Description'
                    component={TextField}
                    fullWidth
                    margin='dense'
                    helperText=' '
                    multiline
                    rowsMax="4"
                />
                <Field
                    name='link'
                    label='Link'
                    component={TextField}
                    fullWidth
                    margin='dense'
                    helperText=' '
                />

                <FormControlLabel
                    control={
                        <Field
                            className={styles.checkbox}
                            name='is_enabled'
                            component={Checkbox}
                            color="primary"
                        />
                    }
                    label="Show wishlist on artist profile page"
                />

                <div className={styles.buttonContainer}>
                    <Button
                        variant="raised"
                        color="primary"
                        type="submit"
                        disabled={submitting || !valid}
                    >
                        Save
                    </Button>
                </div>
            </form>
        )
    }
}

EditArtistWishlistForm.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    submitting: PropTypes.bool.isRequired,
    valid: PropTypes.bool.isRequired,
    initialValues: PropTypes.object
};

export default reduxForm({
    form: 'edit-artist-wishlist-form',
    enableReinitialize: true,
    validate: (values) => {
        const errors = {};

        if (!values.title) {
            errors.title = 'Required';
        }

        if (!values.link) {
            errors.link = 'Required';
        }

        return errors;
    }
})(EditArtistWishlistForm)
