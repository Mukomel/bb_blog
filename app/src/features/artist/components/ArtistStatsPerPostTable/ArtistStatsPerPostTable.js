import React, { Component } from 'react';
import { array, bool } from 'prop-types';
import moment from'moment';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper/Paper';
import SortTableHead from '../../../../base-components/SortTableHead/SortTableHead';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';
import {roundToExactly} from '../../../../helpers/roundTo';


const columnData = [
    { id: 'title', numeric: false, label: 'Post' },
    { id: 'media', numeric: true, label: 'Type' },
    { id: 'created_at', numeric: true, label: 'Date' },
    { id: 'user', numeric: true, label: 'Author' },
    { id: 'donations_amount', numeric: true, label: 'Tip, $' },
];

export default class StatsPerPostTable extends Component {
    static propTypes = {
        data: array.isRequired,
        isFetchingStats: bool.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            order: 'asc',
            orderBy: 'created_at',
            data: this.props.data.sort((a, b) => (a.created_at < b.created_at ? -1 : 1))
        };
    }

    static getDerivedStateFromProps(props) {
        if (props.data) {
            return {
                data: props.data
            }
        }
    }


    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'asc';

        if (this.state.orderBy === property && this.state.order === 'asc') {
            order = 'desc';
        }

        const data =
            order === 'desc'
                ? this.state.data.sort((a, b) => {
                    if (typeof a[orderBy] === 'object' && a[orderBy].username) {
                        return (b[orderBy].username < a[orderBy].username ? -1 : 1)
                    }

                    if (typeof a[orderBy] === 'object' && typeof b[orderBy] === 'object') {
                        return (b[orderBy].type < a[orderBy].type ? -1 : 1)
                    }

                    return (b[orderBy] < a[orderBy] ? -1 : 1)
                })
                : this.state.data.sort((a, b) => {
                    if (typeof a[orderBy] === 'object' && a[orderBy].username) {
                        return (a[orderBy].username < b[orderBy].username ? -1 : 1)
                    }

                    if (typeof a[orderBy] === 'object' && typeof b[orderBy] === 'object') {
                        return (a[orderBy].type < b[orderBy].type ? -1 : 1)
                    }

                    return (a[orderBy] < b[orderBy] ? -1 : 1);
                });

        this.setState({ data, order, orderBy });
    };

    render() {
        const { order, orderBy } = this.state;
        const { data, isFetchingStats } = this.props;
        return (
            <Paper>
                {
                    isFetchingStats && <SpinnerWithBackdrop/>
                }
                <Table>
                    <SortTableHead
                        columnData={columnData}
                        order={order}
                        orderBy={orderBy}
                        onRequestSort={this.handleRequestSort}
                    />
                    <TableBody>
                        {data && data.map(item => (
                            <TableRow key={item._id} hover>
                                <TableCell component="td" scope="row">
                                    {item.title}
                                </TableCell>
                                <TableCell numeric>{
                                    item.media ? item.media.type : 'no type'
                                }</TableCell>
                                <TableCell numeric>
                                    {moment(item.created_at).format('DD MMM')}
                                </TableCell>
                                <TableCell numeric>{item.user.username}</TableCell>
                                <TableCell numeric>
                                    {roundToExactly(item.donations_amount / 100, 2)} $
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </Paper>
        );
    }
};