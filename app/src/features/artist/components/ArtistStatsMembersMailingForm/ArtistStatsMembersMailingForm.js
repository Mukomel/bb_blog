import React from 'react';
import PropTypes from 'prop-types';
import {Field, reduxForm} from 'redux-form';
import {TextField} from 'redux-form-material-ui';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import RichHtmlEditor from '../../../../base-components/RichHtmlEditor/RichHtmlEditor';

import * as styles from './ArtistStatsMembersMailingForm.module.scss';

class ArtistStatsMembersMailingForm extends React.Component {
    static propTypes = {
        onSubmit: PropTypes.func.isRequired
    };

    onChange = (value) => { this.setState({value}) };

    handleFormSubmit = (values) => {
        const {onSubmit, reset} = this.props;
        return onSubmit(values)
            .then(reset);
    };

    componentWillReceiveProps(nextProps) {
        if ((nextProps.valid !== this.props.valid) && this.props.onFormValidChange) {
            this.props.onFormValidChange(nextProps.valid);
        }

        if ((nextProps.submitting !== this.props.submitting) && this.props.onFormSubmittingChange) {
            this.props.onFormSubmittingChange(nextProps.submitting);
        }
    }

    renderCheckbox = ({input, label, meta: { error, valid }}) => (
        <React.Fragment>
            {
                !valid ? <p className={styles.error}>{ error} </p> : null
            }
            <FormControlLabel
                control={
                    <Checkbox
                        checked={!!input.value}
                        onChange={input.onChange}
                        color="primary"
                    />
                }
                label={label}
            />
        </React.Fragment>
    );

    render() {
        const {handleSubmit} = this.props;
        return (
            <form className={styles.form} onSubmit={handleSubmit(this.handleFormSubmit)}>
                <div>
                    <Field
                        name="send_emails"
                        component={this.renderCheckbox}
                        label="Emails"/>
                </div>
                <div>
                    <Field
                        name="send_notifications"
                        component={this.renderCheckbox}
                        label="Notifications"/>
                </div>
                <Field
                    name='title'
                    label='Subject'
                    fullWidth
                    component={TextField}
                    margin='dense'
                    helperText=' '
                />
                <Field
                    name="html"
                    component={RichHtmlEditor}
                    placeholder="Message"
                    errorClassName={styles.error}
                    editorClassName={styles.formEditor}
                />
            </form>
        )
    }
}

ArtistStatsMembersMailingForm.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onFormValidChange: PropTypes.func.isRequired,
    onFormSubmittingChange: PropTypes.func
};

export default reduxForm({
    form: 'artistStatsMembersMailingForm',
    enableReinitialize: true,
    validate: (values) => {
        const errors = {};

        if (!values.send_emails && !values.send_notifications) {
            errors.send_emails = 'At least one checkbox should be selected';
        }

        if (!values.title) {
            errors.title = 'Required';
        }
        if (!values.html) {
            errors.html = 'Required';
        }

        return errors;
    }
})(ArtistStatsMembersMailingForm)
