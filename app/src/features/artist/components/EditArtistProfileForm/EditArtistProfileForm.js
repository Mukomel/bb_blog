import React from 'react';
import PropTypes from 'prop-types';
import config from '../../../../config';
import {Field, reduxForm} from 'redux-form';
import {TextField} from 'redux-form-material-ui';
import Button from '@material-ui/core/Button';
import ImageLoaderZone from '../../../../base-components/ImageLoaderZone/ImageLoaderZone';
import {IMAGE_SIZES} from '../../constants';

import * as styles from './EditArtistProfileForm.module.scss';

class EditArtistProfileForm extends React.Component {

    handleFormSubmit = (values) => {
        const {onSubmit} = this.props;
        return onSubmit(values);
    };

    render() {
        const {handleSubmit} = this.props;
        return (
            <form className={styles.form} onSubmit={handleSubmit(this.handleFormSubmit)}>
                <Field
                    name='avatar'
                    component={ImageLoaderZone}
                    aspectRatio={1}
                    circled
                    sizes={IMAGE_SIZES.avatar}
                    accept={config.acceptedImageMIMETypes}
                    selectFileLabel='Select avatar'
                    changeFileLabel='Change avatar'
                />
                <Field
                    name='name'
                    label='Name'
                    component={TextField}
                    fullWidth
                    margin='dense'
                    helperText=' '
                />
                <Field
                    name='biography'
                    label='Biography'
                    component={TextField}
                    fullWidth
                    margin='dense'
                    helperText=' '
                    multiline
                    rowsMax="4"
                />
                <Field
                    name='cover'
                    component={ImageLoaderZone}
                    accept={config.acceptedImageMIMETypes}
                    aspectRatio={5 / 2}
                    circled={false}
                    sizes={IMAGE_SIZES.background}
                    selectFileLabel='Select cover'
                    changeFileLabel='Change cover'
                />

                <div className={styles.buttonContainer}>
                    <Button
                        variant="raised"
                        color="primary"
                        type="submit"
                    >
                        Submit
                    </Button>
                </div>
            </form>
        )
    }
}

EditArtistProfileForm.propTypes = {
    onSubmit: PropTypes.func.isRequired
};

export default reduxForm({
    form: 'edit-artist-profile-form',
    enableReinitialize: true,
    validate: (values) => {
        const errors = {};

        if (!values.name) {
            errors.name = 'Required';
        }

        if (!values.biography) {
            errors.biography = 'Required';
        }

        return errors;
    }
})(EditArtistProfileForm)