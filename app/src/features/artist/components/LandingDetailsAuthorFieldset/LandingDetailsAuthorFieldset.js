import React from 'react';
import PropTypes from 'prop-types';
import {Field, reduxForm} from 'redux-form';
import {TextField} from 'redux-form-material-ui';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import styles from './LandingDetailsAuthorFieldset.module.scss';

export default class LandingDetailsAuthorFieldset extends React.Component {
    render() {
        const {title, fieldName} = this.props;

        return (
            <Card className={styles.container}>
                <CardContent>
                    <Typography color="textSecondary">
                        {title}
                    </Typography>
                    <Field
                        name={`${fieldName}.biography`}
                        label='Biography'
                        component={TextField}
                        fullWidth
                        margin='dense'
                        helperText=' '
                        multiline
                        rowsMax="4"
                    />
                    <Field
                        name={`${fieldName}.wishlist_link`}
                        label='Wishlist link'
                        component={TextField}
                        fullWidth
                        margin='dense'
                        helperText=' '
                    />
                </CardContent>
            </Card>
        )
    }
}

LandingDetailsAuthorFieldset.propTypes = {
    title: PropTypes.string,
    fieldName: PropTypes.string
};
