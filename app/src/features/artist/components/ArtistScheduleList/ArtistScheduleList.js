import React from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';

import styles from './ArtistScheduleList.module.scss'

const capitalize = (str) => {
    return str[0].toUpperCase() + str.slice(1)
};

const ArtistScheduleList = ({schedule, className}) => {
    return (
        <ul className={cn(styles.scheduleDaysList, className)}>
            <div className={styles.line} />
            {
                //  reverse array, because week should start from Monday
                Object.keys(schedule).reverse().map((scheduleDay) => {
                    if (scheduleDay === "_id") return;
                    const description = schedule[scheduleDay].description;
                    const scheduleDayClasses = cn(styles.step, {
                        [styles.stepActive]: description.length > 0,
                        [styles.disabled]: !description.length > 0,
                    });
                    return (
                        <li key={scheduleDay} className={scheduleDayClasses}>
                            <div className={styles.scheduleDayContainer}>
                                <div className={styles.circle}>{
                                    capitalize(scheduleDay).slice(0, 3)}
                                </div>
                                <div className={styles.scheduleDescription}>
                                    <div className={styles.arrowLeft}/>
                                    <p>{description}</p>
                                </div>
                            </div>
                        </li>
                    )
                })
            }
        </ul>
    )
};

ArtistScheduleList.propTypes = {
    schedule: PropTypes.object,
    className: PropTypes.string
};

export default ArtistScheduleList;
