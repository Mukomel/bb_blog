import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import findLastIndex from 'lodash/findLastIndex';
import ArtistPost from '../../components/ArtistPost/ArtistPost';
import PeriodItem from '../../components/PeriodItem/PeriodItem';
import ErrorBoundary from '../../../../base-components/ErrorBoundary/ErrorBoundary';
import {visibleContentPeriodTypes} from '../../../auth/constants';
import config from '../../../../config';

import styles from './ArtistPostsList.module.scss';

const itemTypes = {
    POST: 'POST',
    PERIOD: 'PERIOD'
};

export default class ArtistPostsList extends React.Component {
    sortItems = (a, b) => {
        const aDate = moment(a.payload.start || a.payload.created_at);
        const bDate = moment(b.payload.start || b.payload.created_at);

        if (a.payload.is_pinned) return -1;
        if (aDate.isAfter(bDate)) return -1;
        if (bDate.isAfter(aDate)) return 1;
        return 0;
    };

    renderItem = ({type, payload}) => {
        const {
            onSubscribeClick,
            onTip,
            onPeriodUncollapse,
            onPeriodUnlock,
            onShowMoreCommentsClick,
            deleteComment,
            onImageClick,
            postLike,
            commentLike,
            isLoggedIn,
            createPostComment,
            user,
            trackVideoView,
            editComment,
            subscribeArtist,
            unsubscribeArtist,
            artistAuthors,
            replyComment,
            layoutType,
            artistProfile,
            topFansList
        } = this.props;

        if (type === itemTypes.POST) {
            return (
                <ArtistPost
                    key={payload._id}
                    artistProfile={artistProfile}
                    currentUser={user}
                    className={styles.post}
                    post={payload}
                    onSubscribeClick={onSubscribeClick}
                    onShowMoreCommentsClick={onShowMoreCommentsClick}
                    onTip={onTip}
                    onImageClick={onImageClick}
                    trackVideoView={trackVideoView}
                    postLike={postLike}
                    commentLike={commentLike}
                    isLoggedIn={isLoggedIn}
                    createPostComment={createPostComment}
                    deleteComment={deleteComment}
                    editComment={editComment}
                    subscribeArtist={subscribeArtist}
                    unsubscribeArtist={unsubscribeArtist}
                    artistAuthors={artistAuthors}
                    replyComment={replyComment}
                    layoutType={layoutType}
                    topFansList={topFansList}
                />
            )
        }

        if (type === itemTypes.PERIOD) {
            return (
                <PeriodItem
                    key={payload.start}
                    className={styles.period}
                    periodPrice={config[process.env.REACT_APP_ENV].PERIOD_UNLOCK_PRICE_CENTS}
                    period={payload}
                    onUncollapse={onPeriodUncollapse}
                    onUnlock={onPeriodUnlock}
                    onTip={onTip}
                    onShowMoreCommentsClick={onShowMoreCommentsClick}
                />
            )
        }
    };

    render() {
        const {posts, shouldShowPeriods, firstPostDate, visibleContentPeriods, postsWithinPeriods, isUserBanned} = this.props;

        let items = [];

        items = items.concat(posts.map(post => ({type: itemTypes.POST, payload: post})));

        if (firstPostDate && !isUserBanned && visibleContentPeriods && (visibleContentPeriods.length > 0)) {
            const subscriptionStartDay = visibleContentPeriods
                .find((period) => period.type === visibleContentPeriodTypes.SUBSCRIPTION)
                .start;

            if (moment(subscriptionStartDay).isAfter(firstPostDate)) {

                let periods = [];
                let daysMultiplier = 1;

                while(moment(subscriptionStartDay).add({days: -30 * daysMultiplier}).isAfter(firstPostDate)) {
                    const periodStartDate = moment(subscriptionStartDay).add({days: -30 * daysMultiplier}).toISOString();
                    const periodEndDate = moment(subscriptionStartDay).add({days: -30 * (daysMultiplier - 1)}).toISOString();
                    const postsByPeriod = postsWithinPeriods.find(item => (item.start === periodStartDate) && (item.end === periodEndDate));
                    periods.push({
                        type: itemTypes.PERIOD,
                        payload: {
                            start: periodStartDate,
                            end: periodEndDate,
                            posts: postsByPeriod ? postsByPeriod.posts : []
                        }
                    });
                    daysMultiplier++;
                }

                const firstPeriodEndDate = moment(subscriptionStartDay).add({days: -30 * (daysMultiplier - 1)}).toISOString();
                const postsByFirstPeriod = postsWithinPeriods.find(item => (item.start === firstPostDate) && (item.end === firstPeriodEndDate));
                periods.push({
                    type: itemTypes.PERIOD,
                    payload: {
                        start: firstPostDate,
                        end: firstPeriodEndDate,
                        posts: postsByFirstPeriod ? postsByFirstPeriod.posts : []
                    }
                });

                periods = periods.filter(period => {
                    return !Boolean(visibleContentPeriods.find(({start, end}) => (start === period.payload.start) && (end === period.payload.end)));
                });

                items = items.concat(periods);
            }
        }

        items.sort(this.sortItems);

        if (!shouldShowPeriods) {
            const lastPostIndex = findLastIndex(items, (item) => item.type === itemTypes.POST);
            items = items.slice(0, lastPostIndex + 1);
        }
        return (
            <ErrorBoundary>
                <div className={styles.container}>
                    {items.map(this.renderItem)}
                </div>
            </ErrorBoundary>
        )
    }
}

ArtistPostsList.propTypes = {
    visibleContentPeriods: PropTypes.arrayOf(PropTypes.shape({
        start: PropTypes.string.isRequired,
        end: PropTypes.string.isRequired,
        type: PropTypes.string.isRequired
    })),
    posts: PropTypes.array,
    postsWithinPeriods: PropTypes.array,
    firstPostDate: PropTypes.string,
    shouldShowPeriods: PropTypes.bool.isRequired,
    isLoggedIn: PropTypes.bool.isRequired,
    isUserBanned: PropTypes.bool,
    onSubscribeClick: PropTypes.func.isRequired,
    onShowMoreCommentsClick: PropTypes.func.isRequired,
    onTip: PropTypes.func.isRequired,
    onPeriodUncollapse: PropTypes.func.isRequired,
    onPeriodUnlock: PropTypes.func.isRequired,
    onImageClick: PropTypes.func.isRequired,
    postLike: PropTypes.func.isRequired,
    commentLike: PropTypes.func.isRequired,
    createPostComment: PropTypes.func.isRequired,
    deleteComment: PropTypes.func.isRequired,
    replyComment: PropTypes.func.isRequired,
    editComment: PropTypes.func.isRequired,
    subscribeArtist: PropTypes.func.isRequired,
    unsubscribeArtist: PropTypes.func.isRequired,
    artistAuthors: PropTypes.array.isRequired,
    layoutType: PropTypes.string.isRequired
};
