import React, {Component} from 'react';
import { object, func, bool } from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs/Tabs';
import Tab from '@material-ui/core/Tab';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import SortTableHead from '../../../../base-components/SortTableHead/SortTableHead';
import { topFansTabs } from '../../../ui/constants';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';
import Button from '@material-ui/core/Button';
import {roundToExactly} from '../../../../helpers/roundTo';

import styles from './ArtistStatsPerFansTable.module.scss';

const columnData = [
    { id: 'username', numeric: false, hidden: false, label: 'Username' },
    { id: 'snapchat_username', numeric: false, hidden: false, label: 'Snapchat username' },
    { id: 'email', numeric: true, hidden: false, label: 'Email' },
    { id: 'donations_amount_total', numeric: true, hidden: true, label: 'Total Amount' },
    { id: 'actions', numeric: false, label: 'Actions' }
];

const tabsList = [
    {tab: topFansTabs.WEEKLY, title: 'THIS WEEK'},
    {tab: topFansTabs.MONTHLY, title: 'THIS MONTH'},
    {tab: topFansTabs.ALL_TIME, title: 'ALL TIME'}
];

class ArtistStatsPerFansTable extends Component {
    static propTypes = {
        data: object.isRequired,
        getStatsPerFans: func.isRequired,
        isFetchingStats: bool.isRequired,
        onDetailsClick: func.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            value: 0,
            order: 'asc',
            orderBy: 'total_amount',
            data: this.props.data && this.props.data.users.sort((a, b) => (a.donations_amount_total < b.donations_amount_total ? -1 : 1))
        };
    }

    static getDerivedStateFromProps(props) {
        if (props.data) {
            return {
                data: props.data.users
            }
        }
    }

    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'asc';

        if (this.state.orderBy === property && this.state.order === 'asc') {
            order = 'desc';
        }

        const data =
            order === 'desc'
                ? this.state.data.sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
                : this.state.data.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));

        this.setState({ data, order, orderBy });
    };

    componentDidMount() {
        this.props.getStatsPerFans(tabsList[this.state.value].tab.toLowerCase())
    }

    handleChange = (event, value) => {
        this.setState({value});
        this.props.getStatsPerFans(tabsList[value].tab.toLowerCase())
    };

    handleDetailsClick = (userId, username) => {
        const {onDetailsClick} = this.props;
        onDetailsClick(userId, username, tabsList[this.state.value].tab.toLowerCase());
    };

    render() {
        const {value, orderBy, order} = this.state;
        const { data, isFetchingStats } = this.props;

        const tableColumnAuthors = data ? data.authors.map(author => ({
            id: author._id,
            numeric: true,
            hidden: false,
            label: `${author.username}, $`
        })) : [];

        const headColumnData = [
            ...columnData.slice(0, 3),
            ...tableColumnAuthors,
            ...columnData.slice(3, columnData.length)
        ];

        return (
            <Paper className={styles.tabsContainer}>
                {
                    isFetchingStats && <SpinnerWithBackdrop/>
                }
                <Tabs
                    value={value}
                    indicatorColor="primary"
                    textColor="primary"
                    centered
                    onChange={this.handleChange}
                >
                    {
                        tabsList.map(item => (
                            <Tab
                                key={item.tab}
                                className={styles.tab}
                                label={item.title}/>
                        ))
                    }
                </Tabs>
                <Table>
                    <SortTableHead
                        columnData={headColumnData}
                        order={order}
                        orderBy={orderBy}
                        onRequestSort={this.handleRequestSort}
                    />
                    <TableBody>
                        {data && data.users.map(item => (
                            <TableRow key={item._id} hover>
                                <TableCell component="td" scope="row">
                                    {item.username}
                                </TableCell>
                                <TableCell component="td" scope="row">
                                    {item.snapchat_username}
                                </TableCell>
                                <TableCell numeric>{item.email}</TableCell>
                                <TableCell className={styles.hidden}>
                                    {item.donations_amount_total / 100} $
                                </TableCell>
                                {
                                    data.authors.map(author => {
                                        const donation = item.donations[author._id];
                                        return (
                                            <TableCell numeric key={author._id}>
                                                {donation ? roundToExactly(donation / 100, 2) : 0} $</TableCell>
                                        )
                                    })
                                }
                                <TableCell>
                                    <Button
                                        onClick={() => this.handleDetailsClick(item._id, item.username)}
                                        variant="raised"
                                        color="primary"
                                    >
                                        Details
                                    </Button>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </Paper>
        )
    }
}

export default ArtistStatsPerFansTable;