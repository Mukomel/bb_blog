import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import moment from 'moment';
import TipInput from '../../../donation/components/TipInput/TipInput';
import {roundToExactly} from '../../../../helpers/roundTo';

import styles from './ArtistGroupTipJar.module.scss'

export default class ArtistGroupTipJar extends React.Component {
    handleTip = (amountCents) => {
        const {onTip, tipJar} = this.props;
        onTip({
            donationAmount: amountCents,
            tipJarId: tipJar.id,
            tipJarTitle: tipJar.title
        });
    };

    render() {
        const {className, tipJar: {id, title, image, description, bakers_count, goal_amount, current_amount, created_at}} = this.props;

        const percentage = current_amount / goal_amount * 100;
        const fillTipWidth = {width: percentage + '%'};
        return (
            <div className={cn(styles.container, className)}>
                <div className={styles.content}>
                    <div className={cn(styles.tipJarText, styles.tipJarCaption)}>
                        <h3>{title}</h3>
                    </div>
                    <div className={styles.tipJarText}>
                        <p>{description}</p>
                    </div>
                    {
                        image ? <div className={styles.tipJarImageContainer}>
                            <img src={image} alt=""/>
                        </div> : null
                    }
                    <div className={styles.donation}>
                        <div className={styles.counter}>
                        <span className={styles.count}>${
                            roundToExactly(current_amount / 100, 2)} <span>Pledged</span></span>
                            <span className={styles.goal}><span>Goal </span>${
                                roundToExactly(goal_amount / 100, 0)}</span>
                        </div>
                        <div className={styles.progressContainer}>
                            <div className={cn(
                                styles.progress, {
                                    [styles.isReached]: percentage >= 100
                                })}>
                                <span style={fillTipWidth}> </span>
                            </div>
                        </div>
                        <div className={styles.donationDescription}>
                            <span className={styles.date}>{moment(created_at).format('MMM DD, YYYY')}</span>
                            <span
                                className={styles.bakersCount}>{bakers_count} {bakers_count === 1 ? 'baker' : 'bakers'}</span>
                        </div>
                        <TipInput
                            onTipClick={this.handleTip}
                            className={styles.tipContainer}/>
                    </div>
                </div>
            </div>
        )
    }
}

ArtistGroupTipJar.propTypes = {
    className: PropTypes.string,
    tipJar: PropTypes.shape({
        _id: PropTypes.string,
        artist: PropTypes.string,
        title: PropTypes.string,
        description: PropTypes.string,
        goal_amount: PropTypes.number,
        __v: PropTypes.number,
        is_active: PropTypes.bool,
        current_amount: PropTypes.number,
        created_at: PropTypes.string
    }),
    onTip: PropTypes.func.isRequired
};
