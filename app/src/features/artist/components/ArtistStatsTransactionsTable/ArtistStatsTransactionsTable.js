import React, {Component} from 'react';
import {array, bool, object} from 'prop-types';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper/Paper';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import LightTooltip from '../../../../base-components/LightTooltip/LightTooltip';
import InfoIcon from '@material-ui/icons/Info';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';
import {roundToExactly} from '../../../../helpers/roundTo';

import * as styles from './ArtistStatsTransactionsTable.module.scss'

const columnData = [
    {id: 'date', numeric: false, label: 'Date'},
    {id: 'revenue', numeric: true, label: 'Revenue'},
    {id: 'expenses', numeric: true, label: 'Expenses', icon: <InfoIcon/>, tooltipTitle: 'Refunds, Chargebacks'},
    {id: 'fees', numeric: true, label: 'Fees', icon: <InfoIcon/>, tooltipTitle: 'Segpay fee 10%'},
    {id: 'net', numeric: true, label: 'Net', icon: <InfoIcon/>, tooltipTitle: 'Revenue - Expenses + Fees'},
];

export default class ArtistStatsTransactionsTable extends Component {
    static propTypes = {
        data: object.isRequired,
        isFetchingStats: bool.isRequired
    };

    state = {
        page: 0,
        rowsPerPage: 50,
    };

    handleChangePage = (event, page) => {
        this.setState({page});
    };

    handleChangeRowsPerPage = event => {
        this.setState({rowsPerPage: event.target.value});
    };

    render() {
        const {isFetchingStats, data} = this.props;
        const days = data.days;
        const total = data.total;
        const {rowsPerPage, page} = this.state;

        return (
            <div className={styles.transactionContainer}>
                <Paper className={styles.transactionInnerContainer}>
                    {
                        isFetchingStats && <SpinnerWithBackdrop/>
                    }
                    <div>
                        <Table aria-labelledby="tableTitle">
                            <TableHead>
                                <TableRow>
                                    {
                                        columnData.map(column => (
                                            <TableCell
                                                key={column.id}
                                                numeric={column.numeric}
                                                className={styles.headerCell}
                                            >
                                                {column.label}
                                                {
                                                    column.icon && column.tooltipTitle &&
                                                    <LightTooltip title={column.tooltipTitle}>
                                                        {column.icon}
                                                    </LightTooltip>
                                                }
                                            </TableCell>
                                        ))
                                    }
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    days && days
                                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                        .map(item => {
                                            return (
                                                <TableRow
                                                    hover
                                                    tabIndex={-1}
                                                    key={item._id}
                                                >
                                                    <TableCell component="th" scope="row" padding="none">
                                                        {item.day}
                                                    </TableCell>
                                                    <TableCell numeric>{
                                                        roundToExactly(item.income / 100, 2)} $</TableCell>
                                                    <TableCell numeric>{
                                                        roundToExactly(item.expense / 100, 2)} $</TableCell>
                                                    <TableCell numeric>{
                                                        roundToExactly(item.fee / 100, 2)} $</TableCell>
                                                    <TableCell numeric>{
                                                        roundToExactly(item.net / 100, 2)} $</TableCell>
                                                </TableRow>
                                            );
                                        })
                                }
                                {
                                    total && <TableRow
                                        className={styles.totalCount}
                                        hover
                                        tabIndex={-1}
                                    >
                                        <TableCell scope="row" padding="none">
                                            Total
                                        </TableCell>
                                        <TableCell numeric>{
                                            roundToExactly(total.income / 100, 2)} $</TableCell>
                                        <TableCell numeric>{
                                            roundToExactly(total.expense / 100, 2)} $</TableCell>
                                        <TableCell numeric>{
                                            roundToExactly(total.fee / 100, 2)} $</TableCell>
                                        <TableCell numeric>{
                                            roundToExactly(total.net / 100, 2)} $</TableCell>
                                    </TableRow>
                                }
                            </TableBody>
                        </Table>
                    </div>
                    <TablePagination
                        component="div"
                        count={days.length}
                        rowsPerPage={rowsPerPage}
                        rowsPerPageOptions={[50]}
                        page={page}
                        backIconButtonProps={{
                            'aria-label': 'Previous Page',
                        }}
                        nextIconButtonProps={{
                            'aria-label': 'Next Page',
                        }}
                        onChangePage={this.handleChangePage}
                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                    />
                </Paper>
            </div>
        );
    }
};
