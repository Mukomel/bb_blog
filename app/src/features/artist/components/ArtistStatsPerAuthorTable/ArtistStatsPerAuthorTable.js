import React, {Component} from 'react';
import { array, func, bool } from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs/Tabs';
import Tab from '@material-ui/core/Tab';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import SortTableHead from '../../../../base-components/SortTableHead/SortTableHead';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';
import {roundToExactly} from '../../../../helpers/roundTo';
import { topFansTabs } from '../../../ui/constants';

import styles from './ArtistStatsPerAuthorTable.module.scss';

const columnData = [
    { id: 'donation_target', numeric: false, label: 'Author' },
    { id: 'donations_amount', numeric: false, label: 'Tip, $' },
];

const tabsList = [
    {tab: topFansTabs.WEEKLY, title: 'THIS WEEK'},
    {tab: topFansTabs.MONTHLY, title: 'THIS MONTH'},
    {tab: topFansTabs.ALL_TIME, title: 'ALL TIME'}
];

class ArtistStatsPerAuthorTable extends Component {
    static propTypes = {
        data: array.isRequired,
        getStatsPerAuthors: func.isRequired,
        isFetchingStats: bool.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            value: 0,
            order: 'asc',
            orderBy: 'author',
            data: this.props.data.sort((a, b) => (a.donation_target < b.donation_target ? -1 : 1))
        };
    }

    static getDerivedStateFromProps(props) {
        if (props.data) {
            return {
                data: props.data
            }
        }
    }

    componentDidMount() {
        this.props.getStatsPerAuthors(tabsList[this.state.value].tab.toLowerCase())
    }

    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'asc';

        if (this.state.orderBy === property && this.state.order === 'asc') {
            order = 'desc';
        }

        const data =
            order === 'desc'
                ? this.state.data.sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
                : this.state.data.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));

        this.setState({ data, order, orderBy });
    };

    handleChange = (event, value) => {
        this.setState({value});
        this.props.getStatsPerAuthors(tabsList[value].tab.toLowerCase())
    };

    render() {
        const {value, order, orderBy } = this.state;
        return (
            <Paper className={styles.tabsContainer}>
                {
                    this.props.isFetchingStats && <SpinnerWithBackdrop/>
                }
                <Tabs
                    value={value}
                    indicatorColor="primary"
                    textColor="primary"
                    centered
                    onChange={this.handleChange}
                >
                    {
                        tabsList.map(item => (
                            <Tab
                                key={item.tab}
                                className={styles.tab}
                                label={item.title}/>
                        ))
                    }
                </Tabs>
                <Table>
                    <SortTableHead
                        orderBy={orderBy}
                        order={order}
                        columnData={columnData}
                        onRequestSort={this.handleRequestSort}
                    />
                    <TableBody>
                        {this.props.data && this.props.data.map(item => (
                            <TableRow key={item._id} hover>
                                <TableCell component="td" scope="row">
                                    {item.username}
                                </TableCell>
                                <TableCell>
                                    {roundToExactly(item.donations_amount / 100, 2)} $
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </Paper>
        )
    }
}

export default ArtistStatsPerAuthorTable;