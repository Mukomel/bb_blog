import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import cn from 'classnames';
import Button from '../../../../base-components/Button/Button';
import likeImg from '../../../../assets/images/like.svg';
import viewImg from '../../../../assets/images/eye.svg';
import PostsIcon from '@material-ui/icons/LocalPostOffice';
import snapchatIcon from '../../../../assets/images/snapchat_for_profile.svg';
import twitterIcon from '../../../../assets/images/twitter_for_profile.svg';
import tumblrIcon from '../../../../assets/images/tumblr_for_profile.svg';
import instagramIcon from '../../../../assets/images/instagram_for_profile.svg';
import giftIcon from '../../../../assets/images/gift_white.svg';

import styles from './ArtistInfo.module.scss';

const ArtistInfo = ({user, className, onSubscribeClick, onWishlistClick, isUserSubscribed, artistProfile: {name, biography, avatar, likes_count, views_count, posts_count, snapchat_link, twitter_link, subscription_price_cents_first_30_days, instagram_link, tumblr_link, wishlist, wishlist_enabled}, isLoggedIn}) => {
    const handleWishlistClick = () => {
        onWishlistClick(wishlist);
    };

    const handleClickLike = () => {

    };
    return (
        <div className={cn(className)}>
            <div className={styles.infoContainer}>
                <div className={styles.avatarContainer}> <img src={avatar} className={styles.avatar}/></div>
                <div className={styles.name}>{name}</div>
                <div className={styles.biography}>{biography}</div>
                <div className={styles.countersContainer}>
                    <div className={styles.counterItem}>
                        <img src={likeImg} alt="" className={styles.icon} onClick={handleClickLike}/>
                        <span className={styles.likesCounter}>{likes_count} Likes</span>
                    </div>
                    <div className={styles.counterItem}>
                        <img src={viewImg} alt="" className={styles.icon}/>
                        <span className={styles.viewsCounter}>{views_count} Views</span>
                    </div>
                    <div className={styles.counterItem}>
                        <PostsIcon className={styles.icon}/>
                        <span className={styles.postsCounter}>{posts_count} Posts</span>
                    </div>
                </div>
                <div className={styles.socialLinks}>
                    {snapchat_link ? <a target="_blank" href={snapchat_link}> <img src={snapchatIcon}/></a> : null}
                    {twitter_link ? <a target="_blank" href={twitter_link}> <img src={twitterIcon}/></a> : null}
                    {instagram_link ? <a target="_blank" href={instagram_link}> <img src={instagramIcon}/></a> : null}
                    {tumblr_link ? <a target="_blank" href={tumblr_link}> <img src={tumblrIcon}/></a> : null}
                </div>
            </div>
            {
                !isUserSubscribed && <Button
                    className={styles.subscribeButton}
                    onClick={onSubscribeClick}>
                    Subscribe (${subscription_price_cents_first_30_days/100}/<small>mo</small>)
                </Button>
            }
            {
                (wishlist && wishlist.is_enabled)
                && <Button className={styles.wishlistButton} onClick={handleWishlistClick}>
                    {wishlist.title} <img src={giftIcon} className={styles.giftIcon}/>
                </Button>
            }
        </div>
    )
};

ArtistInfo.propTypes = {
    className: PropTypes.string,
    artistProfile: PropTypes.shape({
        name: PropTypes.string.isRequired,
        biography: PropTypes.string,
        avatar: PropTypes.string,
        likes_count: PropTypes.number,
    }),
    isUserSubscribed: PropTypes.bool.isRequired,
    isLoggedIn: PropTypes.bool.isRequired,
    onSubscribeClick: PropTypes.func.isRequired,
    onWishlistClick: PropTypes.func.isRequired
};

export default ArtistInfo
