import React from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';
import moment from 'moment';

import styles from './MobileNotificationItem.module.scss';

const MobileNotificationItem = ({className, notification: {created_at, title, message, is_viewed}}) => {
    return (
        <div className={cn(styles.container, className)}>
            <div className={styles.date}>{ moment(created_at).format('MMM DD, YYYY')}</div>
            <div className={styles.title}>{ title }</div>
            <div className={styles.message} dangerouslySetInnerHTML={{ __html: message }} />
            {
                !is_viewed &&
                <span className={styles.newNotificationIcon}>New</span>
            }
        </div>
    )
};

MobileNotificationItem.propTypes = {
    className: PropTypes.string,
    notification: PropTypes.shape({
        created_at: PropTypes.string,
        title: PropTypes.string,
        message: PropTypes.string
    })
};

export default MobileNotificationItem;
