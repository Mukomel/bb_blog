import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import moment from 'moment';
import downArrowImg from '../../../../assets/images/down-arrow.svg';
import blackLockImg from '../../../../assets/images/lock-black-locked.svg';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';
import ArtistPost from '../ArtistPost/ArtistPost';
import {roundToExactly} from '../../../../helpers/roundTo';

import styles from './PeriodItem.module.scss';

export default class PeriodItem extends React.Component {
    constructor() {
        super();

        this.state = {
            isCollapsed: true,
            isLoadingPosts: false
        };
    }

    handleCollapse = () => {
        this.setState({isCollapsed: true});
    };

    handleUncollapse = () => {
        const {onUncollapse, period: {start, end}} = this.props;

        this.setState({isCollapsed: false, isLoadingPosts: true});
        onUncollapse(start, end, this.handlePostsLoaded)
    };

    handleUnlock = () => {
        const {onUnlock, period: {start, end}} = this.props;

        onUnlock(start, end);
    };

    handlePostsLoaded = () => {
        this.setState({isLoadingPosts: false});
    };

    render() {
        const {className, onTip, periodPrice, onShowMoreCommentsClick, period: {start, end, posts}} = this.props;
        const isSameYear = moment(start).isSame(end, 'year');
        let dateFormat;
        if (isSameYear) {
            dateFormat = 'MMMM DD';
        } else {
            dateFormat = 'MMMM DD YYYY';
        }

        return (
            <div className={cn(styles.container, className, {[styles.collapsed]: this.state.isCollapsed})}>
                <div className={styles.header} onClick={this.state.isCollapsed ? this.handleUncollapse : this.handleCollapse}>
                    <img src={blackLockImg} className={styles.lockImg} />
                    All posts from {moment(start).format(dateFormat)} - {moment(end).format(dateFormat)} {isSameYear && (moment(start).format('YYYY'))}
                    <img src={downArrowImg} className={styles.arrowImg} />
                </div>
                {
                    !this.state.isCollapsed
                        && <div className={styles.postsList}>
                            {
                                this.state.isLoadingPosts
                                    ? <SpinnerWithBackdrop />
                                    : (
                                        (posts.length > 0)
                                            ? posts.map(post => (
                                                <ArtistPost
                                                    key={post._id}
                                                    className={styles.post}
                                                    post={post}
                                                    blockedLabel={`Unlock ${moment(start).format(dateFormat)} - ${moment(end).format(dateFormat)} ${isSameYear ? (moment(start).format('YYYY')) : ''}`.toUpperCase()}
                                                    isBlockedInsidePeriod={true}
                                                    onTip={onTip}
                                                    onUnlockClick={this.handleUnlock}
                                                    onShowMoreCommentsClick={onShowMoreCommentsClick}
                                                />
                                            ))
                                            : <div>
                                                No posts for this period
                                            </div>
                                    )
                            }
                        </div>
                }
                <div className={styles.unlockButtonContainer}>
                    <button className={styles.unlockButton} onClick={this.handleUnlock}>
                        Unlock for ${roundToExactly(periodPrice / 100, 2)}
                    </button>
                </div>
            </div>
        )
    }
};

PeriodItem.propTypes = {
    className: PropTypes.string,
    period: PropTypes.shape({
        start: PropTypes.string.isRequired,
        end: PropTypes.string.isRequired,
        posts: PropTypes.array
    }),
    periodPrice: PropTypes.number.isRequired,
    onUncollapse: PropTypes.func.isRequired,
    onUnlock: PropTypes.func.isRequired,
    onTip: PropTypes.func.isRequired,
    onShowMoreCommentsClick: PropTypes.func.isRequired

};