import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import lockImg from '../../../../assets/images/lock-black.svg';

import styles from './PostBlockerMessage.module.scss';

export default class PostBlockerMessage extends React.Component {
    render() {
        const {className, onSubscribeClick} = this.props;

        return (
            <div className={cn(styles.container, className)}>
                <img src={lockImg} className={styles.lockImg} />
                If you want access to the last 30 days, and next 30 days of Brandt, Nash, and Drew's content, you need to <span className={styles.subscribe} onClick={onSubscribeClick}>subscribe</span>.
            </div>
        )
    }
}

PostBlockerMessage.propTypes = {
    className: PropTypes.string,
    onSubscribeClick: PropTypes.func.isRequired
};