import React from 'react';
import PropTypes from 'prop-types';
import {Field, reduxForm} from 'redux-form';
import {TextField} from 'redux-form-material-ui';
import Button from '@material-ui/core/Button';

import * as styles from '../EditArtistProfileForm/EditArtistProfileForm.module.scss';

class EditArtistProfileForm extends React.Component {

    handleFormSubmit = (values) => {
        const {onSubmit} = this.props;
        return onSubmit(values);
    };

    render() {
        const {handleSubmit} = this.props;
        return (
            <form className={styles.form} onSubmit={handleSubmit(this.handleFormSubmit)}>
                <Field
                    name='snapchat_link'
                    label='Snapchat'
                    component={TextField}
                    fullWidth
                    margin='dense'
                    helperText=' '
                />
                <Field
                    name='instagram_link'
                    label='Instagram'
                    component={TextField}
                    fullWidth
                    margin='dense'
                    helperText=' '
                />
                <Field
                    name='twitter_link'
                    label='Twitter'
                    component={TextField}
                    fullWidth
                    margin='dense'
                    helperText=' '
                />
                <Field
                    name='tumblr_link'
                    label='Tumblr'
                    component={TextField}
                    fullWidth
                    margin='dense'
                    helperText=' '
                />
                <div className={styles.buttonContainer}>
                    <Button
                        variant="raised"
                        color="primary"
                        type="submit"
                    >
                        Submit
                    </Button>
                </div>
            </form>
        )
    }
}

EditArtistProfileForm.propTypes = {
    onSubmit: PropTypes.func.isRequired
};

export default reduxForm({
    form: 'edit-artist-social-links-form',
    enableReinitialize: true
})(EditArtistProfileForm)
