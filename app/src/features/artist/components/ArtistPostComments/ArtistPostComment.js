import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames'
import moment from'moment';
import PostComment from '../../../../base-components/PostComment/PostComment';
import ReplyPostCommentForm from '../../../posts/components/ReplyPostCommentForm/ReplyPostCommentForm';

import styles from './ArtistPostComment.module.scss';

class ArtistPostComment extends React.Component {
    constructor(props) {
        super();

        this.state = {
            isEditing: false,
            comment: props.comment,
            isReply: false,
            replyValue: ''
        }
    }

    handleEditButtonClick = () => {
        this.setState({isEditing: true});
    };

    handleCommentChange = (e) => {
        this.setState({comment: e.target.value});
    };

    handleSubmitCommentEditClick = () => {
        const {postId, id, editComment, parentCommentId} = this.props;

        editComment(this.state.comment, id, postId, parentCommentId);
        this.setState({isEditing: false});
    };

    handleCancelCommentEditClick = () => {
        this.setState({
            isEditing: false,
            comment: this.props.comment,
        });
    };

    handleClickCommentLike = () => {
        const {isLoggedIn, isLiked, commentLike, id, parentCommentId} = this.props;
        if (isLoggedIn && !isLiked) {
            commentLike(id, parentCommentId)
        }
    };

    handleReplyButtonClick = () => {
        this.setState({ isReply: true });
    };

    handleSubmitReplyClick = value => {
        const { isLoggedIn, replyComment, id } = this.props;
        if (isLoggedIn && value.trim().length !== 0) {
            replyComment(value, id);
        }
        this.setState({
            isReply: false,
        });
    };

    getTopFans() {
        const {topFansList, _id} = this.props;
        let topFanIcon = '';
        let topFanPoints = null;
        let topFans = {};

        // object of only top fans
        for (let key in topFansList) {
            if(topFansList.hasOwnProperty(key) && topFansList) {
                topFans[key] = topFansList[key][0]
            }
        }

        for (let key in topFans) {
            if (topFans.hasOwnProperty(key) && topFans[key]) {
                if (topFans[key].user._id === _id) {
                    topFanIcon = key;
                    topFanPoints = topFans[key].points
                }
            }
        }
        return {
            icon: topFanIcon,
            points: topFanPoints
        }
    };

    render() {
        const {
            id,
            postId,
            username,
            avatar,
            createdData,
            comment,
            likesCount,
            isLiked,
            deleteComment,
            showDeleteBtn,
            showEditBtn,
            type,
            isUserOrCommentDeleted,
            parentCommentId,
            showReply,
        } = this.props;

        const a = moment();
        const b = moment(createdData);
        const dayPassed = a.diff(b, 'days') < 1;
        const timeComment = b.from(a);
        const { points, icon } = this.getTopFans();

        return (
            <div className={styles.commentsContainer}>
                {
                    this.state.isEditing
                        ? <div className={styles.commentEditContainer}>
                            <textarea
                                className={styles.editCommentTextarea}
                                value={this.state.comment}
                                onChange={this.handleCommentChange}/>
                            <div className={styles.btnGroup}>
                                <button className={styles.commentEditButton} onClick={this.handleCancelCommentEditClick}>
                                    Cancel
                                </button>
                                <button className={styles.commentEditButton} onClick={this.handleSubmitCommentEditClick}>
                                    Submit
                                </button>
                            </div>
                        </div>
                        : <div className={
                            classNames(styles.comment, {
                                [styles.replyComment]: type === 'REPLY'
                            })}>
                            {
                                isUserOrCommentDeleted ? <div className={styles.removedComment}>
                                        <p>Comment was removed</p>
                                        <span className={styles.hour}>
                            {dayPassed ? timeComment : moment(createdData).format('MMM DD, h:mm a')}
                                </span>
                                    </div>:
                                    <React.Fragment>
                                        <PostComment
                                            avatar={avatar}
                                            username={username}
                                            comment={comment}
                                            createdData={createdData}
                                            likesCount={likesCount}
                                            topFanIcon={points && icon}
                                            pointsCount={points}
                                        >
                                            {
                                                showReply && type === 'COMMENT' ? <button
                                                    className={styles.reply}
                                                    onClick={this.handleReplyButtonClick}
                                                >Reply</button> : null
                                            }
                                            {
                                                showReply && this.state.isReply ?
                                                    <ReplyPostCommentForm
                                                        placeholder="Leave a reply"
                                                        onSubmitButtonClick={this.handleSubmitReplyClick}
                                                    /> : null
                                            }
                                        </PostComment>
                                    </React.Fragment>

                            }
                            {
                                !isUserOrCommentDeleted && <div className={styles.likesBox}>
                                <span
                                    onClick={this.handleClickCommentLike}
                                    className={classNames({
                                        [styles.likesActive]: isLiked,
                                        [styles.likes]: true
                                    })}/>
                                </div>
                            }
                            <div className={styles.buttonsContainer}>
                                {
                                    showEditBtn && <button
                                        className={styles.editButton}
                                        onClick={this.handleEditButtonClick}
                                    >Edit</button>
                                }
                                {
                                    showDeleteBtn && <button
                                        className={styles.deleteButton}
                                        onClick={() => deleteComment(id, postId, parentCommentId)}
                                    > Delete </button>
                                }
                            </div>
                        </div>
                }
                {
                    this.props.children
                }
            </div>
        );
    };
}

ArtistPostComment.propTypes = {
    id: PropTypes.string.isRequired,
    postId: PropTypes.string.isRequired,
    comment: PropTypes.string,
    createdData: PropTypes.string.isRequired,
    avatar: PropTypes.string,
    username: PropTypes.string,
    deleteComment: PropTypes.func.isRequired,
    editComment: PropTypes.func.isRequired,
    replyComment: PropTypes.func.isRequired,
    likesCount: PropTypes.number.isRequired,
    isLoggedIn: PropTypes.bool.isRequired,
    isLiked: PropTypes.bool,
    showEditBtn: PropTypes.bool,
    showDeleteBtn: PropTypes.bool.isRequired,
    showReply: PropTypes.bool,
    type: PropTypes.string.isRequired,
    isUserOrCommentDeleted: PropTypes.bool,
};

export default ArtistPostComment;
