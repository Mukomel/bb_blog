import * as constants from './constants';
import * as uiConstants from '../ui/constants';

const initialState = {
    artistProfile: null,
    artistPosts: [],
    artistAuthors: [],
    artistPostsWithinPeriods: [],
    hasMorePosts: true,
    periodUnlockError: null,
    isFetchingArtist: false,
    isUpdatingArtist: false,
    isFetchingPosts: false,
    isFetchingPinnedPost: false,
    isFetchingStats: false,
    isRequestingPeriodUnlock: false,
    isUpdatingSchedule: false,
    isCreatingMemberStatsMailing: false,
    isMemberStatsMailingCreated: false,
    isUserDonationsDetailsFetching: false,
    notificationCount: 0,
    isFetchingNotifications: false,
    notifications: [],
    isSendingMassNotifications: false,
    tipJar: null,
    isUpdatingTipJar: false,
    statsPerPost: [],
    statsPerFans: null,
    statsPerAuthors: [],
    authorPostsFilter: {value: null},
    transactionStats: [],
    memberStats: [],
    artistsList: [],
    userDonationsList: []
};

export default function(state = initialState, action) {
    switch(action.type) {

        case (constants.GET_ARTIST_REQUEST):
            return Object.assign({}, state, {
                artistProfile: null,
                isFetchingArtist: true
            });

        case (constants.GET_ARTIST_FAILURE):
            return Object.assign({}, state, {
                isFetchingArtist: false
            });

        case (constants.GET_ARTIST_SUCCESS):
            return Object.assign({}, state, {
                artistProfile: action.artist,
                isFetchingArtist: false
            });

        case (constants.GET_ARTIST_POSTS_REQUEST):
            return Object.assign({}, state, {
                artistPosts: action.lastPostId ? state.artistPosts : [],
                hasMorePosts: false,
                isFetchingPosts: true
            });

        case (constants.GET_ARTIST_POSTS_FAILURE):
        case (constants.CREATE_POST_COMMENT_FAILURE):
            console.log('error', action.body.error);
            return Object.assign({}, state, {
                isFetchingPosts: false
            });

        case (constants.GET_ARTIST_POSTS_SUCCESS):
            return Object.assign({}, state, {
                artistPosts: state.artistPosts.concat(action.posts),
                hasMorePosts: action.posts.length === constants.POSTS_PER_PAGE,
                isFetchingPosts: false
            });

        case (constants.GET_ARTIST_AUTHORS_SUCCESS):
            return Object.assign({}, state, {
                artistAuthors: action.authors
            });

        case (constants.SUBSCRIBE_ARTIST_SUCCESS):
            const artistAuthorsSubscribe = state.artistAuthors.map(author => {
                if (action.userId === author._id) {
                    return Object.assign({}, author, {is_subscribed: true})
                }
                return author;
            });
            return Object.assign({}, state, {
                artistAuthors: artistAuthorsSubscribe
            });

        case (constants.UNSUBSCRIBE_ARTIST_SUCCESS):
            const artistAuthorsUnsubscribe = state.artistAuthors.map(author => {
                if (action.userId === author._id) {
                    return Object.assign({}, author, {is_subscribed: false})
                }
                return author;
            });

            return Object.assign({}, state, {
                artistAuthors: artistAuthorsUnsubscribe
            });

        case (constants.GET_ARTIST_POSTS_BY_PERIOD_SUCCESS):
            return Object.assign({}, state, {
                artistPostsWithinPeriods: state.artistPostsWithinPeriods.concat({
                    posts: action.posts,
                    start: action.periodStartDate,
                    end: action.periodEndDate
                })
            });

        case (constants.BUY_POSTS_FOR_PERIOD_REQUEST):
        case (constants.CREATE_POST_COMMENT_REQUEST):
            return Object.assign({}, state, {
                isRequestingPeriodUnlock: true
            });

        case (constants.BUY_POSTS_FOR_PERIOD_SUCCESS):
            return Object.assign({}, state, {
                isRequestingPeriodUnlock: false
            });

        case (constants.BUY_POSTS_FOR_PERIOD_FAILURE):
        case (constants.POST_LIKE_FAILURE):
            return Object.assign({}, state, {
                isRequestingPeriodUnlock: false,
                periodUnlockError: action.errorMessage
            });

        case (uiConstants.CHANGE_PERIOD_UNLOCK_MODAL_STEP):
        case (uiConstants.HIDE_MODAL):
            return Object.assign({}, state, {
                periodUnlockError: null
            });

        case (constants.POST_LIKE_SUCCESS):
            const posts = state.artistPosts.map(item => {
                if(item._id === action.postId) {
                    return {
                        ...item,
                        likes_count: item.likes_count + 1,
                        is_liked: true
                    };
                }
                return item;
            });
            return {
                ...state,
                isRequestingPeriodUnlock: false,
                artistProfile: {...state.artistProfile, ...{likes_count: state.artistProfile.likes_count + 1} },
                artistPosts: posts
            };

        case (constants.COMMENT_LIKE_SUCCESS):
            const artistPosts = state.artistPosts.map(post => {
                const comments = post.comments.map(comment => {
                    if (comment._id === action.parentCommentId) {
                        const replies = comment.replies.map(reply => {
                            if (reply._id === action.commentId) {
                                return {
                                    ...reply,
                                    likes_count: reply.likes_count + 1,
                                    is_liked: true
                                };
                            }
                            return reply;
                        });
                        return {
                            ...comment,
                            replies
                        };
                    }

                    if(comment._id === action.commentId) {
                        return {
                            ...comment,
                            likes_count: comment.likes_count + 1,
                            is_liked: true
                        };
                    }
                    return comment
                });
                return {
                    ...post,
                    comments
                };
            });
            return {
                ...state,
                artistPosts
            };

        case (constants.CREATE_POST_COMMENT_SUCCESS):
            const tmpArray = state.artistPosts.slice();
            const findPost = tmpArray.find(item => item._id === action.body.post);
            findPost.comments.unshift(action.body);
            return {
                ...state,
                isRequestingPeriodUnlock: false,
                artistPosts: state.artistPosts.map(post => {
                    if (post._id === action.body.post) {
                        return {
                            ...post,
                            comments: findPost.comments,
                            comments_count: post.comments_count + 1
                        }
                    }
                    return post;
                })
            };

        case (constants.GET_POST_COMMENTS_SUCCESS):
            return Object.assign({}, state, {
                artistPosts: state.artistPosts.map(post => {
                    if (post._id === action.postId) {
                        return Object.assign({}, post, {
                            comments: action.comments,
                            has_more_comments: false
                        })
                    } else {
                        return post
                    }
                }),
                artistPostsWithinPeriods: state.artistPostsWithinPeriods.map(period => {
                    return Object.assign({}, period, {
                        posts: period.posts.map(post => {
                            if (post._id === action.postId) {
                                return Object.assign({}, post, {
                                    comments: action.comments,
                                    has_more_comments: false
                                })
                            } else {
                                return post
                            }
                        })
                    })
                })
            });

        case (constants.DELETE_POST_COMMENT_SUCCESS):
            const tmp = state.artistPosts.slice();
            const post = tmp.find(item => item._id === action.postId);
            let filteredComments = [];
            if (action.parentCommentId) {
                 filteredComments = post.comments.map(comment => {
                    if (action.parentCommentId && comment._id === action.parentCommentId) {
                        const replies = comment.replies.filter(reply => reply._id !== action.commentId);
                        return Object.assign({}, comment, { replies })
                    }
                    return comment;
                });
            } else {
                filteredComments = post.comments.filter(comment => comment._id !== action.commentId)
            }
            return {
                ...state,
                artistPosts: state.artistPosts.map(post => {
                    if (post._id === action.postId) {
                        return {
                            ...post,
                            comments: filteredComments,
                        }
                    }
                    return post;
                })
            };

        case (constants.GET_ARTIST_PINNED_POST_REQUEST):
            return Object.assign({}, state, {
                isFetchingPinnedPost: true
            });

        case (constants.GET_ARTIST_PINNED_POST_SUCCESS):
            return Object.assign({}, state, {
                isFetchingPinnedPost: false,
                artistPosts: action.pinnedPost ? [].concat(action.pinnedPost, state.artistPosts) : state.artistPosts
            });

        case (constants.GET_ARTIST_PINNED_POST_FAILURE):
            return Object.assign({}, state, {
                isFetchingPinnedPost: false
            });

        case (constants.EDIT_POST_COMMENT_SUCCESS):
            const updater = ((post) => {
                if (post._id === action.postId) {
                    return Object.assign({}, post, {
                        comments: post.comments.map(comment => {
                            if (comment._id === action.parentCommentId) {
                                const replies = comment.replies.map(reply => {
                                    if (reply._id === action.commentId) {
                                        return {
                                            ...reply,
                                            comment: action.text
                                        };
                                    }
                                    return reply;
                                });
                                return {
                                    ...comment,
                                    replies
                                };
                            }
                            if (comment._id === action.commentId) {
                                return Object.assign({}, comment, {
                                    comment: action.text
                                })
                            } else {
                                return comment;
                            }
                        })
                    })
                } else {
                    return post
                }
            });

            return Object.assign({}, state, {
                artistPosts: state.artistPosts.map(updater),
                artistPostsWithinPeriods: state.artistPostsWithinPeriods.map(period => {
                    return Object.assign({}, period, {
                        posts: period.posts.map(updater)
                    })
                })
            });

        case (constants.REPLY_POST_COMMENT_SUCCESS):
            const artistPost = state.artistPosts.map(post => {
                const comments = post.comments.map(comment => {
                    if(comment._id === action.commentId) {
                        const copyComments = Object.assign({}, comment);
                        copyComments.replies.unshift(...action.body);
                        return copyComments
                    }
                    return comment
                });
                return {
                    ...post,
                    comments
                };
            });
            return {
                ...state,
                artistPosts: artistPost
            };

        case (constants.UPDATE_ARTIST_REQUEST):
            return Object.assign({}, state, {
                isUpdatingArtist: true
            });

        case (constants.UPDATE_ARTIST_SUCCESS):
            return Object.assign({}, state, {
                isUpdatingArtist: false,
                artistProfile: action.artistProfile
            });

        case (constants.UPDATE_ARTIST_FAILURE):
            return Object.assign({}, state, {
                isUpdatingArtist: false
            });

        case (constants.UPDATE_SCHEDULE_REQUEST):
            return Object.assign({}, state, {
                isUpdatingSchedule: true
            });

        case (constants.UPDATE_SCHEDULE_SUCCESS):
            return Object.assign({}, state, {
                isUpdatingSchedule: false,
                artistProfile: Object.assign({}, state.artistProfile, {
                    schedule: action.schedule
                })
            });

        case (constants.UPDATE_SCHEDULE_FAILURE):
            return Object.assign({}, state, {
                isUpdatingSchedule: false
            });

        case (constants.GET_NOTIFICATION_COUNT_SUCCESS):
            return Object.assign({}, state, {
                notificationCount: action.count
            });

        case (constants.GET_NOTIFICATIONS_REQUEST):
            return Object.assign({}, state, {
                isFetchingNotifications: true,
            });

        case (constants.GET_NOTIFICATIONS_SUCCESS):
            return Object.assign({}, state, {
                notificationCount: 0,
                isFetchingNotifications: false,
                notifications: action.notifications
            });

        case (constants.GET_NOTIFICATIONS_FAILURE):
            return Object.assign({}, state, {
                isFetchingNotifications: false
            });

        case (constants.CLEAR_NOTIFICATIONS):
            return Object.assign({}, state, {
                notifications: []
            });

        case (constants.POST_MASS_MAILING_SUCCESS):
        case (constants.POST_MASS_MAILING_FAILURE):
            return Object.assign({}, state, {
                isSendingMassNotifications: false
            });

        case (constants.POST_MASS_MAILING_REQUEST):
            return Object.assign({}, state, {
                isSendingMassNotifications: true
            });

        case (constants.POST_TIP_JAR_REQUEST):
            return Object.assign({}, state, {
                isUpdatingTipJar: true
            });

        case (constants.POST_TIP_JAR_SUCCESS):
            return Object.assign({}, state, {
                isUpdatingTipJar: false,
                tipJar: action.tipJar,
            });

        case (constants.POST_TIP_JAR_FAILURE):
            return Object.assign({}, state, {
                isUpdatingTipJar: false
            });

        case (constants.GET_TIP_JAR_SUCCESS):
            return Object.assign({}, state, {
                tipJar: action.tipJar
            });

        case (constants.GET_TIP_JAR_FAILURE):
        case (constants.COMPLETE_TIP_JAR_SUCCESS):
            return Object.assign({}, state, {
                tipJar: null
            });

        case (constants.SWITCH_TIP_JAR_SUCCESS):
            return Object.assign({}, state, {
                artistProfile: Object.assign({}, state.artistProfile, {
                    tip_jar_enabled: action.tipJarEnabled
                }),
            });

        case (constants.GET_STATS_PER_POSTS_REQUEST):
        case (constants.GET_STATS_PER_FANS_REQUEST):
        case (constants.GET_STATS_PER_AUTHORS_REQUEST):
        case (constants.GET_MEMBERS_STATS_REQUEST):
            return Object.assign({}, state, {
                isFetchingStats: true
            });

        case (constants.GET_STATS_PER_POSTS_FAILURE):
        case (constants.GET_STATS_PER_FANS_FAILURE):
        case (constants.GET_STATS_PER_AUTHORS_FAILURE):
        case (constants.GET_TRANSACTION_STATS_FAILURE):
        case (constants.GET_MEMBERS_STATS_FAILURE):
            return Object.assign({}, state, {
                isFetchingStats: false
            });

        case (constants.GET_STATS_PER_POSTS_SUCCESS):
            return Object.assign({}, state, {
                statsPerPost: action.body,
                isFetchingStats: false
            });

        case (constants.GET_STATS_PER_FANS_SUCCESS):
            return Object.assign({}, state, {
                statsPerFans: action.body,
                isFetchingStats: false
            });

        case (constants.GET_STATS_PER_AUTHORS_SUCCESS):
            return Object.assign({}, state, {
                statsPerAuthors: action.body,
                isFetchingStats: false
            });

        case (constants.GET_MEMBERS_STATS_SUCCESS):
            return Object.assign({}, state, {
                memberStats: Object.assign({}, state.memberStats, action.response),
                isFetchingStats: false
            });


        case (constants.CHANGE_AUTHOR_POSTS_FILTER):
            return Object.assign({}, state, {
                authorPostsFilter: action.filter
            });

        case (constants.GET_TRANSACTION_STATS_SUCCESS):
            return Object.assign({}, state, {
                transactionStats: action.response,
                isFetchingStats: false
            });

        case (constants.GET_ARTISTS_SUCCESS):
            return Object.assign({}, state, {
                artistsList: action.artists
            });

        case (constants.CREATE_MEMBERS_STATS_MASS_MAILING_REQUEST):
            return Object.assign({}, state, {
                isCreatingMemberStatsMailing: true
            });

        case (constants.CREATE_MEMBERS_STATS_MASS_MAILING_SUCCESS):
            return Object.assign({}, state, {
                isCreatingMemberStatsMailing: false,
                isMemberStatsMailingCreated: true
            });

        case (constants.GET_USER_DONATIONS_DETAILS_REQUEST):
            return Object.assign({}, state, {
                isUserDonationsDetailsFetching: true,
                userDonationsList: []
            });

        case (constants.GET_USER_DONATIONS_DETAILS_SUCCESS):
            return Object.assign({}, state, {
                isUserDonationsDetailsFetching: false,
                userDonationsList: action.donations
            });

        default:
            return state;
    }
}
