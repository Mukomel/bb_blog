import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import CropperEditor from '../../../../base-components/CropperEditor/CropperEditor';
import placeholderImage from '../../../../assets/images/dropzone-image-grey.svg';
import {calcDimensions} from '../../../../helpers/calcDimensions';

export default class CropperModal extends Component {
    static propTypes = {
        aspectRatio: PropTypes.number,
        circled: PropTypes.bool,
        accept: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
        sizes: PropTypes.shape({
            width: PropTypes.number,
            height: PropTypes.number
        }),
        hideCropperDialog: PropTypes.func
    };

    state = {
        imageBlog: null
    };


    shouldComponentUpdate(nextProps) {
        return nextProps.aspectRatio !== this.props.aspectRatio
    }


    handleImageBlobChange = (imageBlog) => {
        this.setState({imageBlog})
    };

    handleSaveButtonClick = () => {
        const {input, hideCropperDialog} = this.props;
        const {imageBlog} = this.state;

        if (imageBlog) {
            input.onChange(imageBlog);
            hideCropperDialog()
        }
    };

    render() {
        const {isOpen, onHide, accept, aspectRatio, circled, sizes, ...rest} = this.props;

        return (
            <Dialog
                fullWidth
                open={isOpen}
                onClose={onHide}
            >
                <DialogTitle>Select Image</DialogTitle>
                <DialogContent>
                    <CropperEditor
                        cropperHeight={sizes ? calcDimensions(sizes).height : undefined}
                        cropperWidth={sizes ? calcDimensions(sizes).width : undefined}
                        aspectRatio={aspectRatio}
                        update={this.handleImageBlobChange}
                        accept={accept}
                        image={<img src={placeholderImage} alt="select photo"/>}
                        circled={circled}
                        {...rest}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={onHide}>Cancel</Button>
                    <Button
                        onClick={this.handleSaveButtonClick}
                        color="primary">
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}
