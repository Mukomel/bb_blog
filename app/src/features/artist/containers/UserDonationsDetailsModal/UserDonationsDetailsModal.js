import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {submit} from 'redux-form';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import {getUserDonationsDetailsRequest} from '../../actions';
import UserDonationsDetailsTable from '../../components/UserDonationsDetailsTable/UserDonationsDetailsTable';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';

import styles from './UserDonationsDetailsModal.module.scss';

class UserConationsDetailsModal extends React.Component {
    componentDidUpdate(prevProps) {
        const {getUserDonationsDetails, userId, period, isOpen} = this.props;

        if (isOpen && userId && period && ((userId !== prevProps.userId) || (period !== prevProps.period))) {
            getUserDonationsDetails(userId, period)
        }
    }

    handleOkClick = () => {
        this.props.onHide();
    };


    render() {
        const {isOpen, username, period, isUserDonationsDetailsFetching, userDonationsList} = this.props;

        return <Dialog
            disableBackdropClick
            disableEscapeKeyDown
            open={isOpen}
            fullWidth={true}
        >
            <DialogTitle>{username} donations ({period})</DialogTitle>
            <DialogContent>
                <UserDonationsDetailsTable donations={userDonationsList} />
                {
                    isUserDonationsDetailsFetching
                        && <SpinnerWithBackdrop />
                }
            </DialogContent>
            <DialogActions>
                <Button
                    onClick={this.handleOkClick}
                    variant="raised"
                    color="primary"
                >
                    Ok
                </Button>
            </DialogActions>
        </Dialog>
    }
}

UserConationsDetailsModal.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onHide: PropTypes.func.isRequired,
    userId: PropTypes.string,
    username: PropTypes.string,
    period: PropTypes.string,
};

export default connect(
    (state) => ({
        isUserDonationsDetailsFetching: state.artist.isUserDonationsDetailsFetching,
        userDonationsList: state.artist.userDonationsList,
    }),
    (dispatch) => ({
        getUserDonationsDetails: (userId, period) => dispatch(getUserDonationsDetailsRequest(userId, period))
    })
)(UserConationsDetailsModal)
