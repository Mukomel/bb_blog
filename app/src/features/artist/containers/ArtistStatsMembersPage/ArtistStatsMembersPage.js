import React from 'react'
import moment from 'moment'
import {connect} from 'react-redux'
import orderBy from 'lodash/orderBy'
import Button from '@material-ui/core/Button'
import {DateRange} from 'react-date-range';
import ArtistStatsMembersTable from '../../components/ArtistStatsMembersTable/ArtistStatsMembersTable'
import CustomSelect from '../../../../base-components/CustomSelect/CustomSelect'
import {STATS_MEMBERS_STATUS, MEMBER_STATS_DATE_RANGES} from "../../constants";
import {getMembersStatsRequest, getArtistsRequest} from "../../actions";
import {getSegPayPromoCodesRequest} from "../../../subscribe/actions";
import {showModal} from "../../../ui/actions";
import {modalTypes} from "../../../ui/constants";
import {userRoles} from "../../../auth/constants";

import * as styles from './ArtistStatsMembersPage.module.scss'

class ArtistStatsMembersPage extends React.Component {
    constructor(props) {
        super();

        this.state = {
            users_type: 'All',
            promo_code: 'All',
            query: {},
            artist: (props.user.role === userRoles.ADMIN) ? props.user.artist.slug : null,
            dateRange: MEMBER_STATS_DATE_RANGES.ALL,
            isDatePickerOpen: false,
            ranges: {
                rangeColors: [
                    "#3d91ff",
                    "#3ecf8e",
                    "#fed14c"
                ],
                startDate: new Date(),
                endDate: new Date(),
                key: 'selection'
            },
        };
    }

    componentDidMount() {
        const {getStatsMembers, getArtists, memberStats, promoCodesList, getSegPayPromoCodes, user} = this.props;
        if (!memberStats.length > 0) getStatsMembers({page: 1, show_total_users_count: true});
        if (!promoCodesList.length > 0) getSegPayPromoCodes((user.role === userRoles.ADMIN) ? true : undefined);
        if (user.role === userRoles.ADMIN) getArtists()
    }

    handleSelectPage = page => {
        let query = Object.assign({}, this.state.query);
        query.page = page;
        this.setState({query});
        this.props.getStatsMembers(query);
    };

    handleSelectChange = ({target: {value}, target}, field) => {
        let page = 1;
        let query = Object.assign({}, this.state.query);
        query.page = page;
        if (target && field) {
            if (field === 'users_type') {
                switch(value) {
                    case('Active Subscribers'):
                        query.users_type = 'ACTIVE';
                        break;
                    case('Canceled'):
                        query.users_type = 'CANCELED';
                        break;
                    case('Registered'):
                        query.users_type = 'REGISTERED';
                        break;
                    case('All'):
                    default:
                        delete query.users_type;
                }
                this.setState({users_type: value});
            } else if (field === 'promo_code') {
                if (value === 'All') {
                    delete query.promo_code;
                } else if(value === 'No Code'){
                    query.promo_code = 'no_code';
                } else {
                    query.promo_code = this.props.promoCodesList.filter(promocode => promocode.code === value)[0]._id;
                }
                this.setState({promo_code: value});
            } else if (field === 'artist') {
                delete query.promo_code;
                query.artist = this.props.artistsList.find(artist => artist.slug === value)._id;
                this.setState({artist: value, promo_code: 'All'});
            } else if (field === 'dateRange') {
                if (value === MEMBER_STATS_DATE_RANGES.ALL) {
                    this.setState({dateRange: value});
                    delete query.period_first_day;
                    delete query.period_last_day;
                } else if (value === MEMBER_STATS_DATE_RANGES.CUSTOM) {
                    this.setState({dateRange: value, isDatePickerOpen: true});
                    return;
                }
            }
        }
        this.setState({query});
        this.props.getStatsMembers({...query, show_total_users_count: true});
    };

    handleDatePickerChange = (ranges) => {
        this.setState({
            ranges: ranges.selection
        })
    };

    handleDatePickerButtonClick = () => {
        const {startDate, endDate} = this.state.ranges;
        let query = Object.assign({}, this.state.query, {page: 1});

        query.period_first_day = moment(startDate).toISOString();
        query.period_last_day = moment(endDate).toISOString();

        this.setState({
            query,
            isDatePickerOpen: false
        });
        this.props.getStatsMembers({...query, show_total_users_count: true});
    };

    handleShowMailingModal = () => {
        const {showMailingModal} = this.props;

        showMailingModal(this.state.query);
    };

    render() {
        const {isFetchingStats, memberStats, promoCodesList, artistsList, user} = this.props;
        const { ranges } = this.state;
        //todo with promocodes
        const artistBySlugFromState = artistsList.find(artist => artist.slug === this.state.artist);
        const currentArtistId = (user.role === userRoles.ADMIN)
            ? artistBySlugFromState && artistBySlugFromState._id
            : user.artist._id;
        const promocodes = promoCodesList
            .filter(promocode => promocode.artist === currentArtistId)
            .map(promocode => promocode.code);
        const orderedCollection = orderBy(memberStats.users, ['reg_date'], ['desc']);
        return (
            <div className={styles.fansContainer}>
                <h2 className={styles.title}>Fans</h2>
                <div>
                    <CustomSelect
                        data={[
                            MEMBER_STATS_DATE_RANGES.ALL,
                            MEMBER_STATS_DATE_RANGES.CUSTOM
                        ]}
                        value={this.state.dateRange}
                        onSelectChange={e => this.handleSelectChange(e, 'dateRange')}
                        label="Date Range"
                    />
                    <CustomSelect
                        data={STATS_MEMBERS_STATUS}
                        onSelectChange={e => this.handleSelectChange(e, 'users_type')}
                        value={this.state.users_type}
                        label="Status"
                    />
                    {
                        ((user.role === userRoles.ADMIN) && (artistsList.length > 0))
                            && <CustomSelect
                                data={artistsList.map(artist => artist.slug)}
                                onSelectChange={e => this.handleSelectChange(e, 'artist')}
                                value={this.state.artist}
                                label="Artist"
                            />
                    }
                    {this.state.users_type !== 'All' ? <CustomSelect
                        data={['All', 'No Code', ...promocodes]}
                        value={this.state.promo_code}
                        label="Promo Code"
                        onSelectChange={e => this.handleSelectChange(e, 'promo_code')}
                    /> : null}
                    {
                        (user.role === userRoles.ADMIN)
                            && <Button
                                variant="contained"
                                color="primary"
                                onClick={this.handleShowMailingModal}
                            >
                                Send email
                            </Button>
                    }
                    {
                        this.state.isDatePickerOpen &&
                            <div className={styles.picker}>
                                <DateRange
                                    className={styles.dateRange}
                                    ranges={[this.state.ranges]}
                                    onChange={this.handleDatePickerChange}
                                />
                                <Button
                                    variant="contained"
                                    color="primary"
                                    onClick={this.handleDatePickerButtonClick}
                                >
                                    Ok
                                </Button>
                            </div>
                    }
                </div>
                <div className={styles.dateInfo}>
                {this.state.dateRange === MEMBER_STATS_DATE_RANGES.CUSTOM ?
                    moment(ranges.startDate).format('l') + " - " + moment(ranges.endDate).format('l')
                    : null
                }
                </div>
                <ArtistStatsMembersTable
                    currentPage={memberStats.current_page}
                    isFetchingStats={isFetchingStats}
                    data={orderedCollection}
                    total={memberStats.total}
                    changePage={this.handleSelectPage}
                />
            </div>
        )
    }
}

export default connect(
    state => ({
        user: state.auth.user,
        artistsList: state.artist.artistsList,
        memberStats: state.artist.memberStats,
        isFetchingStats: state.artist.isFetchingStats,
        promoCodesList: state.subscribe.promoCodesList
    }),
    dispatch => ({
        getStatsMembers: query => dispatch(getMembersStatsRequest(query)),
        getSegPayPromoCodes: (allArtists) => dispatch(getSegPayPromoCodesRequest(allArtists)),
        getArtists: () => dispatch(getArtistsRequest()),
        showMailingModal: (query) => dispatch(showModal(modalTypes.MEMBERS_STATS_MAILING_MODAL, {query}))
    })
)(ArtistStatsMembersPage)
