import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {submit} from 'redux-form';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import ArtistStatsMembersMailingForm from '../../components/ArtistStatsMembersMailingForm/ArtistStatsMembersMailingForm';
import {createMembersStatsMassMailingRequest} from '../../actions';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';

import styles from './ArtistStatsMembersMailingModal.module.scss';

class ArtistStatsMembersMailingModal extends React.Component {
    constructor() {
        super();

        this.state = {
            isFormValid: false,
            isFormSubmitting: false,
        }
    }

    handleFormValidChange = (valid) => {
        this.setState({isFormValid: valid})
    };

    handleFormSubmittingChange = (submitting) => {
        this.setState({isFormSubmitting: submitting})
    };

    handleCancelClick = () => {
        this.props.onHide();
        this.setState({isFormValid: false, isFormSubmitting: false});
    };

    handleFormSubmit = (values) => {
        const {query, createMembersStatsMassMailing} = this.props;
        createMembersStatsMassMailing({...values, ...query});
    };

    render() {
        const {isOpen, submitMailingForm, isCreatingMemberStatsMailing, isMemberStatsMailingCreated} = this.props;

        let dialogTitle, dialogContent, dialogActions;
        dialogTitle = <DialogTitle>Mass Emails/Notifications</DialogTitle>;
        if (isMemberStatsMailingCreated) {
            dialogContent = <DialogContent>
                Mailing created
            </DialogContent>;
            dialogActions = <DialogActions>
                <Button
                    onClick={this.handleCancelClick}
                    variant="raised"
                    color="primary"
                >
                    Ok
                </Button>
            </DialogActions>
        } else {
            dialogContent = <DialogContent>
                <ArtistStatsMembersMailingForm
                    onFormValidChange={this.handleFormValidChange}
                    onFormSubmittingChange={this.handleFormSubmittingChange}
                    onSubmit={this.handleFormSubmit}
                />
                {
                    isCreatingMemberStatsMailing
                        && <SpinnerWithBackdrop />
                }
            </DialogContent>;
                dialogActions = <DialogActions>
                    <Button
                        onClick={this.handleCancelClick}
                    >
                        Cancel
                    </Button>
                    <Button
                        onClick={submitMailingForm}
                        variant="raised"
                        color="primary"
                        disabled={!this.state.isFormValid}
                    >
                        Send
                    </Button>
                </DialogActions>
        }

        return <Dialog
            disableBackdropClick
            disableEscapeKeyDown
            open={isOpen}
            fullWidth={true}
        >
            {dialogTitle}
            {dialogContent}
            {dialogActions}
        </Dialog>
    }
}

ArtistStatsMembersMailingModal.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onHide: PropTypes.func.isRequired,
    query: PropTypes.object
};

export default connect(
    (state) => ({
        isCreatingMemberStatsMailing: state.artist.isCreatingMemberStatsMailing,
        isMemberStatsMailingCreated: state.artist.isMemberStatsMailingCreated,
    }),
    (dispatch) => ({
        submitMailingForm: () => dispatch(submit('artistStatsMembersMailingForm')),
        createMembersStatsMassMailing: (payload) => dispatch(createMembersStatsMassMailingRequest(payload))
    })
)(ArtistStatsMembersMailingModal)
