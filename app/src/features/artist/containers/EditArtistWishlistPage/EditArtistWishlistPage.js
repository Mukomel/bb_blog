import React from 'react';
import {connect} from 'react-redux';
import Paper from '@material-ui/core/Paper';
import EditArtistWishlistForm from '../../components/EditArtistWishlistForm/EditArtistWishlistForm'
import {getArtistRequest, updateArtistWishlistRequest} from '../../actions';

import styles from './EditArtistWishlistPage.module.scss';

class EditArtistWishlistPage extends React.Component {
    componentDidMount = () => {
        const {getArtist, currentUser} = this.props;

        getArtist(currentUser.artist.slug);
    };

    handleEditArtistWishlistFormSubmit = (values) => {
        const {updateArtistWishlist, currentUser} = this.props;

        return new Promise((resolve, reject) => {
            updateArtistWishlist({values, resolve, reject, slug: currentUser.artist.slug});
        })
    };

    render() {
        const {artistProfile} = this.props;
        const editArtistWishlistFormInitialValues = (artistProfile && artistProfile.wishlist)
            ? {
                title: artistProfile.wishlist.title,
                description: artistProfile.wishlist.description,
                link: artistProfile.wishlist.link,
                is_enabled: artistProfile.wishlist.is_enabled,
            }
            : {};

        return (
            <div className={styles.container}>
                <Paper className={styles.paper}>
                    <div className={styles.title}>Group wishlist</div>
                    <EditArtistWishlistForm
                        onSubmit={this.handleEditArtistWishlistFormSubmit}
                        initialValues={editArtistWishlistFormInitialValues}
                    />
                </Paper>
            </div>
        )
    }
}

export default connect(
    state => ({
        currentUser: state.auth.user,
        artistProfile: state.artist.artistProfile
    }),
    dispatch => ({
        getArtist: (slug) => dispatch(getArtistRequest(slug)),
        updateArtistWishlist: (payload) => dispatch(updateArtistWishlistRequest(payload))
    })
)(EditArtistWishlistPage)
