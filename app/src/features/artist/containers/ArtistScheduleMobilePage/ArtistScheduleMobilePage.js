import React from 'react';
import {connect} from 'react-redux';
import {getArtistRequest} from "../../actions";
import {userRoles} from "../../../auth/constants";
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';
import BackgroundImage from '../../../../base-components/BackgroundImage/BackgroundImage';
import Header from '../../../landing/components/Layout/Header';
import Footer from '../../../landing/components/Layout/Footer'
import ArtistScheduleList from '../../../artist/components/ArtistScheduleList/ArtistScheduleList'
import calendarImage from '../../../../assets/images/calendar_white.svg';

import styles from './ArtistScheduleMobilePage.module.scss';

class ArtistScheduleMobilePage extends React.Component {
    componentWillMount() {
        const {artistSlug, getArtist, artistProfile} = this.props;
        if (!artistProfile) getArtist(artistSlug);
    }

    handleBackButtonClick = () => {
        const {history, artistSlug} = this.props;
        history.push(`/${artistSlug}`);
    };

    render() {
        const {isFetchingArtist, artistProfile} = this.props;

        let content;

        if (isFetchingArtist) {
            content = <SpinnerWithBackdrop />
        } else if (!artistProfile) {
            content = <div>Artist not found</div>
        } else {
            content = <div>
                <div className={styles.header}>
                    <BackgroundImage
                        className={styles.cover}
                        url={artistProfile.cover}
                        size='cover'
                        attachment='scroll'
                    />
                    <div className={styles.headerContent}>
                        <div className={styles.titleContainer}>
                            <img src={calendarImage} className={styles.calendarImage} />
                            This week plans
                        </div>
                        <div className={styles.description}>Find out what we are preparing for you</div>
                    </div>
                </div>
                <div className={styles.scheduleListContainer}>
                    <ArtistScheduleList schedule={artistProfile.schedule}/>
                </div>
            </div>
        }

        return (
            <div className={styles.container}>
                <Header
                    isOverlay={true}
                    withBackground={Boolean(artistProfile)}
                    backgroundImgUrl={artistProfile && artistProfile.cover}
                    showBackButton={true}
                    onBackButtonClick={this.handleBackButtonClick}
                />
                {content}
                <Footer />
            </div>
        )
    }
}

export default connect(
    (state, ownProps) => ({
        artistSlug: ownProps.match.params.slug,
        artistProfile: state.artist.artistProfile,
        isFetchingArtist: state.artist.isFetchingArtist,
        user: state.auth.user,
    }),
    dispatch => ({
        getArtist: slug => dispatch(getArtistRequest(slug))
    })
)(ArtistScheduleMobilePage)

