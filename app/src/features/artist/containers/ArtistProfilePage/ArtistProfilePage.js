import React from 'react';
import {connect} from 'react-redux';
import qs from 'query-string';
import moment from 'moment';
import Lightbox from 'react-image-lightbox';
import {getArtistRequest, getArtistPostsRequest, getArtistPostsByPeriodRequest, createPostCommentRequest,
    postLikeRequest, getPostCommentsRequest, deletePostCommentRequest, getArtistPinnedPostRequest, trackVideoViewRequest,
    commentLikeRequest, editPostCommentRequest, subscribeArtistRequest, unsubscribeArtistRequest, getArtistAuthorsRequest,
    replyPostCommentRequest, getTipJarRequest, changeAuthorPostsFilter} from '../../actions';
import {getTopFansRequest} from '../../../landing/actions';
import {showModal} from '../../../ui/actions';
import {getDonationLinkParamsRequest} from '../../../donation/actions';
import {modalTypes} from '../../../ui/constants';
import {userRoles} from '../../../auth/constants';
import {MIN_DONATION_AMOUNT_CENTS, MAX_DONATION_AMOUNT_CENTS} from '../../../donation/constants';
import {subscriptionTypes} from '../../../subscribe/constants';
import Header from '../../../landing/components/Layout/Header';
import Footer from '../../../landing/components/Layout/Footer';
import BackgroundImage from '../../../../base-components/BackgroundImage/BackgroundImage';
import ArtistInfo from '../../components/ArtistInfo/ArtistInfo';
import ArtistSchedule from '../../components/ArtistSchedule/ArtistSchedule';
import ArtistGroupTipJar from '../../components/ArtistGroupTipJar/ArtistGroupTipJar'
import ArtistPostsList from '../../components/ArtistPostsList/ArtistPostsList';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop'
import {layoutTypes} from '../../../ui/constants';
import AuthorPostsFilter from '../../components/AuthorPostsFilter/AuthorPostsFilter';
import ErrorBoundary from '../../../../base-components/ErrorBoundary/ErrorBoundary';
import {getTopFansList} from "../../selectors";

import styles from './ArtistProfilePage.module.scss';

class ArtistProfilePage extends React.Component {
    constructor() {
        super();

        this.state = {
            imageInLightbox: ''
        }
    }

    componentWillMount() {
        const {getArtist, getArtistPinnedPost, getArtistPosts, authorSlug, getArtistAuthors, getTipJar, getTopFans} = this.props;
        getArtist(authorSlug);
        getArtistPinnedPost(authorSlug);
        getArtistPosts(authorSlug);
        getTipJar(authorSlug);
        getArtistAuthors(authorSlug);
        getTopFans()
    }

    handleSubscribeClick = () => {
        const {isLoggedIn, showModal, artistProfile} = this.props;
        if (isLoggedIn) {
            showModal(modalTypes.SUBSCRIPTION_MODAL, {subscriptionType: subscriptionTypes.MONTHLY, reloadAfterSubscription: true});
        } else {
            showModal(modalTypes.SIGN_UP_MODAL, {nextModal: {modalType: modalTypes.SUBSCRIPTION_MODAL, modalProps: {subscriptionType: subscriptionTypes.MONTHLY, reloadAfterSubscription: true, artist_id: artistProfile._id}}});
        }
    };

    handleTip = ({donationAmount, donationTargetId, donationTargetName, postId, tipJarId, tipJarTitle }) => {
        const {user, showModal, isLoggedIn, getDonationLinkParams, artistProfile} = this.props;
        if ((donationAmount >= MIN_DONATION_AMOUNT_CENTS) && (donationAmount <= MAX_DONATION_AMOUNT_CENTS)) {
            if (isLoggedIn) {
                const donationAmountString = String(donationAmount).substr(0, String(donationAmount).length - 2) + '.' + String(donationAmount).substr(String(donationAmount).length - 2);

                new Promise((resolve, reject) => {
                    getDonationLinkParams({
                        amountString: donationAmountString,
                        resolve,
                        reject
                    })
                })
                    .then(({amount_hash, oc_token}) => {
                        let baseSegpayLink, segpayLinkParams;
                        const segpayTickedId = `${process.env.REACT_APP_SEGPAY_DYNAMIC_PACKAGE_ID}:${process.env.REACT_APP_SEGPAY_DYNAMIC_PRICEPOINT_ID}`;
                        segpayLinkParams = {
                            'x-eticketid': segpayTickedId,
                            'username': user._id,
                            'password': user._id,
                            'ppviewoption': '2',
                            'amount': donationAmountString,
                            'dynamictrans': amount_hash,
                            'REF5': artistProfile._id
                        };

                        if (oc_token) {
                            baseSegpayLink = 'https://secure2.segpay.com/billing/OneClick.aspx';
                            segpayLinkParams['OCToken'] = oc_token;
                        } else {
                            baseSegpayLink = 'https://secure2.segpay.com/billing/poset.cgi';
                        }

                        if (donationTargetId && postId) {
                            segpayLinkParams['dynamicdesc'] = encodeURIComponent(`Tip to ${donationTargetName} (id ${donationTargetId})`);
                            segpayLinkParams['REF2'] = postId;
                            segpayLinkParams['REF3'] = donationTargetId;
                        } else if (tipJarId) {
                            segpayLinkParams['dynamicdesc'] = encodeURIComponent(`Tip goal: ${tipJarTitle} (id ${tipJarId})`);
                            segpayLinkParams['REF4'] = tipJarId;
                        }

                        const segpayLink = baseSegpayLink + '?' + qs.stringify(segpayLinkParams, {encode: false});
                        window.location.replace(segpayLink);
                    })
            } else {
                showModal(modalTypes.SIGN_UP_MODAL);
            }
        }
    };

    handleShowMorePostsClick = () => {
        const {artistPosts, authorSlug, getArtistPosts} = this.props;

        getArtistPosts(authorSlug, artistPosts[artistPosts.length - 1]._id);
    };

    handleUncollapsePeriod = (periodStartDate, periodEndDate, resolve) => {
        const {authorSlug, getArtistPostsByPeriod} = this.props;

        getArtistPostsByPeriod(authorSlug, periodStartDate, periodEndDate, resolve);
    };

    handleImageClick = (imageSrc) => {
        this.setState({imageInLightbox: imageSrc});
    };

    handleLightboxClose = () => {
        this.setState({imageInLightbox: ''});
    };

    handleUnlockPeriod = (periodStartDate, periodEndDate) => {
        const {showModal, authorSlug} = this.props;

        showModal(modalTypes.PERIOD_UNLOCK_MODAL, {artistSlug: authorSlug, periodStartDate, periodEndDate});
    };

    handleShowMoreCommentsClick = (postId) => {
        this.props.getPostComments(postId);
    };

    handleScheduleClick = () => {
        const {layoutType, history, authorSlug} = this.props;
        if ((layoutType === layoutTypes.MOBILE) || (layoutType === layoutTypes.TABLET)) {
            history.push(`/${authorSlug}/schedule`)
        }
    };

    handleGroupWishlistClick = (wishlist) => {
        const {showModal} = this.props;
        showModal(modalTypes.GROUP_WISHLIST_MODAL, wishlist);
    };

    renderSubscribeBlock = () => {
        return (
            <div className={styles.subscribeContainer}>
                <p className={styles.subscribedText}>Your Subscription is Active</p>
            </div>
        )
    };

    render() {
        const {
            isLoggedIn,
            user,
            artistProfile,
            artistPosts,
            artistPostsWithinPeriods,
            hasMorePosts,
            isFetchingArtist,
            isFetchingPosts,
            isFetchingPinnedPost,
            postLike,
            commentLike,
            createPostComment,
            deletePostComment,
            editPostComment,
            trackVideoView,
            replyPostComment,
            tipJar,
            subscribeArtist,
            unsubscribeArtist,
            artistAuthors,
            authorPostsFilter,
            changeAuthorPostsFilter,
            layoutType,
            topFansList
        } = this.props;

        const isUserSubscribed = user && user.artists_subscriptions.indexOf(artistProfile && artistProfile._id) > -1;
        let
            content;

        if (isFetchingArtist) {
            content = <div className={styles.notFound}><SpinnerWithBackdrop /></div>
        } else if (!artistProfile) {
            content = <div className={styles.notFound}>Artist not found</div>
        } else {
            content = <div>
                {
                    artistProfile.cover
                        && <BackgroundImage
                            className={styles.cover}
                            url={artistProfile.cover}
                            size='cover'
                            attachment='scroll'
                        />
                }
                <div className={styles.profile}>
                    <ErrorBoundary>
                    <aside className={styles.aside}>
                        <ArtistInfo
                            user={user}
                            isLoggedIn={isLoggedIn}
                            className={styles.infoContainer}
                            artistProfile={artistProfile}
                            isUserSubscribed={isUserSubscribed}
                            onSubscribeClick={this.handleSubscribeClick}
                            onWishlistClick={this.handleGroupWishlistClick}
                            postLike={postLike}
                        />
                        {
                            isUserSubscribed
                                ? this.renderSubscribeBlock() : null
                        }
                        {
                            tipJar ?
                                <ArtistGroupTipJar
                                    onTip={this.handleTip}
                                    tipJar={tipJar}
                                    className={styles.tipJarContainer}
                            />: null
                        }
                        {
                            (artistAuthors.length > 1)
                                && <AuthorPostsFilter
                                    artistAuthors={artistAuthors}
                                    filter={authorPostsFilter}
                                    className={styles.authorPostsFilterContainer}
                                    onChange={changeAuthorPostsFilter}
                                />
                        }
                        <ArtistSchedule
                            className={styles.scheduleContainer}
                            schedule={artistProfile.schedule}
                            onClick={this.handleScheduleClick}
                        />
                    </aside>
                    </ErrorBoundary>
                    <ErrorBoundary>
                    <div className={styles.postsContainer}>
                        {
                            (isFetchingPosts || isFetchingPinnedPost)
                                && <div className={styles.postsSpinnerContainer}><SpinnerWithBackdrop /></div>
                        }
                        {/*{*/}
                            {/*!(user && user.has_monthly_subscription)*/}
                                {/*&& <PostBlockerMessage*/}
                                    {/*className={styles.postsBlockerMessage}*/}
                                    {/*onSubscribeClick={this.handleSubscribeClick}*/}
                                {/*/>*/}
                        {/*}*/}
                        <ArtistPostsList
                            visibleContentPeriods={user && user.visible_content_periods}
                            shouldShowPeriods={!hasMorePosts && user && (user.role !== userRoles.ADMIN) && (user.role !== userRoles.AUTHOR)}
                            firstPostDate={artistProfile.first_post_date}
                            artistProfile={artistProfile}
                            isUserBanned={user && user.is_banned}
                            user={user}
                            posts={artistPosts}
                            postsWithinPeriods={artistPostsWithinPeriods}
                            onSubscribeClick={this.handleSubscribeClick}
                            onShowMoreCommentsClick={this.handleShowMoreCommentsClick}
                            trackVideoView={trackVideoView}
                            onTip={this.handleTip}
                            onPeriodUncollapse={this.handleUncollapsePeriod}
                            onPeriodUnlock={this.handleUnlockPeriod}
                            onImageClick={this.handleImageClick}
                            postLike={postLike}
                            commentLike={commentLike}
                            isLoggedIn={isLoggedIn}
                            createPostComment={createPostComment}
                            deleteComment={deletePostComment}
                            editComment={editPostComment}
                            subscribeArtist={subscribeArtist}
                            unsubscribeArtist={unsubscribeArtist}
                            artistAuthors={artistAuthors}
                            replyComment={replyPostComment}
                            layoutType={layoutType}
                            topFansList={topFansList}
                        />
                        {
                            hasMorePosts
                                && <div className={styles.showMoreButtonContainer}>
                                    <button className={styles.showMoreButton} onClick={this.handleShowMorePostsClick}>
                                        Show More
                                    </button>
                                </div>
                        }
                        {
                            this.state.imageInLightbox
                                && <Lightbox
                                    mainSrc={this.state.imageInLightbox}
                                    onCloseRequest={this.handleLightboxClose}
                                />
                        }
                    </div>
                    </ErrorBoundary>
                </div>
            </div>
        }

        return (
            <div className={styles.container}>
                <Header
                    isOverlay={true}
                    withBackground={Boolean(artistProfile)}
                    backgroundImgUrl={artistProfile && artistProfile.cover}
                    isLoggedIn={isLoggedIn}
                    user={user}/>
                {content}
                <Footer showCompany={true} />
            </div>
        )
    }
}

export default connect(
    (state, ownProps) => ({
        authorSlug: ownProps.match.params.slug,
        artistProfile: state.artist.artistProfile,
        topFansList: getTopFansList(state),
        artistPosts: state.artist.artistPosts,
        artistAuthors: state.artist.artistAuthors,
        artistPostsWithinPeriods: state.artist.artistPostsWithinPeriods,
        user: state.auth.user,
        isLoggedIn: state.auth.isLoggedIn,
        hasMorePosts: state.artist.hasMorePosts,
        isFetchingArtist: state.artist.isFetchingArtist,
        isFetchingPosts: state.artist.isFetchingPosts,
        isFetchingPinnedPost: state.artist.isFetchingPinnedPost,
        tipJar: state.artist.tipJar,
        layoutType: state.ui.layoutType,
        authorPostsFilter: state.artist.authorPostsFilter
    }),
    dispatch => ({
        getArtist: slug => dispatch(getArtistRequest(slug)),
        getArtistPinnedPost: slug => dispatch(getArtistPinnedPostRequest(slug)),
        getArtistPosts: (slug, lastPostId) => dispatch(getArtistPostsRequest(slug, lastPostId)),
        showModal: (modalType, modalProps) => dispatch(showModal(modalType, modalProps)),
        getArtistPostsByPeriod: (slug, periodStartDate, periodEndDate, resolve) => dispatch(getArtistPostsByPeriodRequest(slug, periodStartDate, periodEndDate, resolve)),
        postLike: (postId) => dispatch(postLikeRequest(postId)),
        commentLike: (commentId, parentCommentId) => dispatch(commentLikeRequest(commentId, parentCommentId)),
        createPostComment: (comment, postId) => dispatch(createPostCommentRequest(comment, postId)),
        getPostComments: (postId) => dispatch(getPostCommentsRequest(postId)),
        trackVideoView: (postId) => dispatch(trackVideoViewRequest(postId)),
        deletePostComment: (commentId, postId, parentCommentId) =>
            dispatch(deletePostCommentRequest(commentId, postId, parentCommentId)),
        editPostComment: (text, commentId, postId, parentCommentId) =>
            dispatch(editPostCommentRequest(text, commentId, postId, parentCommentId)),
        replyPostComment: (text, commentId) => dispatch(replyPostCommentRequest(text, commentId)),
        getTipJar: (slug) => dispatch(getTipJarRequest(slug)),
        subscribeArtist: (userId) => dispatch(subscribeArtistRequest(userId)),
        unsubscribeArtist: (userId) => dispatch(unsubscribeArtistRequest(userId)),
        getArtistAuthors: (slug) => dispatch(getArtistAuthorsRequest(slug)),
        changeAuthorPostsFilter: (filter) => dispatch(changeAuthorPostsFilter(filter)),
        getTopFans: () => dispatch(getTopFansRequest()),
        getDonationLinkParams: (payload) => dispatch(getDonationLinkParamsRequest(payload))
    })
)(ArtistProfilePage)
