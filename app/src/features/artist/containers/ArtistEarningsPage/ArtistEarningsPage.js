import React, {Component} from 'react';
import {connect} from 'react-redux';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs/Tabs';
import Tab from '@material-ui/core/Tab';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import LocalPostOfficeIcon from '@material-ui/icons/LocalPostOffice';
import TagFacesIcon from '@material-ui/icons/TagFaces';
import TabContainer from '../../../../base-components/TabContainer/TabContainer';
import ArtistStatsPerPostTable from '../../components/ArtistStatsPerPostTable/ArtistStatsPerPostTable';
import ArtistStatsPerFansTable from '../../components/ArtistStatsPerFansTable/ArtistStatsPerFansTable';
import ArtistStatsPerAuthorTable from '../../components/ArtistStatsPerAuthorTable/ArtistStatsPerAuthorTable';
import {getStatsPerPostRequest, getStatsPerFansRequest, getStatsPerAuthorsRequest} from '../../actions';
import {showModal} from "../../../ui/actions";
import {modalTypes} from "../../../ui/constants";

import styles from './ArtistEarningsPage.module.scss';

const tabsIndex = {
    FIRST_TAB_INDEX: 0,
    SECOND_TAB_INDEX: 1,
    THIRD_TAB_INDEX: 2
};

class ArtistEarningsPage extends Component {
    state = {
        value: 0,
    };

    componentDidMount() {
        this.props.getStatsPerPost();
    }

    handleChange = (event, value) => {
        this.setState({value});
    };

    handleStatsPerFanDetailsClick = (userId, username, period) => {
        this.props.showUserDonationsDetailsModal({
            userId,
            username,
            period
        });
    };

    render() {
        const {value} = this.state;
        const {
            isFetchingStats,
            statsPerPost,
            statsPerFans,
            statsPerAuthors,
            getStatsPerFans,
            getStatsPerAuthors
        } = this.props;
        return (
            <div className={styles.container}>
                <Paper className={styles.tabsContainer}>
                    <Tabs
                        value={value}
                        indicatorColor="primary"
                        textColor="primary"
                        centered
                        onChange={this.handleChange}
                    >
                        <Tab
                            className={styles.tab}
                            label="By Posts"
                            icon={<LocalPostOfficeIcon/>}
                        />
                        <Tab
                            className={styles.tab}
                            label="By Fans"
                            icon={<TagFacesIcon/>}
                        />
                        <Tab
                            className={styles.tab}
                            label="By Authors"
                            icon={<PersonPinIcon/>}
                        />
                    </Tabs>
                </Paper>
                {value === tabsIndex.FIRST_TAB_INDEX && <TabContainer>
                    <ArtistStatsPerPostTable
                        isFetchingStats={isFetchingStats}
                        data={statsPerPost}/>
                </TabContainer>}
                {value === tabsIndex.SECOND_TAB_INDEX && <TabContainer>
                    <ArtistStatsPerFansTable
                        isFetchingStats={isFetchingStats}
                        getStatsPerFans={getStatsPerFans}
                        onDetailsClick={this.handleStatsPerFanDetailsClick}
                        data={statsPerFans}/>
                </TabContainer>}
                {value === tabsIndex.THIRD_TAB_INDEX && <TabContainer>
                    <ArtistStatsPerAuthorTable
                        isFetchingStats={isFetchingStats}
                        getStatsPerAuthors={getStatsPerAuthors}
                        data={statsPerAuthors}/>
                </TabContainer>}
            </div>
        )
    }
}

export default connect(
    (state) => ({
        isFetchingStats: state.artist.isFetchingStats,
        statsPerPost: state.artist.statsPerPost,
        statsPerFans: state.artist.statsPerFans,
        statsPerAuthors: state.artist.statsPerAuthors,
    }),
    (dispatch) => ({
        getStatsPerPost: () => dispatch(getStatsPerPostRequest()),
        getStatsPerFans: (period) => dispatch(getStatsPerFansRequest(period)),
        getStatsPerAuthors: (period) => dispatch(getStatsPerAuthorsRequest(period)),
        showUserDonationsDetailsModal: (modalProps) => dispatch(showModal(modalTypes.USER_DONATIONS_DETAILS_MODAL, modalProps))
    })
)(ArtistEarningsPage);