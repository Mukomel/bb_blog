import React from 'react';
import {connect} from 'react-redux';
import Paper from '@material-ui/core/Paper';
import NotificationsForm from '../../components/NotificationsForm/NotificationsForm';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';
import { postMassMailingRequest } from '../../actions'

import styles from './ArtistNotificationsPage.module.scss';

class ArtistNotificationsPage extends React.Component {
    handleEditScheduleFormSubmit = (values) => {
        const {postMassMailing} = this.props;
        return new Promise((resolve, reject) => {
            postMassMailing({values, resolve, reject});
        })
    };

    render() {
        const {isSendingMassNotifications} = this.props;
        const initialValues = {
            send_emails: true,
            send_notifications: false,
            target: 'ALL_FANS',
            with_promocode: false,
            without_promocode: false,
            august_promocoders: false,
            title: '',
            html: ''
        };
        return (
            <div className={styles.container}>
                <Paper>
                    <div className={styles.title}>Mass Emails/Notifications</div>
                    <NotificationsForm
                        initialValues={initialValues}
                        onSubmit={this.handleEditScheduleFormSubmit}
                    />
                    {
                        isSendingMassNotifications && <SpinnerWithBackdrop/>
                    }
                </Paper>
            </div>
        )
    }
}

export default connect(
    (state) => ({
        isSendingMassNotifications: state.artist.isSendingMassNotifications
    }),
    (dispatch) => ({
        postMassMailing: (payload) => dispatch(postMassMailingRequest(payload))
    })
)(ArtistNotificationsPage);
