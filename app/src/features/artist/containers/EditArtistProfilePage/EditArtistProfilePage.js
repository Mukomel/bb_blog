import React from 'react';
import {connect} from 'react-redux';
import Paper from '@material-ui/core/Paper';
import EditArtistProfileForm from '../../components/EditArtistProfileForm/EditArtistProfileForm';
import EditArtistSocialLinksForm from '../../components/EditArtistSocialLinksForm/EditArtistSocialLinksForm'
import {getArtistRequest, updateArtistRequest} from '../../actions';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';

import styles from './EditArtistProfilePage.module.scss';

class ArtistEditProfilePage extends React.Component {
    componentWillMount = () => {
        const {getArtist, currentUser} = this.props;

        getArtist(currentUser.artist.slug);
    };

    handleEditArtistProfileFormSubmit = (values) => {
        const {updateArtist, currentUser} = this.props;

        return new Promise((resolve, reject) => {
            updateArtist({values, resolve, reject, slug: currentUser.artist.slug});
        })
    };
    render() {
        const {artistProfile, isFetchingArtist, isUpdatingArtist} = this.props;

        const artistProfileFormInitialValues = artistProfile
            ? {
                name: artistProfile.name,
                biography: artistProfile.biography
            }
            : {};

        const artistSocialLinksFormInitialValues = artistProfile
            ? {
                snapchat_link: artistProfile.snapchat_link,
                twitter_link: artistProfile.twitter_link,
                tumblr_link: artistProfile.tumblr_link,
                instagram_link: artistProfile.instagram_link
            }
            : {};

        return (
            <div className={styles.container}>
                <Paper className={styles.artistDetailsContainer}>
                    <div className={styles.title}>Artist details</div>
                    <EditArtistProfileForm
                        initialValues={artistProfileFormInitialValues}
                        onSubmit={this.handleEditArtistProfileFormSubmit}
                    />
                    {
                        (isFetchingArtist || isUpdatingArtist)
                            && <SpinnerWithBackdrop/>
                    }
                </Paper>
                <Paper className={styles.artistDetailsContainer}>
                    <div className={styles.title}>Artist Social Links</div>
                    <EditArtistSocialLinksForm
                        initialValues={artistSocialLinksFormInitialValues}
                        onSubmit={this.handleEditArtistProfileFormSubmit}
                    />
                    {
                        (isFetchingArtist || isUpdatingArtist)
                        && <SpinnerWithBackdrop/>
                    }
                </Paper>
            </div>
        )
    }
}

export default connect(
    (state) => ({
        isFetchingArtist: state.artist.isFetchingArtist,
        isUpdatingArtist: state.artist.isUpdatingArtist,
        currentUser: state.auth.user,
        artistProfile: state.artist.artistProfile
    }),
    (dispatch) => ({
        getArtist: (slug) => dispatch(getArtistRequest(slug)),
        updateArtist: (payload) => dispatch(updateArtistRequest(payload)),
    })
)(ArtistEditProfilePage);

