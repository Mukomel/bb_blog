import React from 'react';
import {connect} from 'react-redux';
import Paper from '@material-ui/core/Paper';
import EditScheduleForm from '../../components/EditScheduleForm/EditScheduleForm';
import {updateScheduleRequest, getArtistRequest} from '../../actions';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';

import styles from './ArtistSchedulePage.module.scss';

class ArtistSchedulePage extends React.Component {
    componentDidMount() {
        const { getArtist, currentUser } = this.props;
        getArtist(currentUser.artist.slug)
    }
    
    resolveScheduleValues(obj) {
        const result = {};
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                Object.defineProperty(result, [key],
                    {
                        value: {description: obj[key]}
                    })
            }
        }
        return result
    }

    handleEditScheduleFormSubmit = (values) => {
        const {updateSchedule, currentUser} = this.props;
        const schedule = this.resolveScheduleValues(values);

        return new Promise((resolve, reject) => {
            updateSchedule({schedule, resolve, reject, slug: currentUser.artist.slug});
        })
    };

    render() {
        const { isUpdatingSchedule, artistProfile } = this.props;
        const initialScheduleValues = artistProfile && artistProfile.schedule ? {
            monday: artistProfile.schedule.monday.description,
            tuesday: artistProfile.schedule.tuesday.description,
            wednesday: artistProfile.schedule.wednesday.description,
            thursday: artistProfile.schedule.thursday.description,
            friday: artistProfile.schedule.friday.description,
            saturday: artistProfile.schedule.saturday.description,
            sunday: artistProfile.schedule.sunday.description
        } : {};
        return (
            <div className={styles.container}>
                <Paper className={styles.artistDetailsContainer}>
                    <div className={styles.title}>Feature Schedule</div>
                    <EditScheduleForm
                        initialValues={initialScheduleValues}
                        onSubmit={this.handleEditScheduleFormSubmit}
                    />
                    {
                        isUpdatingSchedule && <SpinnerWithBackdrop/>
                    }
                </Paper>
            </div>
        )
    }
}

export default connect(
    (state) => ({
        currentUser: state.auth.user,
        isUpdatingSchedule: state.artist.isUpdatingSchedule,
        artistProfile: state.artist.artistProfile,
    }),
    (dispatch) => ({
        getArtist: (slug) => dispatch(getArtistRequest(slug)),
        updateSchedule: (payload) => dispatch(updateScheduleRequest(payload)),
    })
)(ArtistSchedulePage);