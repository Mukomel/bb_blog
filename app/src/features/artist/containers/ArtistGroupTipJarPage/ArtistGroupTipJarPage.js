import React from 'react';
import {connect} from 'react-redux';
import Paper from '@material-ui/core/Paper';
import Switch from '@material-ui/core/Switch';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';
import EditGroupTipJarForm from '../../components/EditGroupTipJarForm/EditGroupTipJarForm';
import { postTipJarRequest, getArtistRequest } from '../../../artist/actions';
import {roundToExactly} from '../../../../helpers/roundTo';
import { switchTipJarRequest, completeTipJarRequest, getTipJarRequest } from '../../actions';

import styles from './ArtistGroupTipJarPage.module.scss';

class ArtistGroupTipJarPage extends React.Component {
    componentDidMount() {
        const { getTipJar, currentUser, getArtist } = this.props;
        getTipJar(currentUser.artist.slug);
        getArtist(currentUser.artist.slug);
    }

    handleEditTipJarFormSubmit = (body) => {
        const {sendTipJar, currentUser} = this.props;
        const values = Object.assign({}, body, {
            goal_amount: body.goal_amount * 100
        });
        return new Promise((resolve, reject) => {
            sendTipJar({values, resolve, reject, slug: currentUser.artist.slug});
        });
    };

    handleVisibleSwitch = ({ target: { checked } }) => {
        const { switchTipJar, currentUser } = this.props;
        switchTipJar(currentUser.artist.slug, checked);
    };

    handleTipJarComplete = () => {
        const { markGoalAsCompleted, currentUser } = this.props;
        markGoalAsCompleted(currentUser.artist.slug)
    };

    render() {
        const { tipJar, isUpdatingTipJar, artistProfile } = this.props;

        const initialGroupTipJarValues = tipJar ? {
            title: tipJar.title,
            description: tipJar.description,
            goal_amount: roundToExactly(tipJar.goal_amount / 100, 2)
        } : {};
        const isTipJarActive = artistProfile && artistProfile.tip_jar_enabled;
        return (
            <div className={styles.container}>
                {
                    tipJar ?
                        <h3>At this moment, {roundToExactly(tipJar.current_amount / 100, 2)}$ of
                            ${roundToExactly(tipJar.goal_amount / 100, 2)} was collected</h3> : null
                }
                <Paper className={styles.paperContainer}>
                    <div className={styles.switchContainer}>
                        <span>Turn {isTipJarActive ? 'On' : 'Off'}</span>
                        <Switch
                            color="primary"
                            checked={!!isTipJarActive}
                            onChange={this.handleVisibleSwitch}/>
                    </div>
                    <div className={styles.title}>Tip Goal</div>
                    <EditGroupTipJarForm
                        isTipJarCompleted={!tipJar && isTipJarActive}
                        onTipJarComplete={this.handleTipJarComplete}
                        initialValues={initialGroupTipJarValues}
                        onSubmit={this.handleEditTipJarFormSubmit} />
                    {
                        isUpdatingTipJar ? <SpinnerWithBackdrop/> : null
                    }
                </Paper>
            </div>
        )
    }
}

export default connect(
    (state) => ({
        currentUser: state.auth.user,
        isUpdatingTipJar: state.artist.isUpdatingTipJar,
        tipJar: state.artist.tipJar,
        artistProfile: state.artist.artistProfile
    }),
    (dispatch) => ({
        sendTipJar: (slug, tipJar) => dispatch(postTipJarRequest(slug, tipJar)),
        switchTipJar: (slug, tipJarEnabled) => dispatch(switchTipJarRequest(slug, tipJarEnabled)),
        markGoalAsCompleted: (slug) => dispatch(completeTipJarRequest(slug)),
        getTipJar: (slug) => dispatch(getTipJarRequest(slug)),
        getArtist: (slug) => dispatch(getArtistRequest(slug))
    })
)(ArtistGroupTipJarPage);