import React from 'react'
import {connect} from 'react-redux'
import moment from 'moment';
import Button from '@material-ui/core/Button'
import {DateRange} from 'react-date-range';
import ArtistStatsTransactionsTable from '../../components/ArtistStatsTransactionsTable/ArtistStatsTransactionsTable'
import CustomSelect from '../../../../base-components/CustomSelect/CustomSelect'
import Checkbox from '../../../../base-components/Checkox/Checkbox'
import {TRANSACTIONS_DATE_RANGES, TRANSACTIONS_TYPES} from '../../constants'
import {getTransactionStats} from '../../selectors'
import {getTransactionStatsRequest, getArtistAuthorsRequest} from "../../actions";

import * as styles from './ArtistStatsTransactionsPage.module.scss';

class ArtistStatsTransactionsPage extends React.Component {
    state = {
        dateRange: TRANSACTIONS_DATE_RANGES.TODAY,
        type: TRANSACTIONS_TYPES.ALL,
        author: 'All',
        isDatePickerOpen: false,
        ranges: {
            rangeColors: [
                "#3d91ff",
                "#3ecf8e",
                "#fed14c"
            ],
            startDate: new Date(),
            endDate: new Date(),
            key: 'selection'
        },
        forecast: false,
        query: {}
    };


    componentDidMount() {
        const {getTransactionStats, transactionStats, getArtistAuthors, currentUser} = this.props;
        if (!transactionStats.length > 0) {
            const date = new Date().toISOString();
            getTransactionStats({
                period_first_day: date,
                period_last_day: date
            });
            getArtistAuthors(currentUser.artist.slug)
        }
    }


    handleSelectChange = ({target: {value}, target}, field) => {
        let query = Object.assign({}, this.state.query);
        if (!query.period_first_day && !query.period_last_day) {
            query.period_first_day = new Date().toISOString();
            query.period_last_day = new Date().toISOString();
        }
        if (target && field) {
            if (value === TRANSACTIONS_DATE_RANGES.CUSTOM) {
                this.openPicker();
                this.setState({dateRange: TRANSACTIONS_DATE_RANGES.CUSTOM});
            }

            if (field === 'type') {
                this.setState({type: value});
                if (value === TRANSACTIONS_TYPES.ALL) {
                    delete query.source
                } else {
                    query.source = value.toUpperCase();
                }
            }

            if (field === 'author') {
                if (value !== 'All') {
                    const author = this.props.artistAuthors.filter(author => author.username === value)[0];
                    this.setState({author: author.username});
                    query.author = author._id;
                } else {
                    delete query.author;
                    this.setState({author: 'All'});
                }
            }

            if (field === 'dateRange') {
                switch (value) {
                    case (TRANSACTIONS_DATE_RANGES.TODAY): {
                        query.period_first_day = new Date().toISOString();
                        query.period_last_day = new Date().toISOString();

                        this.setState({dateRange: TRANSACTIONS_DATE_RANGES.TODAY});
                        break;
                    }
                    case (TRANSACTIONS_DATE_RANGES.THIS_MONTH): {
                        query.period_first_day = moment().startOf('month').toISOString();
                        query.period_last_day = moment().endOf('month').toISOString();

                        this.setState({dateRange: TRANSACTIONS_DATE_RANGES.THIS_MONTH});
                        break;
                    }
                }
            }

            this.setState({query});
            this.props.getTransactionStats(query);
        }
    };

    openPicker = () => {
        this.setState({
            isDatePickerOpen: true
        })
    };

    handleDatePickerChange = (ranges) => {
        this.setState({
            ranges: ranges.selection
        })
    };

    handleDatePickerButtonClick = () => {
        const {startDate, endDate} = this.state.ranges;
        let query = Object.assign({}, this.state.query);

        query.period_first_day = moment(startDate).toISOString();
        query.period_last_day = moment(endDate).toISOString();

        this.setState({
            query,
            isDatePickerOpen: false
        });
        this.props.getTransactionStats(query);
    };

    handleForecastCheckboxChange = (isChecked) => {
        let query = Object.assign({}, this.state.query);
        if (isChecked) {
            query.forecast = true;
        } else {
            delete query.forecast;
        }
        delete query.author;
        delete query.source;

        this.setState({
            type: TRANSACTIONS_TYPES.ALL,
            author: 'All',
            forecast: isChecked,
            query
        });
        this.props.getTransactionStats(query);
    };

    render() {
        const {transactionStats, artistAuthors} = this.props;
        const authors = artistAuthors.map(item => item.username);
        authors.unshift('All');
        const {dateRange, author, type, ranges, isDatePickerOpen, forecast} = this.state;

        if (transactionStats && transactionStats.total) {
            return (
                <div className={styles.statsContainer}>
                    <div>
                        <h2 className={styles.title}>Transaction Summary</h2>
                        <CustomSelect
                            data={[
                                TRANSACTIONS_DATE_RANGES.TODAY,
                                TRANSACTIONS_DATE_RANGES.THIS_MONTH,
                                TRANSACTIONS_DATE_RANGES.CUSTOM
                            ]}
                            value={dateRange}
                            onSelectChange={e => this.handleSelectChange(e, 'dateRange')}
                            label="Date Range"
                        />
                        <CustomSelect
                            data={authors}
                            disabled={forecast}
                            value={author}
                            onSelectChange={e => this.handleSelectChange(e, 'author')}
                            label="Artists"
                        />
                        <CustomSelect
                            data={[TRANSACTIONS_TYPES.ALL, TRANSACTIONS_TYPES.SUBSCRIPTION, TRANSACTIONS_TYPES.TIP]}
                            disabled={forecast}
                            value={type}
                            onSelectChange={e => this.handleSelectChange(e, 'type')}
                            label="Type"
                        />
                        <Checkbox
                            className={styles.forecastCheckbox}
                            defaultChecked={this.state.forecast}
                            onCheck={this.handleForecastCheckboxChange}
                            name='forecast'
                            label='Forecast'
                            tooltip={`Forecast - naive prediction based on current active subscriptions, doesn't take in the account new subscriptions, tips, and expenses.`}
                        />
                        {
                            isDatePickerOpen &&
                            <div className={styles.picker}>
                                <DateRange
                                    className={styles.dateRange}
                                    ranges={[ranges]}
                                    onChange={this.handleDatePickerChange}
                                />
                                <Button
                                    variant="contained"
                                    color="primary"
                                    onClick={this.handleDatePickerButtonClick}>Ok</Button>
                            </div>
                        }
                    </div>
                    <ArtistStatsTransactionsTable
                        isFetchingStats={false}
                        data={transactionStats}
                    />
                    {
                        transactionStats.forecast
                            && <div>
                                <div className={styles.forecastTableTitle}>Forecast</div>
                                <ArtistStatsTransactionsTable
                                    isFetchingStats={false}
                                    data={transactionStats.forecast}
                                />
                            </div>
                    }
                </div>
            )

        } else {
            return <div></div>;
        }
    }
}

export default connect(
    state => ({
        transactionStats: getTransactionStats(state),
        currentUser: state.auth.user,
        artistAuthors: state.artist.artistAuthors
    }),
    dispatch => ({
        getTransactionStats: query => dispatch(getTransactionStatsRequest(query)),
        getArtistAuthors: slug => dispatch(getArtistAuthorsRequest(slug))
    })
)(ArtistStatsTransactionsPage)
