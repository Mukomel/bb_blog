import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Elements} from 'react-stripe-elements';
import moment from 'moment';
import PaymentDetailsForm from '../../../subscribe/components/PaymentDetailsForm/PaymentDetailsForm';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';
import {periodUnlockModalSteps} from '../../../ui/constants';
import {buyPostsForPeriodRequest} from '../../actions';
import {changePeriodUnlockModalStep} from '../../../ui/actions';
import {roundToExactly} from '../../../../helpers/roundTo';
import config from '../../../../config';

import styles from './PeriodUnblockModal.module.scss';

class PeriodUnblockModal extends React.Component {
    constructor() {
        super();

        this.state = {
            isPaymentDetailsFormValid: false,
            isPaymentDetailsFormReady: false,
            isCreatingStripeToken: false
        }
    }

    handlePaymentDetailsFormValidChange = (valid) => {
        this.setState({isPaymentDetailsFormValid: valid})
    };

    submitPaymentDetailsForm = () => {
        this.paymentForm.getWrappedInstance().submit();
    };

    handlePaymentDetailsFormReady = () => {
        this.setState({isPaymentDetailsFormReady: true});
    };

    handleCreatingStripeTokenChange = (isCreatingStripeToken) => {
        this.setState({isCreatingStripeToken: isCreatingStripeToken});
    };

    handleCancelClick = () => {
        this.props.onHide();
        this.setState({isPaymentDetailsFormReady: false, isPaymentDetailsFormValid: false});
    };

    handlePaymentDetailsFormSubmit = (stripeToken) => {
        const {buyPostsForPeriod, artistSlug, periodEndDate} = this.props;

        buyPostsForPeriod({stripeToken, artistSlug, periodEndDate});
    };

    handleSuccessUnlockModalHide = () => {
        window.location.reload();
    };

    render() {
        const {isOpen, currentStep, periodUnlockError, periodStartDate, periodEndDate, isRequestingPeriodUnlock} = this.props;

        let dialogueTitle, dialogueContent, dialogueActions;
        const unlockPeriodPrice = config[process.env.REACT_APP_ENV].PERIOD_UNLOCK_PRICE_CENTS;

        switch (currentStep) {

            case periodUnlockModalSteps.PAYMENT_DETAILS:
                dialogueTitle = <DialogTitle>Pay with Card</DialogTitle>;
                dialogueContent = <DialogContent>
                    <Elements>
                        <PaymentDetailsForm
                            ref={node => this.paymentForm = node}
                            subscriptionPrice={unlockPeriodPrice}
                            paymentRequestLabel={`Unlock ${moment(periodStartDate).format('MMMM DD YYYY')} - ${moment(periodEndDate).format('MMMM DD YYYY')}`}
                            isFormValid={this.state.isPaymentDetailsFormValid}
                            onFormValidChange={this.handlePaymentDetailsFormValidChange}
                            onSubmit={this.handlePaymentDetailsFormSubmit}
                            onFormReady={this.handlePaymentDetailsFormReady}
                            onCreatingStripeTokenChange={this.handleCreatingStripeTokenChange}
                        />
                    </Elements>
                    {
                        periodUnlockError
                            && <div className={styles.periodUnlockError}>{periodUnlockError}</div>
                    }
                    {
                        (isRequestingPeriodUnlock || !this.state.isPaymentDetailsFormReady || this.state.isCreatingStripeToken)
                            && <SpinnerWithBackdrop />
                    }
                </DialogContent>;
                dialogueActions = <DialogActions>
                    <Button
                        onClick={this.handleCancelClick}
                    >
                        Cancel
                    </Button>
                    <Button
                        onClick={this.submitPaymentDetailsForm}
                        variant="raised"
                        color="primary"
                        disabled={!this.state.isPaymentDetailsFormValid}
                    >
                        {'Pay $' + roundToExactly(unlockPeriodPrice / 100, 2)}
                    </Button>
                </DialogActions>;
                break;

            case periodUnlockModalSteps.CONGRATULATIONS:
                dialogueTitle = <DialogTitle>Period was unlocked</DialogTitle>;
                dialogueActions = <DialogActions>
                    <Button
                        onClick={this.handleSuccessUnlockModalHide}
                        variant="raised"
                        color="primary"
                    >
                        Ok
                    </Button>
                </DialogActions>;
                break;
        }

        return <Dialog
            disableBackdropClick
            disableEscapeKeyDown
            open={isOpen}
            fullWidth={true}
        >
            {dialogueTitle}
            {dialogueContent}
            {dialogueActions}
        </Dialog>
    }
}

PeriodUnblockModal.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onHide: PropTypes.func.isRequired,
    periodStartDate: PropTypes.string,
    periodEndDate: PropTypes.string,
    artistSlug: PropTypes.string
};

export default connect(
    (state) => ({
        periodUnlockError: state.artist.periodUnlockError,
        isRequestingPeriodUnlock: state.artist.isRequestingPeriodUnlock,
        currentStep: state.ui.periodUnlockModalStep,

    }),
    (dispatch) => ({
        buyPostsForPeriod: (payload) => dispatch(buyPostsForPeriodRequest(payload)),
        changePeriodUnlockModalStep: (step) => dispatch(changePeriodUnlockModalStep(step))
    })
)(PeriodUnblockModal)
