import { createSelector } from 'reselect';

export const getTopFansList = createSelector([state => state.landing.topFansList], topFansList => topFansList);
export const getTransactionStats = createSelector([state => state.artist.transactionStats], stats => stats);