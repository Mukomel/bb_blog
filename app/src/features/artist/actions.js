import * as constants from './constants';

export const getArtistRequest = (slug) => ({
    type: constants.GET_ARTIST_REQUEST,
    slug
});

export const getArtistSuccess = (artist) => ({
    type: constants.GET_ARTIST_SUCCESS,
    artist
});

export const getArtistFailure = () => ({
    type: constants.GET_ARTIST_FAILURE
});

export const getArtistAuthorsRequest = (slug) => ({
    type: constants.GET_ARTIST_AUTHORS_REQUEST,
    slug
});

export const getArtistAuthorsSuccess = (authors) => ({
    type: constants.GET_ARTIST_AUTHORS_SUCCESS,
    authors
});

export const getArtistAuthorsFailure = () => ({
    type: constants.GET_ARTIST_AUTHORS_FAILURE
});

export const getArtistPostsRequest = (slug, lastPostId) => ({
    type: constants.GET_ARTIST_POSTS_REQUEST,
    slug,
    lastPostId
});

export const getArtistPostsSuccess = (posts) => ({
    type: constants.GET_ARTIST_POSTS_SUCCESS,
    posts
});

export const getArtistPostsFailure = () => ({
    type: constants.GET_ARTIST_POSTS_FAILURE
});

export const getArtistPinnedPostRequest = (slug) => ({
    type: constants.GET_ARTIST_PINNED_POST_REQUEST,
    slug
});

export const getArtistPinnedPostSuccess = (pinnedPost) => ({
    type: constants.GET_ARTIST_PINNED_POST_SUCCESS,
    pinnedPost
});

export const getArtistPinnedPostFailure = () => ({
    type: constants.GET_ARTIST_PINNED_POST_FAILURE
});

export const getArtistPostsByPeriodRequest = (slug, periodStartDate, periodEndDate, resolve) => ({
    type: constants.GET_ARTIST_POSTS_BY_PERIOD_REQUEST,
    slug,
    periodStartDate,
    periodEndDate,
    resolve
});

export const getArtistPostsByPeriodSuccess = (posts, periodStartDate, periodEndDate) => ({
    type: constants.GET_ARTIST_POSTS_BY_PERIOD_SUCCESS,
    posts,
    periodStartDate,
    periodEndDate
});

export const getArtistPostsByPeriodFailure = () => ({
    type: constants.GET_ARTIST_POSTS_BY_PERIOD_FAILURE
});

export const buyPostsForPeriodRequest = (payload) => ({
    type: constants.BUY_POSTS_FOR_PERIOD_REQUEST,
    payload
});

export const buyPostsForPeriodSuccess = () => ({
    type: constants.BUY_POSTS_FOR_PERIOD_SUCCESS
});

export const buyPostsForPeriodFailure = () => ({
    type: constants.BUY_POSTS_FOR_PERIOD_FAILURE
});

export const createPostCommentRequest = (comment, postId) => ({
    type: constants.CREATE_POST_COMMENT_REQUEST,
    comment,
    postId
});

export const createPostCommentSuccess = (body) => ({
    type: constants.CREATE_POST_COMMENT_SUCCESS,
    body
});

export const createPostCommentFailure = () => ({
    type: constants.CREATE_POST_COMMENT_FAILURE,
});

export const postLikeRequest = (postId) => ({
    type: constants.POST_LIKE_REQUEST,
    postId
});

export const postLikeSuccess = (postId) => ({
    type: constants.POST_LIKE_SUCCESS,
    postId
});

export const postLikeFailure = () => ({
    type: constants.POST_LIKE_FAILURE
});

//  comment like request

export const commentLikeRequest = (commentId, parentCommentId) => ({
    type: constants.COMMENT_LIKE_REQUEST,
    commentId,
    parentCommentId
});

export const commentLikeSuccess = (commentId, parentCommentId) => ({
    type: constants.COMMENT_LIKE_SUCCESS,
    commentId,
    parentCommentId
});

export const commentLikeFailure = () => ({
    type: constants.COMMENT_LIKE_FAILURE
});

export const deletePostCommentRequest = (commentId, postId, parentCommentId) => ({
    type: constants.DELETE_POST_COMMENT_REQUEST,
    commentId,
    postId,
    parentCommentId
});

export const deletePostCommentSuccess = (body, commentId, postId, parentCommentId) => ({
    type: constants.DELETE_POST_COMMENT_SUCCESS,
    body,
    commentId,
    postId,
    parentCommentId
});

export const getPostCommentsRequest = (postId) => ({
    type: constants.GET_POST_COMMENTS_REQUEST,
    postId
});

export const getPostCommentsSuccess = (comments, postId) => ({
    type: constants.GET_POST_COMMENTS_SUCCESS,
    comments,
    postId
});

export const getPostCommentsFailure = () => ({
    type: constants.GET_POST_COMMENTS_FAILURE
});

export const trackVideoViewRequest = (postId) => ({
    type: constants.TRACK_VIDEO_VIEW_REQUEST,
    postId
});

export const trackVideoViewSuccess = () => ({
    type: constants.TRACK_VIDEO_VIEW_SUCCESS
});

export const editPostCommentRequest = (text, commentId, postId, parentCommentId) => ({
    type: constants.EDIT_POST_COMMENT_REQUEST,
    text,
    commentId,
    postId,
    parentCommentId
});

export const editPostCommentSuccess = (text, commentId, postId, parentCommentId) => ({
    type: constants.EDIT_POST_COMMENT_SUCCESS,
    text,
    commentId,
    postId,
    parentCommentId
});

export const editPostCommentFailure = () => ({
    type: constants.EDIT_POST_COMMENT_FAILURE
});

export const replyPostCommentRequest = (text, commentId) => ({
    type: constants.REPLY_POST_COMMENT_REQUEST,
    text,
    commentId,
});

export const replyPostCommentSuccess = (commentId, body) => ({
    type: constants.REPLY_POST_COMMENT_SUCCESS,
    commentId,
    body
});

export const replyPostCommentFailure = () => ({
    type: constants.REPLY_POST_COMMENT_FAILURE
});

export const updateArtistRequest = (payload) => ({
    type: constants.UPDATE_ARTIST_REQUEST,
    payload
});

export const updateArtistSuccess = (artistProfile) => ({
    type: constants.UPDATE_ARTIST_SUCCESS,
    artistProfile
});

export const updateArtistFailure = () => ({
    type: constants.UPDATE_ARTIST_FAILURE
});

export const getTransactionStatsRequest = query => ({
    type: constants.GET_TRANSACTION_STATS_REQUEST,
    query
});

export const getTransactionStatsSuccess = response => ({
    type: constants.GET_TRANSACTION_STATS_SUCCESS,
    response
});

export const getTransactionStatsFailure = () => ({
    type: constants.GET_TRANSACTION_STATS_FAILURE
});

export const subscribeArtistRequest = (userId) => ({
    type: constants.SUBSCRIBE_ARTIST_REQUEST,
    userId
});

export const subscribeArtistSuccess = (userId) => ({
    type: constants.SUBSCRIBE_ARTIST_SUCCESS,
    userId
});

export const subscribeArtistFailure = () => ({
    type: constants.SUBSCRIBE_ARTIST_FAILURE
});

export const unsubscribeArtistRequest = (userId) => ({
    type: constants.UNSUBSCRIBE_ARTIST_REQUEST,
    userId
});

export const unsubscribeArtistSuccess = (userId) => ({
    type: constants.UNSUBSCRIBE_ARTIST_SUCCESS,
    userId
});

export const unsubscribeArtistFailure = () => ({
    type: constants.UNSUBSCRIBE_ARTIST_FAILURE
});

export const updateScheduleRequest = (payload) => ({
    type: constants.UPDATE_SCHEDULE_REQUEST,
    payload
});

export const updateScheduleSuccess = (schedule) => ({
    type: constants.UPDATE_SCHEDULE_SUCCESS,
    schedule
});

export const updateScheduleFailure = () => ({
    type: constants.UPDATE_SCHEDULE_FAILURE
});

export const getNotificationCountRequest = () => ({
    type: constants.GET_NOTIFICATION_COUNT_REQUEST
});

export const getNotificationCountSuccess = (count) => ({
    type: constants.GET_NOTIFICATION_COUNT_SUCCESS,
    count
});

export const getNotificationCountFailure = () => ({
    type: constants.GET_NOTIFICATION_COUNT_FAILURE
});

export const getNotificationsRequest = () => ({
    type: constants.GET_NOTIFICATIONS_REQUEST
});

export const getNotificationsSuccess = (notifications) => ({
    type: constants.GET_NOTIFICATIONS_SUCCESS,
    notifications
});

export const getNotificationsFailure = () => ({
    type: constants.GET_NOTIFICATIONS_FAILURE
});

export const clearNotifications = () => ({
    type: constants.CLEAR_NOTIFICATIONS
});

export const postMassMailingRequest = (payload) => ({
    type: constants.POST_MASS_MAILING_REQUEST,
    payload
});

export const postMassMailingSuccess = () => ({
    type: constants.POST_MASS_MAILING_SUCCESS
});

export const postMassMailingFailure = () => ({
    type: constants.POST_MASS_MAILING_FAILURE
});

export const postTipJarRequest = (payload) => ({
    type: constants.POST_TIP_JAR_REQUEST,
    payload
});

export const postTipJarSuccess = (tipJar) => ({
    type: constants.POST_TIP_JAR_SUCCESS,
    tipJar
});

export const postTipJarFailure = () => ({
    type: constants.POST_TIP_JAR_FAILURE
});

export const getTipJarRequest = (slug) => ({
    type: constants.GET_TIP_JAR_REQUEST,
    slug
});

export const getTipJarSuccess = (tipJar) => ({
    type: constants.GET_TIP_JAR_SUCCESS,
    tipJar
});

export const getTipJarFailure = () => ({
    type: constants.GET_TIP_JAR_FAILURE,
});

export const completeTipJarRequest = (slug) => ({
    type: constants.COMPLETE_TIP_JAR_REQUEST,
    slug
});

export const completeTipJarSuccess = () => ({
    type: constants.COMPLETE_TIP_JAR_SUCCESS
});

export const completeTipJarFailure = () => ({
    type: constants.COMPLETE_TIP_JAR_FAILURE
});

export const switchTipJarRequest = (slug, tipJarEnabled) => ({
    type: constants.SWITCH_TIP_JAR_REQUEST,
    slug,
    tipJarEnabled
});

export const switchTipJarSuccess = (tipJarEnabled) => ({
    type: constants.SWITCH_TIP_JAR_SUCCESS,
    tipJarEnabled
});

export const switchTipJarFailure = () => ({
    type: constants.SWITCH_TIP_JAR_FAILURE
});

export const getStatsPerPostRequest = () => ({
    type: constants.GET_STATS_PER_POSTS_REQUEST,
});

export const getStatsPerPostSuccess = (body) => ({
    type: constants.GET_STATS_PER_POSTS_SUCCESS,
    body
});

export const getStatsPerPostFailure = () => ({
    type: constants.GET_STATS_PER_POSTS_FAILURE,
});

export const getStatsPerFansRequest = (period) => ({
    type: constants.GET_STATS_PER_FANS_REQUEST,
    period
});

export const getStatsPerFansSuccess = (body) => ({
    type: constants.GET_STATS_PER_FANS_SUCCESS,
    body
});

export const getStatsPerFansFailure = () => ({
    type: constants.GET_STATS_PER_FANS_FAILURE,
});

export const getStatsPerAuthorsRequest = (period) => ({
    type: constants.GET_STATS_PER_AUTHORS_REQUEST,
    period
});

export const getStatsPerAuthorsSuccess = (body) => ({
    type: constants.GET_STATS_PER_AUTHORS_SUCCESS,
    body
});

export const getStatsPerAuthorsFailure = () => ({
    type: constants.GET_STATS_PER_AUTHORS_FAILURE,
});

export const changeAuthorPostsFilter = (filter) => ({
    type: constants.CHANGE_AUTHOR_POSTS_FILTER,
    filter
});

export const updateArtistWishlistRequest = (payload) => ({
    type: constants.UPDATE_ARTIST_WISHLIST_REQUEST,
    payload
});

export const updateArtistWishlistSuccess = (wishlist) => ({
    type: constants.UPDATE_ARTIST_WISHLIST_SUCCESS,
    wishlist
});

export const updateArtistWishlistFailure = () => ({
    type: constants.UPDATE_ARTIST_WISHLIST_FAILURE
});

export const sendErrorRequest = (userId, error) => ({
    type: constants.SEND_ERROR_REQUEST,
    userId,
    error
});

export const sendErrorSuccess = () => ({
    type: constants.SEND_ERROR_SUCCESS,
});

export const sendErrorFailure = () => ({
    type: constants.SEND_ERROR_FAILURE
});

// stats by fans

export const getMembersStatsRequest = query => ({
    type: constants.GET_MEMBERS_STATS_REQUEST,
    query
});

export const getMembersStatsSuccess = (response, query) => ({
    type: constants.GET_MEMBERS_STATS_SUCCESS,
    response,
    query
});

export const getMembersStatsFailure = () => ({
    type: constants.GET_MEMBERS_STATS_FAILURE
});

export const getArtistsRequest = () => ({
    type: constants.GET_ARTISTS_REQUEST
});

export const getArtistsSuccess = (artists) => ({
    type: constants.GET_ARTISTS_SUCCESS,
    artists
});

export const getArtistsFailure = () => ({
    type: constants.GET_ARTISTS_FAILURE
});

export const createMembersStatsMassMailingRequest = (payload) => ({
    type: constants.CREATE_MEMBERS_STATS_MASS_MAILING_REQUEST,
    payload
});

export const createMembersStatsMassMailingSuccess = () => ({
    type: constants.CREATE_MEMBERS_STATS_MASS_MAILING_SUCCESS
});

export const createMembersStatsMassMailingFailure = () => ({
    type: constants.CREATE_MEMBERS_STATS_MASS_MAILING_FAILURE
});

export const getUserDonationsDetailsRequest = (userId, period) => ({
    type: constants.GET_USER_DONATIONS_DETAILS_REQUEST,
    userId,
    period
});

export const getUserDonationsDetailsSuccess = (donations) => ({
    type: constants.GET_USER_DONATIONS_DETAILS_SUCCESS,
    donations
});

export const getUserDonationsDetailsFailure = () => ({
    type: constants.GET_USER_DONATIONS_DETAILS_FAILURE
});