import { call, put, take, fork, race, select } from 'redux-saga/effects'
import { delay } from 'redux-saga';
import qs from 'query-string';
import * as constants from './constants'
import {
    getArtistSuccess, getArtistFailure, getArtistPostsSuccess, getArtistPostsFailure, getArtistPostsByPeriodSuccess,
    getArtistPostsByPeriodFailure, buyPostsForPeriodFailure, buyPostsForPeriodSuccess,
    postLikeSuccess, postLikeFailure, createPostCommentSuccess, createPostCommentFailure, getPostCommentsSuccess,
    getPostCommentsRequest,
    getPostCommentsFailure, deletePostCommentSuccess, getTransactionStatsSuccess, getTransactionStatsFailure, getArtistPinnedPostSuccess,
    commentLikeFailure, commentLikeSuccess, getArtistPinnedPostFailure, trackVideoViewSuccess, editPostCommentSuccess,
    updateArtistSuccess, updateArtistFailure, replyPostCommentSuccess, replyPostCommentFailure,
    getNotificationCountSuccess, getNotificationCountFailure, getNotificationsFailure, getNotificationsSuccess,
    updateScheduleFailure, updateScheduleSuccess, subscribeArtistSuccess, subscribeArtistFailure,
    unsubscribeArtistSuccess,
    unsubscribeArtistFailure, getArtistAuthorsFailure, getArtistAuthorsSuccess, postTipJarFailure, postTipJarSuccess,
    getTipJarFailure, getTipJarSuccess, completeTipJarFailure, completeTipJarSuccess, switchTipJarFailure,
    switchTipJarSuccess, getTipJarRequest, postMassMailingSuccess, postMassMailingFailure, getStatsPerPostFailure,
    getStatsPerPostSuccess, getStatsPerFansSuccess, getStatsPerFansFailure, getStatsPerAuthorsFailure, getStatsPerAuthorsSuccess,
    getArtistPostsRequest, getArtistPinnedPostRequest, updateArtistWishlistSuccess, updateArtistWishlistFailure, sendErrorFailure,
    sendErrorSuccess, getMembersStatsFailure, getMembersStatsSuccess, getArtistsSuccess, getArtistsFailure,
    createMembersStatsMassMailingSuccess, createMembersStatsMassMailingFailure, getUserDonationsDetailsSuccess,
    getUserDonationsDetailsFailure} from './actions';

import request from '../../helpers/request';
import {changePeriodUnlockModalStep, showSuccessMessage} from '../ui/actions';
import {periodUnlockModalSteps, postComments} from '../ui/constants';
import {uploadImage} from '../assets/sagas';

export function* getArtist() {
    while (true) {
        const {slug} = yield take(constants.GET_ARTIST_REQUEST);
        const response = yield call(request, `/artists/${slug}`);
        const body = yield response.json();
        if (response.ok) {
            yield put(getArtistSuccess(body));
        } else {
            yield put(getArtistFailure());
        }
    }
}

export function* getArtistAuthors() {
    while (true) {
        const {slug} = yield take(constants.GET_ARTIST_AUTHORS_REQUEST);
        const response = yield call(request, `/artists/${slug}/authors`);
        const body = yield response.json();
        if (response.ok) {
            yield put(getArtistAuthorsSuccess(body));
        } else {
            yield put(getArtistAuthorsFailure());
        }
    }
}

export function* getArtistPosts() {
    while (true) {
        const {slug, lastPostId} = yield take(constants.GET_ARTIST_POSTS_REQUEST);
        const state = yield select();
        const query = {};
        if (state.artist.authorPostsFilter.value) query.author_id = state.artist.authorPostsFilter._id || '';
        if (lastPostId) query.last_post_id = lastPostId;
        const queryString = qs.stringify(query);
        const response = yield call(request, `/artists/${slug}/posts/${constants.POSTS_PER_PAGE}/${queryString ? `?${queryString}` : ''}`);
        const body = yield response.json();
        if (response.ok) {
            yield put(getArtistPostsSuccess(body))
        } else {
            yield put(getArtistPostsFailure())
        }
    }
}

export function* getArtistPostsByPeriod() {
    while (true) {
        const {slug, periodStartDate, periodEndDate, resolve} = yield take(constants.GET_ARTIST_POSTS_BY_PERIOD_REQUEST);
        const state = yield select();
        const query = {};
        if (state.artist.authorPostsFilter.value) query.author_id = state.artist.authorPostsFilter._id || '';
        const queryString = qs.stringify(query);
        const response = yield call(request, `/artists/${slug}/posts-by-period/${periodStartDate}/${periodEndDate}${queryString ? `?${queryString}` : ''}`);
        const body = yield response.json();
        if (response.ok) {
            resolve();
            yield put(getArtistPostsByPeriodSuccess(body, periodStartDate, periodEndDate));
        } else {
            yield put(getArtistPostsByPeriodFailure());
        }
    }
}

export function* buyPostsForPeriod() {
    while (true) {
        const {payload: {stripeToken, periodEndDate, artistSlug}} = yield take(constants.BUY_POSTS_FOR_PERIOD_REQUEST);
        const response = yield call(request, `/users/buy-period`, 'POST', {
            stripe_token: stripeToken,
            period_end_date: periodEndDate,
            artist_slug: artistSlug
        });
        const body = yield response.json();
        if (response.ok) {
             yield put(buyPostsForPeriodSuccess());
             yield put(changePeriodUnlockModalStep(periodUnlockModalSteps.CONGRATULATIONS));
        } else {
            if (body.error && body.error.message) {
                yield put(buyPostsForPeriodFailure(body.error.message));
            }
        }
    }
}

export function* commentLike() {
    while (true) {
        const {commentId, parentCommentId} = yield take(constants.COMMENT_LIKE_REQUEST);
        const response = yield call(request, '/comments/' + commentId + '/like', 'POST', {});
        if (response.ok) {
            yield put(commentLikeSuccess(commentId, parentCommentId));
        } else {
            yield put(commentLikeFailure());
        }
    }
}

export function* postLike() {
    while (true) {
        const {postId} = yield take(constants.POST_LIKE_REQUEST);
        const response = yield call(request, '/posts/' + postId + '/like', 'POST', {});
        if (response.ok) {
            yield put(postLikeSuccess(postId));
        } else {
            yield put(postLikeFailure());
        }
    }
}

export function* createPostComment() {
    while(true) {
        const {comment, postId} = yield take(constants.CREATE_POST_COMMENT_REQUEST);
        const response = yield call(request, `/comments`, 'POST', {
            comment,
            post_id: postId
        });
        const body = yield response.json();
        if (response.ok) {
            yield put(createPostCommentSuccess(body));
        } else {
            if (body.error && body.error.message) {
                yield put(createPostCommentFailure(body.error.message));
            }
        }
    }
}

export function* getPostComments() {
    while(true) {
        const {postId} = yield take(constants.GET_POST_COMMENTS_REQUEST);
        const response = yield call(request, '/posts/' + postId + '/comments');
        const body = yield response.json();
        if (response.ok) {
            yield put(getPostCommentsSuccess(body, postId));
        } else {
            yield put(getPostCommentsFailure());
        }
    }
}

export function* deletePostComment() {
    while(true) {
        const {commentId, postId, parentCommentId} = yield take(constants.DELETE_POST_COMMENT_REQUEST);
        const response = yield call(request, `/comments/${commentId}`, 'DELETE', {});
        const body = yield response.json();
        if (response.ok) {
            yield put(deletePostCommentSuccess(body, commentId, postId, parentCommentId));
        }
    }
}

export function* trackVideoView() {
    while (true) {
        const {postId} = yield take(constants.TRACK_VIDEO_VIEW_REQUEST);
        const response = yield call(request, '/posts/' + postId + '/view', 'POST', {});
        if (response.ok) {
            yield put(trackVideoViewSuccess(postId));
        }
    }
}

export function* getArtistPinnedPost() {
    while (true) {
        const {slug} = yield take(constants.GET_ARTIST_PINNED_POST_REQUEST);
        const state = yield select();
        const query = {};
        if (state.artist.authorPostsFilter.value) query.author_id = state.artist.authorPostsFilter._id || '';
        const queryString = qs.stringify(query);
        const response = yield call(request, `/artists/${slug}/pinned-post${queryString ? `?${queryString}` : ''}`);
        const body = yield response.json();
        if (response.ok) {
            yield put(getArtistPinnedPostSuccess(body.pinned_post));
        } else {
            yield put(getArtistPinnedPostFailure());
        }
    }
}

export function* editPostComment() {
    while (true) {
        const {text, commentId, postId, parentCommentId} = yield take(constants.EDIT_POST_COMMENT_REQUEST);
        const response = yield call(request, `/comments/${commentId}`, 'PUT', {comment: text});
        if (response.ok) {
            yield put(editPostCommentSuccess(text, commentId, postId, parentCommentId))
        }
    }
}

export function* replyPostComment() {
    while (true) {
        const { text, commentId } = yield take(constants.REPLY_POST_COMMENT_REQUEST);
        const response = yield call(request, `/comments/${commentId}/reply`, 'POST', {comment: text});
        const body = yield response.json();
        if (response.ok) {
            yield put(replyPostCommentSuccess(commentId, body))
        } else {
            yield put(replyPostCommentFailure())
        }
    }
}

export function* updateArtist() {
    while (true) {
        const {payload: {values, slug, resolve, reject}} = yield take(constants.UPDATE_ARTIST_REQUEST);
        let avatarPath, coverPath;
        if (values.avatar) {
            avatarPath = yield call(uploadImage, values.avatar);
        }
        if (values.cover) {
            coverPath = yield call(uploadImage, values.cover);
        }

        let body = {};
        if (avatarPath) body.avatar = avatarPath;
        if (coverPath) body.cover = coverPath;
        body = Object.assign({}, body, values);

        const response = yield call(request, `/artists/${slug}`, 'PUT', body);


        const responseBody = yield response.json();
        if (response.ok) {
            resolve();
            yield put(updateArtistSuccess(responseBody));
            yield put(showSuccessMessage());
        } else {
            reject();
            yield put(updateArtistFailure());
        }
    }
}

export function* subscribeArtist() {
    while (true) {

        const {userId} = yield take(constants.SUBSCRIBE_ARTIST_REQUEST);
        const response = yield call(request, '/users/subscribe', 'POST', {user_id: userId});
        if (response.ok) {
            yield put(subscribeArtistSuccess(userId));
        } else {
            yield put(subscribeArtistFailure());
        }
    }
}

export function* updateSchedule() {
    while (true) {
        const {payload: {slug, schedule, resolve, reject}} = yield take(constants.UPDATE_SCHEDULE_REQUEST);
        const schedulePayload = {
            monday: schedule.monday,
            tuesday: schedule.tuesday,
            wednesday: schedule.wednesday,
            thursday: schedule.thursday,
            friday: schedule.friday,
            saturday: schedule.saturday,
            sunday: schedule.sunday,
        };

        const response = yield call(request, `/artists/${slug}/schedule`, 'PUT', schedulePayload);
        const body = yield response.json();

        if (response.ok) {
            resolve();
            yield put(updateScheduleSuccess(body))
        } else {
            reject();
            yield put(updateScheduleFailure());
        }
    }
}

export function* unsubscribeArtist() {
    while (true) {

        const {userId} = yield take(constants.UNSUBSCRIBE_ARTIST_REQUEST);
        const response = yield call(request, '/users/unsubscribe', 'POST', {user_id: userId});
        if (response.ok) {
            yield put(unsubscribeArtistSuccess(userId))
        } else {
            yield put(unsubscribeArtistFailure())
        }
    }
}

export function* getNotificationCount() {
    while (true) {
        const NOTIFICATION_POLLING_DELAY_MS = 60000;
        const response = yield call(request, '/notifications/count');
        const body = yield response.json();
        if (response.ok) {
            yield put(getNotificationCountSuccess(body));
            yield call(delay, NOTIFICATION_POLLING_DELAY_MS);
        } else {
            yield put(getNotificationCountFailure());
        }
    }
}

function* getNotifications() {
    while (true) {
        yield take(constants.GET_NOTIFICATIONS_REQUEST);
        const response = yield call(request, '/notifications');
        const body = yield response.json();

        if (response.ok) {
            yield put(getNotificationsSuccess(body))
        } else {
            yield put(getNotificationsFailure())
        }
    }
}

function* createMassMailing() {
    while (true) {
        const {payload: {values, resolve, reject}} = yield take(constants.POST_MASS_MAILING_REQUEST);
        const response = yield call(request, '/mass-mailing', 'POST', values);

        if (response.ok) {
            resolve();
            yield put(postMassMailingSuccess());
        } else {
            reject();
            yield put(postMassMailingFailure());
        }
    }
}

function* postTipJar() {
    while (true) {
        const { payload: {values, slug, resolve, reject} } = yield take(constants.POST_TIP_JAR_REQUEST);
        let imagePath;
        if (values.image) {
            imagePath = yield call(uploadImage, values.image);
        }
        const body = {
            title: values.title,
            description: values.description,
            goal_amount: values.goal_amount
        };
        if (imagePath) body.image = imagePath;
        const response = yield call(request, `/artists/${slug}/tip_jar`, 'POST', body);
        const responseBody = yield response.json();
        if (response.ok) {
            resolve();
            yield put(postTipJarSuccess(responseBody))
        } else {
            reject();
            yield put(postTipJarFailure());
        }
    }
}

function* getTipJar() {
    while (true) {
        const { slug } = yield take(constants.GET_TIP_JAR_REQUEST);
        const response = yield call(request, `/artists/${slug}/tip_jar`);
        const body = yield response.json();

        if (response.ok) {
            yield put(getTipJarSuccess(body))
        } else {
            yield put(getTipJarFailure())
        }
    }
}

function* completeTipJar() {
    while (true) {
        const { slug } = yield take(constants.COMPLETE_TIP_JAR_REQUEST);
        const response = yield call(request, `/artists/${slug}/tip_jar/close`, 'POST', {});

        if (response.ok) {
            yield put(completeTipJarSuccess())
        } else {
            yield put(completeTipJarFailure())
        }
    }
}

function* switchTipJar() {
    while (true) {
        const { slug, tipJarEnabled } = yield take(constants.SWITCH_TIP_JAR_REQUEST);
        const response = yield call(request, `/artists/${slug}/`, 'PUT', { tip_jar_enabled: tipJarEnabled });

        if (response.ok) {
            yield put(switchTipJarSuccess(tipJarEnabled));
            yield put(getTipJarRequest(slug))
        } else {
            yield put(switchTipJarFailure())
        }
    }
}

function* getStatsPerPost() {
    while (true) {
        yield take(constants.GET_STATS_PER_POSTS_REQUEST);
        const response = yield call(request, '/donations/by-posts');
        const body = yield response.json();
        if (response.ok) {
            yield put(getStatsPerPostSuccess(body))
        } else {
            yield put(getStatsPerPostFailure())
        }
    }
}

function* getStatsPerFans() {
    while (true) {
        const { period } = yield take(constants.GET_STATS_PER_FANS_REQUEST);
        const response = yield call(request, `/donations/by-fans/${period}`);
        const body = yield response.json();
        if (response.ok) {
            yield put(getStatsPerFansSuccess(body))
        } else {
            yield put(getStatsPerFansFailure())
        }
    }
}

function* getStatsPerAuthors() {
    while (true) {
        const { period } = yield take(constants.GET_STATS_PER_AUTHORS_REQUEST);
        const response = yield call(request, `/donations/by-authors/${period}`);
        const body = yield response.json();
        if (response.ok) {
            yield put(getStatsPerAuthorsSuccess(body))
        } else {
            yield put(getStatsPerAuthorsFailure())
        }
    }
}

function* getMembersStats() {
    while (true) {
        const { query } = yield take(constants.GET_MEMBERS_STATS_REQUEST);
        const queryString = qs.stringify(query);
        const response = yield call(request, `/statistics/members${queryString ? `?${queryString}` : ''}`);
        const body = yield response.json();
        if (response.ok) {
            yield put(getMembersStatsSuccess(body, query))
        } else {
            yield put(getMembersStatsFailure())
        }
    }
}

function* watchPollSaga() {
    while (true) {
        yield take(constants.GET_NOTIFICATION_COUNT_REQUEST);
        yield race([
            call(getNotificationCount),
        ]);
    }
}

function* changeAuthorPostFilters() {
    while(true) {
        yield take(constants.CHANGE_AUTHOR_POSTS_FILTER);
        const state = yield select();
        const artistSlug = state.artist.artistProfile.slug;
        yield put(getArtistPostsRequest(artistSlug));
        yield put(getArtistPinnedPostRequest(artistSlug));
    }
}

export function* updateArtistWishlist() {
    while (true) {
        const {payload: {slug, values, resolve, reject}} = yield take(constants.UPDATE_ARTIST_WISHLIST_REQUEST);

        const response = yield call(request, `/artists/${slug}/wishlist`, 'PUT', {...values});
        const body = yield response.json();

        if (response.ok) {
            resolve();
            yield put(updateArtistWishlistSuccess(body))
        } else {
            reject();
            yield put(updateArtistWishlistFailure());
        }
    }
}

export function* getTransactionStats() {
    while (true) {
        const {query} = yield take(constants.GET_TRANSACTION_STATS_REQUEST);
        const queryString = qs.stringify(query);
        const response = yield call(request, `/statistics/transactions${queryString ? `?${queryString}` : ''}`);
        const body = yield response.json();

        if (response.ok) {
            yield put(getTransactionStatsSuccess(body))
        } else {
            yield put(getTransactionStatsFailure());
        }
    }
}

export function* sendError() {
    while (true) {
        const { userId, error } = yield take(constants.SEND_ERROR_REQUEST);
        const errorStack = JSON.stringify(error.stack);
        const response = yield call(request, `/logs`, 'POST', {
            user_id: userId,
            message: error.message,
            stack: errorStack
        });

        if (response.ok) {
            yield put(sendErrorSuccess())
        } else {
            yield put(sendErrorFailure());
        }
    }
}

export function* getArtists() {
    while (true) {
        yield take(constants.GET_ARTISTS_REQUEST);
        const response = yield call(request, `/artists`);
        const body = yield response.json();

        if (response.ok) {
            yield put(getArtistsSuccess(body))
        } else {
            yield put(getArtistsFailure());
        }
    }
}

export function* createMembersStatsMassMailing() {
    while (true) {
        const {payload} = yield take(constants.CREATE_MEMBERS_STATS_MASS_MAILING_REQUEST);
        const response = yield call(request, `/mass-mailing/members-stats-selection`, 'POST', payload);

        if (response.ok) {
            yield put(createMembersStatsMassMailingSuccess())
        } else {
            yield put(createMembersStatsMassMailingFailure());
        }
    }
}

export function* getUserDonationsDetails() {
    while (true) {
        const {userId, period} = yield take(constants.GET_USER_DONATIONS_DETAILS_REQUEST);
        const response = yield call(request, `/donations/by-fans/${period}/details/${userId}`);
        const body = yield response.json();

        if (response.ok) {
            yield put(getUserDonationsDetailsSuccess(body))
        } else {
            yield put(getUserDonationsDetailsFailure());
        }
    }
}

function startSagas(...sagas) {
    return function* rootSaga() {
        yield sagas.map(saga => fork(saga))
    }
}

export default startSagas(getArtist, getArtistPosts, getArtistPostsByPeriod, buyPostsForPeriod, postLike, commentLike,
    createPostComment, getPostComments, deletePostComment, getArtistPinnedPost, updateArtist, trackVideoView, editPostComment,
    replyPostComment, getNotifications, watchPollSaga, updateSchedule, subscribeArtist, unsubscribeArtist, getArtistAuthors,
    postTipJar, getTipJar, completeTipJar, switchTipJar, createMassMailing, getStatsPerPost, getStatsPerFans,
    getStatsPerAuthors, changeAuthorPostFilters, updateArtistWishlist, sendError, getTransactionStats, getMembersStats,
    getArtists, createMembersStatsMassMailing, getUserDonationsDetails);
