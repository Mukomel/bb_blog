import * as constants from './constants';

const initialState = {
    isRequestingDonation: false,
    isRequestingTopFansList: false,
    currentRound: {},
    topFansList: []
};

export default function(state = initialState, action) {
    switch(action.type) {

        case (constants.GET_CURRENT_ROUND_SUCCESS):
            return Object.assign({}, state, {
                currentRound: action.currentRound
            });

        case (constants.GET_TOP_FANS_REQUEST):
            return Object.assign({}, state, {
                isRequestingTopFansList: true
            });

        case (constants.GET_TOP_FANS_SUCCESS):
            return Object.assign({}, state, {
                topFansList: action.topFansList,
                isRequestingTopFansList: false
            });

        case (constants.GET_TOP_FANS_FAILURE):
            return Object.assign({}, state, {
                isRequestingTopFansList: false
            });

        default:
            return state;
    }
}
