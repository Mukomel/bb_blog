import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import {topFansTabs} from '../../../ui/constants';
import Tabs from '../../../../base-components/Tabs/Tabs';
import TopFansList from '../TopFansList/TopFansList';
import CircularProgress  from '@material-ui/core/CircularProgress';

import * as styles from './TopFansSection.module.scss';

const tabsList = [
    {tab: topFansTabs.WEEKLY, title: 'THIS WEEK'},
    {tab: topFansTabs.MONTHLY, title: 'THIS MONTH'},
    {tab: topFansTabs.ALL_TIME, title: 'ALL TIME'}
];

export default class TopFansSection extends React.Component {
    render() {
        const {currentTab, topFansList, onTabChange, isRequestingTopFansList} = this.props;

        return (
            <div className={styles.container}>
                <div className={styles.title}>Top Fans</div>
                <div className={styles.subTitle}>Get as many fan points as possible for a chance to be a top fan and win weekly stuff!</div>
                <div className={styles.topFansListContainer}>
                    <Tabs tabsList={tabsList} currentTab={currentTab} onTabChange={onTabChange}/>
                    <TopFansList topFansList={topFansList[currentTab.toLowerCase()]} currentTab={currentTab} className={styles.topFansList} />
                    {
                        isRequestingTopFansList
                            && <div className={styles.spinnerContainer}>
                                <CircularProgress />
                            </div>
                    }
                </div>
                <div className={styles.bottomTitle}>Thanks for your support guys!</div>
            </div>
        )
    }
}

TopFansSection.propTypes = {
    topFansList: PropTypes.array.isRequired,
    currentTab: PropTypes.string.isRequired,
    onTabChange: PropTypes.func.isRequired,
    isRequestingTopFansList: PropTypes.bool.isRequired
};