import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import refreshImg from '../../../../assets/images/refresh.svg';
import catImg from '../../../../assets/images/cat.svg';

import styles from './MonthlySubscription.module.scss';

export default class MonthlySubscription extends React.Component {
    handleSubscribeClick = () => {
        const {isLoggedIn, onSubscribe, onSignUp} = this.props;
        if (isLoggedIn) {
            onSubscribe();
        } else {
            onSignUp();
        }
    };

    render() {
        const {subscriptionPrice, userHasSubscription, userSubscriptonsLink} = this.props;

        return (
            <div className={styles.container}>
                <div className={styles.content}>
                    <div className={styles.calendarContainer}>
                        <div className={styles.calendar}>
                            <div className={styles.daysNumber}>31</div>
                            <img src={refreshImg} className={styles.refreshImg} />
                        </div>
                        <img src={catImg} className={styles.catImg} />
                    </div>

                    <div className={styles.subscriptionContainer}>
                        <div className={styles.titleTemp}>We are taking some time off to do projects and prepare for finals. We will re open subscriptions to the site the first week of May. Be sure to register, and you will receive an email when we open them again!
                        </div>
                        {/*<div className={styles.title}>Get full access and see what the three of us have to offer</div>*/}
                        {/*<div className={styles.description}>Monthly subscription ${subscriptionPrice ? (subscriptionPrice/100).toLocaleString() : ''}</div>*/}
                        {/*{*/}
                            {/*userHasSubscription*/}
                                {/*? <Link to={userSubscriptonsLink} className={styles.subscriptionsLink}>My subscriptions</Link>*/}
                                {/*: <button*/}
                                    {/*className={styles.subscribeButton}*/}
                                    {/*onClick={this.handleSubscribeClick}*/}
                                {/*>*/}
                                    {/*Subscribe*/}
                                {/*</button>*/}
                        {/*}*/}
                        <Link to='/brandtandnash' className={styles.profileLink}>View our profile</Link>
                    </div>
                </div>
            </div>
        )
    }
}

MonthlySubscription.propTypes = {
    userSubscriptonsLink: PropTypes.string.isRequired,
    isLoggedIn: PropTypes.bool.isRequired,
    userHasSubscription: PropTypes.bool,
    subscriptionPrice: PropTypes.number,
    onSubscribe: PropTypes.func.isRequired,
    onSignUp: PropTypes.func.isRequired
};
