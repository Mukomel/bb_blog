import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import notificationIcon from '../../../../assets/images/notification_white.svg';
import menuIcon from '../../../../assets/images/menu.svg';
import Button from '../../../../base-components/Button/Button';

import styles from './MainMenuMobile.module.scss';

class MainMenuMobile extends React.Component {

    renderNotificationIcon = () => {
        const {notificationsCount, onShowMobileSidebar} = this.props;

        return (
            <Button className={styles.notificationButton} onClick={onShowMobileSidebar}>
                <img src={notificationIcon} className={styles.notificationIcon} />
                {
                    (notificationsCount > 0)
                        && <div className={styles.newNotificationIndicator} />
                }
            </Button>
        )
    };

    renderMenuIcon = () => {
        const {onShowMobileSidebar} = this.props;

        return (
            <Button className={styles.menuButton} onClick={onShowMobileSidebar}>
                <img src={menuIcon} className={styles.menuIcon}/>
            </Button>
        )
    };

    render() {
        const {user, isMobileSidebarVisible} = this.props;

        if (!user) return null;

        return (
            <div className={styles.container}>
                {this.renderNotificationIcon()}
                {this.renderMenuIcon()}
            </div>
        )
    }
}

MainMenuMobile.propTypes = {
    className: PropTypes.string,
    notificationsCount: PropTypes.number.isRequired,
    isMobileSidebarVisible: PropTypes.bool.isRequired,
    onShowMobileSidebar: PropTypes.func.isRequired
};

MainMenuMobile.defaultProps = {};

export default MainMenuMobile;
