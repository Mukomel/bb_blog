import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import Notification from '../Notification/Notification'
import { Link } from 'react-router-dom';
import { Manager, Target, Popper } from 'react-popper';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Collapse from '@material-ui/core/Collapse';
import Paper from '@material-ui/core/Paper';
import Portal from '@material-ui/core/Portal';
import Button from '@material-ui/core/Button';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import { withStyles } from '@material-ui/core/styles';
import * as styles from './MainMenu.module.scss';
import {signOut} from '../../../../helpers/auth';
import DownArrowIcon from '@material-ui/icons/ExpandMore';

const style = theme => ({
    collapseContainer: {
        minWidth: 200,
    },
    rootMenuList: {
        padding: 0,
    },
});

class MainMenu extends React.Component {
    state = {
        isMenuOpen: false,
        isNotificationOpen: false
    };

    toggleNotification = () => {
        this.setState(prevState => ({
            isNotificationOpen: !prevState.isNotificationOpen
        }), () => this.fetchNotification());
    };

    fetchNotification() {
        const { getNotifications } = this.props;
        if (this.state.isNotificationOpen) {
            getNotifications()
        }
    }

    handleMenuToggle = () => {
        this.setState(prevState => ({isMenuOpen: !prevState.isMenuOpen}));
    };

    handleMenuClose = event => {
        if (this.target.contains(event.target)) {
            return;
        }

        this.setState({ isMenuOpen: false });
    };

    render() {
        const {items, user, notifications, notificationsCount, isFetchingNotifications, classes} = this.props;
        const {isMenuOpen} = this.state;
        if (user) {

            return (
                <React.Fragment>
                    <Notification
                        onToggleNotification={this.toggleNotification}
                        isFetchingNotifications={isFetchingNotifications}
                        notificationsCount={notificationsCount}
                        isOpen={this.state.isNotificationOpen}>
                        { notifications }
                    </Notification>
                    <Manager>
                            <Target>
                                <div
                                    className={styles.menuProfile}
                                    onClick={this.handleMenuToggle}
                                    ref={node => {
                                        this.target = node
                                    }}
                                >
                                    <div className={styles.menuTitle}>My Profile</div>
                                    <DownArrowIcon className={cn([styles.arrowImg, {
                                        [styles.arrowImgActive]: this.state.isMenuOpen
                                    }])}/>
                                </div>
                            </Target>
                            <Portal>

                                <Popper
                                    placement="bottom"
                                    eventsEnabled={isMenuOpen}
                                >
                                    <ClickAwayListener onClickAway={this.handleMenuClose}>
                                        <Collapse in={isMenuOpen} id="menu-list-collapse" classes={{
                                            container: classes.collapseContainer
                                        }}>
                                            <Paper>
                                                <MenuList
                                                    classes={{
                                                        root: classes.rootMenuList
                                                    }}
                                                    role="menu">
                                                    {
                                                        items.map(item => {
                                                            let element = <Link to={item.route}>{item.label}</Link>;
                                                            return (
                                                                <MenuItem
                                                                    className={styles.menuItem}
                                                                    key={item.key}>
                                                                    {element}
                                                                </MenuItem>
                                                            )
                                                        })
                                                    }
                                                    <MenuItem onClick={signOut} className={styles.menuItem}>Sign out</MenuItem>
                                                </MenuList>
                                            </Paper>
                                        </Collapse>
                                    </ClickAwayListener>
                                </Popper>
                            </Portal>
                        </Manager>
                </React.Fragment>
            )

        } else {
            return null;
        }
    }
}

MainMenu.propTypes = {
    className: PropTypes.string,
    items: PropTypes.arrayOf(PropTypes.shape({
        key: PropTypes.number.isRequired,
        label: PropTypes.string.isRequired,
        route: PropTypes.string.isRequired,
    })).isRequired,
    notificationsCount: PropTypes.number,
    getNotifications: PropTypes.func.isRequired,
    notifications: PropTypes.array.isRequired,
    isFetchingNotifications: PropTypes.bool.isRequired,
    classes: PropTypes.object.isRequired,
};

MainMenu.defaultProps = {};

export default withStyles(style)(MainMenu);
