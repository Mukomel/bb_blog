import React from 'react';
import cn from 'classnames'
import {Link} from 'react-router-dom';
import LogoImg from '../../../../assets/images/logo_solid_no_bb_transparent.png'

import styles from './Footer.module.scss'

const Footer = ({showCompany = false}) => {
    return (
        <footer className={styles.footer}>
            <div className={cn(styles.innerContainer)}>
                <div className={styles.firstLine}>
                <Link to={'/'}> <img src={LogoImg} className={styles.footerLogo}/> </Link>
                <ul className={styles.footerNav}>
                    <li>
                        <Link to={'/privacy-policy'}>Privacy Policy</Link>
                    </li>
                    <li>
                        <Link to={'/copyright-policy'}>Copyright Policy</Link>
                    </li>
                    <li>
                        <Link to={'/terms-of-service'}>Terms of Service</Link>
                    </li>
                    <li>
                        <a target="_blank" href={'http://cs.segpay.com'}>Support</a>
                    </li>
                    <li>
                        <a href="https://paymentsbb.com/compliance-statement.html">18 U.S.C. § 2257 Compliance Statement</a>
                    </li>
                </ul>
            </div>
               <div className={cn(styles.container, styles.info)}>
                   {showCompany ? <div>
                            DNA enterprises LLC
                        </div> : null}
                        <div>
                            Please visit <a href="http://cs.segpay.com">SEGPAY.COM</a>, our authorized sales agent.
                            <br />Charges will appear on your credit card statement as <a href="http://cs.segpay.com">SEGPAY.COM*B.B.</a>
                        </div>
                </div>
        </div>
        </footer>
    )
};

export default Footer
