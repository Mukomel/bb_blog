import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import cn from 'classnames';
import {Link} from 'react-router-dom';
import LogoImg from '../../../../assets/images/logo_solid_no_bb_transparent.png';
import {userRoles} from '../../../auth/constants';
import {getNotificationsRequest, getNotificationCountRequest} from '../../../artist/actions'
import ErrorBoundary from '../../../../base-components/ErrorBoundary/ErrorBoundary';
import MainMenu from './MainMenu';
import MainMenuMobile from './MainMenuMobile';
import {layoutTypes} from '../../../ui/constants';
import Button from '../../../../base-components/Button/Button';
import arrowImage from '../../../../assets/images/arrow.svg';

import styles from './Header.module.scss';
import {showMobileSidebar} from "../../../ui/actions";

class Header extends Component {
    componentDidMount() {
        const {startGetNotification, isLoggedIn} = this.props;
        if (isLoggedIn) {
            startGetNotification()
        }
    }

    render() {
        const {
            isLoggedIn,
            showLinks,
            user,
            layoutType,
            className,
            additionalNode,
            notificationsCount,
            getNotifications,
            isFetchingNotifications,
            notifications,
            isOverlay,
            backgroundImgUrl,
            withBackground,
            isMobileSidebarVisible,
            showMobileSidebar,
            showBackButton,
            onBackButtonClick
        } = this.props;

        const menuItems = [];
        if (isLoggedIn && user) {
            if (user.role === userRoles.ADMIN || user.role === userRoles.AUTHOR) {
                menuItems.push({label: "Dashboard", key: 1, route: "/dashboard/posts"});
                menuItems.push({label: "My Profile", key: 2, route: "/dashboard/artist-profile"});
                menuItems.push({label: "My Account", key: 3, route: "/dashboard/author-profile"});
            } else {
                menuItems.push({label: "My Account", key: 1, route: "/dashboard/subscriptions"})
            }
        }

        const backgroundStyle = {};
        if (backgroundImgUrl) backgroundStyle.backgroundImage = `url(${backgroundImgUrl})`;
        return (
            <header className={cn(styles.header, className, {[styles.overlay]: isOverlay})}>
                {
                    withBackground && <div className={styles.backgroundContainer} style={backgroundStyle}/>
                }
                <div className={styles.backdropContainer}/>
                <div className={styles.headerInner}>
                    {
                        showBackButton
                            ? <Button onClick={onBackButtonClick} className={styles.backButton}>
                                <img src={arrowImage} className={styles.arrowImage}/>
                            </Button>
                            : <Link to={'/'} className={styles.logoContainer}> <img src={LogoImg}
                                                                                    className={styles.logo}/></Link>
                    }
                    <ErrorBoundary>
                        <div className={styles.rightBlock}>
                            {
                                showLinks && (
                                    ((layoutType === layoutTypes.MOBILE) || (layoutType === layoutTypes.TABLET))
                                        ? <MainMenuMobile
                                            user={user}
                                            notificationsCount={notificationsCount.count}
                                            isMobileSidebarVisible={isMobileSidebarVisible}
                                            onShowMobileSidebar={showMobileSidebar}
                                        />
                                        : <MainMenu
                                            isFetchingNotifications={isFetchingNotifications}
                                            getNotifications={getNotifications}
                                            notifications={notifications}
                                            notificationsCount={notificationsCount.count}
                                            user={user}
                                            items={menuItems}
                                        />
                                )
                            }
                            {!isLoggedIn ? <div>
                                <Link to={'/signin'} className={styles.noteLink}>Sign in</Link>
                                <Link to={'/signup'} className={styles.noteLink}>Sign up</Link>
                            </div> : null}
                            {additionalNode}
                        </div>
                    </ErrorBoundary>
                </div>
            </header>
        );
    }
}

Header.propTypes = {
    className: PropTypes.string,
    showLinks: PropTypes.bool.isRequired,
    additionalNode: PropTypes.node,
    isOverlay: PropTypes.bool,
    withBackground: PropTypes.bool,
    backgroundImgUrl: PropTypes.string,
    showBackButton: PropTypes.bool,
    onBackButtonClick: PropTypes.func
};

Header.defaultProps = {
    showLinks: true,
    isOverlay: false,
    withBackground: false,
    showBackButton: false,
};

export default connect(
    (state) => ({
        notificationsCount: state.artist.notificationCount,
        notifications: state.artist.notifications,
        isFetchingNotifications: state.artist.isFetchingNotifications,
        layoutType: state.ui.layoutType,
        isMobileSidebarVisible: state.ui.isMobileSidebarVisible,
        user: state.auth.user,
        isLoggedIn: state.auth.isLoggedIn
    }),
    (dispatch) => ({
        getNotifications: () => dispatch(getNotificationsRequest()),
        startGetNotification: () => dispatch(getNotificationCountRequest()),
        showMobileSidebar: () => dispatch(showMobileSidebar())
    })
)(Header);
