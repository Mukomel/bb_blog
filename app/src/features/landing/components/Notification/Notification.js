import React from 'react';
import { bool, number, func } from 'prop-types';
import cn from 'classnames';
import Badge from '@material-ui/core/Badge';
import notificationsIcon from '../../../../assets/images/Alert.png';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';
import { withStyles } from '@material-ui/core/styles';
import moment from 'moment';

import styles from './Notification.module.scss';

// for notification icon from @material-ui/core
const style = {
    root: {
        borderRadius: '50%',
        color: '#fff',
        padding: 3,
        cursor: 'pointer',
        marginRight: '1rem'
    },
    badge: {
        backgroundColor: '#FF7F00',
        width: 18,
        height: 18,
        top: -14,
        right: -10,
        fontSize: 10
    }
};

const Notification = ({isOpen, notificationsCount, classes: { root, badge }, onToggleNotification, isFetchingNotifications, children}) => {
    return (
        <React.Fragment>
            <Badge
                onClick={onToggleNotification}
                badgeContent={notificationsCount || ''}
                classes={{
                    root,
                    badge: notificationsCount > 0 ? badge : null
                }}>
                <img src={notificationsIcon} alt="badge icon"/>
            </Badge>
            {
                isOpen ?
                    <div className={cn(
                        styles.notificationBox,
                        { [styles.isFetching]: isFetchingNotifications })}>
                        <div className={styles.arrowUp} />
                        <div className={styles.arrowUpBorder} />
                        {
                            isFetchingNotifications ?
                                <SpinnerWithBackdrop /> :
                                <React.Fragment>
                                    {
                                        children.length > 0 ?
                                            <ul className={styles.notificationList}>
                                                {
                                                    children.map(({ _id, created_at, title, message, is_viewed }) => (
                                                        <li key={_id}>
                                                            <span>{ moment(created_at).format('MMM DD, YYYY')}</span>
                                                            <h4>{ title }</h4>
                                                            <p dangerouslySetInnerHTML={{ __html: message }} />
                                                            {
                                                                !is_viewed &&
                                                                <span className={styles.newNotificationIcon}>New</span>
                                                            }
                                                        </li>
                                                    )).reverse()
                                                }
                                            </ul> :
                                            <div className={styles.emptyNotification}>
                                                <p>No new notification!</p>
                                            </div>
                                    }
                                </React.Fragment>
                        }
                    </div>
                    : null
            }
        </React.Fragment>
    )
};

Notification.propTypes = {
    isOpen: bool.isRequired,
    notificationsCount: number,
    isFetchingNotifications: bool.isRequired,
    onToggleNotification: func.isRequired
};

export default withStyles(style)(Notification);
