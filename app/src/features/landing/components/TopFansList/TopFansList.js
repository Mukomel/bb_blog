import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import firstPlaceWeekly from '../../../../assets/images/first_place_green.svg';
import firstPlaceMonthly from '../../../../assets/images/first_place_pink.svg';
import firstPlaceAllTime from '../../../../assets/images/first_place_orange.svg';
import secondPlace from '../../../../assets/images/second_place.svg';
import thirdPlace from '../../../../assets/images/third_place.svg';
import starIcon from '../../../../assets/images/star.png';
import {topFansTabs} from "../../../ui/constants";

import styles from './TopFansList.module.scss';

const topFansFirstPlaceIcon = {
    [topFansTabs.WEEKLY]: firstPlaceWeekly,
    [topFansTabs.MONTHLY]: firstPlaceMonthly,
    [topFansTabs.ALL_TIME]: firstPlaceAllTime
};

export default class TopFansList extends React.Component {
    renderListItem = ({points, user}, index) => {
        let className, numberComponent;

        switch (index) {
            case 0:
                className = styles.first;
                numberComponent = <img src={topFansFirstPlaceIcon[this.props.currentTab]} className={styles.cupImage}/>;
                break;

            case 1:
                className = styles.second;
                numberComponent = <img src={secondPlace} className={styles.cupImage}/>;
                break;

            case 2:
                className = styles.third;
                numberComponent = <img src={thirdPlace} className={styles.cupImage}/>;
                break;

            default:
                numberComponent = <div className={styles.number}>{index + 1}</div>;
                break;
        }

        return (
            <div className={cn(styles.item, className)} key={`${user.username}${index}`}>
                <div className={styles.numberContainer}>
                    {numberComponent}
                </div>
                <div className={styles.name}>
                    {user.username}
                </div>
                <img src={starIcon} className={styles.starImage}/>
                <div className={styles.points}>
                    {points}
                </div>
            </div>
        )
    };

    render() {
        const {topFansList, className} = this.props;

        return (
            <div className={cn(styles.container, className)}>
                <div className={styles.innerContainer}>
                    {
                        topFansList && topFansList.map(this.renderListItem)
                    }
                </div>
                <div className={styles.bottomDescription}>You earn fan points anytime you interact on the site, whether its liking and commenting on a post, leaving a tip or using any of the other features.</div>
            </div>
        )
    }
}

TopFansList.propTypes = {
    topFansList: PropTypes.array,
    currentTab: PropTypes.string
};
