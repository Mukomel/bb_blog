import * as constants from './constants';

export const getCurrentRoundRequest = () => ({
    type: constants.GET_CURRENT_ROUND_REQUEST
});

export const getCurrentRoundSuccess = (currentRound) => ({
    type: constants.GET_CURRENT_ROUND_SUCCESS,
    currentRound
});

export const getCurrentRoundFailure = () => ({
    type: constants.GET_CURRENT_ROUND_FAILURE
});

export const getTopFansRequest = () => ({
    type: constants.GET_TOP_FANS_REQUEST
});

export const getTopFansSuccess = (topFansList) => ({
    type: constants.GET_TOP_FANS_SUCCESS,
    topFansList
});

export const getTopFansFailure = () => ({
    type: constants.GET_TOP_FANS_FAILURE
});
