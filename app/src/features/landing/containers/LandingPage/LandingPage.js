import React from 'react';
import {connect} from 'react-redux';
import Header from '../../components/Layout/Header';
import {changeTopFansTab, showModal} from '../../../ui/actions';
import {getCurrentRoundRequest, getTopFansRequest} from '../../actions';
import Footer from '../../../landing/components/Layout/Footer'
import TopFansSection from '../../components/TopFansSection/TopFansSection';
import MonthlySubscription from '../../components/MonthlySubscription/MonthlySubscription';
import {modalTypes} from '../../../ui/constants';
import {MIN_DONATION_AMOUNT_CENTS} from '../../../donation/constants';
import {subscriptionTypes} from '../../../subscribe/constants';
import ErrorBoundary from '../../../../base-components/ErrorBoundary/ErrorBoundary';

import styles from './LandingPage.module.scss';

class LandingPage extends React.Component {
    componentWillMount() {
        const {getTopFans, getCurrentRound} = this.props;
        getCurrentRound();
        getTopFans();
    }

    handleTip = (donationAmount, donationTarget) => {
        const {showModal, isLoggedIn} = this.props;
        if (donationAmount >= MIN_DONATION_AMOUNT_CENTS) {
            if (isLoggedIn) {
                showModal(modalTypes.DONATION_MODAL, {
                    donationAmount,
                    donationTarget,
                    requestRoundAndTopFansAfterDonation: true
                });
            } else {
                showModal(modalTypes.SIGN_UP_MODAL, {
                    nextModal: {
                        modalType: modalTypes.DONATION_MODAL,
                        modalProps: {donationAmount, donationTarget, requestRoundAndTopFansAfterDonation: true}
                    }
                });
            }
        }
    };

    handleTopFansTabChange = tab => this.props.changeTopFansTab(tab);

    handleViewProfileClick = slug => this.props.history.push(`/${slug}`);

    render() {
        const {
            currentRound, topFansTab, topFansList, isRequestingTopFansList, showModal,
            isLoggedIn, user, artistProfile
        } = this.props;

        return (
            <div className={styles.container}>
                <section className={styles.firstSection}>
                    <Header isLoggedIn={isLoggedIn} user={user} isOverlay={true} withBackground={true}/>
                    <div className={styles.contentWrapper}>
                        <div className={styles.welcomeMessage}>
                            <div className={styles.profileLinkBox}>
                                <div className={styles.profileLinkBoxBackground} />
                                <div className={styles.profileLinkBoxBackdrop} />
                                <div className={styles.profileLinkBoxContent}>
                                    <h4 className={styles.profileCaption}>Brandt & Nash</h4>
                                    <div className={styles.subTitle}>View our profile here for exclusive, explicit content</div>
                                    <div>
                                        <button className={styles.profileBtn}
                                                onClick={() => this.handleViewProfileClick('brandtandnash')}>
                                            View Profile
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div className={styles.profileLinkBox}>
                                <div className={styles.profileLinkBoxBackground} />
                                <div className={styles.profileLinkBoxBackdrop} />
                                <div className={styles.profileLinkBoxContent}>
                                    <h4 className={styles.profileCaption}>Drew</h4>
                                    <div className={styles.subTitle}>View my profile here for exclusive, explicit content</div>
                                    <div>
                                        <button className={styles.profileBtn}
                                                onClick={() => this.handleViewProfileClick('drew')}>
                                            View Profile
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div className={styles.contentWrapper}>
                    {/*<div className={styles.payBlocks}>
                        <MonthlySubscription
                            subscriptionPrice={monthlySubscriptionPrice}
                            userSubscriptonsLink='/dashboard/subscriptions'
                            isLoggedIn={isLoggedIn}
                            userHasSubscription={user && user.has_monthly_subscription}
                            onSubscribe={() => showModal(modalTypes.SUBSCRIPTION_MODAL, {subscriptionType: subscriptionTypes.MONTHLY})}
                            onSignUp={() => showModal(modalTypes.SIGN_UP_MODAL, {
                                nextModal: {
                                    modalType: modalTypes.SUBSCRIPTION_MODAL,
                                    modalProps: {subscriptionType: subscriptionTypes.MONTHLY}
                                }
                            })}
                        />

                        <div className={styles.box}>
                        <div className={styles.boxTitle}>Get access to our premium snapchat for lifetime with a 1 time payment</div>
                        <div className={styles.boxInfo}>Lifetime subscription ${lifetimeSubscriptionPrice ? (lifetimeSubscriptionPrice/100).toLocaleString() : ''}</div>
                        <button
                        className={styles.boxButton}
                        onClick={() => showSubscriptionModal(subscriptionTypes.LIFETIME)}
                        >Subscribe</button>
                        </div>
                    </div>*/}
                </div>
                <div className={styles.contentWrapper}>
                    <ErrorBoundary>
                    <TopFansSection
                        currentTab={topFansTab}
                        onTabChange={this.handleTopFansTabChange}
                        topFansList={topFansList}
                        isRequestingTopFansList={isRequestingTopFansList}
                    />
                    </ErrorBoundary>
                </div>
                <Footer />
            </div>
        )
    }
}

export default connect(
    (state) => ({
        currentRound: state.landing.currentRound,
        topFansTab: state.ui.topFansTab,
        topFansList: state.landing.topFansList,
        user: state.auth.user,
        isRequestingTopFansList: state.landing.isRequestingTopFansList,
        isLoggedIn: state.auth.isLoggedIn,
        artistProfile: state.artist.artistProfile
    }),
    (dispatch) => ({
        getCurrentRound: () => dispatch(getCurrentRoundRequest()),
        changeTopFansTab: (tab) => dispatch(changeTopFansTab(tab)),
        getTopFans: () => dispatch(getTopFansRequest()),
        showModal: (modalType, modalProps) => dispatch(showModal(modalType, modalProps)),
    })
)(LandingPage)
