import { call, put, take, fork } from 'redux-saga/effects'
import * as constants from './constants'
import {getCurrentRoundSuccess, getTopFansSuccess, getTopFansFailure} from './actions';
import request from '../../helpers/request';

export function* getCurrentRound() {
    while (true) {
        yield take(constants.GET_CURRENT_ROUND_REQUEST);

        const response = yield call(request, '/rounds/current');

        const body = yield response.json();
        if (response.ok) {
            yield put(getCurrentRoundSuccess(body))
        }
    }
}

export function* getTopFans() {
    while(true) {
       yield take(constants.GET_TOP_FANS_REQUEST);
        const response = yield call(request, '/earnings/top_fans/by_periods');

        const body = yield response.json();
        if (response.ok) {
            yield put(getTopFansSuccess(body))
        } else {
            yield put(getTopFansFailure())
        }
    }
}

function startSagas(...sagas) {
    return function* rootSaga() {
        yield sagas.map(saga => fork(saga))
    }
}

export default startSagas(getCurrentRound, getTopFans)
