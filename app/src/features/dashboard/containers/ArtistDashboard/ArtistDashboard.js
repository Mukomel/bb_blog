import React from 'react';
import {connect} from 'react-redux';
import Sidebar from '../../../../base-components/Sidebar/Sidebar';
import SubscribersPage from '../../../subscribe/containers/SubscribersPage/SubscribersPage';
import AddPostsPage from '../../../posts/containers/AddPostsPage/AddPostsPage';
import ArtistEditProfilePage from '../../../artist/containers/EditArtistProfilePage/EditArtistProfilePage';
import AuthorProfilePage from '../../../user/containers/AuthorProfilePage/AuthorProfilePage';
import ArtistSchedulePage from '../../../artist/containers/ArtistSchedulePage/ArtistSchedulePage';
import ArtistNotificationsPage from '../../../artist/containers/ArtistNotificationsPage/ArtistNotificationsPage';
import ArtistGroupTipJar from '../../../artist/containers/ArtistGroupTipJarPage/ArtistGroupTipJarPage';
import ArtistEarningsPage from '../../../artist/containers/ArtistEarningsPage/ArtistEarningsPage';
import EditArtistWishlistPage from '../../../artist/containers/EditArtistWishlistPage/EditArtistWishlistPage';
import PromoCodesPage from '../../../subscribe/containers/PromoCodesPage/PromoCodesPage';
import ArtistStatsTransactionsPage from '../../../artist/containers/ArtistStatsTransactionsPage/ArtistStatsTransactionsPage'
import ArtistStatsMembersPage from '../../../artist/containers/ArtistStatsMembersPage/ArtistStatsMembersPage'
import AuthenticatedAuthorRoute from '../../../../base-components/AuthenticatedAuthorRoute/AuthenticatedAuthorRoute';
import AuthenticatedAdminRoute from '../../../../base-components/AuthenticatedAdminRoute/AuthenticatedAdminRoute';
import Header from '../../../landing/components/Layout/Header';
import MenuIcon from '@material-ui/icons/Menu';
import IconButton from '@material-ui/core/IconButton';
import {userRoles} from "../../../auth/constants";

import styles from './ArtistDashboard.module.scss'

const authorSidebarItems = [
    {label: 'Posts', value: 'posts'},
    {label: 'Subscribers', value: 'subscribers'},
    {label: 'My Profile', value: 'artist-profile'},
    {label: 'Schedule', value: 'artist-schedule'},
    {label: 'My Account', value: 'author-profile'},
    {label: 'Earnings', value: 'earnings'},
    {label: 'Group Wishlist', value: 'artist-wishlist'},
    {label: 'Promo Codes', value: 'promocodes'},
    {label: 'Tip Goal', value: 'artist-group-tip-jar'},
    {
        label: 'Statistics',
        value: 'statistics',
        children: [
            {label: 'Transaction Summary', value: 'transactions'},
            {label: 'Fans', value: 'fans'}
        ]
    },
];

const adminSidebarItems = [
    ...authorSidebarItems,
    {label: 'Email Management', value: 'email-management'},
];

class ArtistDashboard extends React.Component {
    constructor() {
        super();

        this.state = {
            isSidebarOpen: false
        }
    }

    handleSidebarClose = () => {
        this.setState({isSidebarOpen: false});
    };

    handleSidebarOpen = () => {
        this.setState({isSidebarOpen: true});
    };

    handleSidebarItemClick = (value) => {
        this.props.history.push('/dashboard/' + value);
    };

    render() {
        const {childProps, user, isLoggedIn} = this.props;

        let sidebarItems = [];
        if (user && (user.role === userRoles.AUTHOR)) {
            sidebarItems = authorSidebarItems;
        } else if (user && (user.role === userRoles.ADMIN)) {
            sidebarItems = adminSidebarItems;
        }

        return (
            <div className={styles.container}>
                <Header
                    isLoggedIn={isLoggedIn}
                    user={user}
                    className={styles.header}
                    showLinks={false}
                    additionalNode={
                        <IconButton
                            onClick={this.handleSidebarOpen}
                        >
                            <MenuIcon/>
                        </IconButton>
                    }
                />
                <Sidebar
                    isOpen={this.state.isSidebarOpen}
                    items={sidebarItems}
                    onClose={this.handleSidebarClose}
                    onItemClick={this.handleSidebarItemClick}/>
                <div className={styles.content}>
                    <AuthenticatedAuthorRoute exact path='/dashboard/subscribers' component={SubscribersPage}
                                              componentProps={childProps}/>
                    <AuthenticatedAuthorRoute exact path='/dashboard/posts' component={AddPostsPage}
                                              componentProps={childProps}/>
                    <AuthenticatedAuthorRoute exact path='/dashboard/artist-profile' component={ArtistEditProfilePage}
                                              componentProps={childProps}/>
                    <AuthenticatedAuthorRoute exact path='/dashboard/artist-schedule' component={ArtistSchedulePage}
                                              componentProps={childProps}/>
                    <AuthenticatedAuthorRoute exact path='/dashboard/author-profile' component={AuthorProfilePage}
                                              componentProps={childProps}/>
                    <AuthenticatedAuthorRoute exact path='/dashboard/earnings' component={ArtistEarningsPage}
                                              componentProps={childProps}/>
                    <AuthenticatedAuthorRoute exact path='/dashboard/artist-wishlist' component={EditArtistWishlistPage}
                                              componentProps={childProps}/>
                    <AuthenticatedAuthorRoute exact path='/dashboard/promocodes' component={PromoCodesPage}
                                              componentProps={childProps}/>

                    <AuthenticatedAuthorRoute exact path='/dashboard/artist-group-tip-jar' component={ArtistGroupTipJar}
                                             componentProps={childProps}/>
                    <AuthenticatedAdminRoute exact path='/dashboard/email-management'
                                             component={ArtistNotificationsPage} componentProps={childProps}/>
                    <AuthenticatedAuthorRoute
                        exact
                        path='/dashboard/transactions'
                        component={ArtistStatsTransactionsPage}
                        componentProps={childProps}
                    />
                    <AuthenticatedAuthorRoute
                        exact
                        path='/dashboard/fans'
                        component={ArtistStatsMembersPage}
                        componentProps={childProps}
                    />
                </div>
            </div>
        )
    }
}

export default connect(
    state => ({
        user: state.auth.user,
        isLoggedIn: state.auth.isLoggedIn
    })
)(ArtistDashboard)
