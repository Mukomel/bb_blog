import React from 'react';
import Header from '../../../landing/components/Layout/Header';
import { connect } from 'react-redux';
import Footer from '../../../landing/components/Layout/Footer';

import styles from './SupportPage.module.scss';

const SupportPage = ({isLoggedIn, user}) => (
    <div className={styles.mainContent}>
        <Header
            user={user}
            isLoggedIn={isLoggedIn}/>
        <div className={styles.container}>
            Support
        </div>
        <Footer/>
    </div>
);

export default connect(
    (state) => ({
        user: state.auth.user,
        isLoggedIn: state.auth.isLoggedIn
    })
)(SupportPage)