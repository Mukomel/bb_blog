import React from 'react';
import Header from '../../../landing/components/Layout/Header';
import { connect } from 'react-redux';
import Footer from '../../../landing/components/Layout/Footer';

import styles from './CopyrightPolicyPage.module.scss';

const CopyrightPolicyPage = ({ isLoggedIn, user }) => (
    <div className={styles.mainContent}>
        <Header
            user={user}
            isLoggedIn={isLoggedIn}/>
        <div className={styles.container}>
            <div className={styles.title}>
                <div>BrandtsBoys.com</div>
             <div>   Copyright/DMCA Policy</div>
        <div>Last Updated: April 12, 2018</div>
            </div>
            <div className={styles.subtitle}> Reporting Claims of Copyright Infringement</div>

            We take claims of copyright infringement seriously. We will respond to notices of alleged copyright infringement that comply with applicable law. If you believe any materials accessible on or from www.brandtsboys.com (the “Website”) infringe your copyright, you may request removal of those materials (or access to them) from the Website by submitting written notification to our Copyright Agent (designated below). In accordance with the Online Copyright Infringement Liability Limitation Act of the Digital Millennium Copyright Act (<a href="https://www.copyright.gov/title17/92chap5.html">17 U.S.C. § 512</a>) (“DMCA”), the written notice (the “DMCA Notice”) must include substantially the following:
            <ul>
                <li>Your physical or electronic signature.</li>
                <li>Identification of the copyright work you believe to have been infringed or, if the claim involves multiple works on the Website, a representative list of the works.</li>
                <li>Identification of the material you believe to be infringing in a sufficiently precise manner to allow us to locate that material.</li>
                <li>Adequate /information by which we can contact you (including your name, postal address, telephone number, and, if available, email address).</li>
                <li>A statement that you have a good faith belief that use of the copyrighted material is not authorized by the copyright owner, its agent, or the law.</li>
                <li>A statement that the information in the written notice is accurate.</li>
                <li> A statement, under penalty of perjury, that you are authorized to act on behalf of the copyright owner.</li>
            </ul>
            <div>Our designated Copyright Agent to receive DMCA notices is:</div>
            <div>Andrew Henderson</div>
            <div>242 main st #131</div>
            <div>Beacon, NY 12508</div>
            <div>8452503977</div>
            <div>Dna Enterprises LLC</div>
            <div><a href="mailto:dmca@brandtsboys.com">dmca@brandtsboys.com</a></div>
            <div>If you fail to comply with all the requirements of section 512(c)(3) of the DMCA, your DMCA Notice may not be effective.</div>
            <br />
            <div>Please be aware that if you knowingly materially misrepresent that material or activity on the Website is infringing your copyright, you may be held liable for damages (including costs and attorneys’ fees) under section 512(f) of the DMCA.
            Counter-Notification Procedures
                If you believe that material you posted on the Website was removed or access to it was disabled by mistake or misidentification, you may file a counter-notification with us (a “Counter-Notice”) by submitting written notification to our Copyright Agent (identified below). In accordance with the DMCA, the Counter-Notice must include substantially the following:</div>

            <ul>
                <li>    Your physical or electronic signature.</li>
                <li>An identification of the material that has been removed or to which access has been disabled and the location at which the material appeared before it was removed or access disabled.</li>
                <li>Adequate information by which we can contact you (including your name, postal address, telephone number and, if available, email address).</li>
                <li>A statement under penalty of perjury by you that you have a good faith belief that the material identified above was removed or disabled as a result of a mistake or misidentification of the material to be removed or disabled.</li>
                <li>A statement that you will consent to the jurisdiction of the Federal District Court for the judicial district in which your address is located (or if you reside outside the United States, the United States District Court for the Northern District of Iowa) and that you will accept service from the person (or an agent of that person) who provided the Website with the complaint at issue.</li>
            </ul>
            <br />
            Completed Counter-Notices should be sent to:
            <br />
            <div>Andrew Henderson</div>
            <div>242 main st #131</div>
            <div>Beacon, NY 12508</div>
            <div>8452503977</div>
            <div>Dna Enterprises LLC</div>
            <div><a href="mailto:dmca@brandtsboys.com">dmca@brandtsboys.com</a></div>
            <br />
            <div>The DMCA allows us to restore the removed content if the party filing the original DMCA Notice does not file a court action against you within ten business days of receiving the copy of your Counter-Notice.</div>
            <div>Please be aware that if you knowingly materially misrepresent that material or activity on the Website was removed or disabled by mistake or misidentification, you may be held liable for damages (including costs and attorneys’ fees) under section 512(f) of the DMCA.</div>

            <div className={styles.subtitle}> Repeat Infringers</div>
            It is our policy in appropriate circumstances to disable or terminate the accounts of users who are repeat infringers in accordance with our <a href="https://paymentsbb.com/repeat-infringer-policy.html">repeat infringer policy</a>.
        </div>
        <Footer/>
    </div>
);

export default connect(
    (state) => ({
        user: state.auth.user,
        isLoggedIn: state.auth.isLoggedIn
    })
)(CopyrightPolicyPage)