import React from 'react';
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import moment from 'moment';
import * as constants from '../../constants';
import {roundToExactly} from '../../../../helpers/roundTo';

import * as styles from './UserSubscriptionsTable.module.scss';

const cellPadding = 'dense';

export default class UserSubscriptionsTable extends React.Component {
    renderHead = () => {
        const cells = [
            <TableCell padding={cellPadding} key='join_date'>Join Date</TableCell>,
            <TableCell padding={cellPadding} key='next_charge_date'>Next Charge Date</TableCell>,
            <TableCell padding={cellPadding} key='price'>Price $</TableCell>,
            <TableCell padding={cellPadding} key='action'>Action</TableCell>
        ];

        return (
            <TableHead>
                <TableRow>
                    {cells}
                </TableRow>
            </TableHead>
        )
    };

    renderBody = () => {
        const {subscriptionsList, cancelSubscription} = this.props;

        return (
            <TableBody>
                {subscriptionsList.map(subscription => (
                    <TableRow key={subscription._id}>
                        <TableCell padding={cellPadding}>{moment(Number(subscription.stripe_creation_date + '000')).format("DD MMM YYYY")}</TableCell>
                        <TableCell padding={cellPadding}>
                            {
                                subscription.is_canceled
                                    ? 'Inactive subscription'
                                    : (subscription.stripe_discount ? subscription.stripe_discount : moment(Number(subscription.stripe_current_period_end + '000')).format("DD MMM YYYY"))
                            }
                        </TableCell>
                        <TableCell padding={cellPadding}>{subscription.stripe_discount ? subscription.stripe_discount : roundToExactly(subscription.subscription_price / 100, 2)}</TableCell>
                        <TableCell padding={cellPadding}>
                            {
                                subscription.is_canceled
                                    ? null
                                    : <Button
                                        color="primary"
                                        onClick={() => cancelSubscription(subscription.subscription_id)}
                                    >
                                        Cancel subscription
                                    </Button>
                            }
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>
        )
    };

    render() {
        return (
            <div className={styles.container}>
                <Table>
                    {this.renderHead()}
                    {this.renderBody()}
                </Table>
            </div>
        )
    }
}

UserSubscriptionsTable.propTypes = {
    subscriptionsList: PropTypes.array.isRequired,
    markUserAdded: PropTypes.func.isRequired,
    cancelSubscription: PropTypes.func.isRequired
};
