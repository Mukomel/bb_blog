import React from 'react';
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import Checkbox from '../../../../base-components/Checkox/Checkbox'
import moment from 'moment';
import { withStyles } from '@material-ui/core/styles';

import * as styles from './UserSubscriptionsTable.module.scss';

const style = {
    tableHead: {
        color: '#475F66',
        fontSize: 12,
        padding: 0,
        fontWeight: 600
    },
    rowHead: {
        height: 'auto',
        display: 'flex',
        justifyContent: 'space-between',
        borderBottom: '1px solid #475F66',
        marginBottom: 30
    },
    cell: {
        color: '#8A9699',
        fontSize: 10,
        padding: 0,
        fontWeight: 600
    },
    row: {
        height: 'auto',
        display: 'flex',
        justifyContent: 'space-between',
        borderBottom: '1px solid #8A9699',
        marginBottom: 25
    }
};

class SegpayUserSubscriptionsTable extends React.Component {
    renderHead = () => {
        const {classes} = this.props;
        const cells = [
            <TableCell classes={{
                root: classes.tableHead}} key='join_date'>Join Date</TableCell>,
            <TableCell classes={{
                root: classes.tableHead}} key='descr'>Description</TableCell>,
            <TableCell classes={{
                root: classes.tableHead}} key='price'>Price $</TableCell>,
            <TableCell classes={{
                root: classes.tableHead}} key='action'>Action</TableCell>
        ];

        return (
            <TableHead>
                <TableRow classes={{ root: classes.rowHead }}>
                    {cells}
                </TableRow>
            </TableHead>
        )
    };

    renderCancelSubscription = (subscription) => {
        const {onCancelSubscription} = this.props;

        if (subscription.is_old_stripe_subscription) {
            return null;
        }

        if (!subscription.is_canceled && !subscription.is_pending_cancellation) {
            return <Checkbox onCheck={checked => onCancelSubscription(checked, subscription._id)} name="is_subscribe"/>
        }

        if (subscription.is_pending_cancellation) {
            return 'Pending cancellation';
        }

        if (subscription.is_canceled) {
            return 'Canceled';
        }
    };

    renderBody = () => {
        const {subscriptionsList, classes} = this.props;

        return (
            <TableBody>
                {subscriptionsList.map(subscription => (
                    <TableRow key={subscription._id} classes={{ root: classes.row }} >
                        <TableCell classes={{ root: classes.cell }}>{moment(subscription.created_at).format("DD MMM YYYY")}</TableCell>
                        <TableCell classes={{ root: classes.cell }}>{subscription.description}</TableCell>
                        <TableCell classes={{ root: classes.cell }}>
                            {subscription.with_stripe_coupon ? '4 months free' : subscription.price}
                        </TableCell>
                        <TableCell classes={{ root: classes.cell }}>
                            { this.renderCancelSubscription(subscription) }
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>
        )
    };

    render() {
        return (
            <div className={styles.container}>
                <Table>
                    {this.renderHead()}
                    {this.renderBody()}
                </Table>
            </div>
        )
    }
}

SegpayUserSubscriptionsTable.propTypes = {
    subscriptionsList: PropTypes.array.isRequired,
    cancelSubscription: PropTypes.func.isRequired,

};

export default withStyles(style)(SegpayUserSubscriptionsTable)
