import React from 'react';
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import moment from 'moment';

import * as styles from './BannedUsersTable.module.scss';

const cellPadding = 'dense';

export default class BannedUsersTable extends React.Component {
    renderHead = () => {
        const cells = [
            <TableCell padding={cellPadding} key='username'>Snapchat username</TableCell>,
            <TableCell padding={cellPadding} key='email'>Email</TableCell>
        ];

        return (
            <TableHead>
                <TableRow>
                    {cells}
                </TableRow>
            </TableHead>
        )
    };

    renderBody = () => {
        const {usersList} = this.props;

        return (
            <TableBody>
                {usersList.map(user => (
                    <TableRow key={user._id}>
                        <TableCell padding={cellPadding}>{user.snapchat_username}</TableCell>
                        <TableCell padding={cellPadding}>{user.email}</TableCell>
                    </TableRow>
                ))}
            </TableBody>
        )
    };

    render() {
        return (
            <div className={styles.container}>
                <Table>
                    {this.renderHead()}
                    {this.renderBody()}
                </Table>
            </div>
        )
    }
}

BannedUsersTable.propTypes = {
    usersList: PropTypes.array.isRequired
};
