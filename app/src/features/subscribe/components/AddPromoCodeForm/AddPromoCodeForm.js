import React from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import {TextField} from 'redux-form-material-ui';
import FormTextField from '../../../../base-components/FormTextField/FormTextField'
import MoneyInputReduxForm from '../../../../base-components/MoneyInputReduxForm/MoneyInputReduxForm'
import Button from '../../../../base-components/Button/Button'

import styles from './AddPromoCodeForm.module.scss';

class AddPromoCodeForm extends React.Component {

    handleFormSubmit = (values) => {
        const { onSubmit, reset } = this.props;
        return onSubmit(values)
            .then(() => reset())
    };

    render() {
        const { handleSubmit, className } = this.props;
        return (
            <form onSubmit={handleSubmit(this.handleFormSubmit)} className={cn(className, styles.form)}>
                <FormTextField
                    name='code'
                    label='Code'
                    component={TextField}
                    fullWidth
                    margin='dense'
                />
                <FormTextField
                    name='segpay_pricepoint_id'
                    label='SegPay Price Point ID'
                    component={TextField}
                    fullWidth
                    margin='dense'
                />
                <FormTextField
                    name='subscription_price_cents_first_30_days'
                    label='Price for first 30 days'
                    component={MoneyInputReduxForm}
                    fullWidth
                    margin='dense'
                />
                <FormTextField
                    name='subscription_price_cents_recurring_30_days'
                    label='Recurring price (every next 30 days)'
                    component={MoneyInputReduxForm}
                    fullWidth
                    margin='dense'
                />
                <Button type='submit' className={styles.submitButton}>
                    Add promo code
                </Button>
            </form>
        )
    }
}

AddPromoCodeForm.propTypes = {
    className: PropTypes.string,
    onSubmit: PropTypes.func.isRequired,
};

export default reduxForm({
    form: 'addPromoCodeForm',
    validate: (values, props) => {
        const errors = {};

        if(!values.code) {
            errors.code = 'Required'
        }

        if (!values.segpay_pricepoint_id) {
            errors.segpay_pricepoint_id = 'Required'
        }

        return errors;
    }
})(AddPromoCodeForm)
