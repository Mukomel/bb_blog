import React from 'react';
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import moment from 'moment';
import * as constants from '../../constants';
import {userRoles} from "../../../auth/constants";

import * as styles from './SegpaySubscribersTable.module.scss';

const cellPadding = 'dense';

export default class SegpaySubscribersTable extends React.Component {
    renderHead = () => {
        const defaultCells = [
            <TableCell padding={cellPadding} key='snapchat_username'>Snapchat Username</TableCell>,
            <TableCell padding={cellPadding} key='username'>Username</TableCell>,
            <TableCell padding={cellPadding} key='join_date'>Join Date</TableCell>,
            <TableCell padding={cellPadding} key='email'>Email</TableCell>,
            <TableCell padding={cellPadding} key='promo_code'>Promo Code</TableCell>,
            <TableCell padding={cellPadding} key='action'>Action</TableCell>
        ];

        return (
            <TableHead>
                <TableRow>
                    {defaultCells}
                </TableRow>
            </TableHead>
        )
    };

    renderRow = (item) => {
        const {copyResetLink, banUser, currentUser} = this.props;

        const isAdmin = currentUser && (currentUser.role === userRoles.ADMIN);
        const artistSubscription = item.subscriptions.find(subscription => subscription.artist._id === currentUser.artist._id);

        return (
            <TableRow key={item.user._id}>
                <TableCell
                    padding={cellPadding}>{item.user && item.user.snapchat_username}</TableCell>
                <TableCell
                    padding={cellPadding}>{item.user && item.user.username}</TableCell>
                <TableCell padding={cellPadding}>
                    {isAdmin
                        ? item.subscriptions.map(subscription => (
                            <div key={subscription._id}>
                                {subscription.artist.name} : {moment(subscription.created_at).format("DD MMM YYYY")}
                            </div>
                        ))
                        : moment(artistSubscription.created_at).format("DD MMM YYYY")
                    }
                </TableCell>
                <TableCell padding={cellPadding}>{item.user && item.user.email}</TableCell>
                <TableCell padding={cellPadding}>
                    {
                        isAdmin
                            ? item.subscriptions.map(subscription => (
                                <div key={subscription._id}>
                                    {subscription.artist.name} : {subscription.with_stripe_coupon ? 'Yes' : 'No'}
                                </div>
                            ))
                            : (artistSubscription.with_stripe_coupon ? 'Yes' : 'No')
                    }
                </TableCell>
                <TableCell padding={cellPadding}>
                    {item.user.password_reset_token ? <Button
                        color="primary"
                        onClick={() => copyResetLink(item.user.password_reset_token)}
                    >
                        Copy Reset Link
                    </Button> : null}
                    {
                        (currentUser && (currentUser.role === userRoles.ADMIN))
                        && <Button
                            color="primary"
                            onClick={() => banUser(item.user)}
                        >
                            Ban user
                        </Button>
                    }
                </TableCell>
            </TableRow>
        )
    };

    renderBody = () => {
        const {subscribersList} = this.props;

        return (
            <TableBody>
                {subscribersList.map(this.renderRow)}
            </TableBody>
        )
    };

    render() {
        return (
            <div className={styles.container}>
                <Table>
                    {this.renderHead()}
                    {this.renderBody()}
                </Table>
            </div>
        )
    }
}

SegpaySubscribersTable.propTypes = {
    currentUser: PropTypes.object,
    activeTab: PropTypes.oneOf([constants.tabs.UPDATES, constants.tabs.ACTIVE, constants.tabs.EXPIRED]),
    subscribersList: PropTypes.array.isRequired,
    markUserAdded: PropTypes.func.isRequired,
    markUserArchived: PropTypes.func.isRequired,
    banUser: PropTypes.func.isRequired
};
