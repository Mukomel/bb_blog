import React from 'react';
import PropTypes from 'prop-types';
import {injectStripe} from 'react-stripe-elements';
import {CardElement, PaymentRequestButtonElement} from 'react-stripe-elements';
import config from '../../../../config';

import styles from './PaymentDetailsForm.module.scss';

const cardElementStyles = {
    base: {
        color: '#32325d',
        lineHeight: '18px',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
            color: '#aab7c4'
        }
    },
    invalid: {
        color: '#f44336',
        iconColor: '#f44336'
    }
};


class PaymentDetailsForm extends React.Component {
    constructor(props) {
        super();

        this.paymentRequest = props.stripe.paymentRequest({
            country: config[process.env.NODE_ENV].STRIPE_ACCOUNT_COUNTRY_CODE,
            currency: 'usd',
            total: {
                label: props.paymentRequestLabel,
                amount: props.subscriptionPrice,
            },
        });

        this.paymentRequest.on('token', ({complete, token, ...data}) => {
            props.onSubmit(token.id);
            complete('success');
        });

        this.paymentRequest.canMakePayment().then(result => {
            this.setState({canMakePayment: Boolean(result)});
        });

        this.state = {
            canMakePayment: false,
            isPaymentRequestButtonReady: false,
            isCardElementReady: false
        };

    }

    handleSubmit = (ev) => {
        this.props.onCreatingStripeTokenChange(true);
        if (ev) ev.preventDefault();

        this.props.stripe.createToken().then(({token}) => {
            this.props.onSubmit(token.id);
            this.props.onCreatingStripeTokenChange(false);
        });
    };

    handleChange = (form) => {
        const {isFormValid, isFormEmpty, onFormValidChange, onFormEmptyChange} = this.props;
        if (form.complete !== isFormValid) {
            onFormValidChange(form.complete);
        }

        if (form.empty !== isFormEmpty) {
            onFormEmptyChange(form.empty);
        }
    };

    submit = () => {
        this.handleSubmit();
    };

    handlePaymentRequestButtonReady = () => {
        this.setState({isPaymentRequestButtonReady: true});
        if (this.state.isCardElementReady) {
            this.props.onFormReady();
        }
    };

    handleCardElementReady = () => {
        this.setState({isCardElementReady: true});
        if (!this.state.canMakePayment || this.state.isPaymentRequestButtonReady) {
            this.props.onFormReady();
        }
    };

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                {
                    this.state.canMakePayment
                        && <div className={styles.requestPaymentContainer}>
                            <PaymentRequestButtonElement
                                paymentRequest={this.paymentRequest}
                                className={styles.requestPaymentButton}
                                style={{
                                    paymentRequestButton: {
                                        theme: 'dark'
                                    },
                                }}
                                onReady={this.handlePaymentRequestButtonReady}
                            />
                            {/*<div className={styles.choosePaymentVariantLabel}>or enter card details</div>*/}
                    </div>
                }
                <CardElement
                    hidePostalCode={true}
                    onChange={this.handleChange}
                    style={cardElementStyles}
                    onReady={this.handleCardElementReady}
                />
            </form>
        );
    }
}

PaymentDetailsForm.propTypes = {
    isFormValid: PropTypes.bool.isRequired,
    isFormEmpty: PropTypes.bool,
    onFormValidChange: PropTypes.func.isRequired,
    onFormEmptyChange: PropTypes.func,
    subscriptionPrice: PropTypes.number.isRequired,
    paymentRequestLabel: PropTypes.string.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onFormReady: PropTypes.func.isRequired,
    onCreatingStripeTokenChange: PropTypes.func.isRequired,
};

export default injectStripe(PaymentDetailsForm, {withRef: true});