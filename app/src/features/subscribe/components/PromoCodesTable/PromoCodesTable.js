import React from 'react';
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Checkbox from '@material-ui/core/Checkbox';
import {roundToExactly} from '../../../../helpers/roundTo';

import * as styles from './PromoCodesTable.module.scss';

const cellPadding = 'dense';

export default class PromoCodesTable extends React.Component {
    renderHead = () => {
        const cells = [
            <TableCell padding={cellPadding} key='code'>Code</TableCell>,
            <TableCell padding={cellPadding} key='segpay_pricepoint_id'>SegPay Price Point ID</TableCell>,
            <TableCell padding={cellPadding} key='subscription_price_cents_first_30_days'>First 30 days price, $</TableCell>,
            <TableCell padding={cellPadding} key='subscription_price_cents_recurring_30_days'>Recurring price, $ (next 30 days)</TableCell>,
            <TableCell padding={cellPadding} key='active'>Active</TableCell>
        ];

        return (
            <TableHead>
                <TableRow>
                    {cells}
                </TableRow>
            </TableHead>
        )
    };

    renderBody = () => {
        const {promoCodesList, onChangeActivity} = this.props;

        return (
            <TableBody>
                {promoCodesList.map(promoCode => (
                    <TableRow key={promoCode._id}>
                        <TableCell padding={cellPadding}>{promoCode.code}</TableCell>
                        <TableCell padding={cellPadding}>{promoCode.segpay_pricepoint_id}</TableCell>
                        <TableCell padding={cellPadding}>{roundToExactly(promoCode.subscription_price_cents_first_30_days / 100, 2)}</TableCell>
                        <TableCell padding={cellPadding}>{roundToExactly(promoCode.subscription_price_cents_recurring_30_days / 100, 2)}</TableCell>
                        <TableCell padding={cellPadding}>
                            <Checkbox
                                checked={promoCode.is_active}
                                onChange={() => onChangeActivity(promoCode._id, !promoCode.is_active)}
                                color="primary"
                            />
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>
        )
    };

    render() {
        return (
            <div className={styles.container}>
                <Table>
                    {this.renderHead()}
                    {this.renderBody()}
                </Table>
            </div>
        )
    }
}

PromoCodesTable.propTypes = {
    promoCodesList: PropTypes.array.isRequired,
    onChangeActivity: PropTypes.func.isRequired
};
