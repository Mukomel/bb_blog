import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import Checkbox from '@material-ui/core/Checkbox';
import {Link} from 'react-router-dom';
import TextField from '@material-ui/core/TextField';

import styles from './SubscriptionInfo.module.scss'
import {roundToExactly} from "../../../../helpers/roundTo";

export default class SubscriptionInfo extends React.Component {
    handlePromoCodeInputChange = event => {
        this.props.onPromoCodeInputChange(event.target.value);
    };

    render() {
        const {subscriptionPriceFirst30Days, subscriptionPriceRecurring30Days, isTermsAgreed, onTermsCheckboxChange,
            promoCodeInputValue, showPromoCodeValidation, isPromoCodeValid, checkedPromoCode} = this.props;

        return (
            <div className={styles.container}>
                <div className={styles.description}>Your subscription: ${roundToExactly((checkedPromoCode ? checkedPromoCode.subscription_price_cents_first_30_days : subscriptionPriceFirst30Days) / 100, 2)} for first 30 days, and then ${roundToExactly((checkedPromoCode ? checkedPromoCode.subscription_price_cents_recurring_30_days : subscriptionPriceRecurring30Days) / 100, 2)} for every 30 days after that</div>
                <div className={styles.description}>
                    Monthly subscription that grants you access to content of <Link to={'/'}>Original Brandt`s Boys</Link>
                </div>
                <div className={styles.checkboxContainer}>
                    <Checkbox
                        color="primary"
                        className={styles.checkbox}
                        checked={isTermsAgreed}
                        onChange={onTermsCheckboxChange}
                    />
                    <div className={styles.checkboxText} onClick={() => onTermsCheckboxChange()}>
                        By checking this box, you certify you agreed to the <Link to="/terms-of-service" target="_blank">Terms of Service Agreement</Link> and <Link to="/privacy-policy" target="_blank">Privacy Policy</Link> and that you are over <span className={styles.ageRestriction}>18</span>. You may cancel your subscription at any time. For that, go to My Account and click Cancel Subscription.
                    </div>
                </div>
                <div className={styles.importantNote}>
                    Brandtsboys.com will automatically continue your membership at the end of your current billing period and charge the subscription fee (${roundToExactly((checkedPromoCode ? checkedPromoCode.subscription_price_cents_recurring_30_days : subscriptionPriceRecurring30Days) / 100, 2)}) to your payment method on a monthly basis until you cancel. There are no refunds for partial months.
                </div>
                <TextField
                    error={showPromoCodeValidation}
                    className={cn(styles.promoCodeInput, {[styles.valid]: isPromoCodeValid})}
                    label="Have a promo code?"
                    placeholder="Promo code"
                    margin="normal"
                    value={promoCodeInputValue}
                    onChange={this.handlePromoCodeInputChange}
                />
            </div>
        )
    }
}

SubscriptionInfo.propTypes = {
    promoCodeInputValue: PropTypes.string.isRequired,
    artistName: PropTypes.string,
    subscriptionPrice: PropTypes.number,
    isTermsAgreed: PropTypes.bool.isRequired,
    showPromoCodeValidation: PropTypes.string,
    checkedPromoCode: PropTypes.object,
    isPromoCodeValid: PropTypes.bool.isRequired,
    onPromoCodeInputChange: PropTypes.func.isRequired,
    onTermsCheckboxChange: PropTypes.func.isRequired
};
