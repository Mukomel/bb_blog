import React from 'react';
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import moment from 'moment';
import * as constants from '../../constants';

import * as styles from './SubscribersTable.module.scss';

const cellPadding = 'dense';

export default class SubscribersTable extends React.Component {
    renderHead = () => {
        const {activeTab} = this.props;

        const defaultCells = [
            <TableCell padding={cellPadding} key='username'>Snapchat Username</TableCell>,
            <TableCell padding={cellPadding} key='join_date'>Join Date</TableCell>,
            <TableCell padding={cellPadding} key='email'>Email</TableCell>
        ];

        if (activeTab === constants.tabs.UPDATES) {
            defaultCells.push(
                <TableCell padding={cellPadding} key='promo_code'>Promo Code</TableCell>
            );
            defaultCells.push(
                <TableCell padding={cellPadding} key='action'>Action</TableCell>
            );
        }

        return (
            <TableHead>
                <TableRow>
                    {defaultCells}
                </TableRow>
            </TableHead>
        )
    };

    renderBody = () => {
        const {subscribersList, activeTab, copyResetLink, banUser} = this.props;

        return (
            <TableBody>
                {subscribersList.map(subscriber => (
                    <TableRow key={subscriber._id}>
                        <TableCell
                            padding={cellPadding}>{subscriber.user && subscriber.user.snapchat_username}</TableCell>
                        <TableCell
                            padding={cellPadding}>{moment(Number(subscriber.stripe_creation_date + '000')).format("DD MMM YYYY")}</TableCell>
                        <TableCell padding={cellPadding}>{subscriber.user && subscriber.user.email}</TableCell>
                        {
                            (activeTab === constants.tabs.UPDATES)
                            && <TableCell padding={cellPadding}>
                                {subscriber.stripe_discount ? 'Yes' : 'No'}
                            </TableCell>
                        }
                        {
                            (activeTab === constants.tabs.UPDATES)
                            && <TableCell padding={cellPadding}>
                                {subscriber.user.password_reset_token ? <Button
                                    color="primary"
                                    onClick={() => copyResetLink(subscriber.user.password_reset_token)}
                                >
                                    Copy Reset Link
                                </Button> : null}
                                <Button
                                    color="primary"
                                    onClick={() => banUser(subscriber.user)}
                                >
                                    Ban user
                                </Button>
                            </TableCell>
                        }
                    </TableRow>
                ))}
            </TableBody>
        )
    };

    render() {
        return (
            <div className={styles.container}>
                <Table>
                    {this.renderHead()}
                    {this.renderBody()}
                </Table>
            </div>
        )
    }
}

SubscribersTable.propTypes = {
    activeTab: PropTypes.oneOf([constants.tabs.UPDATES, constants.tabs.ACTIVE, constants.tabs.EXPIRED]),
    subscribersList: PropTypes.array.isRequired,
    markUserAdded: PropTypes.func.isRequired,
    markUserArchived: PropTypes.func.isRequired,
    banUser: PropTypes.func.isRequired
};
