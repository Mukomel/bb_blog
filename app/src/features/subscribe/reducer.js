import * as constants from './constants';
import * as uiConstants from '../ui/constants';

const initialState = {
    subscribersList: [],
    segpayUserSubscriptionsList: [],
    bannedUsersList: [],
    subscriptionError: null,
    promoCodesList: [],
    promoCodeInputValue: '',
    isCheckingPromoCode: false,
    checkedPromoCodeValue: '',
    checkedPromoCode: null,
    isFetchingSubscribers: false,
    isFetchingBannedUsers: false,
    isRequestingSubscribe: false,
    isRequestingMarkUser: false,
    isFetchingUserSubscriptions: false,
    isRequestingCancelSubscription: false,
    isRequestingBanUser: false
};

export default function(state = initialState, action) {
    switch(action.type) {

        case (constants.GET_SUBSCRIBERS_REQUEST):
            return Object.assign({}, state, {
                isFetchingSubscribers: true
            });

        case (constants.GET_SUBSCRIBERS_SUCCESS):
            return Object.assign({}, state, {
                subscribersList: action.subscribers,
                isFetchingSubscribers: false
            });

        case (constants.MARK_USER_ADDED_REQUEST):
        case (constants.MARK_USER_ARCHIVED_REQUEST):
            return Object.assign({}, state, {
                isRequestingMarkUser: true
            });

        case (constants.MARK_USER_ADDED_SUCCESS):
            return Object.assign({}, state, {
                subscribersList: state.subscribersList.filter(subscriber => subscriber._id !== action.userId),
                isRequestingMarkUser: false
            });

        case (constants.MARK_USER_ARCHIVED_SUCCESS):
            return Object.assign({}, state, {
                subscribersList: state.subscribersList.filter(subscriber => subscriber._id !== action.userId),
                isRequestingMarkUser: false
            });

        case (constants.SUBSCRIBE_REQUEST):
            return Object.assign({}, state, {
                isRequestingSubscribe: true
            });

        case (constants.SUBSCRIBE_SUCCESS):
            return Object.assign({}, state, {
                isRequestingSubscribe: false
            });

        case (constants.SUBSCRIBE_FAILURE):
            return Object.assign({}, state, {
                subscriptionError: action.errorMessage,
                isRequestingSubscribe: false
            });

        case (uiConstants.HIDE_MODAL):
            return Object.assign({}, state, {
                promoCodeInputValue: '',
                isCheckingPromoCode: false,
                checkedPromoCodeValue: '',
                checkedPromoCode: null
            });

        case (constants.GET_SEGPAY_USER_SUBSCRIPTIONS_SUCCESS):
            return Object.assign({}, state, {
                segpayUserSubscriptionsList: action.subscriptionsList
            });

        case (constants.CANCEL_SUBSCRIPTION_REQUEST):
            return Object.assign({}, state, {
                isRequestingCancelSubscription: true
            });

        case (constants.CANCEL_SUBSCRIPTION_SUCCESS):
            return Object.assign({}, state, {
                isRequestingCancelSubscription: false,
                segpayUserSubscriptionsList: state.segpayUserSubscriptionsList.map(subscription => {
                    if (subscription._id === action.subscriptionId) {
                        return Object.assign({}, subscription, {
                            is_pending_cancellation: true
                        })
                    } else {
                        return subscription
                    }
                })
            });

        case (constants.BAN_USER_REQUEST):
            return Object.assign({}, state, {
                isRequestingBanUser: true
            });

        case (constants.BAN_USER_SUCCESS):
            return Object.assign({}, state, {
                isRequestingBanUser: false,
                subscribersList: state.subscribersList.filter(subscriber => subscriber.user._id !== action.userId)
            });

        case (constants.GET_BANNED_USERS_REQUEST):
            return Object.assign({}, state, {
                isFetchingBannedUsers: true
            });

        case (constants.GET_BANNED_USERS_SUCCESS):
            return Object.assign({}, state, {
                isFetchingBannedUsers: false,
                bannedUsersList: action.bannedUsers
            });

        case (constants.GET_SEGPAY_PROMO_CODES_SUCCESS):
            return Object.assign({}, state, {
                promoCodesList: action.promoCodes
            });

        case (constants.CREATE_SEGPAY_PROMO_CODE_SUCCESS):
            return Object.assign({}, state, {
                promoCodesList: [].concat(action.promoCode, state.promoCodesList)
            });

        case (constants.UPDATE_SEGPAY_PROMO_CODE_SUCCESS):
            return Object.assign({}, state, {
                promoCodesList: state.promoCodesList.map(promoCode => {
                    if (promoCode._id === action.promoCode._id) {
                        return action.promoCode
                    } else {
                        return promoCode;
                    }
                })
            });

        case (constants.CHANGE_PROMO_CODE_INPUT):
            return Object.assign({}, state, {
                promoCodeInputValue: action.value
            });

        case (constants.CHECK_PROMO_CODE_REQUEST):
            return Object.assign({}, state, {
                isCheckingPromoCode: true,
                checkedPromoCode: null,
            });

        case (constants.CHECK_PROMO_CODE_SUCCESS):
            return Object.assign({}, state, {
                isCheckingPromoCode: false,
                checkedPromoCode: action.promoCode,
                checkedPromoCodeValue: action.promoCodeValue
            });

        case (constants.CHECK_PROMO_CODE_FAILURE):
            return Object.assign({}, state, {
                isCheckingPromoCode: false,
                checkedPromoCode: null,
                checkedPromoCodeValue: action.promoCodeValue
            });

        default:
            return state;
    }
}
