import { call, put, take, fork, cancel, select, takeEvery } from 'redux-saga/effects'
import { delay} from 'redux-saga'
import { SubmissionError } from 'redux-form';
import * as constants from './constants'
import qs from 'query-string';
import {getSubscribersSuccess, markUserAddedSuccess,
    getUserSubscriptionsSuccess, cancelSubscriptionSuccess, markUserArchivedSuccess, banUserSuccess,
    getBannedUsersSuccess, deleteUserAccountSuccess, deleteUserAccountFailure, getSegpayUserSubscriptionsSuccess,
    createSegPayPromoCodeSuccess, createSegPayPromoCodeFailure, getSegPayPromoCodesSuccess, getSegPayPromoCodesFailure,
    updateSegPayPromoCodeSuccess, updateSegPayPromoCodeFailure, checkPromoCodeRequest, checkPromoCodeSuccess,
    checkPromoCodeFailure} from './actions';
import { logout } from "../auth/actions";
import request from '../../helpers/request';

const PROMO_CODE_SEARCH_DEBOUNCE_TIME = 500;

export function* getSubscribers() {
    while (true) {
        const {activeTab} = yield take(constants.GET_SUBSCRIBERS_REQUEST);

        let url;
        switch (activeTab) {

            case constants.tabs.ACTIVE:
                url = '/segpay/subscriptions/active';
                break;
        }

        const response = yield call(request, url);
        const body = yield response.json();
        if (response.ok) {
            yield put(getSubscribersSuccess(body))
        }
    }
}

export function* markUserAdded() {
    while(true) {
        const {userId} = yield take(constants.MARK_USER_ADDED_REQUEST);
        const response = yield call(request, '/subscriptions/reset_anchor_now', 'POST', {user_id: userId});
        if (response.ok) {
            yield put(markUserAddedSuccess(userId));
        }
    }
}

export function* markUserArchived() {
    while(true) {
        const {userId} = yield take(constants.MARK_USER_ARCHIVED_REQUEST);
        const response = yield call(request, '/subscriptions/mark_archived', 'POST', {user_id: userId});
        if (response.ok) {
            yield put(markUserArchivedSuccess(userId));
        }
    }
}

export function* getSegpayUserSubscriptions() {
    while (true) {
        yield take(constants.GET_SEGPAY_USER_SUBSCRIPTIONS_REQUEST);

        const response = yield call(request, '/segpay/my');
        const body = yield response.json();
        if (response.ok) {
            yield put(getSegpayUserSubscriptionsSuccess(body));
        }
    }
}

export function* cancelSubscription() {
    while (true) {
        const {subscriptionId} = yield take(constants.CANCEL_SUBSCRIPTION_REQUEST);
        console.log(subscriptionId)
        const response = yield call(request, '/segpay/subscriptions/cancel', 'POST', {subscription_id: subscriptionId});
        if (response.ok) {
            yield put(cancelSubscriptionSuccess(subscriptionId));
        }
    }
}

function* watchCancelSubscription() {
    yield takeEvery(constants.CANCEL_SUBSCRIPTION_REQUEST, cancelSubscription)
}

export function* banUser() {
    while (true) {
        const {userId} = yield take(constants.BAN_USER_REQUEST);

        const response = yield call(request, '/users/ban', 'POST', {user_id: userId});
        if (response.ok) {
            yield put(banUserSuccess(userId));
        }
    }
}

export function* getBannedUsers() {
    while (true) {
        yield take(constants.GET_BANNED_USERS_REQUEST);

        const response = yield call(request, '/users/banned');
        const body = yield response.json();
        if (response.ok) {
            yield put(getBannedUsersSuccess(body));
        }
    }
}

export function* deleteUserAccount() {
    while (true) {
        yield take(constants.DELETE_ACCOUNT_REQUEST);

        const response = yield call(request, '/users/me', 'DELETE');
        if (response.ok) {
            yield put(deleteUserAccountSuccess());
            yield put(logout())
        } else {
            yield put(deleteUserAccountFailure())
        }
    }
}

export function* createSegPayPromoCode() {
    while (true) {
        const {payload: {values, resolve, reject}} = yield take(constants.CREATE_SEGPAY_PROMO_CODE_REQUEST);

        const response = yield call(request, '/segpay/promocodes', 'POST', values);
        const body = yield response.json();

        if (response.ok) {
            resolve();
            yield put(createSegPayPromoCodeSuccess(body));
        } else {
            if (body.errors) {
                const errors = {};
                for (let key in body.errors) {
                    errors[key] = body.errors[key].message
                }
                reject(new SubmissionError(errors));
            }
            yield put(createSegPayPromoCodeFailure())
        }
    }
}

export function* getSegPayPromoCodes() {
    while (true) {
        const {allArtists} = yield take(constants.GET_SEGPAY_PROMO_CODES_REQUEST);
        const queryString = qs.stringify({all_artists: allArtists});
        const response = yield call(request, `/segpay/promocodes${queryString ? `?${queryString}` : ''}`);
        const body = yield response.json();

        if (response.ok) {
            yield put(getSegPayPromoCodesSuccess(body));
        } else {
            yield put(getSegPayPromoCodesFailure())
        }
    }
}

export function* updateSegPayPromoCode() {
    while (true) {
        const {promoCodeId, updatedValues} = yield take(constants.UPDATE_SEGPAY_PROMO_CODE_REQUEST);

        const response = yield call(request, `/segpay/promocodes/${promoCodeId}`, 'PUT', updatedValues);
        const body = yield response.json();

        if (response.ok) {
            yield put(updateSegPayPromoCodeSuccess(body));
        } else {
            yield put(updateSegPayPromoCodeFailure())
        }
    }
}

export function* checkPromoCodeDebounced(promoCodeValue) {
    yield delay(PROMO_CODE_SEARCH_DEBOUNCE_TIME);
    const state = yield select();
    yield put(checkPromoCodeRequest(promoCodeValue, state.artist.artistProfile._id));
}

export function* changePromoCodeInputValueWatcher() {
    let delayedTask;
    while (true) {
        const {value} = yield take(constants.CHANGE_PROMO_CODE_INPUT);
        if (delayedTask) {
            yield cancel(delayedTask);
        }
        delayedTask = yield fork(checkPromoCodeDebounced, value)
    }

}

export function* checkPromoCode() {
    while (true) {
        const {promoCodeValue, artistId} = yield take(constants.CHECK_PROMO_CODE_REQUEST);

        const response = yield call(request, `/segpay/check_promocode`, 'POST', {code: promoCodeValue, artist_id: artistId});
        const body = yield response.json();

        if (response.ok) {
            yield put(checkPromoCodeSuccess(promoCodeValue, body));
        } else {
            yield put(checkPromoCodeFailure(promoCodeValue))
        }
    }
}

function startSagas(...sagas) {
    return function* rootSaga() {
        yield sagas.map(saga => fork(saga))
    }
}

export default startSagas(getSubscribers, markUserAdded, markUserArchived, watchCancelSubscription, banUser, getBannedUsers,
    deleteUserAccount, getSegpayUserSubscriptions, createSegPayPromoCode, getSegPayPromoCodes, updateSegPayPromoCode,
    changePromoCodeInputValueWatcher, checkPromoCode)
