import React from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Elements} from 'react-stripe-elements';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '../../../../base-components/Button/Button';
import creditCardIcon from '../../../../assets/images/credit-card.svg';
import {subscriptionTypes} from '../../constants';
import SubscriptionInfo from '../../components/SubscriptionInfo/SubscriptionInfo';
import CloseIcon from '@material-ui/icons/Close';
import {changePromoCodeInput} from '../../actions';

import styles from './SubscriptionModal.module.scss';

class SubscriptionModal extends React.Component {
    constructor() {
        super();

        this.state = {
            isTermsAgreed: false
        }
    }

    handleTermsCheckboxChange = () => {
        this.setState({isTermsAgreed: !this.state.isTermsAgreed})
    };

    handleReadyToSubscribeClick = () => {
        const {user, artist, checkedPromoCode} = this.props;
        const segpayTickedId = `${artist.segpay_package_id}:${checkedPromoCode ? checkedPromoCode.segpay_pricepoint_id : artist.segpay_pricepoint_id}`;
        const segpayLink = `https://secure2.segpay.com/billing/poset.cgi?x-eticketid=${segpayTickedId}&username=${user._id}&password=${user._id}&ppviewoption=2${checkedPromoCode ? '&REF1=' + checkedPromoCode.code : ''}`;
        window.location.replace(segpayLink);
    };

    handleCancelClick = () => {
        this.setState({isTermsAgreed: false});
        this.props.onHide();
    };

    render() {
        const {isOpen, user, artist, promoCodeInputValue, changePromoCodeInput, checkedPromoCodeValue, checkedPromoCode,
            isCheckingPromoCode} = this.props;

        let dialogueTitle, dialogueContent, dialogueActions;
        const isUserActivated = user && user.is_activated;
        if (isUserActivated) {
            dialogueTitle = <div className={styles.formHeader}>
                <CloseIcon className={styles.closeIcon} onClick={this.handleCancelClick}/>
                <img src={creditCardIcon} alt=""/>
                <h3>Pay with Card</h3>
            </div>;
            dialogueContent = <div>
                <SubscriptionInfo
                    artistName={artist && artist.name}
                    subscriptionPriceFirst30Days={artist && artist.subscription_price_cents_first_30_days}
                    subscriptionPriceRecurring30Days={artist && artist.subscription_price_cents_recurring_30_days}
                    isTermsAgreed={this.state.isTermsAgreed}
                    showPromoCodeValidation={promoCodeInputValue && (promoCodeInputValue === checkedPromoCodeValue)}
                    isPromoCodeValid={Boolean(checkedPromoCode)}
                    promoCodeInputValue={promoCodeInputValue}
                    checkedPromoCode={checkedPromoCode}
                    onPromoCodeInputChange={changePromoCodeInput}
                    onTermsCheckboxChange={this.handleTermsCheckboxChange}
                />
            </div>;
            dialogueActions = <DialogActions className={styles.dialogAction}>
                <Button
                    onClick={this.handleReadyToSubscribeClick}
                    disabled={!this.state.isTermsAgreed || (promoCodeInputValue !== checkedPromoCodeValue) || isCheckingPromoCode}
                    className={styles.submitButton}
                >
                    Ready to subscribe
                </Button>
                <Button onClick={this.handleCancelClick} className={styles.cancelButton}>
                    Cancel
                </Button>
            </DialogActions>;
        } else {
            dialogueTitle = <h3>
                Please confirm your email address to complete your subscription
            </h3>;
            dialogueActions = <DialogActions className={styles.notActiveAction}>
                <Button onClick={this.handleCancelClick} className={styles.submitButton}>
                    Ok
                </Button>
            </DialogActions>
        }

        return <Dialog
            disableBackdropClick
            disableEscapeKeyDown
            open={isOpen}
            fullWidth={true}
            className={cn(styles.container, { [styles.notActiveContainer]: !isUserActivated})}
        >
            <div className={styles.innerBox}>
                {dialogueTitle}
                {dialogueContent}
                {dialogueActions}
                <span className={styles.policyText}>* No refunds for any reasons</span>
            </div>
        </Dialog>
    }
}

SubscriptionModal.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onHide: PropTypes.func.isRequired,
    subscriptionType: PropTypes.oneOf([subscriptionTypes.LIFETIME, subscriptionTypes.MONTHLY]),
};

export default connect(
    (state) => ({
        currentStep: state.ui.subscriptionModalStep,
        subscriptionError: state.subscribe.subscriptionError,
        isRequestingSubscribe: state.subscribe.isRequestingSubscribe,
        artist: state.artist.artistProfile,
        user: state.auth.user,
        promoCodeInputValue: state.subscribe.promoCodeInputValue,
        isCheckingPromoCode: state.subscribe.isCheckingPromoCode,
        checkedPromoCode: state.subscribe.checkedPromoCode,
        checkedPromoCodeValue: state.subscribe.checkedPromoCodeValue,
    }),
    (dispatch) => ({
        changePromoCodeInput: value => dispatch(changePromoCodeInput(value))
    })
)(SubscriptionModal)

