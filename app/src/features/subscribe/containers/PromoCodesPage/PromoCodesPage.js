import React from 'react';
import {connect} from 'react-redux';
import AddPromoCodeForm from '../../components/AddPromoCodeForm/AddPromoCodeForm';
import PromoCodesTable from '../../components/PromoCodesTable/PromoCodesTable';
import Paper from '@material-ui/core/Paper';
import {roundTo} from '../../../../helpers/roundTo';
import {createSegPayPromoCodeRequest, getSegPayPromoCodesRequest, updateSegPayPromoCodeRequest} from '../../actions';

import styles from './PromoCodesPage.module.scss';

class PromoCodesPage extends React.Component {
    componentDidMount() {
        this.props.getSegPayPromoCodes();
    }

    handlePromoCodeFormSubmit = values => {
        const updatedValues = {
            ...values,
            subscription_price_cents_first_30_days: values.subscription_price_cents_first_30_days ?
                roundTo(Number(values.subscription_price_cents_first_30_days) * 100, 0) : 0,
            subscription_price_cents_recurring_30_days: values.subscription_price_cents_recurring_30_days ?
                roundTo(Number(values.subscription_price_cents_recurring_30_days) * 100, 0) : 0,
        };
        return new Promise((resolve, reject) => {
            this.props.createSegPayPromoCode({values: updatedValues, resolve, reject})
        })
    };

    handleChangePromoCodeActivity = (promoCodeId, isActive) => {
        this.props.updateSegPayPromoCode(promoCodeId, {is_active: isActive});
    };

    render() {
        const {promoCodesList} = this.props;

        return (
            <div className={styles.container}>
                <Paper className={styles.formContainer}>
                    <AddPromoCodeForm onSubmit={this.handlePromoCodeFormSubmit} />
                </Paper>
                <Paper className={styles.tableContainer}>
                    <PromoCodesTable
                        promoCodesList={promoCodesList}
                        onChangeActivity={this.handleChangePromoCodeActivity}
                    />
                </Paper>
            </div>
        )
    }
}

export default connect(
    state => ({
        promoCodesList: state.subscribe.promoCodesList
    }),
    dispatch => ({
        getSegPayPromoCodes: () => dispatch(getSegPayPromoCodesRequest()),
        createSegPayPromoCode: (payload) => dispatch(createSegPayPromoCodeRequest(payload)),
        updateSegPayPromoCode: (promoCodeId, updatedValues) => dispatch(updateSegPayPromoCodeRequest(promoCodeId, updatedValues))
    })
)(PromoCodesPage)
