import React from 'react';
import {connect} from 'react-redux';
import { compose } from 'recompose';
import withWidth from '@material-ui/core/withWidth';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import CircularProgress from '@material-ui/core/CircularProgress';
import Paper from '@material-ui/core/Paper';
import * as constants from '../../constants';
import {copyToClipboard} from '../../../../helpers/copyToClipboard';
import SubscribersTable from '../../components/SubscribersTable/SubscribersTable';
import SegpaySubscribersTable from '../../components/SegpaySubscribersTable/SegpaySubscribersTable';
import BannedUsersTable from '../../components/BannedUsersTable/BannedUsersTable';
import {getSubscribersRequest, markUserAddedRequest, markUserArchivedRequest, banUserRequest, getBannedUsersRequest} from '../../actions';

import * as styles from './SubscribersPage.module.scss';
import config from "../../../../config";
import {tabs} from "../../constants";

class SubscribersPage extends React.Component {
    constructor() {
        super();

        this.state = {
            activeTab: constants.tabs.ACTIVE
        }
    }

    componentWillMount() {
        this.props.getSubscribers(this.state.activeTab);
    }

    handleTabChange = (tab, value) => {
        this.setState({activeTab: value});
        if (value === constants.tabs.BANNED) {
            this.props.getBannedUsers();
        } else {
            this.props.getSubscribers(value);
        }
    };

    handleBanUser = (user) => {
        if (window.confirm(`Banning ${user.snapchat_username}. Are you sure you want to ban this user? We will stop charging his credit card every month and he won't have access to your posts`)) {
            this.props.banUser(user._id);
        }
    };

    renderTable = () => {
        const {subscribersList, markUserAdded, markUserArchived, bannedUsersList, currentUser} = this.props;

        switch (this.state.activeTab) {
            case constants.tabs.BANNED:
                return <BannedUsersTable usersList={bannedUsersList}/>;

            case constants.tabs.ACTIVE:
                return <SegpaySubscribersTable
                    currentUser={currentUser}
                    copyResetLink={str => copyToClipboard(config[process.env.REACT_APP_ENV].FRONTEND_DOMAIN + '/reset-password/' + str)}
                    subscribersList={subscribersList}
                    markUserAdded={markUserAdded}
                    markUserArchived={markUserArchived}
                    banUser={this.handleBanUser}
                />
        }
    };

    render() {
        const {isFetchingSubscribers, isRequestingMarkUser, isRequestingBanUser, isFetchingBannedUsers, width} = this.props;

        return (
            <div className={styles.container}>
                <Paper className={styles.paper}>
                    <Tabs
                        value={this.state.activeTab}
                        onChange={this.handleTabChange}
                        indicatorColor='primary'
                        textColor='primary'
                        centered
                        scrollable={width === 'xs'}
                    >
                        <Tab label='Active' value={constants.tabs.ACTIVE}/>
                        <Tab label='Banned' value={constants.tabs.BANNED} />
                    </Tabs>
                    {
                        !isFetchingSubscribers
                            && this.renderTable()
                    }
                    {
                        (isFetchingSubscribers || isRequestingMarkUser || isRequestingBanUser || isFetchingBannedUsers)
                            && <div className={styles.spinnerContainer}>
                                <CircularProgress />
                            </div>
                    }
                </Paper>
            </div>
        )
    }
}

export default compose(
    withWidth(),
    connect(
        (state) => ({
            subscribersList: state.subscribe.subscribersList,
            bannedUsersList: state.subscribe.bannedUsersList,
            isFetchingSubscribers: state.subscribe.isFetchingSubscribers,
            isFetchingBannedUsers: state.subscribe.isFetchingBannedUsers,
            isRequestingMarkUser: state.subscribe.isRequestingMarkUser,
            isRequestingBanUser: state.subscribe.isRequestingBanUser,
            currentUser: state.auth.user
        }),
        (dispatch) => ({
            getSubscribers: (activeTab) => dispatch(getSubscribersRequest(activeTab)),
            markUserAdded: (userId) => dispatch(markUserAddedRequest(userId)),
            markUserArchived: (userId) => dispatch(markUserArchivedRequest(userId)),
            banUser: (userId) => dispatch(banUserRequest(userId)),
            getBannedUsers: () => dispatch(getBannedUsersRequest())
        })
    )
)(SubscribersPage);
