import * as constants from './constants';

export const getSubscribersRequest = (activeTab) => ({
    type: constants.GET_SUBSCRIBERS_REQUEST,
    activeTab
});

export const getSubscribersSuccess = (subscribers) => ({
    type: constants.GET_SUBSCRIBERS_SUCCESS,
    subscribers
});

export const getSubscribersFailure = () => ({
    type: constants.GET_SUBSCRIBERS_FAILURE
});

export const markUserAddedRequest = (userId) => ({
    type: constants.MARK_USER_ADDED_REQUEST,
    userId
});

export const markUserAddedSuccess = (userId) => ({
    type: constants.MARK_USER_ADDED_SUCCESS,
    userId
});

export const markUserAddedFailure = (userId) => ({
    type: constants.MARK_USER_ADDED_FAILURE
});

export const cancelSubscriptionRequest = (subscriptionId) => ({
    type: constants.CANCEL_SUBSCRIPTION_REQUEST,
    subscriptionId
});

export const cancelSubscriptionSuccess = (subscriptionId) => ({
    type: constants.CANCEL_SUBSCRIPTION_SUCCESS,
    subscriptionId
});

export const cancelSubscriptionFailure = () => ({
    type: constants.CANCEL_SUBSCRIPTION_FAILURE
});

export const markUserArchivedRequest = (userId) => ({
    type: constants.MARK_USER_ARCHIVED_REQUEST,
    userId
});

export const markUserArchivedSuccess = (userId) => ({
    type: constants.MARK_USER_ARCHIVED_SUCCESS,
    userId
});

export const markUserArchivedFailure = () => ({
    type: constants.MARK_USER_ARCHIVED_FAILURE
});

export const banUserRequest = (userId) => ({
    type: constants.BAN_USER_REQUEST,
    userId
});

export const banUserSuccess = (userId) => ({
    type: constants.BAN_USER_SUCCESS,
    userId
});

export const banUserFailure = () => ({
    type: constants.BAN_USER_FAILURE
});

export const getBannedUsersRequest = () => ({
    type: constants.GET_BANNED_USERS_REQUEST
});

export const getBannedUsersSuccess = (bannedUsers) => ({
    type: constants.GET_BANNED_USERS_SUCCESS,
    bannedUsers
});

export const getBannedUsersFailure = () => ({
    type: constants.GET_BANNED_USERS_FAILURE
});

export const deleteUserAccountRequest = () => ({
    type: constants.DELETE_ACCOUNT_REQUEST
});

export const deleteUserAccountSuccess = () => ({
    type: constants.DELETE_ACCOUNT_SUCCESS
});

export const deleteUserAccountFailure = () => ({
    type: constants.DELETE_ACCOUNT_FAILURE,
});

export const getSegpayUserSubscriptionsRequest = () => ({
    type: constants.GET_SEGPAY_USER_SUBSCRIPTIONS_REQUEST
});

export const getSegpayUserSubscriptionsSuccess = (subscriptionsList) => ({
    type: constants.GET_SEGPAY_USER_SUBSCRIPTIONS_SUCCESS,
    subscriptionsList
});

export const createSegPayPromoCodeRequest = (payload) => ({
    type: constants.CREATE_SEGPAY_PROMO_CODE_REQUEST,
    payload
});

export const createSegPayPromoCodeSuccess = (promoCode) => ({
    type: constants.CREATE_SEGPAY_PROMO_CODE_SUCCESS,
    promoCode
});

export const createSegPayPromoCodeFailure = () => ({
    type: constants.CREATE_SEGPAY_PROMO_CODE_FAILURE
});

export const getSegPayPromoCodesRequest = (allArtists) => ({
    type: constants.GET_SEGPAY_PROMO_CODES_REQUEST,
    allArtists
});

export const getSegPayPromoCodesSuccess = (promoCodes) => ({
    type: constants.GET_SEGPAY_PROMO_CODES_SUCCESS,
    promoCodes
});

export const getSegPayPromoCodesFailure = () => ({
    type: constants.GET_SEGPAY_PROMO_CODES_FAILURE
});

export const updateSegPayPromoCodeRequest = (promoCodeId, updatedValues) => ({
    type: constants.UPDATE_SEGPAY_PROMO_CODE_REQUEST,
    promoCodeId,
    updatedValues
});

export const updateSegPayPromoCodeSuccess = (promoCode) => ({
    type: constants.UPDATE_SEGPAY_PROMO_CODE_SUCCESS,
    promoCode
});

export const updateSegPayPromoCodeFailure = () => ({
    type: constants.UPDATE_SEGPAY_PROMO_CODE_FAILURE
});

export const changePromoCodeInput = (value) => ({
    type: constants.CHANGE_PROMO_CODE_INPUT,
    value
});

export const checkPromoCodeRequest = (promoCodeValue, artistId) => ({
    type: constants.CHECK_PROMO_CODE_REQUEST,
    promoCodeValue,
    artistId
});

export const checkPromoCodeSuccess = (promoCodeValue, promoCode) => ({
    type: constants.CHECK_PROMO_CODE_SUCCESS,
    promoCodeValue,
    promoCode
});

export const checkPromoCodeFailure = (promoCodeValue) => ({
    type: constants.CHECK_PROMO_CODE_FAILURE,
    promoCodeValue
});
