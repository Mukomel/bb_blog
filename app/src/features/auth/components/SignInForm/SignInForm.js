import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import {TextField, Checkbox} from 'redux-form-material-ui';
import isValidEmail from '../../../../helpers/isValidEmail';
import FormTextField from '../../../../base-components/FormTextField/FormTextField'

class SignInForm extends React.Component {

    handleFormSubmit = (values) => {
        const { onSubmit } = this.props;
        return onSubmit(values);
    };

    componentWillReceiveProps(nextProps) {
        if ((nextProps.valid !== this.props.valid) && this.props.onFormValidChange) {
            this.props.onFormValidChange(nextProps.valid);
        }

        if ((nextProps.submitting !== this.props.submitting) && this.props.onFormSubmittingChange) {
            this.props.onFormSubmittingChange(nextProps.submitting);
        }
    }

    render() {
        const { handleSubmit } = this.props;
        return (
            <form onSubmit={handleSubmit(this.handleFormSubmit)}>
                <FormTextField
                    name='email'
                    label='E-mail'
                    component={TextField}
                    fullWidth
                    margin='dense'
                    helperText='Your email will be hidden from public'
                    type='email'
                />
                <FormTextField
                    name='password'
                    label='Password'
                    component={TextField}
                    fullWidth
                    margin='dense'
                    helperText=' '
                    type='password'
                />
            </form>
        )
    }
}

SignInForm.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onFormValidChange: PropTypes.func.isRequired,
    onFormSubmittingChange: PropTypes.func
};

export default reduxForm({
    form: 'signInForm',
    validate: (values, props) => {
        const errors = {};

        if (!isValidEmail(values.email)) {
            errors.email = 'Email is not valid'
        }

        if(!values.email) {
            errors.email = 'Required'
        }

        if (!values.password) {
            errors.password = 'Required'
        }

        return errors;
    }
})(SignInForm)