import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import {TextField} from 'redux-form-material-ui';
import isValidEmail from '../../../../helpers/isValidEmail';

import * as styles from './ResetPasswordForm.module.scss';

class ResetPasswordForm extends React.Component {

    handleFormSubmit = (values) => {
        const { onSubmit } = this.props;
        return onSubmit(values);
    };

    componentWillReceiveProps(nextProps) {
        if ((nextProps.valid !== this.props.valid) && this.props.onFormValidChange) {
            this.props.onFormValidChange(nextProps.valid);
        }

        if ((nextProps.submitting !== this.props.submitting) && this.props.onFormSubmittingChange) {
            this.props.onFormSubmittingChange(nextProps.submitting);
        }
    }

    render() {
        const { handleSubmit, error } = this.props;
        return (
            <form className={styles.form} onSubmit={handleSubmit(this.handleFormSubmit)}>
                <Field
                    name='password'
                    label='Password'
                    component={TextField}
                    fullWidth
                    margin='dense'
                    helperText=''
                    type='password'
                />
                <Field
                    name='password_repeat'
                    label='Repeat password'
                    component={TextField}
                    fullWidth
                    margin='dense'
                    helperText=''
                    type='password'
                />
                {
                    error
                        && <div className={styles.formError}>{error}</div>
                }
            </form>
        )
    }
}

ResetPasswordForm.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onFormValidChange: PropTypes.func.isRequired,
    onFormSubmittingChange: PropTypes.func
};

export default reduxForm({
    form: 'resetPasswordForm',
    validate: (values, props) => {
        const errors = {};

        if (values.password && values.password.length < 6) {
            errors.password = 'Should be at least 6 characters'
        }

        if (!values.password) {
            errors.password = 'Required'
        }

        if (values.password_repeat !== values.password) {
            errors.password_repeat = 'Should be the same as password'
        }

        return errors;
    }
})(ResetPasswordForm)