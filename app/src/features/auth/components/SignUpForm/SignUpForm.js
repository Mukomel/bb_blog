import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import FormControlLabel  from '@material-ui/core/FormControlLabel'
import {TextField, Checkbox} from 'redux-form-material-ui';
import isValidEmail from '../../../../helpers/isValidEmail';
import FormTextField from '../../../../base-components/FormTextField/FormTextField';
import TextFieldWithPopover from '../../../../base-components/TextFieldWithPopover/TextFieldWithPopover';
import { DISCLAIMED_MAIL_REG_EXP } from '../../constants';

import * as styles from './SignUpForm.module.scss';

class SignUpForm extends React.Component {

    state = {
        anchorEl: null,
    };

    handleClick = event => {
        this.setState({
            anchorEl: event.currentTarget,
        });
    };

    handleClose = () => {
        this.setState({
            anchorEl: null,
        });
    };

    handleFormSubmit = (values) => {
        const { onSubmit } = this.props;
        return onSubmit(values);
    };

    componentWillReceiveProps(nextProps) {
        if ((nextProps.valid !== this.props.valid) && this.props.onFormValidChange) {
            this.props.onFormValidChange(nextProps.valid);
        }

        if ((nextProps.submitting !== this.props.submitting) && this.props.onFormSubmittingChange) {
            this.props.onFormSubmittingChange(nextProps.submitting);
        }
    }

    render() {
        const { handleSubmit } = this.props;
        return (
            <form className={styles.form} onSubmit={handleSubmit(this.handleFormSubmit)}>
                <FormTextField
                    name='email'
                    autoComplete="off"
                    label="E-mail"
                    onClick={this.handleClick}
                    component={TextField}
                    onCloseIconClick={this.handleClose}
                    anchorEl={this.state.anchorEl}
                    fullWidth
                    margin='dense'
                    helperText='Your email will be hidden from public'
                    type='email'
                />
                <FormTextField
                    name='email_repeat'
                    autoComplete="off"
                    label="Repeat e-mail"
                    component={TextField}
                    fullWidth
                    margin='dense'
                    helperText=''
                    type='email'
                />
                <FormTextField
                    name='snapchat_username'
                    autoComplete="off"
                    label="Snapchat Username (optional)"
                    component={TextField}
                    fullWidth
                    margin='dense'
                    helperText='Your Snapchat username will be hidden from public'
                />
                <FormTextField
                    name='username'
                    autoComplete="off"
                    label="Username"
                    component={TextField}
                    fullWidth
                    margin='dense'
                    helperText='Your username will be visible to everyone'
                />
                <FormTextField
                    name='password'
                    autoComplete="off"
                    label='Password'
                    component={TextField}
                    fullWidth
                    margin='dense'
                    helperText=''
                    type='password'
                />

                <FormControlLabel
                    control={
                        <Field
                            className={styles.checkbox}
                            name='is_adult'
                            component={Checkbox}
                            color="primary"
                        />
                    }
                    label="By checking this box, I certify I am at least 18-years old and have reached the age of majority where I live."
                />
                <FormControlLabel
                    control={
                        <Field
                            className={styles.checkbox}
                            name='terms'
                            component={Checkbox}
                            color="primary"
                        />
                    }
                    label="By checking this box, I certify I have read and agree to the Terms-of-Service Agreement."
                />
                <FormControlLabel
                    control={
                        <Field
                            name='not_receive_emails'
                            component={Checkbox}
                            className={styles.checkbox}
                            color="primary"
                        />
                    }
                    label="I do not want to receive updates and special offers targeted to my interests by email from BrandtsBoys.com and its partners."
                />
            </form>
        )
    }
}

SignUpForm.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onFormValidChange: PropTypes.func.isRequired,
    onFormSubmittingChange: PropTypes.func
};

export default reduxForm({
    form: 'signUpForm',
    validate: (values, props) => {
        const errors = {};
        if (!isValidEmail(values.email)) {
            errors.email = 'Email is not valid'
        }

        if(!values.email) {
            errors.email = 'Required'
        }

        if (!values.username) {
            errors.username = 'Required'
        }

        if (values.username === values.email) {
            errors.username = 'Username cannot be same as email'
        }

        if (values.email !== values.email_repeat) {
            errors.email_repeat = 'Emails are different. Please provide a correct email'
        }

        if (values.username === values.snapchat_username) {
            errors.username = 'Username cannot be same as snapchat username'
        }

        if (values.password && values.password.length < 6) {
            errors.password = 'Should be at least 6 characters'
        }

        if (!values.password) {
            errors.password = 'Required'
        }

        if (!values.is_adult) {
            errors.is_adult = 'Required'
        }

        if (!values.terms) {
            errors.terms = 'Required'
        }

        return errors;
    },
    warn: values => {
        const warnings = {};

        if (DISCLAIMED_MAIL_REG_EXP.test(values.email)) {
            warnings.email = 'Note, if you’re using Yahoo or AOL your email may not send; due to their preferences. If you don’t receive the email, try a different provider, Gmail works with certainty. Thank you.'
        }
        return warnings
    }
})(SignUpForm);
