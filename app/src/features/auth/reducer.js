import * as constants from './constants';
import * as postConstants from '../artist/constants'
import * as subscribeConstants from '../subscribe/constants';
import {hasToken} from '../../helpers/auth';

const initialState = {
    isAuthChecked: false,
    isLoggedIn: false,
    user: null,
    isFetchingUser: false,
    isRequestingAccountActivation: false,
    accountActivationError: null,
    isActivationEmailResent: false
};

export default function(state = initialState, action) {
    switch(action.type) {

        case (constants.SIGN_UP_SUCCESS):
        case (constants.SIGN_IN_SUCCESS):
            return Object.assign({}, state, {
                isLoggedIn: true
            });

        case (constants.CHECK_AUTH):
            return Object.assign({}, state, {
                isAuthChecked: true,
                isLoggedIn: hasToken()
            });

        case (constants.GET_USER_SUCCESS):
            return Object.assign({}, state, {
                user: action.user,
                isFetchingUser: false
            });

        case (constants.GET_USER_REQUEST):
            return Object.assign({}, state, {
                isFetchingUser: true
            });

        case (constants.GET_USER_FAILURE):
            return Object.assign({}, state, {
                isFetchingUser: false
            });

        case (constants.LOGOUT):
            return Object.assign({}, state, {
                isLoggedIn: false,
                user: null
            });

        case (constants.ACTIVATE_ACCOUNT_REQUEST):
            return Object.assign({}, state, {
                isRequestingAccountActivation: true,
                accountActivationError: null
            });

        case (constants.ACTIVATE_ACCOUNT_SUCCESS):
            return Object.assign({}, state, {
                isRequestingAccountActivation: false
            });

        case (constants.ACTIVATE_ACCOUNT_FAILURE):
            return Object.assign({}, state, {
                isRequestingAccountActivation: false,
                accountActivationError: action.error
            });

        case (constants.UPDATE_USER_SUCCESS):
            return Object.assign({}, state, {
                user: action.user
            });

        case (constants.RESEND_ACTIVATION_LINK_SUCCESS):
            return Object.assign({}, state, {
                isActivationEmailResent: true
            });

        case (postConstants.POST_LIKE_SUCCESS):
            if (state.user) {
                return {
                    ...state,
                    user: {
                        ...state.user,
                        points: state.user.points + 1
                    }
                };
            }
            return {
                ...state
            };

        default:
            return state;
    }
}
