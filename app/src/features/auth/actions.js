import * as constants from './constants';

export const signUpRequest = (payload) => ({
    type: constants.SIGN_UP_REQUEST,
    payload
});

export const signUpSuccess = () => ({
    type: constants.SIGN_UP_SUCCESS
});

export const signUpFailure = () => ({
    type: constants.SIGN_UP_FAILURE
});

export const checkAuth = () => ({
    type: constants.CHECK_AUTH
});

export const signInRequest = (payload) => ({
    type: constants.SIGN_IN_REQUEST,
    payload
});

export const signInSuccess = () => ({
    type: constants.SIGN_IN_SUCCESS
});

export const signInFailure = () => ({
    type: constants.SIGN_IN_FAILURE
});

export const getUserRequest = () => ({
    type: constants.GET_USER_REQUEST
});

export const getUserSuccess = (user) => ({
    type: constants.GET_USER_SUCCESS,
    user
});

export const getUserFailure = () => ({
    type: constants.GET_USER_FAILURE
});

export const logout = () => ({
    type: constants.LOGOUT
});

export const forgotPasswordRequest = (payload) => ({
    type: constants.FORGOT_PASSWORD_REQUEST,
    payload
});

export const forgotPasswordSuccess = () => ({
    type: constants.FORGOT_PASSWORD_SUCCESS
});

export const forgotPasswordFailure = () => ({
    type: constants.FORGOT_PASSWORD_FAILURE
});

export const resetPasswordRequest = (payload) => ({
    type: constants.RESET_PASSWORD_REQUEST,
    payload
});

export const resetPasswordSuccess = () => ({
    type: constants.RESET_PASSWORD_SUCCESS
});

export const resetPasswordFailure = () => ({
    type: constants.RESET_PASSWORD_FAILURE
});

export const activateAccountRequest = (activationToken) => ({
    type: constants.ACTIVATE_ACCOUNT_REQUEST,
    activationToken
});

export const activateAccountSuccess = () => ({
    type: constants.ACTIVATE_ACCOUNT_SUCCESS
});

export const activateAccountFailure = (error) => ({
    type: constants.ACTIVATE_ACCOUNT_FAILURE,
    error
});

export const updateUserRequest = (payload) => ({
    type: constants.UPDATE_USER_REQUEST,
    payload
});

export const updateUserSuccess = (user) => ({
    type: constants.UPDATE_USER_SUCCESS,
    user
});

export const updateUserFailure = () => ({
    type: constants.UPDATE_USER_FAILURE
});

export const resendActivationLinkRequest = () => ({
    type: constants.RESEND_ACTIVATION_LINK_REQUEST
});

export const resendActivationLinkSuccess = () => ({
    type: constants.RESEND_ACTIVATION_LINK_SUCCESS
});

export const resendActivationLinkFailure = () => ({
    type: constants.RESEND_ACTIVATION_LINK_FAILURE
});