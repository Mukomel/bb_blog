import { call, put, take, fork, select } from 'redux-saga/effects'
import { SubmissionError } from 'redux-form';
import * as constants from './constants'
import {signUpSuccess, signUpFailure, signInSuccess, signInFailure, getUserSuccess, getUserFailure, getUserRequest,
    forgotPasswordSuccess, forgotPasswordFailure, resetPasswordSuccess, resetPasswordFailure, activateAccountSuccess,
    activateAccountFailure, updateUserSuccess, updateUserFailure, resendActivationLinkSuccess, resendActivationLinkFailure} from './actions';
import {hideModal, showModal, changeForgotPasswordModalStep, changeResetPasswordModalStep, changeSignUpModalStep} from '../ui/actions';
import {modalTypes, forgotPasswordModalSteps, resetPasswordModalStep, signUpModalSteps} from '../ui/constants';
import request from '../../helpers/request';
import {getTopFansRequest} from '../landing/actions';
import {setToken, hasToken, removeToken} from '../../helpers/auth';
import {uploadImage} from '../assets/sagas';
import {showSuccessMessage} from '../ui/actions';

export function* signUp() {
    while (true) {
        const {payload: {redirect, resolve, reject, credentials: {email, username, snapchat_username, password, not_receive_emails}}} = yield take(constants.SIGN_UP_REQUEST);

        const response = yield call(request, '/users', 'POST', {
            email,
            username,
            snapchat_username,
            password,
            receive_emails: !not_receive_emails
        });

        const body = yield response.json();
        if (response.ok) {
            setToken(body.token);
            yield put(signUpSuccess());
            resolve();
            yield put(getUserRequest());
            const state = yield select();
            yield put(getTopFansRequest(state.ui.topFansTab));
            yield put(changeSignUpModalStep(signUpModalSteps.CONFIRMATION_EMAIL_MESSAGE));

        } else {
            if (body.errors) {
                const errors = {};
                for (let key in body.errors) {
                    errors[key] = body.errors[key].message
                }

                reject(new SubmissionError(errors));
            }
            yield put(signUpFailure());
        }
    }
}

export function* signIn() {
    while (true) {
        const {payload: {resolve, reject, credentials: {email, password}, redirect}} = yield take(constants.SIGN_IN_REQUEST);

        const response = yield call(request, '/users/login', 'POST', {email, password});
        const body = yield response.json();
        if (response.ok) {
            setToken(body.token);
            yield put(signInSuccess());
            resolve();
            yield put(getUserRequest());
            const {user} = yield take(constants.GET_USER_SUCCESS);
            const state = yield select();
            if (state.ui.modalProps.nextModal) {
                if ((state.ui.modalProps.nextModal.modalType === modalTypes.SUBSCRIPTION_MODAL)
                    && state.ui.modalProps.nextModal.modalProps.reloadAfterSubscription
                    && user.visible_content_periods
                    && (user.visible_content_periods.length > 0)
                ) {
                    window.location.reload();
                }

                if ((state.ui.modalProps.nextModal.modalType === modalTypes.SUBSCRIPTION_MODAL)
                    && user.artists_subscriptions.indexOf(state.ui.modalProps.nextModal.modalProps.artist_id) > -1) {
                    yield put(hideModal());
                } else {
                    yield put(showModal(state.ui.modalProps.nextModal.modalType, state.ui.modalProps.nextModal.modalProps));
                }
            } else {
                yield put(hideModal());
                if (redirect) {
                    window.location = '/';
                }
            }
        } else {
            if (response.status === 422) {
                reject(new SubmissionError({password: 'The email or password you entered is not valid'}));
            }
            yield put(signInFailure());
        }
    }
}

export function* getUser() {
    while (true) {
        yield take(constants.GET_USER_REQUEST);

        const response = yield call(request, '/users/me');
        const body = yield response.json();
        // window.Intercom('update', {
        //     _id: body._id,
        //     is_activated: body.is_activated,
        //     receive_emails: body.receive_emails,
        //     username: body.username,
        //     email: body.email,
        // });
        if (response.ok) {
            yield put(getUserSuccess(body));
        } else {
            yield put(getUserFailure());
        }
    }
}

export function* checkAuth() {
    while (true) {
        yield take(constants.CHECK_AUTH);
        if (hasToken()) {
            yield put(getUserRequest());
        }
    }
}

export function* logout() {
    while (true) {
        yield take(constants.LOGOUT);
        removeToken();
    }
}

export function* forgotPassword() {
    while (true) {
        const {payload: {resolve, reject, email}} = yield take(constants.FORGOT_PASSWORD_REQUEST);
        const response = yield call(request, '/users/forgot-password', 'POST', {email});
        if (response.ok) {
            resolve();
            yield put(changeForgotPasswordModalStep(forgotPasswordModalSteps.CONGRATULATIONS));
            yield put(forgotPasswordSuccess());
        } else {
            reject();
            yield put(forgotPasswordFailure());
        }
    }
}

export function* resetPassword() {
    while (true) {
        const {payload: {resolve, reject, password, resetPasswordToken}} = yield take(constants.RESET_PASSWORD_REQUEST);
        const response = yield call(request, '/users/reset-password', 'POST', {
            password,
            password_reset_token: resetPasswordToken
        });
        if (response.ok) {
            resolve();
            yield put(changeResetPasswordModalStep(resetPasswordModalStep.CONGRATULATIONS));
            yield put(resetPasswordSuccess());
        } else {
            if (response.status === 404) {
                reject(new SubmissionError({_error: 'Error. Please try to request password recovery email again'}));
            }
            yield put(resetPasswordFailure());
        }
    }
}

export function* activateAccount() {
    while (true) {
        const {activationToken} = yield take(constants.ACTIVATE_ACCOUNT_REQUEST);
        const response = yield call(request, '/users/activate-account/', 'POST', {
            activation_token: activationToken
        });
        const body = yield response.json();
        if (response.ok) {
            yield put(activateAccountSuccess());
        } else {
            yield put(activateAccountFailure(body.message));
        }
    }
}

export function* updateUser() {
    while (true) {
        const {payload: {values, resolve, reject}} = yield take(constants.UPDATE_USER_REQUEST);

        const reqBody = {
            email: values.email,
            username: values.username,
            snapchat_username: values.snapchat_username,
            receive_emails: values.receive_emails,
            wishlist_link: values.wishlist_link,
            biography: values.biography
        };

        if (values.avatar) {
            reqBody.avatar = yield call(uploadImage, values.avatar);
        }

        const response = yield call(request, '/users/me', 'PUT', reqBody);
        const body = yield response.json();
        if (response.ok) {
            resolve();
            yield put(updateUserSuccess(body));
            yield put(showSuccessMessage())
        } else {
            if (body.errors) {
                const errors = {};
                for (let key in body.errors) {
                    errors[key] = body.errors[key].message
                }
                reject(new SubmissionError(errors));
            }
            yield put(updateUserFailure());
        }
    }
}

export function* resendActivationLink() {
    while (true) {
        yield take(constants.RESEND_ACTIVATION_LINK_REQUEST);

        const response = yield call(request, '/users/resend-activation-link', 'POST', {});
        if (response.ok) {
            yield put(resendActivationLinkSuccess());
        } else {
            yield put(resendActivationLinkFailure());
        }
    }
}

function startSagas(...sagas) {
    return function* rootSaga() {
        yield sagas.map(saga => fork(saga))
    }
}

export default startSagas(signUp, signIn, getUser, checkAuth, logout, forgotPassword, resetPassword, activateAccount,
    updateUser, resendActivationLink);
