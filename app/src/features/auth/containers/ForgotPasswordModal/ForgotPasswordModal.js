import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import {submit} from 'redux-form';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';
import {forgotPasswordModalSteps} from '../../../ui/constants';
import {forgotPasswordRequest} from '../../actions'
import ForgotPasswordForm from '../../components/ForgotPasswordForm/ForgotPasswordForm';
import CloseFormIconButton from '../../../../base-components/CloseFormIconButton/CloseFormIconButton'

import styles from './ForgotPasswordModal.module.scss';

class ForgotPasswordModal extends React.Component {
    constructor() {
        super();

        this.state = {
            isForgotPasswordFormValid: false,
            isForgotPasswordFormSubmitting: false
        };
    }

    handleForgotPasswordFormValidChange = (isValid) => {
        this.setState({
            isForgotPasswordFormValid: isValid
        })
    };

    handleForgotPasswordFormSubmittingChange = (isSubmitting) => {
        this.setState({
            isForgotPasswordFormSubmitting: isSubmitting
        })
    };

    handleForgotPasswordFormSubmit = (values) => {
        const {forgotPassword} = this.props;
        return new Promise((resolve, reject) => {
            forgotPassword({email: values.email, resolve, reject});
        });
    };

    render() {
        const {isOpen, onHide, currentStep, submitForgotPasswordForm} = this.props;

        let dialogueTitle, dialogueContent, dialogueActions;

        switch (currentStep) {

            case forgotPasswordModalSteps.EMAIL_FORM:
                dialogueTitle = <DialogTitle className={styles.caption}>
                    <CloseFormIconButton onClick={onHide}/>
                    Password recovery
                </DialogTitle>;
                dialogueContent = <DialogContent className={styles.content}>
                    <ForgotPasswordForm
                        onSubmit={this.handleForgotPasswordFormSubmit}
                        onFormValidChange={this.handleForgotPasswordFormValidChange}
                        onFormSubmittingChange={this.handleForgotPasswordFormSubmittingChange}
                    />
                    {
                        (this.state.isForgotPasswordFormSubmitting)
                            && <SpinnerWithBackdrop />
                    }
                </DialogContent>;
                dialogueActions = <div>
                    <button
                        className={styles.resetButton}
                        onClick={submitForgotPasswordForm}
                        disabled={!this.state.isForgotPasswordFormValid}
                    >
                        Reset my password
                    </button>
                    <button onClick={onHide} className={styles.cancelButton}>Cancel</button>
                </div>;
                break;

            case forgotPasswordModalSteps.CONGRATULATIONS:
                dialogueTitle = <DialogTitle className={styles.congratulationTitle}>Success</DialogTitle>;
                dialogueContent = <DialogContent>
                    <div className={styles.congratulationText}>If this email address exists, we will immediately send Reset Password link and further instructions</div>
                </DialogContent>;
                dialogueActions = <DialogActions>
                    <button
                        className={styles.resetButton}
                        onClick={onHide}
                    >
                        Ok
                    </button>
                </DialogActions>;
                break;
        }

        return (
                <Dialog
                    className={styles.modal}
                    disableBackdropClick
                    disableEscapeKeyDown
                    open={isOpen}
                    fullWidth
                    fullScreen
                >
                    {dialogueTitle}
                    {dialogueContent}
                    {dialogueActions}
                </Dialog>
        )
    }
}

ForgotPasswordModal.propTypes = {
    isOpen: PropTypes.bool,
    onHide: PropTypes.func,
};

export default connect(
    (state) => ({
        currentStep: state.ui.forgotPasswordModalStep
    }),
    (dispatch) => ({
        forgotPassword: (payload) => dispatch(forgotPasswordRequest(payload)),
        submitForgotPasswordForm: () => dispatch(submit('forgotPasswordForm')),
    })
)(ForgotPasswordModal);
