import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import CloseFormIconButton from '../../../../base-components/CloseFormIconButton/CloseFormIconButton'
import {submit} from 'redux-form';
import SignUpForm from '../../components/SignUpForm/SignUpForm';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';
import {signUpRequest} from '../../actions';
import {modalTypes, signUpModalSteps} from '../../../ui/constants';
import {showModal} from '../../../ui/actions';

import styles from './SignUpModal.module.scss';

const style = {
    content: {
        overflow: 'initial',
        padding: 0
    },
    caption: { padding: '24px 0 0' },
    modal: {
        overflow: 'auto',
        textAlign: 'center',
        color: '#fff',
    }
};

class SignUpModal extends React.Component {
    constructor() {
        super();

        this.state = {
            isSignUpFormValid: false,
            isSignUpFormSubmitting: false
        };
    }

    handleSignUpFormValidChange = (isValid) => {
        this.setState({
            isSignUpFormValid: isValid
        })
    };

    handleSignUpFormSubmittingChange = (isSubmitting) => {
        this.setState({
            isSignUpFormSubmitting: isSubmitting
        })
    };

    handleSignUpFormSubmit = (values) => {
        const {signUp, redirect} = this.props;
        return new Promise((resolve, reject) => {
            signUp({credentials: values, resolve, reject, redirect});
        });
    };

    handleSignInLinkClick = () => {
        const {nextModal, showSignInModal} = this.props;
        if (nextModal) {
            showSignInModal({nextModal});
        } else {
            showSignInModal();
        }
    };

    render() {
        const {isOpen, onHide, submitSignUpForm, currentStep, user, classes} = this.props;

        let dialogueTitle, dialogueContent, dialogueActions;

        switch (currentStep) {
            case signUpModalSteps.SIGN_UP_FORM:
                dialogueTitle = <DialogTitle classes={{ root: classes.caption}}>
                    <CloseFormIconButton onClick={onHide}/>
                    <p className={styles.caption}>Sign up now <br/> and get <span className={styles.count}>10</span> fan points!</p>
                </DialogTitle>;

                dialogueContent = <DialogContent
                    classes={{
                        root: classes.content
                    }}
                >
                    <SignUpForm
                        onFormValidChange={this.handleSignUpFormValidChange}
                        onFormSubmittingChange={this.handleSignUpFormSubmittingChange}
                        onSubmit={this.handleSignUpFormSubmit}
                    />
                    {
                        this.state.isSignUpFormSubmitting
                        && <SpinnerWithBackdrop />
                    }
                </DialogContent>;

                dialogueActions = <div className={styles.modalFooter}>
                    <button
                        className={styles.submitButton}
                        onClick={submitSignUpForm}
                        disabled={!this.state.isSignUpFormValid}
                    >
                        Sign up
                    </button>
                    <button onClick={onHide} className={styles.cancelButton}>Cancel</button>
                    <div className={styles.signInLinkContainer}>Already a member? <span className={styles.signInLink} onClick={this.handleSignInLinkClick}>Sign in.</span></div>
                </div>;
                break;

            case signUpModalSteps.CONFIRMATION_EMAIL_MESSAGE:
                dialogueTitle = <DialogTitle className={styles.confirmationTitle}>
                    Thanks for registering. Email with confirmation link has been sent to {user && user.email}. Please
                    check spam folder
                </DialogTitle>;

                dialogueActions = <DialogActions>
                    <button
                        className={styles.submitButton}
                        onClick={onHide}>Ok</button>
                </DialogActions>
        }
        const modalStyles = cn([styles.modal, {
                [styles.confirmationContainer]: currentStep === signUpModalSteps.CONFIRMATION_EMAIL_MESSAGE
            }
        ]);
        return (
            <Dialog
                className={modalStyles}
                disableBackdropClick
                disableEscapeKeyDown
                open={isOpen}
                fullWidth
                fullScreen
                classes={{
                    root: classes.modal
                }}
            >
                {dialogueTitle}
                {dialogueContent}
                {dialogueActions}
            </Dialog>
        )
    }
}

SignUpModal.propTypes = {
    isOpen: PropTypes.bool,
    onHide: PropTypes.func,
};

SignUpModal.defaultProps = {
    isOpen: true,
    onHide: () => window.location = '/',
    redirect: true
};

export default withStyles(style)(connect(
    (state) => ({
        currentStep: state.ui.signUpModalStep,
        user: state.auth.user
    }),
    (dispatch) => ({
        submitSignUpForm: () => dispatch(submit('signUpForm')),
        signUp: (payload) => dispatch(signUpRequest(payload)),
        showSignInModal: (modalProps) => dispatch(showModal(modalTypes.SIGN_IN_MODAL, modalProps))
    })
)(SignUpModal));

