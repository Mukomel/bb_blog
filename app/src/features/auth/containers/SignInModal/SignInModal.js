import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import CloseFormIconButton from '../../../../base-components/CloseFormIconButton/CloseFormIconButton'
import {submit} from 'redux-form';
import { withStyles } from '@material-ui/core/styles';
import SignInForm from '../../components/SignInForm/SignInForm';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';
import {signInRequest} from '../../actions';
import {showModal} from '../../../ui/actions';
import {modalTypes} from '../../../ui/constants';

import styles from './SignInModal.module.scss';

const style = {
    content: {
        padding: 0,
        overflow: 'initial'
    },
    caption: { padding: '24px 0 0' },
    modal: {
        textAlign: 'center',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
};

class SignInModal extends React.Component {
    constructor() {
        super();

        this.state = {
            isSignInFormValid: false,
            isSignInFormSubmitting: false
        };
    }

    handleSignInFormValidChange = (isValid) => {
        this.setState({
            isSignInFormValid: isValid
        })
    };

    handleSignInFormSubmittingChange = (isSubmitting) => {
        this.setState({
            isSignInFormSubmitting: isSubmitting
        })
    };

    handleSignInFormSubmit = (values) => {
        const {signIn, redirect} = this.props;
        return new Promise((resolve, reject) => {
            signIn({credentials: values, resolve, reject, redirect});
        });
    };

    handleForgotPasswordClick = () => {
        const {showForgotPasswordModal} = this.props;

        showForgotPasswordModal();
    };

    handleSignUpLinkClick = () => {
        const {nextModal, showSignUpModal} = this.props;
        if (nextModal) {
            showSignUpModal({nextModal});
        } else {
            showSignUpModal();
        }
    };

    render() {
        const {isOpen, onHide, submitSignInForm, classes} = this.props;

        return <Dialog
            className={styles.modal}
            disableBackdropClick
            disableEscapeKeyDown
            open={isOpen}
            fullWidth
            fullScreen
            classes={{
                root: classes.modal
            }}
        >
            <CloseFormIconButton onClick={onHide}/>
            <DialogTitle classes={{ root: classes.caption }}>
                <p className={styles.caption}>Sign in</p>
            </DialogTitle>
            <DialogContent classes={{ root: classes.content }}>
                <SignInForm
                    onFormValidChange={this.handleSignInFormValidChange}
                    onFormSubmittingChange={this.handleSignInFormSubmittingChange}
                    onSubmit={this.handleSignInFormSubmit}
                />
                <p className={styles.forgetPasswordLinkContainer}>
                    <span className={styles.forgetPasswordLink} onClick={this.handleForgotPasswordClick}>Forgot your password?</span>
                </p>
                {
                    this.state.isSignInFormSubmitting
                        && <SpinnerWithBackdrop />
                }
            </DialogContent>
            <div className={styles.modalFooter}>
                <button
                    className={styles.submitButton}
                    onClick={submitSignInForm}
                    disabled={!this.state.isSignInFormValid}>
                    Sign in
                </button>
                <button
                    className={styles.cancelButton}
                    onClick={onHide}
                >
                    Cancel
                </button>
                <div className={styles.signUpLinkContainer}>Not a member yet? <span className={styles.signUpLink} onClick={this.handleSignUpLinkClick}>Sign up.</span></div>

            </div>
        </Dialog>
    }
}

SignInModal.propTypes = {
    isOpen: PropTypes.bool,
    onHide: PropTypes.func,
};

SignInModal.defaultProps = {
    isOpen: true,
    onHide: () => window.location = '/',
    redirect: true
};

export default withStyles(style)(connect(
    (state) => ({}),
    (dispatch) => ({
        submitSignInForm: () => dispatch(submit('signInForm')),
        signIn: (payload) => dispatch(signInRequest(payload)),
        showForgotPasswordModal: () => dispatch(showModal(modalTypes.FORGOT_PASSWORD_MODAL)),
        showSignUpModal: (modalProps) => dispatch(showModal(modalTypes.SIGN_UP_MODAL, modalProps))
    })
)(SignInModal));
