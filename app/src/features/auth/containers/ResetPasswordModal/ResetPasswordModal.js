import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import {submit} from 'redux-form';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';
import {resetPasswordModalStep} from '../../../ui/constants';
import {resetPasswordRequest} from '../../actions'
import ResetPasswordForm from '../../components/ResetPasswordForm/ResetPasswordForm';

import styles from './ResetPasswordModal.module.scss';

class ResetPasswordModal extends React.Component {
    constructor() {
        super();

        this.state = {
            isResetPasswordFormValid: false,
            isResetPasswordFormSubmitting: false
        };
    }

    handleResetPasswordFormValidChange = (isValid) => {
        this.setState({
            isResetPasswordFormValid: isValid
        })
    };

    handleResetPasswordFormSubmittingChange = (isSubmitting) => {
        this.setState({
            isResetPasswordFormSubmitting: isSubmitting
        })
    };

    handleResetPasswordFormSubmit = (values) => {
        const {resetPassword, resetPasswordToken} = this.props;

        return new Promise((resolve, reject) => {
            resetPassword({password: values.password, resetPasswordToken, resolve, reject});
        });
    };

    handleSignInClick = () => {
        this.props.history.push('/signin');
    };

    render() {
        const {isOpen, currentStep, submitResetPasswordForm} = this.props;

        let dialogueTitle, dialogueContent, dialogueActions;

        switch (currentStep) {

            case resetPasswordModalStep.PASSWORD_FORM:
                dialogueTitle = <DialogTitle>Reset a Password</DialogTitle>;
                dialogueContent = <DialogContent>
                    <ResetPasswordForm
                        onSubmit={this.handleResetPasswordFormSubmit}
                        onFormValidChange={this.handleResetPasswordFormValidChange}
                        onFormSubmittingChange={this.handleResetPasswordFormSubmittingChange}
                    />
                    {
                        (this.state.isResetPasswordFormSubmitting)
                            && <SpinnerWithBackdrop />
                    }
                </DialogContent>;
                dialogueActions = <DialogActions>
                    <Button
                        onClick={submitResetPasswordForm}
                        variant="raised"
                        color="primary"
                        disabled={!this.state.isResetPasswordFormValid}
                    >
                        Submit
                    </Button>
                </DialogActions>;
                break;

            case resetPasswordModalStep.CONGRATULATIONS:
                dialogueTitle = <DialogTitle>Congratulations!</DialogTitle>;
                dialogueContent = <DialogContent>
                    <div className={styles.congratulationText}>Your password has been successfully changed. Please sign in <span className={styles.signInLink} onClick={this.handleSignInClick}>here</span></div>
                </DialogContent>;
                break;
        }

        return <Dialog
            disableBackdropClick
            disableEscapeKeyDown
            open={isOpen}
            fullWidth={true}
        >
            {dialogueTitle}
            {dialogueContent}
            {dialogueActions}
        </Dialog>
    }
}

ResetPasswordModal.propTypes = {
    isOpen: PropTypes.bool,
    onHide: PropTypes.func,
    resetPasswordToken: PropTypes.string,
};

ResetPasswordModal.defaultProps = {
    isOpen: true,
    onHide: () => window.location = '/'
};

export default connect(
    (state, ownProps) => ({
        resetPasswordToken: ownProps.match.params.reset_password_token,
        currentStep: state.ui.resetPasswordModalStep
    }),
    (dispatch) => ({
        resetPassword: (payload) => dispatch(resetPasswordRequest(payload)),
        submitResetPasswordForm: () => dispatch(submit('resetPasswordForm')),
    })
)(ResetPasswordModal);
