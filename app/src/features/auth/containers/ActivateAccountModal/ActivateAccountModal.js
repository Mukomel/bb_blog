import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';
import {activateAccountRequest, getUserRequest} from '../../actions'

import styles from './ActivateAccountModal.module.scss';

class ActivateAccountModal extends React.Component {

    handleOkClick = () => {
        const {isLoggedIn, history, getUser} = this.props;
        if (isLoggedIn) {
            getUser();
        }
        history.push('/');
    };

    componentWillMount = () => {
        const {activationToken, activateAccount} = this.props;
        activateAccount(activationToken);
    };

    render() {
        const {isOpen, isFetchingUser, isRequestingAccountActivation, accountActivationError} = this.props;

        return <Dialog
            disableBackdropClick
            disableEscapeKeyDown
            open={isOpen}
            fullWidth={true}
        >
            {
                (isRequestingAccountActivation || isFetchingUser)
                    ? <DialogContent><SpinnerWithBackdrop/></DialogContent>
                    : (
                        accountActivationError
                            ? <div>
                                <DialogContent><div className={styles.activationError}>{accountActivationError}</div></DialogContent>
                            </div>
                            : <div>
                                <DialogTitle>Success! Your email was confirmed</DialogTitle>
                                <DialogActions>
                                    <Button
                                        onClick={this.handleOkClick}
                                        variant="raised"
                                        color="primary"
                                    >
                                        Ok
                                    </Button>
                                </DialogActions>
                            </div>
                    )
            }
        </Dialog>
    }
}

ActivateAccountModal.propTypes = {
    isOpen: PropTypes.bool,
    onHide: PropTypes.func,
    activationToken: PropTypes.string,
};

ActivateAccountModal.defaultProps = {
    isOpen: true,
    onHide: () => window.location = '/'
};

export default connect(
    (state, ownProps) => ({
        activationToken: ownProps.match.params.activation_token,
        isRequestingAccountActivation: state.auth.isRequestingAccountActivation,
        accountActivationError: state.auth.accountActivationError,
        isFetchingUser: state.auth.isFetchingUser,
        isLoggedIn: state.auth.isLoggedIn
    }),
    (dispatch) => ({
        getUser: () => dispatch(getUserRequest()),
        activateAccount: (activationToken) => dispatch(activateAccountRequest(activationToken))
    })
)(ActivateAccountModal);
