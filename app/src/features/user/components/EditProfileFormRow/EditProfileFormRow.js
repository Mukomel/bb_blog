import React from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';

import styles from './EditProfileFormRow.module.scss';

const EditProfileFormRow = ({className, title, field}) => (
    <div className={cn(styles.container, className)}>
        <div className={styles.title}>{title}:</div>
        <div className={styles.fieldContainer}>{field}</div>
    </div>
);

EditProfileFormRow.propTypes = {
    className: PropTypes.string,
    title: PropTypes.string.isRequired,
    field: PropTypes.node.isRequired
};

export default EditProfileFormRow;