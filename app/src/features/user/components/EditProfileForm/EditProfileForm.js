import React from 'react';
import PropTypes from 'prop-types';
import {Field, reduxForm} from 'redux-form';
import {TextField, Checkbox} from 'redux-form-material-ui';
import FormControlLabel  from '@material-ui/core/FormControlLabel'
import FormTextField from '../../../../base-components/FormTextField/FormTextField';
import ImageLoaderZone from '../../../../base-components/ImageLoaderZone/ImageLoaderZone';
import Switch from '../../../../base-components/Switch/Switch';
import isValidEmail from '../../../../helpers/isValidEmail';
import config from '../../../../config';
import {IMAGE_SIZES} from "../../../artist/constants";

import * as styles from './EditProfileForm.module.scss';

class EditProfileForm extends React.Component {

    static propTypes = {
        onSubmit: PropTypes.func.isRequired,
    };

    handleFormSubmit = (values) => {
        const {onSubmit} = this.props;
        return onSubmit(values);
    };

    renderSwitch = ({input, label, meta: {error, valid}}) => (
        <FormControlLabel
            className={styles.receiveEmailsCheckboxContainer}
            control={
                <Switch
                    defaultChecked={!!input.value}
                    onSwitch={input.onChange}
                />
            }
            label={label}
        />
    );

    render() {
        const {handleSubmit, showAvatarField, showBiographyField, showWishlistField} = this.props;
        return (
            <form className={styles.form} onSubmit={handleSubmit(this.handleFormSubmit)}>
                <FormTextField
                    className={styles.field}
                    name='username'
                    label='Username'
                    component={TextField}
                    fullWidth
                    margin='dense'
                    helperText=' '
                />
                <FormTextField
                    className={styles.field}
                    name='email'
                    label='E-mail'
                    component={TextField}
                    fullWidth
                    margin='dense'
                    helperText=' '
                    type='email'
                />
                <FormTextField
                    className={styles.field}
                    name='snapchat_username'
                    label='Snapchat Username'
                    component={TextField}
                    fullWidth
                    margin='dense'
                    helperText=' '
                />
                <Field
                    name='receive_emails'
                    component={this.renderSwitch}
                    label="Receive updates by email"
                />
                {/*<Field
                    name='receive_emails'
                    component={Switch}
                    onSwitch={() => {}}
                />*/}
                {
                    showAvatarField
                        && <Field
                            name='avatar'
                            component={ImageLoaderZone}
                            aspectRatio={1}
                            circled
                            sizes={IMAGE_SIZES.avatar}
                            accept={config.acceptedImageMIMETypes}
                            selectFileLabel='Select avatar'
                            changeFileLabel='Change avatar'
                        />
                }
                {
                    showBiographyField
                        && <FormTextField
                            className={styles.field}
                            name='biography'
                            label='Biography'
                            component={TextField}
                            fullWidth
                            margin='dense'
                            helperText=' '
                        />
                }
                {
                    showWishlistField
                        && <FormTextField
                            className={styles.field}
                            name='wishlist_link'
                            label='Wishlist link'
                            component={TextField}
                            fullWidth
                            margin='dense'
                            helperText=' '
                        />
                }
            </form>
        )
    }
}

export default reduxForm({
    form: 'editProfileForm',
    enableReinitialize: true,
    validate: (values) => {
        const errors = {};

        if (!isValidEmail(values.email)) {
            errors.email = 'Email is not valid'
        }

        if(!values.email) {
            errors.email = 'Required'
        }

        if (!values.username) {
            errors.username = 'Required'
        }

        if (values.username === values.email) {
            errors.username = 'Username cannot be same as email'
        }

        if (values.username === values.snapchat_username) {
            errors.username = 'Username cannot be same as snapchat username'
        }

        return errors;
    }
})(EditProfileForm)
