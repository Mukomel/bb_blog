import React from 'react';
import {func, object, string, bool} from 'prop-types';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import * as styles from './UserProfileSnackbar.module.scss';

const UserProfileSnackbar = ({isOpen, label, snackbarPosition, onClose}) => (
    <Snackbar
        className={styles.notActivatedMessage}
        anchorOrigin={snackbarPosition}
        open={isOpen}
        autoHideDuration={null}
        onClose={onClose}
        ContentProps={{'aria-describedby': 'message-id'}}
        message={<span id="message-id">{label}</span>}
        action={[
            <IconButton
                key="close"
                aria-label="Close"
                color="inherit"
                onClick={onClose}
            >
                <CloseIcon />
            </IconButton>,
        ]}
    />
);

UserProfileSnackbar.propTypes = {
    label: string.isRequired,
    label: bool.isRequired,
    snackbarPosition: object.isRequired,
    onClose: func.isRequired,
};

export default UserProfileSnackbar
