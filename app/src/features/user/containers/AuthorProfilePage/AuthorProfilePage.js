import React from 'react';
import {connect} from 'react-redux';
import {reset, submit} from 'redux-form';
import Paper from '@material-ui/core/Paper';
import Button from '../../../../base-components/Button/Button';
import EditProfileForm from '../../components/EditProfileForm/EditProfileForm';
import {updateUserRequest} from '../../../auth/actions';

import styles from './AuthorProfilePage.module.scss';

class AuthorProfilePage extends React.Component {
    handleEditProfileFormSubmit = (values) => {
        const {updateUser} = this.props;
        return new Promise((resolve, reject) => {
            updateUser({values, resolve, reject});
        })
    };

    render() {
        const {user, submitEditProfileForm} = this.props;

        const profileFormInitialValues = {
            email: user.email,
            username: user.username,
            snapchat_username: user.snapchat_username,
            wishlist_link: user.wishlist_link,
            biography: user.biography,
            receive_emails: user.receive_emails
        };

        return (
            <div className={styles.container}>
                <Paper className={styles.authorDetailsContainer}>
                    <div className={styles.title}>Profile details</div>
                    <EditProfileForm
                        initialValues={profileFormInitialValues}
                        showAvatarField={true}
                        showBiographyField={true}
                        showWishlistField={true}
                        onSubmit={this.handleEditProfileFormSubmit}
                    />
                    <Button
                        className={styles.submitButton}
                        onClick={submitEditProfileForm}
                        variant="default"
                    >
                        Update Profile
                    </Button>
                </Paper>
            </div>
        )
    }
}

export default connect(
    (state) => ({
        user: state.auth.user
    }),
    (dispatch) => ({
        updateUser: (payload) => dispatch(updateUserRequest(payload)),
        resetEditProfileForm: () => dispatch(reset('editProfileForm')),
        submitEditProfileForm: () => dispatch(submit('editProfileForm'))
    })
)(AuthorProfilePage);

