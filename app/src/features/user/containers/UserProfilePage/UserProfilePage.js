import React, { Component } from 'react';
import {connect} from 'react-redux';
import {submit} from 'redux-form';
import Paper from '@material-ui/core/Paper';
import Input from '@material-ui/core/Input';
import Header from '../../../landing/components/Layout/Header';
import Footer from '../../../landing/components/Layout/Footer';
import Button from '../../../../base-components/Button/Button';
import Snackbar from '../../components/UserProfileSnackbar/UserProfileSnackbar'
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';
import BackgroundImage from '../../../../base-components/BackgroundImage/BackgroundImage';
import UserDescription from '../../../../base-components/UserDescription/UserDescription';
import {cancelSubscriptionRequest, deleteUserAccountRequest, getSegpayUserSubscriptionsRequest} from '../../../subscribe/actions';
import {getArtistRequest} from "../../../artist/actions";
import {updateUserRequest, resendActivationLinkRequest} from '../../../auth/actions';
import EditProfileForm from '../../components/EditProfileForm/EditProfileForm';

import * as styles from './UserProfilePage.module.scss';
import SegpayUserSubscriptionsTable
    from "../../../subscribe/components/UserSubscriptionsTable/SegpayUserSubscriptionsTable";
import { layoutTypes } from "../../../ui/constants";

const DELETE_PHRASE = "I want my account to be removed";

class UserProfilePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isSnackbarOpen: !props.user.is_activated,
            deleteAccountTextareaValue: '',
            canceledSubscription: []
        };
    }

    componentDidMount() {
        const {getArtist, artistProfile, getSegpayUserSubscriptions} = this.props;
        if (!artistProfile) {
            getArtist('brandtandnash')
        }
        getSegpayUserSubscriptions();
    };

    handleClose = (event, reason) => this.setState({ isSnackbarOpen: false });

    isDeletePhraseCorrect() {
        return DELETE_PHRASE.toLowerCase() === this.state.deleteAccountTextareaValue.trim().toLowerCase();
    };

    handleTextAreaChange = ({target}) => {
        this.setState({deleteAccountTextareaValue: target.value});
        this.isDeletePhraseCorrect();
    };

    handleDeleteAccount = () => {
        const { deleteUserAccount } = this.props;
        deleteUserAccount();
    };

    handleEditProfileFormSubmit = (values) => {
        return new Promise((resolve, reject) => {
            this.props.updateUser({values, resolve, reject});
        });
    };

    handleAddCancelSubscriptionItem = (checked, subscriptionId) => {
        let canceledSubscriptionCopied = this.state.canceledSubscription.slice();
        if(checked) {
            canceledSubscriptionCopied.push(subscriptionId)
        } else {
            canceledSubscriptionCopied = canceledSubscriptionCopied.filter(id => id !== subscriptionId)
        }
        this.setState({ canceledSubscription: canceledSubscriptionCopied })
    };

    handleCancelSubscription = () => {
        console.log(this.state.canceledSubscription);
        this.state.canceledSubscription.map(subscriptionId => this.props.cancelSubscription(subscriptionId))
    };

    render() {
        const {
            isFetchingUserSubscriptions,
            isRequestingCancelSubscription,
            segpayUserSubscriptionsList,
            isLoggedIn,
            user,
            resendActivationLink,
            isActivationEmailResent,
            submitEditProfileForm,
            artistProfile,
            layoutType
        } = this.props;

        const profileFormInitialValues = {
            email: user.email,
            username: user.username,
            snapchat_username: user.snapchat_username,
            receive_emails: user.receive_emails
        };

        // snackbar for alert of confirm email

        const snackbarPosition = {
            [layoutTypes.MOBILE]: {
                vertical: 'top',
                horizontal: 'center',
            },
            [layoutTypes.DESKTOP]: {
                vertical: 'bottom',
                horizontal: 'right',
            }
        };

        return (
            <div className={styles.container}>
                <Header
                    isOverlay
                    withBackground={Boolean(artistProfile)}
                    backgroundImgUrl={artistProfile && artistProfile.cover}
                    isLoggedIn={isLoggedIn}
                    user={user}/>
                {
                    <BackgroundImage
                        className={styles.cover}
                        url={artistProfile && artistProfile.cover}
                        size='cover'
                        attachment='scroll'
                    />
                }
                <div className={styles.firstSection}>
                    <h3>My Account</h3>
                    <Paper className={styles.paper}>
                        {
                            user.is_banned
                            && <Snackbar
                                isOpen={this.state.isSnackbarOpen}
                                label="Your account was suspended by administration"
                                snackbarPosition={snackbarPosition[layoutType]}
                                onClose={this.handleClose}/>
                        }
                        {
                            !user.is_activated
                            && <div>
                                <Snackbar
                                    isOpen={this.state.isSnackbarOpen}
                                    label="Please confirm your email"
                                    snackbarPosition={snackbarPosition[layoutType]}
                                    onClose={this.handleClose}/>
                                <div>

                                </div>
                                {
                                    isActivationEmailResent
                                        ? <div className={styles.emailLink}>Email was sent. Please check spam folder.</div>
                                        : <div className={styles.emailLink} onClick={resendActivationLink}>Resend the confirmation email to {user.email}</div>
                                }
                            </div>
                        }
                        <div className={styles.accountWrapper}>
                            <UserDescription user={user} className={styles.user}/>
                            <EditProfileForm
                                initialValues={profileFormInitialValues}
                                onSubmit={this.handleEditProfileFormSubmit}
                            />
                            {
                                this.state.isEditProfileFormSubmitting
                                && <SpinnerWithBackdrop />
                            }
                        </div>
                    </Paper>
                    <Button
                        className={styles.submitButton}
                        onClick={submitEditProfileForm}
                        variant="default"
                    >
                        Update Profile
                    </Button>
                </div>

                {
                    segpayUserSubscriptionsList.length > 0 &&
                    <div className={styles.section}>
                        <Paper className={styles.paper}>
                            <h3>My Subscriptions</h3>
                            {
                                <SegpayUserSubscriptionsTable
                                    subscriptionsList={segpayUserSubscriptionsList}
                                    onCancelSubscription={this.handleAddCancelSubscriptionItem}
                                />
                            }
                            {
                                (isFetchingUserSubscriptions || isRequestingCancelSubscription)
                                && <SpinnerWithBackdrop />
                            }
                        </Paper>
                        <Button
                            disabled={!this.state.canceledSubscription.length > 0}
                            onClick={this.handleCancelSubscription}
                            variant="default"
                        >
                            Cancel Subscription
                        </Button>
                    </div>
                }

                <div className={styles.section}>
                    <Paper className={styles.paper}>
                        <h3>Delete My Account</h3>
                        <div className={styles.paperContent}>
                            <p className={styles.label}>
                                If you want to delete your account, please type: "I want my account to be removed". If you will click "Erase my account", all your data will be erased from our system. You will lose all access to the website and all your subscriptions will be canceled. This action is irreversible!</p>
                            <Input
                                className={styles.input}
                                placeholder="Type here"
                                fullwidth
                                inputProps={{
                                    'aria-label': 'Description',
                                }}
                                onChange={this.handleTextAreaChange}
                            />
                        </div>
                    </Paper>
                    <Button
                        disabled={!this.isDeletePhraseCorrect()}
                        onClick={this.handleDeleteAccount}
                        variant="default"
                    >
                        Erase my account
                    </Button>
                </div>
                <Footer/>
            </div>
        )
    }
}

export default connect(
    (state) => ({
        artistProfile: state.artist.artistProfile,
        segpayUserSubscriptionsList: state.subscribe.segpayUserSubscriptionsList,
        isFetchingUserSubscriptions: state.subscribe.isFetchingUserSubscriptions,
        isRequestingCancelSubscription: state.subscribe.isRequestingCancelSubscription,
        user: state.auth.user,
        isLoggedIn: state.auth.isLoggedIn,
        isActivationEmailResent: state.auth.isActivationEmailResent,
        layoutType: state.ui.layoutType,
    }),
    (dispatch) => ({
        submitEditProfileForm: () => dispatch(submit('editProfileForm')),
        getArtist: slug => dispatch(getArtistRequest(slug)),
        getSegpayUserSubscriptions: () => dispatch(getSegpayUserSubscriptionsRequest()),
        cancelSubscription: (subscriptionId) => dispatch(cancelSubscriptionRequest(subscriptionId)),
        deleteUserAccount: () => dispatch(deleteUserAccountRequest()),
        updateUser: (payload) => dispatch(updateUserRequest(payload)),
        resendActivationLink: (payload) => dispatch(resendActivationLinkRequest()),
    })
)(UserProfilePage)
