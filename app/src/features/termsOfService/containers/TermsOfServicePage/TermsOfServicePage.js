import React from 'react';
import Header from '../../../landing/components/Layout/Header';
import { connect } from 'react-redux';
import Footer from '../../../landing/components/Layout/Footer';

import styles from './TermsOfServicePage.module.scss';

const TermsOfServicePage = ({isLoggedIn, user}) => (
    <div className={styles.mainContent}>
        <Header
            user={user}
            isLoggedIn={isLoggedIn}/>
        <div className={styles.container}>
            <p align="center">
                <strong>
                    BrandtsBoys.com
                    <br/>
                    Terms-of-Service Agreement
                </strong>
            </p>
            <p align="center">
                <a name="_GoBack"></a>
                <strong>Last Updated: April 13, 2018</strong>
            </p>
            <p align="justify">
                <strong>
                    This agreement contains disclaimers of warranties, limitations on
                    liability, releases, a class-action waiver, and the requirement to
                    mediate and arbitrate all claims that may arise under this agreement.
                    These provisions are an essential basis of this agreement.
                </strong>
            </p>
            <p align="justify">
                <strong>Section 230(d) Notice: </strong>
                In accordance with <a href="https://www.law.cornell.edu/uscode/text/47/230">
                    47 U.S.C. § 230(d)
                </a>
                , you are notified that parental control protections (including computer
                hardware, software, or filtering services) are commercially available that
                may help in limiting access to material that is harmful to minors. You may
                find information about providers of these protections on the Internet by
                searching “parental control protection” or similar terms.
            </p>
            <p align="justify">
                <strong>Minors Prohibited. </strong>
                The Website may contain, or direct you to websites containing, adult
                oriented content and is not intended for minors. Only adults (1) who are at
                least 18-years old and (2) who have reached the age of majority where they
                live may access the Website. The Company forbids all persons who do not
                meet these age requirements from accessing the Website. If minors have
                access to your computer, please restrain their access to sexually explicit
                material by using any of the following products, which the Company provides
                for informational purposes only and does not endorse: <a href="http://www.cybersitter.com/">CYBERsitter™</a> |<a href="http://www.netnanny.com/">Net Nanny®</a> |<a href="http://www.cyberpatrol.com/">CyberPatrol</a> |    <a href="http://www.asacp.org/">ASACP</a>.
            </p>
            <p align="justify">
                <strong>Child Pornography Prohibited.</strong>
                <strong> </strong>
                The Company prohibits pornographic content involving minors. The Company
                only allows visual media of consenting adults for consenting adults on the
                Website. If you see any visual media, real or simulated, depicting minors
                engaged in sexual activity on the Website, please promptly report this to
                the Company at    <a href="mailto:abuse@brandtsboys.com">abuse@brandtsboys.com</a>. Please
                include with your report all appropriate evidence, including the date and
                time of identification. The Company will promptly investigate all reports
                and take appropriate action. The Company cooperates with any
                law-enforcement agency investigating child pornography.
            </p>
            <ol>
                <li>
                        <strong>Acceptance of Agreement. </strong>
                        This terms-of-service agreement is between DNA Enterprises, LLC, an
                        Iowa limited liability company (the “<strong>Company</strong>”),
                        the owner and operator of            <a href="http://www.BrandtsBoys.com/">www.BrandtsBoys.com</a> (the
                        “<strong>Website</strong>”), and you, a user of the Website. By
                        using the Website, you agree to this agreement. If you choose to
                        not agree to this agreement, you must not use the Website.
                </li>
                <li>
                    <h4>
                        <strong>Changes to Agreement</strong>
                    </h4>
                    <ol>
                        <li>
                                <strong>Right to Change Agreement.</strong>
                                The Company may change this agreement (the “                    <strong>Updated Agreement</strong>”) on one or more
                                occasions.
                        </li>
                        <li>
                                <strong>Notice of Updated Agreement. </strong>
                                Unless the Company makes a change for legal or
                                administrative reasons, the Company will provide reasonable
                                advance notice before the Updated Agreement becomes
                                effective. The Company may notify you of the Updated
                                Agreement by posting it on the Website.
                        </li>
                        <li>
                                <strong>Acceptance of Updated Agreement. </strong>
                                Your use of the Website after the effective date of the
                                Updated Agreement constitutes your agreement to the Updated
                                Agreement. You should review this agreement and any Updated
                                Agreement before using the Website. If you do not agree to
                                the Updated Agreement, your sole remedy is to stop using
                                the Website.
                        </li>
                        <li>
                                <strong>Effective Date of Updated Agreement.</strong>
                                The Updated Agreement will be effective as of the time of
                                posting, or that later date as may be stated in the Updated
                                Agreement, and will apply to your use of the Website from
                                that point forward, except that the Updated Agreement will
                                not apply to continuing disputes or disputes arising out of
                                or relating to events happening before the Updated
                                Agreement’s effective date.
                        </li>
                    </ol>
                </li>
                <li>
                        <a name="_Hlk492634916"></a>
                        <strong>Adult-Oriented Content. </strong>
                        The Website contains uncensored sexually explicit material
                        unsuitable for minors. Only adults (1) who are at least 18-years
                        old and (2) who have reached the age of majority where they live
                        may access the Website.
                        <strong>
                            If you do not meet these age requirements, you must not access
                            the Website and
                        </strong>
                        <a href="http://www.google.com/">
                            <strong>must leave now</strong>
                        </a>
                        . By accessing the Website, you state that the following facts are
                        accurate:
                    <ol>
                        <li>
                                You are at least 18-years old and have reached the age of
                                majority where you live;
                        </li>
                        <li>
                                You are aware of the adult nature of the content available
                                on the Website, and you are not offended by visual images,
                                verbal descriptions, and audio sounds of a sexual nature,
                                which may include graphic visual depictions and
                                descriptions of nudity and sexual activity;
                        </li>
                        <li>
                                You are familiar with your community’s laws affecting your
                                right to access adult-oriented materials, including
                                sexually explicit material depicting bondage, S/M, and
                                other fetish activities;
                        </li>
                        <li>
                                You have the legal right to access adult-oriented
                                materials, including sexually explicit material depicting
                                bondage, S/M, and other fetish activities, and the Company
                                has the legal right to transmit them to you;
                        </li>
                        <li>
                                You are voluntarily requesting adult-oriented materials for
                                your own private enjoyment;
                        </li>
                        <li>
                                You are not accessing the Website from a place, country, or
                                location in which doing so would, or could be considered a
                                violation of law; and
                        </li>
                        <li>
                                You will not<strong> </strong>share these materials with a
                                minor or otherwise make them available to a minor.
                        </li>
                    </ol>
                </li>
                <li>
                    <h4>
                        <strong>Your Account</strong>
                    </h4>
                    <ol>
                        <li>
                                <strong>Account Creation.</strong>
                                You must complete the registration process by providing the
                                Company with accurate information as prompted by the
                                applicable registration form. You also will choose a
                                password and a username. By creating an account, you state
                                that (a) all account registration, and profile information
                                you provide is your own and is accurate; (b) if you
                                previously had an account on the Website, your old account
                                was not terminated or suspended by the Company for
                                violation of this agreement; and (c) your creating an
                                account for your own personal use and you will not sell,
                                rent, or transfer your account to any third party.
                        </li>
                        <li>
                                <strong>Responsibility for Account. </strong>
                                You are responsible for keeping your password and account
                                confidential. Further, you are responsible for all
                                activities that occur under your account. You must notify
                                the Company promptly of any unauthorized use of your
                                account or any other security breach.
                        </li>
                        <li>
                                <strong>Liability for Account Misuse. </strong>
                                The Company will not be liable for any loss that you may
                                incur as a result of someone else using your password or
                                account, either with or without your knowledge. You could
                                be held liable for losses incurred by the Company or
                                another party due to someone else using your account or
                                password.
                        </li>
                        <li>
                                <strong>Use of Other Accounts.</strong>
                                You must not use anyone else’s account at any time.
                        </li>
                        <li>
                                <strong>Account Security. </strong>
                                The Company cares about the integrity and security of your
                                personal information. But the Company cannot guarantee that
                                unauthorized third parties will never be able to defeat the
                                Website’s security measures or use any personal information
                                you provide to the Company for improper purposes. You
                                acknowledge that you provide your personal information at
                                your own risk.
                        </li>
                        <li>
                                <strong>Consent to Electronic Communications. </strong>
                                By registering for an account, you consent to receive
                                electronic communications from the Company relating to your
                                account. These communications may involve sending emails to
                                your email address provided during registration or posting
                                communications on the Website and will include notices
                                about your account (e.g., change in password, confirmation
                                emails, and other transactional information) and are part
                                of your relationship with the Company. You acknowledge that
                                any notices, agreements, disclosures, or other
                                communications that the Company sends to you electronically
                                will satisfy any legal communication requirements,
                                including that these communications be in writing. The
                                Company recommends that you keep copies of electronic
                                communications by printing a paper copy or saving an
                                electronic copy. You also consent to receive certain other
                                communications from the Company, including newsletters
                                about new features and content, special offers, promotional
                                announcements, and customer surveys via email or other
                                methods. You acknowledge that communications you receive
                                from the Company may contain sexually-explicit material
                                unsuitable for minors. If you wish to withdraw your consent
                                to receiving certain non-transactional communications from
                                the Company at any time, you may withdraw your consent in
                                the manner stated in the Privacy Policy.
                        </li>
                    </ol>
                </li>
                <li>
                    <h4>
                        <strong>Use of Website</strong>
                    </h4>
                    <ol>
                        <li>
                                <strong>License.</strong>
                                During this agreement, the Company hereby grants you a
                                limited, nonexclusive, nontransferable license to access
                                the Website and its content for your personal and
                                noncommercial use in accordance with this agreement. For
                                purposes of this agreement, the following definitions
                                apply,
                            <ol type="a">
                                <li>
                                    
                                        “<strong>Access</strong>” means visit the Website,
                                        use its services, and view or download its content.
                                    
                                </li>
                                <li>
                                    
                                        “<strong>Content</strong>” means any material,
                                        including the text, software, scripts, graphics,
                                        photos, sounds, music, videos, audiovisual
                                        combinations, interactive features, communications,
                                        profiles, streams, data, and other materials found
                                        on the Website.
                                    
                                </li>
                                <li>
                                    
                                        “<strong>Personal and noncommercial use</strong>”
                                        means a presentation of the content for which no
                                        fee or consideration is charged or received, which
                                        takes place in your private residence or, if
                                        outside your residence, is limited to a private
                                        viewing by you. Personal and noncommercial use
                                        excludes any public or private event presentation
                                        even if no fee is charged.
                                    
                                </li>
                            </ol>
                        </li>
                        <li>
                            
                                <strong>Intellectual Property Rights</strong>
                            
                            <ol type="a">
                                <li>
                                    
                                        <strong>Website Ownership. </strong>
                                        Other than user content, the Website and its entire
                                        contents, features, and functionality (including
                                        all information, software, text, displays, images,
                                        video, audio, and audiovisual combinations and the
                                        design, selection, and arrangement of it) are owned
                                        by the Company, its licensors, or other providers
                                        of the material and are protected by United States
                                        and international copyright, trademark, patent,
                                        trade secret, and other intellectual property or
                                        proprietary rights laws. The Company reserves all
                                        rights not expressly granted in and to the Website.
                                        You will not engage in the use, copying, or
                                        distribution of any of the Website other than as
                                        expressly permitted.
                                    
                                </li>
                                <li>
                                    
                                        <strong>Trademarks. </strong>
                                        The Company’s name, logos, domain names, and the
                                        terms BRANDT’S BOYS and BRANDTS BOYS are the
                                        Company’s trademarks, and must not be copied,
                                        imitated, or used, in whole or in part, without the
                                        Company’s advance written permission. In addition,
                                        all page headers, custom graphics, button icons,
                                        and scripts are the Company’s service marks,
                                        trademarks, and trade dress, and must not be
                                        copied, imitated, or used, in whole or in part,
                                        without the Company’s advance written permission.
                                        Other names of actual companies, products, or
                                        services mentioned on the Website may be the
                                        trademarks of their respective owners and reference
                                        to them does not suggest sponsorship, endorsement,
                                        or association by or with the Company, or that
                                        those owners endorse or have any affiliation with
                                        the Website. Nothing contained on the Website
                                        should be construed as granting, by implication or
                                        otherwise, any license or right to use any marks
                                        displayed on the Website, meta tags, or any other
                                        “hidden text” using marks that belong to the
                                        Company and its licensors, without advanced written
                                        permission from the Company or the third party who
                                        may own the mark.
                                    
                                </li>
                            </ol>
                        </li>
                        <li>
                            
                                <strong>User Conduct</strong>
                            
                            <ol type="a">
                                <li>
                                    
                                        You must not engage in any of the following
                                        prohibited activities while using the Website:
                                    
                                    <ol type="i">
                                        <li>
                                            <h4>
                                                copying, distributing, or disclosing any
                                                part of the Website in any medium,
                                                including by any automated or nonautomated
                                                “scraping;”
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                using any automated system, including
                                                “robots,” “spiders,” “offline readers,”
                                                etc., to access the Website;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                transmitting spam, chain letters, or other
                                                unsolicited email;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                attempting to interfere with, compromise
                                                the system integrity or security, or
                                                decipher any transmissions to or from the
                                                servers running the Website;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                taking any action that imposes, or may
                                                impose at the Company’s sole discretion an
                                                unreasonable or disproportionately large
                                                load on the Website infrastructure;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                uploading invalid data, viruses, worms, or
                                                other software agents through the Website;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                collecting or harvesting any personally
                                                identifiable information, including account
                                                names, from the Website;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                using the Website for any commercial
                                                solicitation purposes;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                impersonating another person or otherwise
                                                misrepresenting your affiliation with a
                                                person or entity, conducting fraud, hiding
                                                or attempting to hide your identity;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                interfering with the proper working of the
                                                Website;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                accessing any content on the Website
                                                through any technology or means other than
                                                those provided or authorized by the
                                                Website; or
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                bypassing the measures that the Company may
                                                use to prevent or restrict access to the
                                                Website, including features that prevent or
                                                restrict use or copying of any content or
                                                enforce limitations on use of the service
                                                or the content in it.
                                            </h4>
                                        </li>
                                    </ol>
                                </li>
                                <li>
                                    
                                        You must not do any of the following while using
                                        the Website’s interactive features:
                                    
                                    <ol type="i">
                                        <li>
                                            <h4>
                                                Harass, stalk, threaten, embarrass, or
                                                cause distress or discomfort to the model;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                Use language that could be considered
                                                offensive or likely to harass, upset,
                                                intimidate, embarrass, alarm, or annoy;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                Transmit any information, data, text,
                                                files, links, software, communication, or
                                                other materials that a reasonable person
                                                would consider to be unlawful, harmful,
                                                threatening, abusive, harassing,
                                                defamatory, libelous, slanderous, vulgar,
                                                obscene, hateful, or racially, ethnically,
                                                or otherwise objectionable;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                Create, upload, post, display, publish, or
                                                distribute any content that violates
                                                another’s copyright, trademark, right of
                                                privacy, right of publicity, or other
                                                property or personal right (for example,
                                                using the name, likeness, image, or other
                                                identity of another without proper
                                                consent);
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                Distribute messages between you and any
                                                model;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                Record, rebroadcast, or distribute any
                                                video or other content found on the
                                                Website;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                Post private or personal information about
                                                any person;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                Request personal information from or share
                                                your personal information with any model,
                                                including financial information, email
                                                address, telephone number, or mailing
                                                address;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                Request money from, or otherwise defraud, a
                                                model;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                Impersonate any person;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                Discuss or engage in any activity that may
                                                be considered obscene in your community or
                                                may be generally considered obscene
                                                worldwide, including pedophilia, the
                                                exploitation of children, age-play, incest,
                                                rape, extreme violence, genital mutilation,
                                                sadomasochistic abuse or bondage, torture,
                                                bestiality, necrophilia, urination,
                                                defecation, “going to the bathroom,” enema
                                                play, vomiting, menstrual bleeding, or
                                                paraphilia;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                Offer money or other consideration in
                                                exchange for sex;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                Promote or advertise prostitution or escort
                                                services;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                Promote or advertise firearms or other
                                                weapons, tobacco, drugs, or drug
                                                paraphernalia;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                Discuss or promote any illegal activity
                                                (including posting links to other websites
                                                that deal with illegal activities), or
                                                advocate, promote, or assist any unlawful
                                                act;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                Post or transmit any unsolicited
                                                advertising, promotional materials, or
                                                other forms of solicitation through the
                                                Website, including the solicitation of
                                                models for commercial ventures or for
                                                prostitution or escort services;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                Transmit “junk mail,” “chain letters,” or
                                                “spam;” or
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                Engage in antisocial, disruptive, or
                                                destructive behavior, including “bombing,”
                                                “flaming,” “flooding,” “trolling,” and
                                                “griefing” as those terms are commonly
                                                understood and used on the Internet.
                                            </h4>
                                        </li>
                                    </ol>
                                </li>
                            </ol>
                        </li>
                        <li>
                            
                                <a name="_Ref447191283"></a>
                                <strong>Monitoring and Enforcement</strong>
                            
                            <ol type="a">
                                <li>
                                    
                                        The Company may do any of the following:
                                    
                                    <ol type="i">
                                        <li>
                                            <h4>
                                                Remove or refuse to post any submission or
                                                communication for any reason, including
                                                obscene or defamatory material or excessive
                                                length;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                Take any action against any submission or
                                                communication that the Company considers
                                                necessary or appropriate, including if the
                                                Company believes that the submission or
                                                communication breaches this agreement,
                                                infringes any intellectual property right
                                                of any person, threatens the personal
                                                safety of users of the Website or the
                                                public, or could create liability for the
                                                Company;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                Disclose your identity or other information
                                                about you to any person who claims that
                                                your submission or communication violates
                                                their rights, including their
                                                intellectual-property rights or their
                                                privacy rights;
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                Take appropriate legal action, including
                                                referral to law enforcement, for any
                                                illegal or unauthorized use of the Website;
                                                or
                                            </h4>
                                        </li>
                                        <li>
                                            <h4>
                                                Terminate or suspend your access to all or
                                                part of the Website for any reason,
                                                including breach of this agreement.
                                            </h4>
                                        </li>
                                    </ol>
                                </li>
                                <li>
                                    
                                        The Company will fully cooperate with any law
                                        enforcement authorities or court order requesting
                                        or directing the Company to disclose the identity
                                        of or other information about anyone posting any
                                        submission or communication on or through the
                                        Website or otherwise engaging in any alleged
                                        wrongful conduct. You hereby waive any claims you
                                        might have against the Company—including its
                                        affiliates, licensees, and service
                                        providers—resulting from any action taken by the
                                        Company during or because of the Company’s
                                        investigations and from any actions taken as a
                                        consequence of investigations by either the Company
                                        or law enforcement authorities.
                                    
                                </li>
                                <li>
                                    
                                        The Company cannot and does not review all material
                                        before it is posted on the Website and cannot
                                        ensure prompt removal of objectionable material
                                        after it has been posted. You remain solely
                                        responsible for the content of your submissions and
                                        communications. The Company will not be liable for
                                        any action or inaction regarding submissions,
                                        transmissions, communications, or content provided
                                        by any user or third party. The Company will not be
                                        liable to anyone for performance or nonperformance
                                        of the activities described in this section 5.4.
                                        But if you know of any submission or communication
                                        that violates this agreement, please email the
                                        Company at <a href="mailto:abuse@brandtsboys.com">
                                            abuse@brandtsboys.com
                                        </a>
                                        . Please provide as much detail as possible,
                                        including (1) a copy of the objectionable
                                        submission or the location where the Company may
                                        find it, (2) the reason the Company should remove
                                        it, and (3) a statement certifying the accuracy of
                                        the information you provided to the Company.
                                    
                                </li>
                            </ol>
                        </li>
                    </ol>
                </li>
                <li>
                    <h4>
                        <strong>Paid Services</strong>
                    </h4>
                    <ol>
                        <li>
                            
                                <strong>In General.</strong>
                                The Website is a social media platform that lets you
                                subscribe to a model’s profile to view the model’s
                                otherwise restricted photos and videos as well as to
                                interact with that model in exchange for a recurring fee.
                                Prices for any subscription may change at any time, and the
                                Company does not provide price protection or refunds in the
                                event of a price reduction or promotional offering. You may
                                also send the model a tip.
                            
                        </li>
                        <li>
                            
                                <strong>Payment. </strong>
                                You will pay for any subscription that you order. You must
                                have a valid accepted form of payment on file to purchase a
                                subscription. The Company uses third-party payment
                                processors to process all payments. The Company’s
                                third-party payment processor will charge your credit card
                                or other form of payment for the price listed for the
                                relevant subscription, along with any additional amounts
                                relating to applicable taxes, bank fees, and currency
                                fluctuations. The Company’s third-party payment processor
                                stores your payment card information. The Company does not
                                store any payment card information.
                            
                        </li>
                        <li>
                            
                                <strong>Recurring Billing.</strong>
                                If you purchase an automatically renewing subscription, you
                                hereby authorize the Company’s payment processor to charge
                                the payment method on file on the first day of each billing
                                period for the relevant subscription, and if the payment
                                method on file becomes invalid due to an expired credit
                                card or other similar reason and the Company’s payment
                                processor is unable to charge you on the next billing
                                period, the Company may immediately revoke your access to
                                any subscription you have purchased until you update your
                                payment method. If you fail to update your payment method
                                within a reasonable amount of time, the Company may cancel
                                your subscription.
                            
                        </li>
                        <li>
                            
                                <strong>Taxes. </strong>
                                If the Company is required to collect or pay any taxes in
                                connection with your purchase of a subscription, those
                                taxes will be charged to you at the time of each purchase
                                transaction. Additionally, if required by law, you are
                                responsible for reporting and paying certain taxes in
                                connection with your purchase and use of a subscription.
                                These taxes may include duties, customs fees, or other
                                taxes (other than income tax), along with any related
                                penalties or interest, as applicable to your purchase or
                                country of purchase.
                            
                        </li>
                        <li>
                            
                                <strong>Billing Errors. </strong>
                                The Company will correct any mistakes in a charge and add
                                or credit them against your future payments. If you become
                                aware of any errors in a charge, please notify the Company
                                promptly at <a href="mailto:support@brandtboys.com">
                                    support@brandtboys.com
                                </a>
                                . If an error occurs in the billing bank, gateway,
                                processor, or intermediate processor and a transaction is
                                lost, the Company has up to 30 days to work with the
                                billing bank, gateway, processor, or intermediate processor
                                to locate this transaction and solve this issue, including
                                providing credit to your payment method or refunding the
                                transaction. You waive any error unless you notify the
                                Company of the error within three months after you receive
                                the bill in which the error first appears. You hereby
                                release the Company from any liability for any error that
                                you do not report to the Company within three months after
                                you receive the bill in which the error first appeared.
                            
                        </li>
                        <li>
                            
                                <strong>No Refunds. </strong>
                                All sales and transactions are final. Payments are
                                nonrefundable and fully earned on payment. There are no
                                refunds or credits for partially used periods. But the
                                Company may approve a refund in the form of a credit on
                                request if exceptional circumstances exist. The amount and
                                form of a refund and the decision to provide it is at the
                                Company’s sole discretion. The provision of a refund in one
                                instance does not entitle you to a refund in the future for
                                similar instances; nor does it obligate the Company to
                                provide refunds in the future, under any circumstance.
                            
                        </li>
                        <li>
                            
                                <strong>Subscription Cancellations. </strong>
                                If you purchase a subscription to a model’s profile, you
                                may cancel that subscription anytime before the end of the
                                current billing period, and the cancellation will take
                                effect on the next billing period. To cancel a
                                subscription, you may (a) turn off the “Auto-Renew” switch
                                located under the relevant model profile, (b) email the
                                Company at <a href="mailto:support@brandtsboys.com">
                                    support@brandtsboys.com
                                </a>
                                , or (c) contact the third-party payment processor that you
                                signed up through. You retain access to the model’s profile
                                from the time you cancel until the start of the next
                                billing period. You will not receive a refund or credit for
                                any remaining days in your current billing period.
                            
                        </li>
                    </ol>
                </li>
                <li>
                    <h4>
                        <strong>User Content</strong>
                    </h4>
                    <ol>
                        <li>
                            
                                <strong>Content Ownership. </strong>
                                You retain all ownership rights to content uploaded to the
                                Website.
                            
                        </li>
                        <li>
                            
                                <strong>Content License. </strong>
                                By submitting content to the Website, you hereby grant the
                                Company a worldwide, nonexclusive, royalty-free,
                                sublicensable, and transferable license to use, reproduce,
                                distribute, prepare derivative works of, display, and
                                perform the content in connection with the Website and the
                                Company’s (and its successors’ and affiliates’) business,
                                including for promoting and redistributing part or all of
                                the Website (and derivative works of it) in any media
                                formats and through any media channels.
                            
                        </li>
                    </ol>
                </li>
                <li>
                    <h4>
                        <strong>Third-Party Content. </strong>
                        Through the Website, you will have the ability to access or use
                        content provided by third parties. The Company cannot guarantee
                        that that third-party content will be free of material you may find
                        objectionable or otherwise. The Company will not be liable to you
                        for your access or use of any third-party content.
                    </h4>
                </li>
                <li>
                    <h4>
                        <strong>Links to other Websites. </strong>
                        The Website may contain links to third-party websites or resources.
                        You acknowledge that the Company is not responsible or liable for:
                        (1) the availability or accuracy of those websites or resources; or
                        (2) the content, products, or services on or available from those
                        websites or resources. Links to third-party websites or resources
                        do not imply any endorsement by the Company of those websites or
                        resources. You acknowledge sole responsibility for and assume all
                        risk arising from your use of any third-party websites or
                        resources.
                    </h4>
                </li>
                <li>
                    <h4>
                        <strong>No Endorsement. </strong>
                        The Company operates the Website as a neutral host, and the Company
                        does not regularly monitor, regulate, or police the Website’s use
                        by any of its participants. The participation in the Website by a
                        visitor, user, model, studio, or other third party (collectively,
                        the “<strong>participants</strong>”) does not constitute the
                        Company’s endorsement that participant. The Company is not
                        responsible for the acts, omissions, agreements, promises, content,
                        products, or other services, comments, opinions, advice,
                        statements, offers, or information of any participant. Participants
                        are independent parties, and the Company does not, and will not,
                        have any responsibility or liability for the acts, omissions,
                        agreements, promises, comments, opinions, advice, statements, or
                        offers of any participant.
                    </h4>
                </li>
                <li>
                    <h4>
                        <strong>Privacy. </strong>
                        For information about how the Company collects, uses, and shares
                        your information, please review the Privacy Policy. You acknowledge
                        that by using the Website, you consent to the collection, use, and
                        sharing (as stated in the Privacy Policy) of that information
                        (including the transfer of this information to the United States or
                        other countries for the Company’s storage, processing, and use).
                    </h4>
                </li>
                <li>
                    <h4>
                        <strong>Copyright Policy. </strong>
                        The Company respects the intellectual property rights of others and
                        expects the Website’s users to do the same. The Company will
                        respond to notices of alleged copyright infringement that comply
                        with applicable law and are properly provided to the Company. If
                        you believe that your content has been copied in a way that
                        constitutes copyright infringement, please provide the Company’s
                        copyright agent with the following information in accordance with
                        the Digital Millennium Copyright Act (DMCA):
                    </h4>
                    <ol>
                        <li>
                            
                                a physical or electronic signature of the copyright owner
                                or a person authorized to act on their behalf;
                            
                        </li>
                        <li>
                            
                                identification of the copyrighted work claimed to have been
                                infringed;
                            
                        </li>
                        <li>
                            
                                identification of the material that is claimed to be
                                infringing or to be the subject of infringing activity and
                                that is to be removed or access to which is to be disabled,
                                and information reasonably sufficient to permit the Company
                                to locate the material;
                            
                        </li>
                        <li>
                            
                                your contact information, including your address, telephone
                                number, and an email address;
                            
                        </li>
                        <li>
                            
                                a statement by you that you have a good faith belief that
                                use of the material in the manner complained of is not
                                authorized by the copyright owner, its agent, or the law;
                                and
                            
                        </li>
                        <li>
                            
                                a statement that the information in the notification is
                                accurate, and, under penalty of perjury, that you are
                                authorized to act on behalf of the copyright owner.
                            
                        </li>
                    </ol>
                </li>
            </ol>
            
                It is the Company’s policy in appropriate circumstances to disable or
                terminate the user accounts of repeat infringers in accordance with the
                Company’s repeat infringer policy.
            
            <ol start="13">
                <li>
                    <h4>
                        <strong>Termination</strong>
                    </h4>
                    <ol>
                        <li>
                            
                                <strong>Termination on Notice</strong>
                                . Either party may terminate this agreement at any time by
                                notifying the other party.
                            
                        </li>
                        <li>
                            
                                <strong>Termination by the Company</strong>
                                . The Company may terminate or suspend your access to or
                                ability to use the Website immediately, without notice or
                                liability, for any reason or no reason, including breach of
                                this agreement.
                            
                        </li>
                        <li>
                            
                                <strong>Effect of Termination</strong>
                                . On termination of your access to or ability to use the
                                Website, your right to use or access the Website will
                                immediately end. Termination of your access to the Website
                                will not relieve you of any obligations arising or accruing
                                before termination or limit any liability that you
                                otherwise may have to the Company or any third party. You
                                are solely responsible for making sure that any recurring
                                billing is canceled. To cancel a subscription, you may (a)
                                turn off the “Auto-Renew” switch located under the relevant
                                model profile, (b) email the Company at <a href="mailto:support@brandtsboys.com">
                                    support@brandtsboys.com
                                </a>
                                , or (c) contact the third-party payment processor that you
                                signed up through.
                            
                        </li>
                        <li>
                            
                                <strong>Survival. </strong>
                                This agreement’s provisions that by their nature should
                                survive termination will survive termination, including
                                intellectual-property rights, warranty disclaimers, and
                                limitations of liability.
                            
                        </li>
                    </ol>
                </li>
                <li>
                    <h4>
                        <strong>Changes to the Website; Availability</strong>
                    </h4>
                    <ol>
                        <li>
                            
                                Although the Company may update the content on the Website
                                on one or more occasions, the content is not necessarily
                                complete or up-to-date. Any of the material on the Website
                                may be out of date at any given time, and the Company is
                                not required to update that material. If you believe you
                                have found errors or omissions on the Website, you can
                                bring them to the Company’s attention by contacting it at <a href="mailto:support@brandtsboys.com">
                                    support@brandtsboys.com
                                </a>
                                .
                            
                        </li>
                        <li>
                            
                                While the Company will try to make sure that the Website is
                                always available, it does not guarantee continuous,
                                uninterrupted, or secure access to the Website. Many
                                factors or circumstances outside of the Company’s control
                                may interfere with or adversely affect its operation of the
                                Website.
                            
                        </li>
                    </ol>
                </li>
                <li>
                    <h4>
                        <a name="_Ref429683947"></a>
                        <strong>Compliance with Law. </strong>
                        The Website is hosted in the United States. The Company is not
                        making any statement that the Website or any of its content is
                        accessible or appropriate outside of the United States. Access to
                        the Website might not be legal by certain persons or in certain
                        countries. If you access the Website from outside the United
                        States, you do so on your own initiative and are responsible for
                        complying with all local laws. If you access the Website in a
                        jurisdiction that prohibits or restricts its use, the Company will
                        not have any liability to you for your use of the Website.
                    </h4>
                </li>
                <li>
                    <h4>
                        <a name="_Ref428360910"></a>
                        <strong>Acknowledgments and Warranty Disclaimers</strong>
                    </h4>
                    <ol>
                        <li>
                            
                                You acknowledge that the Company cannot and does not state
                                that files available for downloading from the Internet or
                                the Website will be free from loss, corruption, attack,
                                viruses or other destructive code, interference, hacking,
                                or other security intrusions. You are responsible for
                                implementing sufficient procedures and checkpoints to
                                satisfy your particular requirements for antivirus
                                protection and accuracy of data input and output, and for
                                keeping a means external to the Website for any
                                reconstruction of any lost data. The Company will not be
                                liable for any loss or damage caused by a distributed
                                denial-of-service (DDoS) attack, viruses, or other
                                technologically harmful material that might infect your
                                computer equipment, computer programs, data, or other
                                proprietary material due to your use of the Website or any
                                services or items obtained through the Website or to your
                                downloading of any material posted on the Website, or on
                                any website linked to the Website.
                            
                        </li>
                        <li>
                            
                                You acknowledge that the Website includes content provided
                                by third parties, including materials provided by other
                                users, models, studios, third-party licensors, syndicators,
                                or aggregators (“<strong>third-party materials</strong>”),
                                and that the Company does not prescreen or preemptively
                                monitor third-party materials. All statements or opinions
                                expressed in third-party materials, and all responses to
                                questions and other content, other than the content
                                provided by the Company, are solely the opinions and the
                                responsibility of the person providing third-party
                                materials. Third-party materials do not reflect the opinion
                                of the Company. The Company will not be liable to you or
                                any other person for the content or accuracy of any
                                third-party materials. You further acknowledge that you may
                                be exposed to third-party materials that are inaccurate,
                                offensive, indecent, obscene, or otherwise objectionable,
                                and you hereby waive any legal or equitable rights or
                                remedies you have or may have against the Company with
                                respect to those third-party materials.
                            
                        </li>
                        <li>
                            
                                Your use of the Website, its content, and any services or
                                items obtained through the Website is at your own risk. The
                                Company provides the Website, its content, and any services
                                or items obtained through the Website “as is,” “with all
                                faults,” and “as available,” without making any warranty,
                                either express or implied. The Company is not making any
                                warranty (1) that the Website, its content, or any services
                                or items obtained through the Website will be accurate,
                                reliable, error-free, or uninterrupted; (2) that defects
                                will be corrected; (3) that the Website or the server that
                                makes it available are free of viruses or other harmful
                                components; or (4) that the Website or any services or
                                items obtained through the Website will otherwise meet your
                                needs or expectations.
                            
                        </li>
                        <li>
                            
                                The Company is not making any warranty, whether express,
                                implied, statutory, or otherwise, including the warranty of
                                merchantability, title, noninfringement, privacy, security,
                                and fitness for a particular purpose. No advice or
                                information, whether oral or written, obtained from the
                                Company, the Website, or elsewhere will create any warranty
                                not expressly stated in this agreement.
                            
                        </li>
                    </ol>
                </li>
                <li>
                    <h4>
                        <a name="_Ref428360925"></a>
                        <strong>Limit on Liability; Release</strong>
                    </h4>
                    <ol>
                        <li>
                            
                                The Company, its directors, officers, employees, agents,
                                subsidiaries, affiliates, licensors, content providers, and
                                service providers will not be liable to you for any of the
                                following:
                            
                            <ol type="a">
                                <li>
                                    
                                        Errors, mistakes, or inaccuracies of content;
                                    
                                </li>
                                <li>
                                    
                                        Personal injury or property damage resulting from
                                        your access to and use of the Website or its
                                        content;
                                    
                                </li>
                                <li>
                                    
                                        Content (including user submissions) or conduct
                                        that is infringing, inaccurate, obscene, indecent,
                                        offensive, threatening, harassing, defamatory,
                                        libelous, abusive, invasive of privacy, or illegal;
                                    
                                </li>
                                <li>
                                    
                                        Unauthorized access to or use of the Company’s
                                        servers and any personal or financial information
                                        stored in them, including unauthorized access or
                                        changes to your account, submissions,
                                        transmissions, or data;
                                    
                                </li>
                                <li>
                                    
                                        Interruption or cessation of transmission to or
                                        from the Website;
                                    
                                </li>
                                <li>
                                    
                                        Bugs, viruses, Trojan horses, malware, ransomware,
                                        or other disabling code that may be transmitted to
                                        or through the Website by any person or that might
                                        infect your computer or affect your access to or
                                        use of the Website, your other services, hardware,
                                        or software;
                                    
                                </li>
                                <li>
                                    
                                        Incompatibility between the Website and your other
                                        services, hardware, or software;
                                    
                                </li>
                                <li>
                                    
                                        Delays or failures you might experience in
                                        starting, conducting, or completing any
                                        transmissions to or transactions with the Website;
                                        or
                                    
                                </li>
                                <li>
                                    
                                        Loss or damage incurred because of the use of any
                                        content posted, emailed, sent, or otherwise made
                                        available through the Website.
                                    
                                </li>
                            </ol>
                        </li>
                        <li>
                            
                                You hereby release the Company, its directors, officers,
                                employees, agents, subsidiaries, affiliates, licensors,
                                content providers, and service providers from all liability
                                arising out of user submissions or the conduct of other
                                users or third parties, including disputes between you and
                                one or more other users or third parties.
                            
                        </li>
                    </ol>
                </li>
                <li>
                    <h4>
                        <a name="_Ref428360936"></a>
                        <strong>Exclusion of Damages; Exclusive Remedy</strong>
                    </h4>
                    <ol>
                        <li>
                            
                                Unless caused by gross negligence or intentional
                                misconduct, the Company, its directors, officers,
                                employees, agents, subsidiaries, affiliates, licensors,
                                content providers, and service providers will not be liable
                                to you for any direct, indirect, special (including
                                so-called consequential damages), statutory, punitive, or
                                exemplary damages arising out of or relating to your access
                                or your inability to access the Website or the content.
                                This exclusion applies regardless of the theory of
                                liability and even if you told the Company about the
                                possibility of these damages or the Company knew or should
                                have known about the possibility of these damages.
                            
                        </li>
                        <li>
                            
                                The Company, its directors, officers, employees, agents,
                                subsidiaries, affiliates, licensors, content providers, and
                                service providers will not be liable to you for any damages
                                for (1) personal injury, (2) pain and suffering, (3)
                                emotional distress, (4) loss of revenue, (5) loss of
                                profits, (6) loss of business or anticipated savings, (7)
                                loss of use, (8) loss of goodwill, (9) loss of data, (10)
                                loss of privacy, or (11) computer failure related to your
                                access of or your inability to access the Website or the
                                content. This exclusion applies regardless of the theory of
                                liability and even if you told the Company about the
                                possibility of these damages or the Company knew or should
                                have known about the possibility of these damages.
                            
                        </li>
                        <li>
                            
                                If you are dissatisfied with the Website or have any other
                                complaint, your exclusive remedy is to stop using the
                                Website and cancel your subscriptions. The maximum
                                liability of the Company and its directors, officers,
                                employees, agents, subsidiaries, affiliates, licensors,
                                content providers, and service providers to you for any
                                claim will not exceed the greater of $100 and the amount
                                you have paid to the Company for the applicable purchase
                                out of which liability arose even if the remedy fails of
                                its essential purpose.
                            
                        </li>
                    </ol>
                </li>
                <li>
                    <h4>
                        <strong>Scope of Disclaimers, Exclusions, and Limits. </strong>
                        The disclaimers, exclusions, and limits stated in sections 16, 17,
                        and 18 apply to the greatest extent allowed by law, but no more.
                        The Company does not intend to deprive you of any mandatory
                        protections provided to you by law. Because some jurisdictions may
                        prohibit the disclaimer of some warranties, the exclusion of some
                        damages or other matters, one or more of the disclaimers,
                        exclusions, or limits will not apply to you.
                    </h4>
                </li>
                <li>
                    <h4>
                        <a name="_Ref452479484"></a>
                        <strong>Indemnification</strong>
                    </h4>
                    <ol>
                        <li>
                            
                                <strong>In General. </strong>
                                You will pay the Company, its directors, officers,
                                employees, agents, contractors, subsidiaries, affiliates,
                                partners, licensors, content providers, and service
                                providers (collectively, the “                    <strong>Indemnified Parties</strong>”) for any loss of an
                                Indemnified Party’s that is caused by any of the following
                                (whether actual or alleged): (a) your use of the Website;
                                (b) your breach of this agreement; (c) your violation of
                                law; (d) your submission, posting, or transmission of user
                                content to the Website; or (e) your violation of a third
                                party’s rights. But you are not required to pay if the loss
                                was caused by the Indemnified Party’s actual intentional
                                misconduct.
                            
                        </li>
                        <li>
                            
                                <strong>Definitions</strong>
                            
                            <ol type="a">
                                <li>
                                    
                                        “<strong>Loss</strong>” means an amount that the
                                        Indemnified Party is legally responsible for or
                                        pays in any form. Amounts include, for example, a
                                        judgment, a settlement, a fine, damages, injunctive
                                        relief, staff compensation, a decrease in property
                                        value, and expenses for defending against a claim
                                        for a loss (including fees for legal counsel,
                                        expert witnesses, and other advisers). A loss can
                                        be tangible or intangible; can arise from bodily
                                        injury, property damage, or other causes; can be
                                        based on tort, breach of contract, or any other
                                        theory of recovery; and includes incidental,
                                        direct, and consequential damages.
                                    
                                </li>
                                <li>
                                    
                                        A loss is “<strong>caused by</strong>”                            <em><strong> </strong></em>an event if the loss
                                        would not have happened without the event, even if
                                        the event is not a proximate cause of the loss.
                                    
                                </li>
                            </ol>
                        </li>
                        <li>
                            
                                <strong>
                                    The Indemnified Party’s Duty to Notify You.
                                </strong>
                                The Indemnified Party will notify you before the 30th day
                                after the Indemnified Party knows or should reasonably have
                                known of a claim for a loss that you might be compelled to
                                pay. But the Indemnified Party’s failure to timely notify
                                you does not end your obligation, except if that failure
                                prejudices your ability to defend or mitigate losses.
                            
                        </li>
                        <li>
                            
                                <strong>Legal Defense of a Claim. </strong>
                                The Indemnified Party has control over defending a claim
                                for a loss (including settling it) unless the Indemnified
                                Party directs you to control the defense. If the
                                Indemnified Party directs you to control the defense, you
                                will not settle any litigation without the Indemnified
                                Party’s written consent if the settlement (1) imposes a
                                penalty or limitation on the Indemnified Party, (2) admits
                                the Indemnified Party’s fault, or (3) does not fully
                                release the Indemnified Party from liability. You and the
                                Indemnified Party will cooperate with each other in good
                                faith on a claim.
                            
                        </li>
                        <li>
                            
                                <strong>No Exclusivity. </strong>
                                The Indemnified Party’s rights under this section 20 do not
                                affect other rights the Indemnified Parties might have.
                            
                        </li>
                    </ol>
                </li>
                <li>
                    <h4>
                        <strong>Dispute Resolution</strong>
                    </h4>
                    <ol>
                        <li>
                            
                                <strong>Litigation Election. </strong>
                                Either party may elect to litigate the following type of
                                case or controversy: (a) an action seeking injunctive or
                                other equitable relief, or (b) a suit to compel compliance
                                with this dispute resolution procedure.
                            
                        </li>
                        <li>
                            
                                <strong>Negotiation. </strong>
                                Each party will allow the other a reasonable opportunity to
                                comply before it claims that the other has not met the
                                duties under this agreement. The parties will first meet
                                and negotiate with each other in good faith to try to
                                resolve all disputes between the parties arising out of or
                                relating to the Website.
                            
                        </li>
                        <li>
                            
                                <a name="_Ref428360966"></a>
                                <strong>Mediation. </strong>
                                If the parties cannot settle a dispute arising out of or
                                relating to the Website or this agreement through
                                negotiation after 30 days, either party may, by notice to
                                the other party and the International Institute for
                                Conflict Prevention &amp; Resolution (“                    <em><strong>CPR</strong></em>”), demand mediation under the
                                Mediation Procedure of CPR. Mediation will take place in
                                Cedar Rapids, Iowa, U.S.A. The language of the mediation
                                will be English. Each party will bear its own costs in
                                mediation, and the parties will share equally between them
                                all third-party mediation costs unless the parties agree
                                differently in writing. Each party will participate
                                actively and constructively in mediation proceedings once
                                started and will attend at least one joint meeting between
                                the mediator and the parties. Any party may terminate
                                mediation at any time after an initial meeting between the
                                mediator and the parties.
                            
                        </li>
                        <li>
                            
                                <a name="_Ref428360975"></a>
                                <strong>Arbitration</strong>
                            
                            <ol type="a">
                                <li>
                                    
                                        <strong>Claim Procedure. </strong>
                                        If the parties cannot settle a dispute through
                                        mediation, the parties will settle any unresolved
                                        dispute arising out of or relating to the Website
                                        or this agreement by arbitration administered by
                                        CPR under its Rules for Administered Arbitration. A
                                        single arbitrator will preside over the
                                        arbitration. The arbitrator, and not any federal,
                                        state, or local court or agency, will have
                                        exclusive authority to resolve all disputes arising
                                        out of or relating to the interpretation,
                                        enforceability, or formation of this agreement,
                                        including any claim that all or any part of this
                                        agreement is void or voidable.
                                    
                                </li>
                                <li>
                                    
                                        <strong>Arbitration Location.</strong>
                                        Unless the parties agree otherwise in writing, the
                                        arbitration will take place in Cedar Rapids, Iowa,
                                        U.S.A.
                                    
                                </li>
                                <li>
                                    
                                        <strong>Arbitration Fees. </strong>
                                        Each party will be responsible for paying any
                                        filing, administrative, and arbitrator fees
                                        associated with the arbitration.
                                    
                                </li>
                                <li>
                                    
                                        <strong>Arbitration Award. </strong>
                                        The arbitrator may grant whatever relief that would
                                        be available in a court at law or in equity, except
                                        that the arbitrator must not award punitive or
                                        exemplary damages, or damages otherwise limited or
                                        excluded in this agreement. The award rendered by
                                        the arbitrator must include costs of arbitration,
                                        reasonable legal fees in accordance with section
                                        21.7, and reasonable costs for expert and other
                                        witnesses. The arbitrator’s award will bind the
                                        parties and any judgment on the award rendered by
                                        the arbitrator may be entered in any court of
                                        competent jurisdiction.
                                    
                                </li>
                                <li>
                                    
                                        <strong>Confidentiality. </strong>
                                        Unless required by law, neither a party nor an
                                        arbitrator will disclose the existence, content, or
                                        results of any arbitration under this agreement
                                        without the advance written consent of both
                                        parties.
                                    
                                </li>
                            </ol>
                        </li>
                        <li>
                            
                                <strong>Injunctive Relief.</strong>
                                The parties acknowledge that breach by either party of the
                                obligations under this agreement could cause irreparable
                                harm for which damages would be an inadequate remedy. If
                                any breach occurs or is threatened, the injury party may
                                promptly seek enforcement of this agreement by means of
                                specific performance, injunction, restraining order, or any
                                other equitable remedy, in each case without posting a bond
                                or other security and without proof of actual money damages
                                in connection with the claim.
                            
                        </li>
                        <li>
                            
                                <a name="_Ref426657918"></a>
                                <strong>Jurisdiction and Venue</strong>
                            
                            <ol type="a">
                                <li>
                                    
                                        <a name="_Ref435788100"></a>
                                        If a party brings any court proceeding authorized
                                        under this agreement, that party will bring that
                                        court proceeding only in the United States District
                                        Court for the Northern District of Iowa or in any
                                        state court of competent jurisdiction in Linn
                                        County, Iowa, and each party hereby submits to the
                                        exclusive jurisdiction and venue of those courts
                                        for purposes of any court proceeding, except that
                                        judgement on an award rendered in arbitration may
                                        be entered in any court of competent jurisdiction.
                                    
                                </li>
                                <li>
                                    
                                        Each party hereby waives any claim that any court
                                        proceeding brought in accordance with section
                                        21.6(a) has been brought in an inconvenient forum
                                        or that the venue of that court proceeding is
                                        improper.
                                    
                                </li>
                            </ol>
                        </li>
                        <li>
                            
                                <a name="_Ref482009941"></a>
                                <a name="_Ref444606184"></a>
                                <strong>Recovery of Expenses. </strong>
                                In any proceedings between the parties arising out of or
                                relating to the subject matter of this agreement, the
                                prevailing party will be entitled to recover from the other
                                party, besides any other relief awarded, all expenses that
                                the prevailing party incurs in those proceedings, including
                                legal fees and expenses. For purposes of this section 21.7,
                                “<strong>prevailing party</strong>” means, for any
                                proceeding, the party in whose favor an award is rendered,
                                except that if in those proceedings the award finds in
                                favor of one party on one or more claims or counterclaims
                                and in favor of the other party on one or more other claims
                                or counterclaims, neither party will be the prevailing
                                party. If any proceedings are voluntarily dismissed or are
                                dismissed as part of settlement of that dispute, neither
                                party will be the prevailing party in those proceedings.
                            
                        </li>
                        <li>
                            
                                <strong>Jury Trial Waiver. </strong>
                                Each party hereby waives its right to a trial by jury in
                                any proceedings arising out of, or relating to the subject
                                matter of, this agreement. Either party may enforce this
                                waiver up to and including the first day of trial.
                            
                        </li>
                        <li>
                            
                                <a name="_Ref428360985"></a>
                                <strong>Class Action Waiver. </strong>
                                All claims must be brought in the parties’ individual
                                capacity, and not as a plaintiff or class member in any
                                purported class or representative proceeding, and, unless
                                the Company agrees otherwise, the arbitrator will not
                                consolidate more than one person’s claims. Both parties
                                acknowledge that each party is waiving the right to
                                participate in a class action.
                            
                        </li>
                        <li>
                            
                                <a name="_Ref428360992"></a>
                                <strong>Limitation on Time to Bring Claims. </strong>
                                A party will not bring a claim arising out of or relating
                                to the Website or this agreement more than one year after
                                the cause of action arose. Any claim brought after one year
                                is barred.
                            
                        </li>
                    </ol>
                </li>
                <li>
                    <h4>
                        <strong>General</strong>
                    </h4>
                    <ol>
                        <li>
                            
                                <strong>Entire Agreement. </strong>
                                This agreement constitutes the entire agreement between you
                                and the Company about your access to and use of the
                                Website. It supersedes all earlier or contemporaneous
                                agreements between you and the Company about access to and
                                use of the Website. A printed version of this agreement
                                will be admissible in any proceedings arising out of or
                                relating to this agreement to the same extent and subject
                                to the same conditions as other business documents and
                                records originally generated and kept in printed form. Any
                                additional terms on the Website will govern the items to
                                which they pertain.
                            
                        </li>
                        <li>
                            
                                <strong>Copy of this Agreement. </strong>
                                You may, and the Company recommends that you, print this
                                agreement on your printer or save it to your computer. If
                                you have trouble printing a copy, please contact the
                                Company at <a href="mailto:support@brandtsboys.com">
                                    support@brandtsboys.com
                                </a>
                                and it will email you a copy.
                            
                        </li>
                        <li>
                            
                                <a name="_Ref452478240"></a>
                                <strong>Assignment and Delegation. </strong>
                                The Company may assign its rights or delegate any
                                performance under this agreement without your consent. You
                                will not assign your rights or delegate your performance
                                under this agreement without The Company’s advance written
                                consent. Any attempted assignment of rights or delegation
                                of performance in breach of this section 22.3 is void.
                            
                        </li>
                        <li>
                            
                                <strong>Waivers.</strong>
                                The parties may waive any provision in this agreement only
                                by a writing signed by the party or parties against whom
                                the waiver is sought to be enforced. No failure or delay in
                                exercising any right or remedy, or in requiring the
                                satisfaction of any condition, under this agreement, and no
                                act, omission, or course of dealing between the parties,
                                operates as a waiver or estoppel of any right, remedy, or
                                condition. A waiver made in writing on one occasion is
                                effective only in that instance and only for the purpose
                                stated. A waiver once given is not to be construed as a
                                waiver on any future occasion or against any other person.
                            
                        </li>
                        <li>
                            
                                <a name="_Ref478490534"></a>
                                <strong>Severability. </strong>
                                The parties intend as follows:
                            
                            <ol type="a">
                                <li>
                                    
                                        that if any provision of this agreement is held to
                                        be unenforceable, then that provision will be
                                        modified to the minimum extent necessary to make it
                                        enforceable, unless that modification is not
                                        permitted by law, in which case that provision will
                                        be disregarded;
                                    
                                </li>
                                <li>
                                    
                                        that if modifying or disregarding the unenforceable
                                        provision would result in failure of an essential
                                        purpose of this agreement, the entire agreement
                                        will be held unenforceable;
                                    
                                </li>
                                <li>
                                    
                                        that if an unenforceable provision is modified or
                                        disregarded in accordance with this section 22.5,
                                        then the rest of the agreement will remain in
                                        effect as written; and
                                    
                                </li>
                                <li>
                                    
                                        that any unenforceable provision will remain as
                                        written in any circumstances other than those in
                                        which the provision is held to be unenforceable.
                                    
                                </li>
                            </ol>
                        </li>
                        <li>
                            
                                <strong>Notices. </strong>
                                For a notice or other communication under this agreement to
                                be valid, it must be in writing and delivered by email. A
                                valid notice or other communication under this agreement
                                will be effective when received by the party to which it is
                                addressed. It will be deemed to have been received as
                                follows: (a) when the party to which the email is addressed
                                acknowledges having received that email; and (b) if the
                                party to which it is addressed rejects or otherwise refuses
                                to accept it, or if it cannot be delivered because of a
                                change in email address for which no notice was given, then
                                on that rejection, refusal, or inability to deliver. You
                                may address notice to the Company to <a href="mailto:legal@brandtsboys.com">
                                    legal@brandtsboys.com
                                </a>
                                <strong> </strong>
                                unless a specific email address is stated for giving notice
                                for that particular issue. The Company may address any
                                notice to you to the last known email address the Company
                                has for you.
                            
                        </li>
                        <li>
                            
                                <strong>Governing Law. </strong>
                                Iowa law, without giving effect to its conflicts of law
                                principles, governs all matters arising out of or relating
                                to this agreement, including the validity, interpretation,
                                construction, performance, and enforcement of this
                                agreement. The predominant purpose of this agreement is
                                providing services and licensing access to intellectual
                                property and not a “sale of goods.”
                            
                        </li>
                        <li>
                            
                                <strong>Force Majeure. </strong>
                                The Company is not responsible for any failure to perform
                                if unforeseen circumstances or causes beyond The Company’s
                                reasonable control delays or continues to delay The
                                Company’s performance, including:
                            
                            <ol type="a">
                                <li>
                                    
                                        Acts of God, including fire, flood, earthquakes,
                                        hurricanes, tropical storms, or other natural
                                        disasters;
                                    
                                </li>
                                <li>
                                    
                                        War, riot, arson, embargoes, acts of civil or
                                        military authority, or terrorism;
                                    
                                </li>
                                <li>
                                    
                                        Fiber cuts;
                                    
                                </li>
                                <li>
                                    
                                        Strikes, or shortages in transportation,
                                        facilities, fuel, energy, labor, or materials;
                                    
                                </li>
                                <li>
                                    
                                        Failure of the telecommunications or information
                                        services infrastructure; and
                                    
                                </li>
                                <li>
                                    
                                        Hacking, SPAM, DDOS attacks, or any failure of a
                                        computer, server, network, or software.
                                    
                                </li>
                            </ol>
                        </li>
                        <li>
                            
                                <strong>No Third-Party Beneficiaries. </strong>
                                This agreement does not, and the parties do not intend it
                                to, confer any rights or remedies on any person other than
                                the parties to this agreement.
                            
                        </li>
                        <li>
                            
                                <strong>Relationship of the Parties. </strong>
                                This agreement does not, and the parties do not intend it
                                to, create a partnership, joint venture, agency, franchise,
                                or employment relationship between the parties and the
                                parties expressly disclaim the existence of any of these
                                relationships between them. Neither of the parties is the
                                agent for the other, and neither party has the right to
                                bind the other on any agreement with a third party.
                            
                        </li>
                        <li>
                            
                                <a name="_Ref482011964"></a>
                                <strong>Successors and Assigns.</strong>
                                This agreement inures to the benefit of, and binds, the
                                parties and their respective successors and assigns. This
                                section 22.11 does not address, directly or indirectly,
                                whether a party may assign rights or delegate obligations
                                under this agreement. Section 22.3 addresses these matters.
                            
                        </li>
                        <li>
                            
                                <strong>Electronic Communications Not Private. </strong>
                                The Company does not provide facilities for sending or
                                receiving confidential electronic communications. You
                                should consider all messages sent to the Company or from
                                the Company as open communications readily accessible to
                                the public. You should not use the Website to send or
                                receive messages you only intend the sender and named
                                recipients to read. Users or operators of the Website may
                                read all messages you send to the Website regardless of
                                whether they are intended recipients.
                            
                        </li>
                        <li>
                            
                                <strong>Electronic Signatures. </strong>
                                Any affirmation, assent, or agreement you send through the
                                Website will bind you. You acknowledge that when you click
                                on an “I agree,” “I consent,” or other similarly worded
                                “button” or entry field with your mouse, keystroke, or
                                other computer device, your agreement or consent will be
                                legally binding and enforceable and the legal equivalent of
                                your handwritten signature.
                            
                        </li>
                        <li>
                            
                                <strong>Consumer Rights Information—</strong>
                                <strong>California Residents Only.</strong>
                                This provision applies only to California residents. In
                                compliance with section 1789 of the California Civil Code,
                                please note the following:
                            
                        </li>
                    </ol>
                </li>
            </ol>
            <p>
                DNA Enterprises, LLC
                <br/>
                147 Summer Circle
                <br/>
                Cedar Rapids, Iowa 52402
            </p>
            <p align="justify">
                To access certain parts of the Website or certain features, you must a
                subscription. You may contact the Company at    <a href="mailto:support@brandtsboys.com">support@brandtsboys.com</a> to
                resolve any disputes or to receive further information about the Website.
            </p>
            <ol>
                <ol start="15">
                    <li>
                        
                            <strong>Complaints</strong>
                            —<strong>California Residents Only.</strong> You may contact in
                            writing the Complaint Assistance Unit of the Division of
                            Consumer Services of the Department of Consumer Affairs at 1020
                            North Street, #501, Sacramento, California 95814, or by
                            telephone at +1 (916) 445-1254.
                        
                    </li>
                    <li>
                        
                            <strong>Feedback. </strong>
                            The Company encourages you to give feedback about the Website.
                            But the Company will not treat as confidential any suggestion
                            or idea you give, and nothing in this agreement will restrict
                            the Company’s right to use, profit from, disclose, publish, or
                            otherwise exploit any feedback, without payment to you.
                        
                    </li>
                    <li>
                        
                            <strong>English language. </strong>
                            The Company has drafted this agreement in the English language.
                            The Company assumes that you can read and understand the
                            English language. The Company is not liable to you or any other
                            person for any costs or expenses incurred to translate this
                            agreement into another language. The English language version
                            controls over any translated version.
                        
                    </li>
                    <li>
                        
                            <strong>No Reliance</strong>
                            . You acknowledge that in electronically signing this
                            agreement, you do not rely and have not relied on any statement
                            by the Company or its agents, except those statements contained
                            in this agreement.
                        
                    </li>
                    <li>
                        
                            <strong>Contact Information. </strong>
                            If you have any questions or comments about this agreement or
                            the Website, please email the Company at <a href="mailto:support@brandtsboys.com">
                                support@brandtsboys.com
                            </a>
                            .
                        
                    </li>
                </ol>
            </ol>
        </div>
        <Footer/>
    </div>
);

export default connect(
    (state) => ({
        user: state.auth.user,
        isLoggedIn: state.auth.isLoggedIn
    })
)(TermsOfServicePage)