import { call, put, take, fork, select } from 'redux-saga/effects'
import * as constants from './constants'
import * as landingConstants from '../landing/constants';
import {getDonationLinkParamsSuccess, getDonationLinkParamsFailure} from './actions';
import {getTopFansRequest, getCurrentRoundRequest} from '../landing/actions';
import {changeDonationModalStep} from '../ui/actions';
import {donationModalSteps} from '../ui/constants';
import request from '../../helpers/request';

export function* getDonationLinkParams() {
    while (true) {
        const {payload: {amountString, resolve, reject}} = yield take(constants.GET_DONATION_LINK_PARAMS_REQUEST);

        const response = yield call(request, `/donations/segpay_link_params/${amountString}`)
        const body = yield response.json();
        if (response.ok) {
            resolve(body);
            yield put(getDonationLinkParamsSuccess());
        } else {
            reject();
            yield put(getDonationLinkParamsFailure());
        }
    }
}

/*export function* donate() {
    while (true) {
        const {payload: {stripeToken, postId, amount, donationType, target, requestRoundAndTopFansAfterDonation}} = yield take(constants.DONATE_REQUEST);
        let response;
        if (donationType === 'tipJar') {
            response = yield call(request, '/donations/tip_jar', 'POST', {
                amount,
                tip_jar_id: target
            });
        } else {
            response = yield call(request, '/donations', 'POST', {
                stripe_token: stripeToken,
                amount,
                target,
                post_id: postId
            });
        }

        const body = yield response.json();
        if (response.ok) {
            if (requestRoundAndTopFansAfterDonation) {
                yield put(getCurrentRoundRequest());
                yield take(landingConstants.GET_CURRENT_ROUND_SUCCESS);
                yield put(donateSuccess());
                yield put(changeDonationModalStep(donationModalSteps.CONGRATULATIONS));
                const state = yield select();
                yield put(getTopFansRequest(state.ui.topFansTab));
            } else {
                yield put(donateSuccess());
                yield put(changeDonationModalStep(donationModalSteps.CONGRATULATIONS));
            }
        } else {
            if (body.error && body.error.message) {
                yield put(donateFailure(body.error.message));
            }
        }
    }
}*/

function startSagas(...sagas) {
    return function* rootSaga() {
        yield sagas.map(saga => fork(saga))
    }
}

export default startSagas(getDonationLinkParams)