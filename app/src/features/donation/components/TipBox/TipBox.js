import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import {Link} from 'react-router-dom';
import starIcon from '../../../../assets/images/star.png';
import shieldIcon from '../../../../assets/images/shield.png';
import TipInput from '../TipInput/TipInput';
import styles from './TipBox.module.scss';

const MIN_TIP_VALUE_CENTS = 100;
const LEVEL_UP_POINTS = 100;

export default class TipBox extends React.Component {
    constructor() {
        super();

        this.state = {
            value: '',
            amountCents: 0
        }
    }

    renderProgressBar = (points) => {
        const barPercentage = points % LEVEL_UP_POINTS;
        const level = Math.floor(points / LEVEL_UP_POINTS);

        return (
            <div className={styles.progressBarContainer}>
                <div className={styles.progressBar}>
                    <div className={styles.innerBar} style={{width: barPercentage + '%'}}> </div>
                    <div className={styles.levelShield}>
                        <span className={styles.progressLevelNumber}>{level}</span>
                        <img className={styles.progressShieldIcon} src={shieldIcon} />
                    </div>
                    <div className={styles.progressStar}>
                        <img className={styles.progressStarIcon} src={starIcon} />
                    </div>
                </div>
            </div>
        )
    };

    render() {
        const {className, avatar, name, artistLink, donationPoints = 0, description, wishlistLink, onTipClick} = this.props;

        return (
            <div className={cn(styles.container, className)}>
                <div className={styles.infoSection}>
                    <div className={styles.infoHeader}>
                        <Link to={artistLink}>
                            <img src={avatar} className={styles.avatar}/>
                        </Link>
                        <div className={styles.nameAndPointsContainer}>
                            <div className={styles.name}>{name}</div>
                            <div className={styles.pointTag}>
                                <img className={styles.ratingIcon} src={starIcon} />
                                {donationPoints}
                                </div>
                        </div>
                    </div>
                </div>
                <div className={styles.mainSection}>
                    {this.renderProgressBar(donationPoints)}
                    <div className={styles.description}>{description}</div>
                    <div className={styles.buttonsContainer}>
                        <TipInput onTipClick={onTipClick} className={styles.tipContainer}/>
                        <div className={styles.divider}></div>
                        <div className={styles.wishlistContainer}>
                            <a target="_blank" href={wishlistLink} className={styles.wishlistButton}>Wishlist</a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

TipBox.propTypes = {
    className: PropTypes.string,
    avatar: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    donationPoints: PropTypes.number,
    description: PropTypes.string.isRequired,
    wishlistLink: PropTypes.string.isRequired,
    artistLink: PropTypes.string.isRequired,
    onTipClick: PropTypes.func.isRequired
};