import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import NumberFormat from 'react-number-format';
import {roundTo} from '../../../../helpers/roundTo';
import {MAX_DONATION_AMOUNT_CENTS, MIN_DONATION_AMOUNT_CENTS} from '../../constants';

import styles from './TipInput.module.scss';

function NumberFormatCustom(props) {
    const {inputRef, onChange, ...other} = props;

    return (
        <NumberFormat
            {...other}
            ref={inputRef}
            decimalScale={2}
            fixedDecimalScale={true}
            allowNegative={false}
            onValueChange={values => {
                onChange({
                    target: {
                        value: values.value,
                    },
                });
            }}
            thousandSeparator
        />
    );
}

export default class TipInput extends React.Component {
    constructor() {
        super();

        this.state = {
            value: '2.95',
            amountCents: 295
        }
    }

    handleChange = (e) => {
        this.setState({
            value: e.target.value,
            amountCents: e.target.value ? roundTo(Number(e.target.value) * 100, 0) : 0
        });
    };

    handleTipClick = () => {
        this.props.onTipClick(this.state.amountCents);
    };

    render() {
        const {className} = this.props;
        const {amountCents} = this.state;

        return (
            <div className={cn(styles.container, className)}>
                <TextField
                    className={styles.tipTextField}
                    margin="normal"
                    placeholder='0.00'
                    type='tel'
                    InputProps={{
                        startAdornment: <InputAdornment position="start"
                                                        className={styles.addon}>$</InputAdornment>,
                        inputComponent: NumberFormatCustom,
                        disableUnderline: true
                    }}
                    onChange={this.handleChange}
                    value={this.state.value}
                />
                <button
                    className={styles.tipButton}
                    disabled={!((amountCents >= MIN_DONATION_AMOUNT_CENTS) && (amountCents <= MAX_DONATION_AMOUNT_CENTS))}
                    onClick={this.handleTipClick}
                >
                    Tip
                </button>
            </div>
        )
        //return <div>1111</div>
    }
}

TipInput.propTypes = {
    className: PropTypes.string,
    onTipClick: PropTypes.func.isRequired
};
