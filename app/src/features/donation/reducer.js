import * as constants from './constants';
import * as uiConstants from '../ui/constants';

const initialState = {
    isRequestingDonation: false,
    donationError: null,
};

export default function(state = initialState, action) {
    switch(action.type) {

        case (uiConstants.CHANGE_DONATION_MODAL_STEP):
        case (uiConstants.HIDE_MODAL):
            return Object.assign({}, state, {
                donationError: null
            });

        default:
            return state;
    }
}