import * as constants from './constants';

export const getDonationLinkParamsRequest = (payload) => ({
    type: constants.GET_DONATION_LINK_PARAMS_REQUEST,
    payload
});

export const getDonationLinkParamsSuccess = () => ({
    type: constants.GET_DONATION_LINK_PARAMS_SUCCESS
});

export const getDonationLinkParamsFailure = (errorMessage) => ({
    type: constants.GET_DONATION_LINK_PARAMS_FAILURE,
    errorMessage
});