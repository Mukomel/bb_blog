import { call, put, take, fork } from 'redux-saga/effects'
import request, {getDefaultHeaders} from '../../helpers/request';

export function* uploadImage(image) {
    const formData = new FormData();
    formData.append('file', image);
    const headers = Object.assign({}, getDefaultHeaders());
    delete headers['Content-Type'];
    const uploadFileResponse = yield call(request, '/assets/image', 'POST', formData, headers, false);
    const body = yield uploadFileResponse.json();
    return body.path;
}
