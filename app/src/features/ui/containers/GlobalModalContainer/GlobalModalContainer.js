import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {hideModal} from '../../actions';
import {modalTypes} from '../../constants';
import SubscriptionModal from '../../../subscribe/containers/SubscriptionModal/SubscriptionModal';
import SignUpModal from '../../../auth/containers/SignUpModal/SignUpModal';
import SignInModal from '../../../auth/containers/SignInModal/SignInModal';
import AddPostModal from '../../../posts/containers/AddPostModal/AddPostModal';
import EditPostModal from '../../../posts/containers/EditPostModal/EditPostModal';
import PeriodUnblockModal from '../../../artist/containers/PeriodUnblockModal/PeriodUnblockModal';
import ForgotPasswordModal from '../../../auth/containers/ForgotPasswordModal/ForgotPasswordModal';
import GroupWIshlistModal from '../../../artist/components/GroupWIshlistModal/GroupWIshlistModal';
import ErrorBoundaryModal from '../../components/ErrorBoundaryModal/ErrorBoundaryModal';
import CropperModal from '../../../artist/containers/CropperModal/CropperModal';
import ArtistStatsMembersMailingModal from '../../../artist/containers/ArtistStatsMembersMailingModal/ArtistStatsMembersMailingModal';
import UserDonationsDetailsModal from '../../../artist/containers/UserDonationsDetailsModal/UserDonationsDetailsModal';

class GlobalModalContainer extends React.Component {
    render() {
        const {modalType, modalProps, hideModal} = this.props;

        return (
            <div>
                <SubscriptionModal isOpen={modalType === modalTypes.SUBSCRIPTION_MODAL} onHide={hideModal} {...modalProps}/>
                <SignUpModal redirect={false} isOpen={modalType === modalTypes.SIGN_UP_MODAL} onHide={hideModal} {...modalProps}/>
                <SignInModal redirect={false} isOpen={modalType === modalTypes.SIGN_IN_MODAL} onHide={hideModal} {...modalProps}/>
                <AddPostModal isOpen={modalType === modalTypes.ADD_POST_MODAL} onHide={hideModal} {...modalProps}/>
                <EditPostModal isOpen={modalType === modalTypes.EDIT_POST_MODAL} onHide={hideModal} {...modalProps}/>
                <PeriodUnblockModal isOpen={modalType === modalTypes.PERIOD_UNLOCK_MODAL} onHide={hideModal} {...modalProps} />
                <ForgotPasswordModal isOpen={modalType === modalTypes.FORGOT_PASSWORD_MODAL} onHide={hideModal} {...modalProps} />
                <GroupWIshlistModal isOpen={modalType === modalTypes.GROUP_WISHLIST_MODAL} onHide={hideModal} {...modalProps} />
                <CropperModal isOpen={modalType === modalTypes.CROPPER_EDITOR_MODAL} onHide={hideModal} {...modalProps} />
                <ErrorBoundaryModal isOpen={modalType === modalTypes.ERROR_BOUNDARY_MODAL} onHide={hideModal} {...modalProps} />
                <CropperModal isOpen={modalType === modalTypes.CROPPER_EDITOR_MODAL} onHide={hideModal} {...modalProps} />
                <ArtistStatsMembersMailingModal isOpen={modalType === modalTypes.MEMBERS_STATS_MAILING_MODAL} onHide={hideModal} {...modalProps} />
                <UserDonationsDetailsModal isOpen={modalType === modalTypes.USER_DONATIONS_DETAILS_MODAL} onHide={hideModal} {...modalProps} />
            </div>
        )
    }
}

export default connect(
    (state) => ({
        modalType: state.ui.modalType,
        modalProps: state.ui.modalProps
    }),
    (dispatch) => ({
        hideModal: () => dispatch(hideModal())
    })
)(GlobalModalContainer);
