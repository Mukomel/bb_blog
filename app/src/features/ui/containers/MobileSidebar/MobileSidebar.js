import React from 'react';
import {connect} from 'react-redux';
import cn from 'classnames';
import {withRouter} from 'react-router-dom';
import Drawer from '@material-ui/core/Drawer';
import DashboardIcon from '@material-ui/icons/Dashboard';
import AccountCircle from '@material-ui/icons/AccountCircle';
import SearchBar from '../../../../base-components/SearchBar/SearchBar';
import {hideMobileSidebar} from '../../actions';
import crossIcon from '../../../../assets/images/close.svg'
import Button from '../../../../base-components/Button/Button';
import UserDescription from '../../../../base-components/UserDescription/UserDescription';
import MobileSidebarMenuItem from '../../components/MobileSidebarMenuItem/MobileSidebarMenuItem';
import profileIcon from '../../../../assets/images/profile.svg';
import notificationIcon from '../../../../assets/images/notification_grey.svg';
import logoutIcon from '../../../../assets/images/logout.svg';
import searchIcon from '../../../../assets/images/Search.svg';
import {getNotificationsRequest, getArtistRequest, clearNotifications} from '../../../artist/actions';
import {userRoles} from '../../../auth/constants';
import MobileNotificationItem from '../../../artist/components/MobileNotificationItem/MobileNotificationItem';
import {signOut} from '../../../../helpers/auth';
import snapchatIcon from '../../../../assets/images/snapchat_grey.svg';
import twitterIcon from '../../../../assets/images/twitter_grey.svg';
import tumblrIcon from '../../../../assets/images/tumblr_grey.svg';
import instagramIcon from '../../../../assets/images/instagram_grey.svg';
import starIcon from '../../../../assets/images/star.png';

import styles from './MobileSidebar.module.scss';

const classes = {
    paper: styles.container
};

class MobileSidebar extends React.Component {
    componentWillMount = () => {
        const {isMobileSidebarVisible, getNotifications} = this.props;
        if (isMobileSidebarVisible) getNotifications();
    };

    componentDidUpdate = (prevProps) => {
        if (!prevProps.isMobileSidebarVisible && this.props.isMobileSidebarVisible) {
            this.props.getNotifications();
            if (!this.props.artistProfile) this.props.getArtist();
        }
    };

    handleMyAccountClick = () => {
        const {history, hideMobileSidebar} = this.props;
        history.push('/dashboard/subscriptions');
        hideMobileSidebar();
    };

    handleMyProfileClick = () => {
        const {history, hideMobileSidebar} = this.props;
        history.push('/dashboard/artist-profile');
        hideMobileSidebar();
    };

    handleDashboardClick = () => {
        const {history, hideMobileSidebar} = this.props;
        history.push('/dashboard/posts');
        hideMobileSidebar();
    };

    handleSearch = value => {
        //send value
    };

    render() {
        const {isMobileSidebarVisible, hideMobileSidebar, user, notifications, artistProfile, clearNotifications} = this.props;
        const isAuthorOrAdmin = user && (user.role === userRoles.ADMIN || user.role === userRoles.AUTHOR);
        if (!user) return null;

        return (
            <Drawer
                anchor="right"
                open={isMobileSidebarVisible}
                onClose={hideMobileSidebar}
                classes={classes}
            >
                <div>
                    <Button onClick={hideMobileSidebar} className={styles.closeButton}>
                        <img src={crossIcon} className={styles.crossIcon} />
                    </Button>
                    {/*  need to next task  */}
                    {/*<div className={styles.searchBarSection}>
                        <SearchBar
                            className={styles.searchBar}
                            searchIcon={<img src={searchIcon}/>}
                            onSearch={this.handleSearch}
                        />
                    </div>*/}
                    <div className={styles.divider} />
                    <div className={styles.section}>
                        <UserDescription user={user}/>
                        <div className={cn(styles.divider, styles.userBlockDivider)} />
                        {
                            isAuthorOrAdmin &&
                            <React.Fragment>
                                <MobileSidebarMenuItem
                                    icon={<DashboardIcon className={styles.sidebarIcon}/>}
                                    onClick={this.handleDashboardClick}
                                    title='Dashboard'
                                />
                                <MobileSidebarMenuItem
                                    icon={<AccountCircle className={styles.sidebarIcon} />}
                                    onClick={this.handleMyProfileClick}
                                    title='My Profile'
                                />
                            </React.Fragment>
                        }

                        <MobileSidebarMenuItem
                            icon={<img src={profileIcon} className={styles.profileIcon} />}
                            onClick={this.handleMyAccountClick}
                            title='My Account'
                        />
                        <MobileSidebarMenuItem
                            icon={<img src={notificationIcon} className={styles.notificationIcon} />}
                            title='Notifications'
                        >
                            {/* ignore for this release */}
                           {/* {
                                (notifications.length > 0) &&
                                <button
                                    onClick={clearNotifications}
                                    className={styles.clearButton}
                                > Clear all</button>
                            }*/}
                        </MobileSidebarMenuItem>
                        {
                            (notifications.length > 0)
                            && <div className={styles.notificationsList}>
                                {
                                    notifications.map(notification => (
                                        <MobileNotificationItem
                                            className={styles.notificationItem}
                                            key={notification._id}
                                            notification={notification}
                                        />
                                    )).reverse()
                                }
                            </div>
                        }
                        <MobileSidebarMenuItem
                            icon={<img src={logoutIcon} className={styles.logoutIcon} />}
                            onClick={signOut}
                            title='Log Out'
                        />
                    </div>
                    <div className={cn(styles.divider, styles.bottomDivider)} />
                    {
                        artistProfile
                        && <div className={cn(styles.section, styles.socials)}>
                            <a href={artistProfile.snapchat_link} target='_blank' className={styles.socialLink}>
                                <img src={snapchatIcon} className={styles.snapchatIcon} />
                            </a>
                            <a href={artistProfile.instagram_link} target='_blank' className={styles.socialLink}>
                                <img src={instagramIcon} className={styles.instagramIcon} />
                            </a>
                            <a href={artistProfile.twitter_link} target='_blank' className={styles.socialLink}>
                                <img src={twitterIcon} className={styles.twitterIcon} />
                            </a>
                            <a href={artistProfile.tumblr_link} target='_blank' className={styles.socialLink}>
                                <img src={tumblrIcon} className={styles.tumblrIcon} />
                            </a>
                        </div>
                    }
                </div>
            </Drawer>
        )
    }
}

export default withRouter(connect(
    state => ({
        isMobileSidebarVisible: state.ui.isMobileSidebarVisible,
        user: state.auth.user,
        notifications: state.artist.notifications,
        artistProfile: state.artist.artistProfile
    }),
    dispatch => ({
        hideMobileSidebar: () => dispatch(hideMobileSidebar()),
        getNotifications: () => dispatch(getNotificationsRequest()),
        getArtist: () => dispatch(getArtistRequest('brandtandnash')),
        clearNotifications: () => dispatch(clearNotifications())
    })
)(MobileSidebar));
