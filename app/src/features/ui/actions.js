import * as constants from './constants';

export const changeDonationModalStep = (step) => ({
    type: constants.CHANGE_DONATION_MODAL_STEP,
    step
});

export const changePeriodUnlockModalStep = (step) => ({
    type: constants.CHANGE_PERIOD_UNLOCK_MODAL_STEP,
    step
});

export const changeForgotPasswordModalStep = (step) => ({
    type: constants.CHANGE_FORGOT_PASSWORD_MODAL_STEP,
    step
});

export const changeResetPasswordModalStep = (step) => ({
    type: constants.CHANGE_RESET_PASSWORD_MODAL_STEP,
    step
});

export const changeSignUpModalStep = (step) => ({
    type: constants.CHANGE_SIGN_UP_MODAL_STEP,
    step
});

export const changeTopFansTab = (tab) => ({
    type: constants.CHANGE_TOP_FANS_TAB,
    tab
});

export const showModal = (modalType, modalProps) => ({
    type: constants.SHOW_MODAL,
    modalType,
    modalProps,
});

export const hideModal = () => ({
    type: constants.HIDE_MODAL
});

export const windowSizeChanged = (windowWidth) => ({
    type: constants.WINDOW_SIZE_CHANGED,
    windowWidth
});

export const showMobileSidebar = () => ({
    type: constants.SHOW_MOBILE_SIDEBAR
});

export const hideMobileSidebar = () => ({
    type: constants.HIDE_MOBILE_SIDEBAR
});

export const showSuccessMessage = () => ({
    type: constants.SHOW_SUCCESS_MESSAGE
});

export const hideSuccessMessage = () => ({
    type: constants.HIDE_SUCCESS_MESSAGE
});
