import * as constants from './constants';

const initialState = {
    modalType: null,
    modalProps: {},
    isSubscriptionModalVisible: false,
    subscriptionModalType: null,
    isDonationModalVisible: false,
    donationModalStep: constants.donationModalSteps.PAYMENT_DETAILS,
    topFansTab: constants.topFansTabs.WEEKLY,
    periodUnlockModalStep: constants.periodUnlockModalSteps.PAYMENT_DETAILS,
    forgotPasswordModalStep: constants.forgotPasswordModalSteps.EMAIL_FORM,
    resetPasswordModalStep: constants.resetPasswordModalStep.PASSWORD_FORM,
    signUpModalStep: constants.signUpModalSteps.SIGN_UP_FORM,
    layoutType: constants.layoutTypes.MOBILE,
    isMobileSidebarVisible: false,
    isSuccessMessageVisible: false
};

export default function(state = initialState, action) {
    switch(action.type) {

        case (constants.CHANGE_SUBSCRIPTION_MODAL_STEP):
            return Object.assign({}, state, {
                subscriptionModalStep: action.step
            });

        case (constants.CHANGE_DONATION_MODAL_STEP):
            return Object.assign({}, state, {
                donationModalStep: action.step
            });

        case (constants.CHANGE_PERIOD_UNLOCK_MODAL_STEP):
            return Object.assign({}, state, {
                periodUnlockModalStep: action.step
            });

        case (constants.CHANGE_FORGOT_PASSWORD_MODAL_STEP):
            return Object.assign({}, state, {
                forgotPasswordModalStep: action.step
            });

        case (constants.CHANGE_RESET_PASSWORD_MODAL_STEP):
            return Object.assign({}, state, {
                resetPasswordModalStep: action.step
            });

        case (constants.CHANGE_SIGN_UP_MODAL_STEP):
            return Object.assign({}, state, {
                signUpModalStep: action.step
            });

        case (constants.CHANGE_TOP_FANS_TAB):
            return Object.assign({}, state, {
                topFansTab: action.tab
            });

        case (constants.SHOW_MODAL):
            return Object.assign({}, state, {
                modalType: action.modalType,
                modalProps: action.modalProps
            });

        case (constants.HIDE_MODAL):
            return Object.assign({}, state, {
                modalType: null,
                modalProps: {
                    onProgress: state.modalProps && state.modalProps.onProgress
                }
            });

        case (constants.WINDOW_SIZE_CHANGED):
            let layoutType;

            if (action.windowWidth < constants.layoutBreakpoints[constants.layoutTypes.TABLET]) {
                layoutType = constants.layoutTypes.MOBILE;
            } else if (action.windowWidth < constants.layoutBreakpoints[constants.layoutTypes.LAPTOP]) {
                layoutType = constants.layoutTypes.TABLET;
            } else if (action.windowWidth < constants.layoutBreakpoints[constants.layoutTypes.DESKTOP]) {
                layoutType = constants.layoutTypes.LAPTOP
            } else {
                layoutType = constants.layoutTypes.DESKTOP;
            }

            return Object.assign({}, state, {
                layoutType
            });

        case (constants.SHOW_MOBILE_SIDEBAR):
            return Object.assign({}, state, {
                isMobileSidebarVisible: true
            });

        case (constants.HIDE_MOBILE_SIDEBAR):
            return Object.assign({}, state, {
                isMobileSidebarVisible: false
            });

        case (constants.SHOW_SUCCESS_MESSAGE):
            return Object.assign({}, state, {
                isSuccessMessageVisible: true
            });

        case (constants.HIDE_SUCCESS_MESSAGE):
            return Object.assign({}, state, {
                isSuccessMessageVisible: false
            });

        default:
            return state;
    }
}
