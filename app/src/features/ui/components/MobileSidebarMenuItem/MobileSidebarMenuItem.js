import React from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';

import styles from './MobileSidebarMenuItem.module.scss';

const MobileSidebarMenuItem = ({className, onClick, title, icon, children}) => {
    return (
        <div className={cn(styles.container, className)} onClick={onClick}>
            <div className={styles.menuLeftContent}>
                {icon}
                <div className={styles.title}>{title}</div>
            </div>
            {children}
        </div>
    )
};

MobileSidebarMenuItem.propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.func,
    title: PropTypes.string,
    icon: PropTypes.node
};

export default MobileSidebarMenuItem;
