import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import CloseIcon from '@material-ui/icons/Close';
import WarningIcon from '@material-ui/icons/Warning';

import * as styles from './ErrorBoundaryModal.module.scss';


const ErrorBoundaryModal = ({isOpen, onHide}) => (
    <Dialog
        open={isOpen}
        onClose={onHide}
    >
        <div className={styles.container}>
            <div className={styles.topWrapper}>
                <div className={styles.imageTitle}>
                    <WarningIcon className={styles.warningIcon}/>
                </div>
            </div>
            <div className={styles.title}>Oh no!</div>
            <div className={styles.description}>
                Something wrong happened. We've sent this issue to our tech team. Please try again later.
            </div>
            <DialogActions>
                <Button onClick={onHide} color="primary" className={styles.closeButton}>
                    <CloseIcon/> Close
                </Button>
            </DialogActions>
        </div>
    </Dialog>
);

export default ErrorBoundaryModal
