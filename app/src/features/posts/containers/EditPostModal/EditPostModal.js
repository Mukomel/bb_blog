import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {submit} from 'redux-form';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';
import AddPostForm from '../../components/AddPostForm/AddPostForm';
import {editPostRequest, uploadPostMediaRequest} from '../../actions';

import styles from './EditPostModal.module.scss';

class EditPostModal extends React.Component {
    constructor() {
        super();

        this.state = {
            isEditPostFormValid: true,
            isEditPostFormSubmitting: false,
        }
    }

    handleEditPostFormValidChange = (valid) => {
        this.setState({isEditPostFormValid: valid})
    };

    handleEditPostFormSubmittingChange = (submitting) => {
        this.setState({isEditPostFormSubmitting: submitting})
    };

    handleCancelClick = () => {
        this.props.onHide();
        this.setState({ isEditPostFormSubmitting: false });
    };

    handleEditPostFormSubmit = (values) => {
        const {editPost, post} = this.props;
        return new Promise((resolve, reject) => {
            editPost({values, resolve, reject, postId: post._id});
        }).then((postId) => {
            const data = Object.assign({}, values);
            data.postId = postId;
            this.props.uploadPostMedia({values: data, onProgress: this.props.onProgress})
        });
    };

    render() {
        const {isOpen, post, submitEditPostForm} = this.props;
        let initialValues = {};
        if (post) {
            initialValues.title = post.title;
            initialValues.body = post.body;
        }

        return <Dialog
            disableBackdropClick
            disableEscapeKeyDown
            open={isOpen}
            fullWidth={true}
        >
            <DialogTitle>Edit post</DialogTitle>
            <DialogContent>
                <AddPostForm
                    form='editPostForm'
                    initialValues={initialValues}
                    onFormValidChange={this.handleEditPostFormValidChange}
                    onFormSubmittingChange={this.handleEditPostFormSubmittingChange}
                    onSubmit={this.handleEditPostFormSubmit}
                />
                {
                    this.state.isEditPostFormSubmitting
                        && <SpinnerWithBackdrop />
                }
            </DialogContent>
            <DialogActions>
                <Button
                    onClick={this.handleCancelClick}
                >
                    Cancel
                </Button>
                <Button
                    onClick={submitEditPostForm}
                    variant="raised"
                    color="primary"
                    disabled={!this.state.isEditPostFormValid}
                >
                    Edit post
                </Button>
            </DialogActions>
        </Dialog>
    }
}

EditPostModal.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onHide: PropTypes.func.isRequired,
    post: PropTypes.object
};

export default connect(
    (state) => ({}),
    (dispatch) => ({
        submitEditPostForm: () => dispatch(submit('editPostForm')),
        editPost: (payload) => dispatch(editPostRequest(payload)),
        uploadPostMedia: (payload) => dispatch(uploadPostMediaRequest(payload))
    })
)(EditPostModal)
