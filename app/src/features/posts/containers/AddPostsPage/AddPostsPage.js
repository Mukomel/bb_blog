import React from 'react';
import {connect} from 'react-redux';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import {showModal} from '../../../ui/actions';
import {getUserArtistPostsRequest, deletePostRequest, pinPostRequest, unpinPostRequest} from '../../actions';
import {modalTypes} from '../../../ui/constants';
import UserArtistPostsTable from '../../components/UserArtistPostsTable/UserArtistPostsTable';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';

import styles from './AddPostsPage.module.scss';

class AddPostsPage extends React.Component {
    state = {
        mediaUploadProgress: null
    };

    componentWillMount() {
        this.props.getUserArtistPosts();
    }

    handleDeletePost = (postId) => {
        if (window.confirm('Delete post?')) {
            this.props.deletePost(postId);
        }
    };

    onProgress = e => {
        const progressPercentage = e.loaded * 100/e.total;
        if (progressPercentage > 0 && progressPercentage < 100) {
            this.setState({mediaUploadProgress: (e.loaded * 100)/e.total});
        } else {
            this.setState({mediaUploadProgress: null, watermarkProcess: true});
        }


    };

    handleEditPost = (post) => {
        this.props.showEditPostModal(post, this.onProgress);
    };

    render() {
        const {showAddPostModal, posts, pinPost, unpinPost, isFetchingUserArtistPosts, isFetchingDeletePost, isFetchingPinPost, isFetchingUnpinPost} = this.props;
        return (
            <div className={styles.container}>
                <Paper className={styles.paper}>
                    <div className={styles.buttonContainer}>
                        <Button
                            onClick={() => showAddPostModal({onProgress: this.onProgress})}
                            variant="raised"
                            color="primary"
                        >
                            Add post
                        </Button>
                    </div>
                    <UserArtistPostsTable
                        mediaUploadProgress={this.state.mediaUploadProgress}
                        watermarkProcess={this.state.watermarkProcess}
                        posts={posts}
                        onDeletePost={this.handleDeletePost}
                        onEditPost={this.handleEditPost}
                        onPinPost={pinPost}
                        onUnpinPost={unpinPost}
                    />
                    {
                        (isFetchingDeletePost || isFetchingUserArtistPosts || isFetchingPinPost || isFetchingUnpinPost)
                            && <SpinnerWithBackdrop />
                    }
                </Paper>
            </div>
        )
    }
}

export default connect(
    state => ({
        posts: state.posts.userArtistPosts,
        isFetchingUserArtistPosts: state.posts.isFetchingUserArtistPosts,
        isFetchingDeletePost: state.posts.isFetchingDeletePost,
        isFetchingPinPost: state.posts.isFetchingPinPost,
        isFetchingUnpinPost: state.posts.isFetchingUnpinPost
    }),
    dispatch => ({
        showAddPostModal: modalProps => dispatch(showModal(modalTypes.ADD_POST_MODAL, modalProps)),
        showEditPostModal: (post, onProgress) => dispatch(showModal(modalTypes.EDIT_POST_MODAL, {post, onProgress})),
        getUserArtistPosts: () => dispatch(getUserArtistPostsRequest()),
        deletePost: (postId) => dispatch(deletePostRequest(postId)),
        pinPost: (postId) => dispatch(pinPostRequest(postId)),
        unpinPost: (postId) => dispatch(unpinPostRequest(postId))
    })
)(AddPostsPage);