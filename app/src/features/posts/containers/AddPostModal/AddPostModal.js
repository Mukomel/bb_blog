import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {submit} from 'redux-form';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';
import AddPostForm from '../../components/AddPostForm/AddPostForm';
import {createPostRequest, editPostRequest, uploadPostMediaRequest} from '../../actions';

import styles from './AddPostModal.module.scss';

class AddPostModal extends React.Component {
    constructor() {
        super();

        this.state = {
            isAddPostFormValid: false,
            isAddPostFormSubmitting: false,
        }
    }

    handleAddPostFormValidChange = (valid) => {
        this.setState({isAddPostFormValid: valid})
    };

    handleAddPostFormSubmittingChange = (submitting) => {
        this.setState({isAddPostFormSubmitting: submitting})
    };

    handleCancelClick = () => {
        this.props.onHide();
        this.setState({isAddPostFormValid: false, isAddPostFormSubmitting: false});
    };

    handleAddPostFormSubmit = (values) => {
        return new Promise((resolve, reject) => {
            this.props.createPost({values, resolve, reject})
        }).then(postId => {
            const data = Object.assign({}, values);
            data.postId = postId;
            this.props.uploadPostMedia({values: data, onProgress: this.props.onProgress})
        });
    };

    render() {
        const {isOpen, submitAddPostForm} = this.props;
        return <Dialog
            disableBackdropClick
            disableEscapeKeyDown
            open={isOpen}
            fullWidth={true}
        >
            <DialogTitle>Add post</DialogTitle>
            <DialogContent>
                <AddPostForm
                    form='addPostForm'
                    onFormValidChange={this.handleAddPostFormValidChange}
                    onFormSubmittingChange={this.handleAddPostFormSubmittingChange}
                    onSubmit={this.handleAddPostFormSubmit}
                />
                {
                    this.state.isAddPostFormSubmitting
                        && <SpinnerWithBackdrop />
                }
            </DialogContent>
            <DialogActions>
                <Button
                    onClick={this.handleCancelClick}
                >
                    Cancel
                </Button>
                <Button
                    onClick={submitAddPostForm}
                    variant="raised"
                    color="primary"
                    disabled={!this.state.isAddPostFormValid}
                >
                    Add post
                </Button>
            </DialogActions>
        </Dialog>
    }
}

AddPostModal.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onHide: PropTypes.func.isRequired,
};

export default connect(
    (state) => ({}),
    (dispatch) => ({
        submitAddPostForm: () => dispatch(submit('addPostForm')),
        createPost: (payload) => dispatch(createPostRequest(payload)),
        uploadPostMedia: (payload) => dispatch(uploadPostMediaRequest(payload)),
        editPost: (payload) => dispatch(editPostRequest(payload))
    })
)(AddPostModal)
