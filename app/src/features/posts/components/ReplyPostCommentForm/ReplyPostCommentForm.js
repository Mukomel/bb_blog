import React, {Component} from 'react';
import {string, func} from 'prop-types';
import * as styles from './ReplyPostCommentForm.module.scss';

export default class ReplyPostCommentForm extends Component {
    static propTypes = {
        placeholder: string,
        onSubmitButtonClick: string.isReired,
    };

    static defaultProps = { placeholder: '' };

    state = {value: ''};

    handleChange = ({target: {value}}) => this.setState({value});

    handleSubmitButtonClick = () => this.props.onSubmitButtonClick(this.state.value);

    render() {
        return (
            <div className={styles.commentFormContainer}>
                <textarea
                    placeholder={this.props.placeholder}
                    className={styles.commentFormTextarea}
                    value={this.state.value}
                    onChange={this.handleChange}/>
                <button
                    className={styles.submitButton}
                    onClick={this.handleSubmitButtonClick}>
                    Reply
                </button>
            </div>
        )
    }
}
