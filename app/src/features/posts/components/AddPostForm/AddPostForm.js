import React from 'react';
import PropTypes from 'prop-types';
import {Field, reduxForm} from 'redux-form';
import {TextField} from 'redux-form-material-ui';
import FileInput from '../../../../base-components/FileInput/FileInput';
import {acceptedPostMediaMIMETypes} from '../../constants';

import * as styles from './AddPostForm.module.scss';

class AddPostForm extends React.Component {

    handleFormSubmit = (values) => {
        const {onSubmit} = this.props;
        return onSubmit(values);
    };

    componentWillReceiveProps(nextProps) {
        if ((nextProps.valid !== this.props.valid) && this.props.onFormValidChange) {
            this.props.onFormValidChange(nextProps.valid);
        }

        if ((nextProps.submitting !== this.props.submitting) && this.props.onFormSubmittingChange) {
            this.props.onFormSubmittingChange(nextProps.submitting);
        }
    }

    render() {
        const {handleSubmit} = this.props;
        return (
            <form className={styles.form} onSubmit={handleSubmit(this.handleFormSubmit)}>
                <Field
                    name='title'
                    label='Title'
                    component={TextField}
                    fullWidth
                    margin='dense'
                    helperText=' '
                />
                <Field
                    name='body'
                    label='Body'
                    component={TextField}
                    fullWidth
                    margin='dense'
                    helperText=' '
                    multiline
                    rowsMax="4"
                />
                {
                    <Field
                        name='media'
                        component={FileInput}
                        accept={acceptedPostMediaMIMETypes}
                    />
                }
            </form>
        )
    }
}

AddPostForm.propTypes = {
    form: PropTypes.string.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onFormValidChange: PropTypes.func.isRequired,
    onFormSubmittingChange: PropTypes.func
};

export default reduxForm({
    validate: (values) => {
        const errors = {};

        if (!values.title) {
            errors.title = 'Required';
        }

        return errors;
    }
})(AddPostForm)