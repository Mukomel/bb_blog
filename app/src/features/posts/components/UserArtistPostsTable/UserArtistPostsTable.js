import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import {mediaTypes, WATERMARKING_MESSAGE, UPLOADING_MESSAGE} from '../../constants';
import LinearProgress from '@material-ui/core/LinearProgress';

import * as styles from './UserArtistPostsTable.module.scss';

const cellPadding = 'dense';

export default class UserArtistPostsTable extends React.Component {
    renderHead = () => {
        const cells = [
            <TableCell padding={cellPadding} key='title'>Title</TableCell>,
            <TableCell padding={cellPadding} key='body'>Body</TableCell>,
            <TableCell padding={cellPadding} key='date'>Date</TableCell>,
            <TableCell padding={cellPadding} key='author'>Author</TableCell>,
            <TableCell padding={cellPadding} key='media'>Media</TableCell>,
            <TableCell padding={cellPadding} key='actions'>Actions</TableCell>
        ];

        return (
            <TableHead>
                <TableRow>
                    {cells}
                </TableRow>
            </TableHead>
        )
    };

    renderMedia = (media) => {
        if (media.type === mediaTypes.IMAGE) {
            return <img src={media.path} className={styles.mediaImage}/>
        }

        if (media.type === mediaTypes.VIDEO) {
            return <video src={media.path} className={styles.mediaVideo} controls={true}/>
        }
    };

    renderProgressFeedback = (is_watermarking) => {
        const {mediaUploadProgress, watermarkProcess} = this.props;
        let progressBar = null;
        if (mediaUploadProgress) {
            progressBar = <div>
                {UPLOADING_MESSAGE} <LinearProgress variant="determinate" value={mediaUploadProgress}/>
                <a href="#" onClick={() => window.location.reload()}>Cancel</a>
            </div>;
        } else if (watermarkProcess || is_watermarking) {
            progressBar = WATERMARKING_MESSAGE;
        }

        return progressBar;
    };

    renderBody = () => {
        const {posts, onDeletePost, onEditPost, onPinPost, onUnpinPost} = this.props;

        return (
            <TableBody>
                {posts.map((post, key) => {

                    const progressBar = this.renderProgressFeedback(post.is_watermarking);
                    return <TableRow key={post._id}>
                        <TableCell padding={cellPadding} className={styles.cell}>{post.title}</TableCell>
                        <TableCell padding={cellPadding} className={styles.cell}>{post.body}</TableCell>
                        <TableCell padding={cellPadding}
                                   className={styles.cell}>{moment(post.created_at).format('DD MMM YYYY, HH:mm')}</TableCell>
                        <TableCell padding={cellPadding} className={styles.cell}>{post.user.username}</TableCell>
                        <TableCell padding={cellPadding} className={styles.cell}>
                            {key === 0 && progressBar ? progressBar :
                                post.media && this.renderMedia(post.media)
                            }
                        </TableCell>
                        <TableCell padding={cellPadding} className={styles.cell}>
                            <Button color="primary" onClick={() => onDeletePost(post._id)}>
                                Delete
                            </Button>
                            <Button color="primary" onClick={() => onEditPost(post)}>
                                Edit
                            </Button>
                            {
                                post.is_pinned
                                    ? <Button color="primary" variant="raised" onClick={() => onUnpinPost(post._id)}>
                                        Unpin
                                    </Button>
                                    : <Button color="primary" onClick={() => onPinPost(post._id)}>
                                        Pin
                                    </Button>
                            }
                        </TableCell>
                    </TableRow>
                })}
            </TableBody>
        )
    };

    render() {
        return (
            <div className={styles.container}>
                <Table>
                    {this.renderHead()}
                    {this.renderBody()}
                </Table>
            </div>
        )
    }
}

UserArtistPostsTable.propTypes = {
    posts: PropTypes.array.isRequired,
    onDeletePost: PropTypes.func.isRequired,
    onEditPost: PropTypes.func.isRequired,
    onPinPost: PropTypes.func.isRequired,
    onUnpinPost: PropTypes.func.isRequired
};