import React from 'react';
import PropTypes from 'prop-types';

import * as styles  from './PostCommentTextArea.module.scss';


const PostCommentTextArea = ({ input, placeholder }) => (
    <div className="formGroup">
        <textarea
            className={styles.commentTextArea}
            {...input}
            placeholder={placeholder}
        />
    </div>
);

PostCommentTextArea.propTypes = {
    input: PropTypes.object.isRequired,
    placeholder: PropTypes.string.isRequired,
};

export default PostCommentTextArea;
