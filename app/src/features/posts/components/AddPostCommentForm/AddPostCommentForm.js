import React, { Component }  from 'react';
import PropTypes from 'prop-types';
import { Form, Field, reduxForm } from 'redux-form';

import PostCommentTextArea from './PostCommentTextArea/PostCommentTextArea';

import * as styles from './AddPostCommentForm.module.scss';


class AddPostCommentForm extends Component {
    render () {
        const { handleSubmit, saveData, reset } = this.props;
        return (
            <Form
                className={styles.form}
                name="contact"
                onSubmit={handleSubmit(values => saveData(values, reset))}
            >
                <Field
                    name="comment"
                    component={PostCommentTextArea}
                    placeholder="Leave a comment"
                />
                <div className={styles.subButton}>
                    <button className={styles.subscribeButton} type="submit">
                        Submit
                    </button>
                </div>
            </Form>
        );
    }
}

AddPostCommentForm.propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    saveData: PropTypes.func.isRequired,
    reset: PropTypes.func.isRequired,
};

export default reduxForm({ form: 'postComment' })(AddPostCommentForm);

