import { call, put, take, fork } from 'redux-saga/effects'
import * as constants from './constants'
import {
    createPostFailure, createPostSuccess, getUserArtistPostsSuccess, getUserArtistPostsFailure,
    getUserArtistPostsRequest, deletePostSuccess, deletePostFailure, editPostSuccess, editPostFailure, pinPostSuccess,
    pinPostFailure, unpinPostSuccess, unpinPostFailure
} from './actions';
import {hideModal} from '../ui/actions';
import request, {getDefaultHeaders, xhrRequest} from '../../helpers/request';

export function* createPost() {
    while (true) {
        const {payload: {values, resolve, reject}} = yield take(constants.CREATE_POST_REQUEST);

        const is_published = !Boolean(values.media);
        const response = yield call(request, '/posts', 'POST', {
            title: values.title,
            body: values.body,
            is_published
        });
        const body = yield response.json();
        if (response.ok) {
            resolve(body._id);
            yield put(getUserArtistPostsRequest());
            yield put(createPostSuccess());
            yield put(hideModal());
        } else {
            reject();
            yield put(createPostFailure());
        }
    }
}

export function* uploadPostMedia() {
    while (true) {
        const {payload: {values, onProgress}} = yield take(constants.UPLOAD_POST_MEDIA_REQUEST);
        const formData = new FormData();
        formData.append('file', values.media);
        formData.append('postId', values.postId);
        const headers = Object.assign({}, getDefaultHeaders());
        delete headers['Content-Type'];
        yield call(xhrRequest, '/assets/post-media', 'POST', formData, onProgress, headers, false);
    }
}

export function* getUserArtistPosts() {
    while (true) {
        yield take(constants.GET_USER_ARTIST_POSTS_REQUEST);
        const response = yield call(request, '/posts/my-artist');
        const body = yield response.json();
        if (response.ok) {
            yield put(getUserArtistPostsSuccess(body));
        } else {
            yield put(getUserArtistPostsFailure());
        }
    }
}

export function* deletePost() {
    while (true) {
        const {postId} = yield take(constants.DELETE_POST_REQUEST);
        const response = yield call(request, '/posts/' + postId, 'DELETE');
        if (response.ok) {
            yield put(deletePostSuccess(postId));
        } else {
            yield put(deletePostFailure());
        }
    }
}

export function* editPost() {
    while (true) {
        const {payload: {resolve, reject, values, postId}} = yield take(constants.EDIT_POST_REQUEST);
        const response = yield call(request, '/posts/' + postId + '/edit', 'PUT', {
            title: values.title,
            body: values.body,
            is_published: values.is_published,
            media: values.media
        });
        const body = yield response.json();
        if (response.ok) {
            resolve(body._id);
            yield put(editPostSuccess(body));
            yield put(hideModal());
        } else {
            reject();
            yield put(editPostFailure());
        }
    }
}


export function* pinPost() {
    while (true) {
        const {postId} = yield take(constants.PIN_POST_REQUEST);
        const response = yield call(request, '/posts/' + postId + '/pin', 'POST', {});
        if (response.ok) {
            yield put(pinPostSuccess(postId));
        } else {
            yield put(pinPostFailure());
        }
    }
}

export function* unpinPost() {
    while (true) {
        const {postId} = yield take(constants.UNPIN_POST_REQUEST);
        const response = yield call(request, '/posts/' + postId + '/unpin', 'POST', {});
        if (response.ok) {
            yield put(unpinPostSuccess(postId));
        } else {
            yield put(unpinPostFailure());
        }
    }
}



function startSagas(...sagas) {
    return function* rootSaga() {
        yield sagas.map(saga => fork(saga))
    }
}

export default startSagas(createPost, deletePost, editPost, getUserArtistPosts, pinPost, unpinPost, uploadPostMedia)