import config from '../../config';

export const CREATE_POST_REQUEST = 'CREATE_POST_REQUEST';
export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';
export const CREATE_POST_FAILURE = 'CREATE_POST_FAILURE';

export const GET_USER_ARTIST_POSTS_REQUEST = 'GET_USER_ARTIST_POSTS_REQUEST';
export const GET_USER_ARTIST_POSTS_SUCCESS = 'GET_USER_ARTIST_POSTS_SUCCESS';
export const GET_USER_ARTIST_POSTS_FAILURE = 'GET_USER_ARTIST_POSTS_FAILURE';

export const DELETE_POST_REQUEST = 'DELETE_POST_REQUEST';
export const DELETE_POST_SUCCESS = 'DELETE_POST_SUCCESS';
export const DELETE_POST_FAILURE = 'DELETE_POST_FAILURE';

export const EDIT_POST_REQUEST = 'EDIT_POST_REQUEST';
export const EDIT_POST_SUCCESS = 'EDIT_POST_SUCCESS';
export const EDIT_POST_FAILURE = 'EDIT_POST_FAILURE';

export const PIN_POST_REQUEST = 'PIN_POST_REQUEST';
export const PIN_POST_SUCCESS = 'PIN_POST_SUCCESS';
export const PIN_POST_FAILURE = 'PIN_POST_FAILURE';

export const UNPIN_POST_REQUEST = 'UNPIN_POST_REQUEST';
export const UNPIN_POST_SUCCESS = 'UNPIN_POST_SUCCESS';
export const UNPIN_POST_FAILURE = 'UNPIN_POST_FAILURE';

export const UPLOAD_POST_MEDIA_REQUEST = 'UPLOAD_POST_MEDIA_REQUEST';
export const UPLOAD_POST_MEDIA_SUCCESS = 'UPLOAD_POST_MEDIA_SUCCESS';

export const acceptedPostMediaMIMETypes = [...config.acceptedImageMIMETypes, ...config.acceptedVideoMIMETypes];

export const mediaTypes = {
    IMAGE: 'image',
    VIDEO: 'video'
};

export const videoViewDuration = 20;

export const WATERMARKING_MESSAGE = "2. Watermarking... Approx. time - 2 mins";
export const UPLOADING_MESSAGE = "1. Uploading: ";