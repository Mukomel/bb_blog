import * as constants from './constants';

export const createPostRequest = (payload) => ({
    type: constants.CREATE_POST_REQUEST,
    payload
});

export const createPostSuccess = (post) => ({
    type: constants.CREATE_POST_SUCCESS,
    post
});

export const createPostFailure = () => ({
    type: constants.CREATE_POST_FAILURE
});

export const getUserArtistPostsRequest = () => ({
    type: constants.GET_USER_ARTIST_POSTS_REQUEST
});

export const getUserArtistPostsSuccess = (posts) => ({
    type: constants.GET_USER_ARTIST_POSTS_SUCCESS,
    posts
});

export const getUserArtistPostsFailure = () => ({
    type: constants.GET_USER_ARTIST_POSTS_FAILURE
});

export const deletePostRequest = (postId) => ({
    type: constants.DELETE_POST_REQUEST,
    postId
});

export const deletePostSuccess = (postId) => ({
    type: constants.DELETE_POST_SUCCESS,
    postId
});

export const deletePostFailure = () => ({
    type: constants.DELETE_POST_FAILURE
});

export const editPostRequest = (payload) => ({
    type: constants.EDIT_POST_REQUEST,
    payload
});

export const editPostSuccess = (post) => ({
    type: constants.EDIT_POST_SUCCESS,
    post
});

export const editPostFailure = () => ({
    type: constants.EDIT_POST_FAILURE
});

export const pinPostRequest = (postId) => ({
    type: constants.PIN_POST_REQUEST,
    postId
});

export const pinPostSuccess = (postId) => ({
    type: constants.PIN_POST_SUCCESS,
    postId
});

export const pinPostFailure = () => ({
    type: constants.PIN_POST_FAILURE
});

export const unpinPostRequest = (postId) => ({
    type: constants.UNPIN_POST_REQUEST,
    postId
});

export const unpinPostSuccess = (postId) => ({
    type: constants.UNPIN_POST_SUCCESS,
    postId
});

export const unpinPostFailure = () => ({
    type: constants.UNPIN_POST_FAILURE
});

export const uploadPostMediaRequest = (payload) => ({
    type: constants.UPLOAD_POST_MEDIA_REQUEST,
    payload
});

export const uploadPostMediaSuccess = () => ({
    type: constants.UPLOAD_POST_MEDIA_SUCCESS
});