import * as constants from './constants';

const initialState = {
    userArtistPosts: [],
    isFetchingUserArtistPosts: false,
    isFetchingDeletePost: false,
    isFetchingPinPost: false,
    isFetchingUnpinPost: false
};

export default function(state = initialState, action) {
    switch(action.type) {

        case (constants.GET_USER_ARTIST_POSTS_REQUEST): {
            return Object.assign({}, state, {
                isFetchingUserArtistPosts: true
            })
        }

        case (constants.GET_USER_ARTIST_POSTS_SUCCESS):
            return Object.assign({}, state, {
                userArtistPosts: action.posts,
                isFetchingUserArtistPosts: false
            });

        case (constants.GET_USER_ARTIST_POSTS_FAILURE): {
            return Object.assign({}, state, {
                isFetchingUserArtistPosts: false
            })
        }

        case (constants.DELETE_POST_REQUEST): {
            return Object.assign({}, state, {
                isFetchingDeletePost: true
            })
        }

        case (constants.DELETE_POST_SUCCESS):
            return Object.assign({}, state, {
                isFetchingDeletePost: false,
                userArtistPosts: state.userArtistPosts.filter(post => post._id !== action.postId)
            });

        case (constants.DELETE_POST_FAILURE): {
            return Object.assign({}, state, {
                isFetchingDeletePost: false
            })
        }

        case (constants.EDIT_POST_SUCCESS): {
            return Object.assign({}, state, {
                userArtistPosts: state.userArtistPosts.map(post => {
                    if (post._id === action.post._id) {
                        return action.post
                    } else {
                        return post
                    }
                })
            })
        }

        case (constants.PIN_POST_REQUEST): {
            return Object.assign({}, state, {
                isFetchingPinPost: true
            })
        }

        case (constants.PIN_POST_SUCCESS): {
            return Object.assign({}, state, {
                isFetchingPinPost: false,
                userArtistPosts: state.userArtistPosts.map(post => {
                    if (post._id === action.postId) {
                        return Object.assign({}, post, {is_pinned: true});
                    } else if (post.is_pinned) {
                        return Object.assign({}, post, {is_pinned: false});
                    } else {
                        return post;
                    }
                })
            })
        }

        case (constants.PIN_POST_FAILURE): {
            return Object.assign({}, state, {
                isFetchingPinPost: false
            })
        }

        case (constants.UNPIN_POST_REQUEST): {
            return Object.assign({}, state, {
                isFetchingUnpinPost: true
            })
        }

        case (constants.UNPIN_POST_SUCCESS): {
            return Object.assign({}, state, {
                isFetchingUnpinPost: false,
                userArtistPosts: state.userArtistPosts.map(post => {
                    if (post._id === action.postId) {
                        return Object.assign({}, post, {is_pinned: false})
                    } else {
                        return post;
                    }
                })
            })
        }

        case (constants.UNPIN_POST_FAILURE): {
            return Object.assign({}, state, {
                isFetchingUnpinPost: false
            })
        }

        default:
            return state;
    }
}