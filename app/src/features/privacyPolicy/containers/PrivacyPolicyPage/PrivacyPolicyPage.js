import React from 'react';
import Header from '../../../landing/components/Layout/Header';
import { connect } from 'react-redux';
import Footer from '../../../landing/components/Layout/Footer';

import styles from './PrivacyPolicyPage.module.scss';

const PrivacyPolicyPage = ({isLoggedIn, user}) => (
    <div className={styles.mainContent}>
        <Header
            user={user}
            isLoggedIn={isLoggedIn}/>
        <div className={styles.container}>
            <p align="center">
                <strong>
                    BrandtsBoys.com
                    <br/>
                    Privacy Policy
                </strong>
            </p>
            <p align="center">
                <a name="_GoBack"></a>
                <strong>Last Revised: April 13, 2018</strong>
            </p>
            <p align="justify">
                This policy describes the Company’s practices for collecting, using,
                maintaining, protecting, and disclosing the personal data it may collect
                from you or that you may provide when you visit the website located at <a href="http://www.brandtsboys.com/">www.brandtsboys.com</a> (the “    <strong>Website</strong>”) and the Company’s practices for collecting,
                using, keeping, protecting, and disclosing that information. This policy
                applies to the personal data collected through the Website, regardless of
                the country where you are located.
            </p>
            <p align="justify">
                The Website may include links to third-party websites, plug-ins, services,
                social networks, or applications. Clicking on those links or enabling those
                connections may allow the third party to collect or share data about you.
                The Company does not control these third-party websites, and the Company
                encourages you to read the privacy policy of every website you visit.
            </p>
            <p align="justify">
                Please read this policy carefully to understand the Company’s policies and
                practices for processing and storing your personal data. By engaging with
                the Website, you accept and consent to the practices described in this
                policy. This policy may change from time to time. Your continued engaging
                with the Website after any such revisions indicates that you accept and
                consent to them, so please check the policy periodically for updates.
            </p>
            <ol>
                <li>
                    <p align="justify">
                        <strong>Are minors welcome?</strong>
                    </p>
                </li>
            </ol>
            <p align="justify">
                The Website is not intended for anyone under 18-years old. You will only
                access the Website or register for an account if (1) you are at least
                18-years old and (2) have reached the age of majority where you live. The
                Company prohibits all persons who do not meet the age requirements from
                accessing the Website. Minors must not access the Website or use its
                services.
            </p>
            <p align="justify">
                The Company does not knowingly collect or solicit any information or data
                from minors or allow minors to register for the Website. The Website and
                its content are not directed at minors. If the Company learns that it has
                collected personal data from a minor, the Company will delete that data as
                quickly as possible. If you believe that the Company might have any
                information from or about a minor, please contact the Company at    <a href="mailto:privacy@brandtsboys.com">privacy@brandtsboys.com</a>.
            </p>
            <ol start="2">
                <li>
                    <p align="justify">
                        <strong>What data may the Company collect about you?</strong>
                    </p>
                </li>
            </ol>
            <p align="justify">
                Personal data, or personal information, means any information about an
                individual from which that person can be identified. It does not include
                data where the identity has been removed (anonymous data).
            </p>
            <p align="justify">
                The Company may collect, use, store, and transfer different kinds of
                personal data about you, which the Company has grouped together as follows:
            </p>
            <ul>
                <li>
                    <p align="justify">
                        <strong>Identity Data</strong>
                        includes first name, last name, username or similar identifier,
                        date of birth, and gender.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        <strong>Contact Data</strong>
                        includes billing address, email address, and telephone numbers.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        <strong>Financial Data</strong>
                        includes bank account and payment card details.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        <strong>Transaction Data</strong>
                        includes details about payments to and from you and other details
                        of products and services you have purchased from the Company.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        <strong>Technical Data</strong>
                        includes internet protocol (IP) address, your login data, browser
                        type and version, time zone setting and location, browser plug-in
                        types and versions, operating system and platform, and other
                        technology on the devices you use to access this Website.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        <strong>Profile Data </strong>
                        includes your username and password, purchases made by you, your
                        interests, preferences, feedback, and survey responses.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        <strong>Usage Data </strong>
                        includes information about how you use the Company’s Website,
                        products, and services.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        <strong>Marketing and Communications Data</strong>
                        includes your preferences in receiving marketing from the Company
                        and its third parties and your communication preferences.
                    </p>
                </li>
            </ul>
            <p align="justify">
                The Company also collects, uses, and shares    <strong>Aggregated Data</strong> such as statistical or demographic data
                for any purpose. Aggregated Data may be derived from your personal data but
                is not considered personal data in law as this data does    <strong>not</strong> directly or indirectly reveal your identity. For
                example, the Company may aggregate your Usage Data to calculate the
                percentage of users accessing a specific website feature. However, if the
                Company combines or connects Aggregated Data with your personal data so
                that it can directly or indirectly identify you, the Company treats the
                combined data as personal data which will be used in accordance with this
                policy.
            </p>
            <p align="justify">
                Where the Company needs to collect personal data by law or under the terms
                of a contract it has with you and you fail to provide that data when
                requested, the Company may not be able to perform the contract it has or is
                trying to enter into with you (for example, to provide you with goods or
                services). In this case, the Company may have to cancel a product or
                service you have with the Company, but the Company will notify you if this
                is the case at the time.
            </p>
            <ol start="3">
                <li>
                    <p align="justify">
                        <strong>How is your personal data collected?</strong>
                    </p>
                </li>
            </ol>
            <p align="justify">
                The Company uses different methods to collect data from and about you
                including through:
            </p>
            <ul>
                <li>
                    <p align="justify">
                        <strong>Direct interactions</strong>
                        . You may give the Company information about you by filling in
                        forms or by corresponding with the Company by phone, email, or
                        otherwise. This includes information you provide when you create an
                        account; subscribe to a service; search for a person or content;
                        place an order; participate in discussion boards or other social
                        media functions on the Website; enter a competition, promotion, or
                        survey; and when you report a problem with the Website.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        <strong>Automated technologies or interactions</strong>
                        . As you interact with the Website, the Company may automatically
                        collect technical data about your equipment, browsing actions, and
                        patterns as specified above. The Company collects this information
                        by using cookies, server logs, and other similar technologies (see
                        <em>
                            <strong>
                                Cookies and automatic data collection technologies
                            </strong>
                        </em>
                        ).
                    </p>
                </li>
                <li>
                    <p align="justify">
                        <strong>Third parties or publicly available sources</strong>
                        . The Company may receive information about you if you visit other
                        websites employing the Company’s cookies or from third parties
                        including, for example, business partners; subcontractors in
                        technical, payment, and delivery services; advertising networks;
                        analytics providers; search information providers; credit reference
                        agencies; data brokers; or aggregators.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        <strong>User contributions</strong>
                        . You also may provide information for the Company to publish or
                        display (“<strong>post</strong>”) on public Website areas or
                        transmit to other Website users or third parties (collectively, “            <strong>user contributions</strong>”). You submit user
                        contributions for posting and transmission to others at your own
                        risk. Although you may set certain privacy settings for user
                        contributions by logging into your account profile, please be aware
                        that no security measures are perfect or impenetrable.
                        Additionally, the Company cannot control the actions of any Website
                        users with whom you choose to share your user contributions.
                        Therefore, we cannot and do not guarantee that unauthorized persons
                        will not view your user contributions.
                    </p>
                </li>
            </ul>
            <p align="justify">
                <em>
                    <strong>Cookies and Automatic Data Collection Technologies</strong>
                </em>
            </p>
            <p align="justify">
                The Company’s Website uses cookies (small files placed on your device) or
                other automatic data collection technologies to distinguish you from other
                Website users. This helps the Company deliver a better and more
                personalized service when you browse the Website. It also allows the
                Company to improve the Website by enabling it to:
            </p>
            <ul>
                <li>
                    <p align="justify">
                        Estimate the Website’s audience size and usage patterns.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        Store your preferences so the Company may customize the Website
                        according to your individual interests.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        Speed up your searches.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        Recognize you when you return to the Website.
                    </p>
                </li>
            </ul>
            <p align="justify">
                The Company also may use these technologies to collect information about
                your online activities over time and across third-party websites or other
                online services (behavioral tracking).
            </p>
            <p align="justify">
                You may refuse to accept browser cookies by activating the appropriate
                setting on your browser. However, if you select this setting, certain parts
                of the Website may become inaccessible and certain features may not work
                correctly. Unless you adjust your browser settings to refuse cookies, the
                Company’s system will issue them. For detailed information on the cookies
                the Company uses and the purposes for which it uses them, see the Cookie
                Policy.
            </p>
            <p align="justify">
                The Company’s Website pages and emails may contain web beacons (small
                transparent embedded images or objects, also known as clear gifs, pixel
                tags, and single-pixel gifs) that permits it, for example, to count website
                page visitors or email readers, or to compile other similar statistics such
                as recording Website content popularity or verifying system and server
                integrity.
            </p>
            <p align="justify">
                <em>
                    <strong>
                        Third-party Use of Cookies and Other Tracking Technologies
                    </strong>
                </em>
            </p>
            <p align="justify">
                Some content or applications, including advertisements, on the Website are
                served by third parties, including advertisers, ad networks and servers,
                content providers, and application providers. These third parties may use
                cookies alone or in conjunction with web beacons or other tracking
                technologies to collect information about you when you use the Website.
                They may associate the information collected with your personal data or
                they may collect information, including personal data, about your online
                activities over time and across different websites and other online
                services. They may use this information to provide you with interest-based
                (behavioral) advertising or other targeted content.
            </p>
            <p align="justify">
                The Company does not control how these third-party tracking technologies
                operate or how they may use the collected data. If you have any questions
                about an advertisement or other targeted content, you should contact the
                responsible provider directly.
            </p>
            <ol start="4">
                <li>
                    <p align="justify">
                        <strong>How the Company uses your personal data</strong>
                    </p>
                </li>
            </ol>
            <p align="justify">
                The Company uses your personal data to provide you with products, offer you
                services, communicate with you, deliver advertising and marketing, or to
                conduct other business operations, such as using data to improve and
                personalize your experiences. Examples of how the Company may use the
                personal data it collects includes to:
            </p>
            <ul>
                <li>
                    <p align="justify">
                        Present the Website and provide you with the information, products,
                        services, and support that you request from the Company.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        Meet the Company’s obligations and enforce its rights arising from
                        any contracts with you, including for billing and collections, or
                        to comply with legal requirements
                    </p>
                </li>
                <li>
                    <p align="justify">
                        Fulfill the purposes for which you provided the data or that were
                        described when it was collected.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        Notify you about changes to the Website, products, or services.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        Ensure that the Company presents the Website content in the most
                        effective manner for you and for your computer.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        Administer the Website and conduct internal operations, including
                        for troubleshooting, data analysis, testing, research, statistical,
                        and survey purposes.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        Improve the Website, products or services, marketing, or customer
                        relationships and experiences.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        Enable your participation in the Website’s interactive, social
                        media, or other similar features.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        Protect the Website, the Company’s employees, or the Company’s
                        operations.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        Measure or understand the effectiveness of the advertising the
                        Company serves to you and others, and to deliver relevant
                        advertising to you.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        Make suggestions and recommendations to you and other users of the
                        Website about goods or services that may interest you or them.
                    </p>
                </li>
            </ul>
            <p align="justify">
                The Company may also use personal data to contact you about its own and
                third parties’ goods and services that may be of interest to you. If you do
                not want the Company to use your data in this way, please send the Company
                an email at    <a href="mailto:privacy@brandtsboys.com">privacy@brandtsboys.com</a> or
                adjust your user preferences in your account profile. For more information,
                see <em><strong>Your personal data use choices</strong></em>.
            </p>
            <p align="justify">
                The Company may use personal data to enable it to display advertisements to
                its advertisers’ target audiences. Even though the Company does not
                disclose your personal data for these purposes without your consent, if you
                click on or otherwise interact with an advertisement, the advertiser may
                assume that you meet its target criteria.
            </p>
            <p align="justify">
                The Company may use nonpersonal data for any business purpose.
            </p>
            <ol start="5">
                <li>
                    <p align="justify">
                        <strong>Disclosure of your personal data</strong>
                    </p>
                </li>
            </ol>
            <p align="justify">
                The Company may share your personal data with:
            </p>
            <ul>
                <li>
                    <p align="justify">
                        Any member of the Company’s corporate group, which means its
                        subsidiaries, its ultimate holding company and its subsidiaries,
                        and affiliates.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        Business partners, suppliers, service providers, subcontractors,
                        and other third parties that the Company uses to support its
                        business (such as analytics and search engine providers that assist
                        the Company with Website improvement and optimization). The Company
                        contractually requires these third parties to keep that personal
                        data confidential and use it only for the contracted purposes.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        Third parties to market their products or services to you if you
                        have not opted out of these disclosures. For more information, see            <em><strong>Your personal data use choices</strong></em>.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        Advertisers and advertising networks that require the data to
                        select and serve relevant adverts to you and others.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        To fulfill the purpose for which you provide it. For example, if
                        you give the Company an email address to use the Website’s “email a
                        friend” feature, the Company will transmit the contents of that
                        email and your email address to the recipients.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        For any other purposes that the Company discloses in writing when
                        you provide the data.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        With your consent.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        Payment-service providers to (a) process payments; (b) prevent,
                        detect, and investigate fraud or other prohibited activities; (c)
                        facilitate dispute resolution such as chargebacks or refunds; and
                        (d) for other purposes associated with the acceptance of credit or
                        debit cards. The Company contractually requires these third parties
                        to keep that personal data confidential and use it only for the
                        contracted purposes. The Company may share your credit or debit
                        card number with payment-service providers or card networks to
                        monitor card transactions at participating merchants and track
                        redemption activity for the purposes of providing card-linked
                        services.
                    </p>
                </li>
            </ul>
            <p align="justify">
                The Company may also disclose your personal data to third parties:
            </p>
            <ul>
                <li>
                    <p align="justify">
                        If the Company sells or buys any business or assets, in which case
                        the Company may disclose your personal data to the prospective
                        seller or buyer of the business or assets.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        To a buyer or other successor in the event of merger, divestiture,
                        restructuring, reorganization, dissolution, or other sale or
                        transfer of some or all the Company’s assets, whether as a going
                        concern or as part of bankruptcy, liquidation, or similar
                        proceeding, where one of the transferred assets is the personal
                        data the Company holds.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        If you submit a copyright infringement notice under the Digital
                        Millennium Copyright Act (DMCA), the Company forwards DMCA
                        infringement notices (including any personal data contained in the
                        notices) to the person or entity who stored, transmitted, or linked
                        to the content addressed by your notice as submitted without any
                        deletions.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        To comply with any court order, law, or legal process, including to
                        respond to any government or regulatory request.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        To enforce or apply the Company’s <a href="https://paymentsbb.com/terms-of-service.html">Terms-of-Service</a> Agreement, <a href="https://paymentsbb.com/model-agreement.html">Model
                        Agreement</a>, and other agreements.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        To protect the rights, property, or safety of the Company’s
                        business, its employees, its customers, or others. This includes
                        exchanging information with other companies and organizations for
                        the purposes of cybersecurity, fraud protection, and credit risk
                        reduction.
                    </p>
                </li>
            </ul>
            <p align="justify">
                The Company may share nonpersonal data without restriction.
            </p>
            <ol start="6">
                <li>
                    <p align="justify">
                        <strong>Consent to personal data transfer</strong>
                    </p>
                </li>
            </ol>
            <p align="justify">
                The Company is based in the United States of America. The Company may
                process, store, and transfer personal data it collects, in and to a country
                outside your own, with different privacy laws that may or may not be as
                comprehensive as your own.
            </p>
            <p align="justify">
                By submitting your personal data or engaging with the Website, you consent
                to this transfer, storing, or processing.
            </p>
            <ol start="7">
                <li>
                    <p align="justify">
                        <strong>Your personal data use choices</strong>
                    </p>
                </li>
            </ol>
            <p align="justify">
                The Company strives to provide you with choices regarding certain personal
                data uses, particularly around marketing and advertising. The Company has
                established the following personal data control mechanisms:
            </p>
            <ul>
                <li>
                    <p align="justify">
                        <strong>Promotional Offers from the Company</strong>
                        . If you do not want the Company to use your email address to
                        promote its own products and services, or third parties’ products
                        or services, you can optout by checking the relevant box located on
                        the form where the Company collects your data (the registration
                        form) or at any other time by logging into the Website and checking
                        or unchecking the relevant boxes to adjust your account profile’s
                        user preferences, or by sending the Company an email with your
                        request to
                        <a href="mailto:privacy@brandtsboys.com">
                            privacy@brandtsboys.com
                        </a>
                        . You may also optout of further marketing communications by
                        replying to any promotional email the Company has sent you or
                        following the optout links on that message. This opt out does not
                        apply to information provided to the Company as a result of a
                        purchase or other transactions.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        <strong>Third-Party Advertising</strong>
                        . If you do not want the Company to share your personal data with
                        unaffiliated or non-agent third parties for promotional purposes,
                        you can optout by checking the relevant box located on the form
                        where the Company collects your data (the registration form). You
                        can also always optout by logging into the Website and checking or
                        unchecking the relevant boxes to adjust your account profile’s user
                        preferences, or by sending the Company an email stating your
                        request to
                        <a href="mailto:privacy@brandtsboys.com">
                            privacy@brandtsboys.com
                        </a>
                        .
                    </p>
                </li>
                <li>
                    <p align="justify">
                        <strong>Tracking Technologies and Advertising</strong>
                        . You can set your browser to refuse all or some browser cookies,
                        or to alert you when websites set or access cookies. If you disable
                        or refuse cookies, please note that some parts of this Website may
                        become inaccessible or not function properly. For more information
                        about tracking technologies, please see
                        <em>
                            <strong>
                                Cookies and automatic data collection technologies
                            </strong>
                        </em>
                        .
                    </p>
                </li>
                <li>
                    <p align="justify">
                        <strong>Targeted Advertising</strong>
                        . If you do not want the Company to use information that it
                        collects or that you provide to the Company to deliver
                        advertisements according to its advertisers’ target-audience
                        preferences, you can optout by checking the relevant box located on
                        the form where the Company collects your data (the registration
                        form). You can also always adjust your user advertising preferences
                        in your account profile by checking or unchecking the relevant
                        boxes, or by sending the Company an email stating your request to
                        <a href="mailto:privacy@brandtsboys.com">
                            privacy@brandtsboys.com
                        </a>
                        . For this optout to function, you must have your browser set to
                        accept browser cookies.
                    </p>
                </li>
            </ul>
            <p align="justify">
                The Website may, from time to time, contain links to and from the websites
                of our partner networks, advertisers and affiliates, or plug-ins enabling
                third-party features. If you follow a link to any third-party website or
                engage a third-party plug-in, please note that these third parties have
                their own privacy policies and that the Company does not accept any
                responsibility or liability for these policies. Please check these policies
                before you submit any personal data to these third parties.
            </p>
            <ol start="8">
                <li>
                    <p align="justify">
                        <strong>Accessing and correcting your personal data</strong>
                    </p>
                </li>
            </ol>
            <p align="justify">
                You can access, review, and change your personal data by logging into the
                Website and visiting your account profile page.
            </p>
            <p align="justify">
                You may also send the Company an email at    <a href="mailto:privacy@brandtsboys.com">privacy@brandtsboys.com</a> to
                request access to, correct, or delete any personal data that you have
                provided to the Company. The Company cannot delete your personal data
                except by also deleting your user account. The Company may not accommodate
                a request to change information if the Company believes the change would
                violate any law or legal requirement or negatively affect the information’s
                accuracy. In addition, if you are a model, the Company may retain
                indefinitely certain personal data you submit to the Company, including
                your identification, in case the information is needed to comply with 18
                U.S.C. §§<a href="https://www.law.cornell.edu/uscode/text/18/2257">2257</a>–    <a href="https://www.law.cornell.edu/uscode/text/18/2257A">2257A</a> and
                <a href="https://www.law.cornell.edu/cfr/text/28/part-75">
                    28 C.F.R. Part 75
                </a>
                or in a good-faith belief that preservation or disclosure of that data is
                reasonably necessary in the Company’s opinion to (a) comply with legal
                process, including civil and criminal subpoenas, court orders, or other
                compulsory disclosure; (b) enforce the <a href="https://paymentsbb.com/model-agreement.html">Model Agreement</a>; (c) respond to
                claims of a violation of the rights of third parties, regardless of whether
                the third party is a user, individual, or government agency; or (d) protect
                the rights, property, or personal safety of the Company, the Website’s
                users, or the public.
            </p>
            <p align="justify">
                If you delete your user contributions from the Website, copies of your user
                contributions may remain viewable in cached and archived pages or might
                have been copied or stored by other Website users. The Company’s
                <a href="https://paymentsbb.com/terms-of-service.html">Terms-of-Service Agreement</a> and <a href="https://paymentsbb.com/model-agreement.html">Model Agreement</a> govern proper access and use
                of information provided on the Website, including user contributions.
            </p>
            <ol start="9">
                <li>
                    <p align="justify">
                        <strong>Data security</strong>
                    </p>
                </li>
            </ol>
            <p align="justify">
                The security of your personal data is very important to the Company. The
                Company uses reasonable and appropriate security measures designed to
                protect your personal data from loss, misuse, and unauthorized access, use,
                alteration, or disclosure. The Company stores all personal data behind
                firewalls on severs employing security protections. The Company encrypts
                any payment transactions using SSL technology.
            </p>
            <p align="justify">
                The safety and security of your information also depends on you. Where the
                Company has given you (or where you have chosen) a password for access to
                certain parts of the Website, you are responsible for keeping this password
                confidential. The Company asks you not to share your password with anyone.
                The Company urges you to take care when providing information in public
                areas of the Website, which any Website visitor can view.
            </p>
            <p align="justify">
                Unfortunately, the transmission of information via the Internet is not
                completely secure. Although the Company does its best to protect your
                personal data, the Company cannot guarantee the security of your personal
                data transmitted to the Website. Any transmission of personal data is at
                your own risk. The Company is not responsible for the circumvention of any
                privacy settings or security measures contained on the Website.
            </p>
            <ol start="10">
                <li>
                    <p align="justify">
                        <strong>Do Not Track Policy</strong>
                    </p>
                </li>
            </ol>
            <p align="justify">
                Do Not Track (“DNT”) is a privacy preference that you can set in your
                browser. DNT is a way for you to inform websites and services that you do
                not want certain information about your webpage visits collected over time
                and across websites or online services. The Company is committed to
                providing you with meaningful choices about the information it collects and
                that is why the Company provides you the ability to opt out. But the
                Company does not recognize or respond to any DNT signals as the Internet
                industry works toward defining exactly what DNT means, what it means to
                comply with DNT, and a common approach to responding to DNT. For more
                information, visit    <a href="http://www.allaboutdnt.com/">www.allaboutdnt.com</a>.
            </p>
            <ol start="11">
                <li>
                    <p align="justify">
                        <strong>Your California Privacy Rights</strong>
                    </p>
                </li>
            </ol>
            <p align="justify">
                If you are a California resident, you may have certain additional rights.
                California Civil Code Section 1798.83 permits you to request information
                regarding the disclosure of your personal information by the Company to
                third parties for the third parties’ direct marketing purposes. Further, if
                you are a California resident and would like to opt out from the disclosure
                of your personal information to any third party for direct marketing
                purposes, please send an email to    <a href="mailto:privacy@brandtsboys.com">privacy@brandtsboys.com</a>. If
                you opt out from permitting your personal information to be shared, you may
                still receive selected offers directly from the Company in accordance with
                California law.
            </p>
            <ol start="12">
                <li>
                    <p align="justify">
                        <strong>Your European Economic Area Privacy Rights</strong>
                    </p>
                </li>
            </ol>
            <p align="justify">
                If you are located in the European Economic Area, under certain
                circumstances, you have rights under data protection laws in relation to
                your personal data. Your rights may include the following:
            </p>
            <ul>
                <li>
                    <p align="justify">
                        <strong>Request access</strong>
                        to your personal data. This enables you to receive a copy of the
                        personal data the Company holds about you and to check that the
                        Company is lawfully processing it.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        <strong>Request correction</strong>
                        of your personal data. This enables you to have any incomplete or
                        inaccurate data the Company holds about you corrected, though the
                        Company may need to verify the accuracy of the new data you provide
                        to it.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        <strong>Request erasure</strong>
                        of your personal data. This enables you to ask the Company to
                        delete or remove personal data where there is no good reason for
                        the Company continuing to process it. You also have the right to
                        ask the Company to delete or remove your personal data where you
                        have successfully exercised your right to object to processing (see
                        below), where the Company may have processed your information
                        unlawfully, or where the Company is required to erase your personal
                        data to comply with local law. Note, however, that the Company may
                        not always be able to comply with your request of erasure for
                        specific legal reasons that will be notified to you, if applicable,
                        at the time of your request.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        <strong>Object to processing</strong>
                        of your personal data where the Company is relying on a legitimate
                        interest (or those of a third party) and there is something about
                        your particular situation that makes you want to object to
                        processing on this ground as you feel it impacts on your
                        fundamental rights and freedoms. You also have the right to object
                        where the Company is processing your personal data for direct
                        marketing purposes. In some cases, the Company may demonstrate that
                        it has compelling legitimate grounds to process your information
                        that overrides your rights and freedoms.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        <strong>Request restriction of processing</strong>
                        your personal data. This enables you to ask the Company to suspend
                        the processing of your personal data in the following scenarios:
                        (a) if you want the Company to establish the data’s accuracy; (b)
                        where the Company’s use of the data is unlawful but you do not want
                        the Company to erase it; (c) where you need the Company to hold the
                        data even if the Company no longer requires it as you need it to
                        establish, exercise, or defend legal claims; or (d) you have
                        objected to the Company’s use of your data but the Company needs to
                        verify whether the Company has overriding legitimate grounds to use
                        it.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        <strong>Request transfer</strong>
                        of your personal data to you or to a third party. The Company will
                        provide to you, or a third party you have chosen, your personal
                        data in a structured, commonly used, machine-readable format. Note
                        that this right only applies to automated information that you
                        initially provided consent for the Company to use or where it used
                        the information to perform a contract with you.
                    </p>
                </li>
                <li>
                    <p align="justify">
                        <strong>Right to withdraw consent</strong>
                        at any time where the Company is relying on consent to process your
                        personal data. However, this will not affect the lawfulness of any
                        processing carried out before you withdraw your consent. If you
                        withdraw your consent, the Company may not be able to provide
                        certain products or services to you. The Company will advise you if
                        this is the case at the time you withdraw your consent.
                    </p>
                </li>
            </ul>
            <p align="justify">
                If you wish to exercise any of the rights set out above, please contact the
                Company at    <a href="mailto:privacy@brandtsboys.com">privacy@brandtsboys.com</a>.
            </p>
            <p align="justify">
                You will not have to pay a fee to access your personal data (or to exercise
                any of the other rights). However, the Company may charge a reasonable fee
                if your request is clearly unfounded, repetitive, or excessive.
                Alternatively, the Company may refuse to comply with your request in these
                circumstances.
            </p>
            <p align="justify">
                The Company may need to request specific information from you to help it
                confirm your identity and ensure your right to access your personal data
                (or to exercise any of your other rights). This is a security measure to
                ensure that personal data is not disclosed to any person who has no right
                to receive it. The Company may also contact you to ask you for further
                information in relation to your request to speed up its response.
            </p>
            <p align="justify">
                The Company tries to respond to all legitimate requests within one month.
                Occasionally it may take the Company longer than a month if your request is
                particularly complex or you have made a number of requests. In this case,
                the Company will notify you and keep you updated.
            </p>
            <ol start="13">
                <li>
                    <p align="justify">
                        <strong>Changes to Privacy Policy</strong>
                    </p>
                </li>
            </ol>
            <p align="justify">
                <a name="_Hlk511140378"></a>
                The Company will post any changes it makes to its privacy policy on this
                page. If the changes materially alter how the Company uses or treats your
                personal data, it will notify you by email to the primary email address
                specified in your account or through a notice on the Website home page. The
                date the privacy policy was last revised is identified at the top of the
                page. You are responsible for ensuring that the Company has an up-to-date
                active and deliverable email address for you. Please check back frequently
                to see any updates or changes to this privacy policy.
            </p>
            <ol start="14">
                <li>
                    <p align="justify">
                        <strong>Contact Information</strong>
                    </p>
                </li>
            </ol>
            <p align="justify">
                Questions, comments, and requests regarding this policy and the Company’s
                privacy practices are welcomed and should be addressed to    <a href="mailto:privacy@brandtsboys.com">privacy@brandtsboys.com</a>.
            </p>
            <p align="justify">
                DNA Enterprises, LLC, an Iowa limited liability company (the “    <strong>Company</strong>”), respects your privacy and is committed to
                protecting it through this privacy policy (the “<strong>policy</strong>”).
            </p>
        </div>
        <Footer/>
    </div>
);

export default connect(
    (state) => ({
        user: state.auth.user,
        isLoggedIn: state.auth.isLoggedIn
    })
)(PrivacyPolicyPage)