import {layoutBreakpoints, layoutOrientations} from '../features/ui/constants';

export const calcDimensions = (item) => {
    if (!item) return;

    const width = window.innerWidth;
    const height = window.innerHeight;

    let orientation;

    if (width < layoutBreakpoints.MOBILE) {
        if (width < height) orientation = layoutOrientations.PORTRAIT;
        else orientation = layoutOrientations.LANDSCAPE;
    } else orientation = layoutOrientations.DESKTOP;

    let ratio = item.width/item.height;

    switch (orientation) {
        case (layoutOrientations.PORTRAIT): {
            console.log(orientation);
            return {
                ratio,
                width: width*0.8,
                height: width*0.8/ratio
            }
        }

        case (layoutOrientations.LANDSCAPE): {
            console.log(orientation);
            return {
                ratio,
                width: height*0.8,
                height: height*0.8/ratio
            }
        }

        case (layoutOrientations.DESKTOP): {
            return (ratio > 1)
                ?  {
                    ratio,
                    width: width*0.8,
                    height: width*0.8/ratio
                }
                :  {
                    ratio,
                    width: height*0.8,
                    height: height*0.8/ratio
                };
        }

        default: return {
            ratio,
            width: width*0.8,
            height: width*0.8/ratio
        };
    }
};