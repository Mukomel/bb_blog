export default function ensureEnvVariablesExists () {
    if (!process.env.REACT_APP_SEGPAY_DYNAMIC_PRICEPOINT_ID) {
        throw 'REACT_APP_SEGPAY_DYNAMIC_PRICEPOINT_ID is missing in .env file'
    }
    if (!process.env.REACT_APP_SEGPAY_DYNAMIC_PACKAGE_ID) {
        throw 'REACT_APP_SEGPAY_DYNAMIC_PACKAGE_ID is missing in .env file'
    }
}