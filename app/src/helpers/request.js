import config from '../config';
import {getToken, setToken} from './auth';
import store from '../store';
import {logout} from '../features/auth/actions';

export const getDefaultHeaders = () => {

    let headers =  {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + getToken()
    };

    return headers;
};

export default function(route, method = "GET", data, headers = getDefaultHeaders(), stringifyData = true) {
    data = stringifyData ? JSON.stringify(data) : data;

    const url = config[process.env.REACT_APP_ENV].API_DOMAIN + route;

    return fetch(url, {
        method: method,
        headers: headers,
        body: data
    })
        .then(res => {
            if (res.status === 401) {
                store.dispatch(logout());
            }
            return res;
        })
};

export const xhrRequest = (route, method = "GET", data, onProgress, headers = getDefaultHeaders(), stringifyData = true) => {
    const url = config[process.env.REACT_APP_ENV].API_DOMAIN + route;
    data = stringifyData ? JSON.stringify(data) : data;
    return new Promise( (res, rej)=>{
        let xhr = new XMLHttpRequest();
        xhr.open(method, url);
        for (let k in headers)
            xhr.setRequestHeader(k, headers[k]);
        xhr.onload = e => res(e.target.responseText);
        xhr.onerror = rej;
        if (xhr.upload && onProgress)
            xhr.upload.onprogress = onProgress;
        xhr.send(data);
        return xhr;
    });
};