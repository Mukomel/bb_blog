import store from 'store';

export const setToken = token => {
    store.set('bb_token', token);
};

export const setCookie = (cookie, value) => {
    store.set(cookie, value);
};

export const getCookie = (cookie) => {
    return store.get(cookie);
};

export const getToken = () => {
    return store.get('bb_token');
};

export const hasToken = () => {
    return Boolean(getToken());
};

export const removeToken = () => {
    store.remove('bb_token');
};

export const signOut = () => {
    removeToken();
    window.location.reload();
};