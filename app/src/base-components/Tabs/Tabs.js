import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import * as styles from './Tabs.module.scss';

export default class Tabs extends React.Component {
    render() {
        const {tabsList, currentTab, onTabChange} = this.props;

        return (
            <div className={styles.container}>
                {tabsList.map(({tab, title}) => (
                    <div
                        key={tab}
                        className={cn(styles.tab, {[styles.active]: tab === currentTab})}
                        onClick={() => onTabChange(tab)}
                    >
                        {title}
                    </div>
                ))}
            </div>
        )
    }
}

Tabs.propTypes = {
    tabsList: PropTypes.arrayOf(PropTypes.shape({
        tab: PropTypes.string,
        title: PropTypes.string
    })),
    currentTab: PropTypes.string.isRequired,
    onTabChange: PropTypes.func.isRequired
};