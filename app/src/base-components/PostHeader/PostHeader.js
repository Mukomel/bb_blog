import React from 'react';
import { string, func } from 'prop-types';
import TipInput from '../../features/donation/components/TipInput/TipInput';

import * as styles from './PostHeader.module.scss';

const PostHeader = ({title, avatar, username, onTip, children}) => (
    <div className={styles.header}>
        {
            title && <div className={styles.postTitle}>{title}</div>
        }
        <div className={styles.author}>
            <div className={styles.authorContainer}>
                <img src={avatar} className={styles.authorAvatar}/>
                <div className={styles.authorName}>{username}</div>
                {children}
            </div>
            <TipInput onTipClick={onTip} className={styles.tipInputContainer}/>
        </div>
    </div>

);

PostHeader.propTypes = {
    title: string.isRequired,
    username: string.isRequired,
    avatar: string.isRequired,
    onTip: func.isRequired,
};

export default PostHeader