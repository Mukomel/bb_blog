import React from 'react';
import Button from '@material-ui/core/Button';

import styles from './FileInput.module.scss';

export default class FileInput extends React.Component {
    adaptFileEventToValue = delegate => e => delegate(e.target.files[0]);

    handleButtonClick = () => this.input.click();

    render() {
        const {
            input: {
                value,
                onChange,
                onBlur,
                ...inputProps,
            },
            meta: omitMeta,
            selectFileLabel,
            changeFileLabel,
            ...props,
        } = this.props;

        return (
            <div>
                {
                    value
                        && <div className={styles.filename}>{value.name}</div>
                }
                <Button
                    variant="raised"
                    color="primary"
                    type="button"
                    onClick={this.handleButtonClick}
                >
                    {
                        value
                            ? changeFileLabel || 'Change file'
                            : selectFileLabel || 'Select file'
                    }
                </Button>
                <input
                    ref={node => this.input = node}
                    className={styles.input}
                    onChange={this.adaptFileEventToValue(onChange)}
                    onBlur={this.adaptFileEventToValue(onBlur)}
                    type="file"
                    {...inputProps}
                    {...props}
                />
            </div>
        )
    }
}