import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import {showModal, hideModal} from '../../features/ui/actions';
import {modalTypes} from '../../features/ui/constants';

class ImageLoaderZone extends Component {
    state = {
        imageUrl: null,
        filled: false
    };

    handleButtonClick = () => this.props.showCropperDialog(this.props);

    render () {
        const {
            input: {
                value,
            },
            meta: {
                error,
                visited,
                submitFailed
            },
            selectFileLabel,
            changeFileLabel
        } = this.props;

        return (
            <div>
                <Button
                    variant="raised"
                    color="primary"
                    type="button"
                    onClick={this.handleButtonClick}
                >
                    {
                        value
                            ? changeFileLabel || 'Change file'
                            : selectFileLabel || 'Select file'
                    }
                </Button>
                {
                    ((visited || submitFailed) && error) &&
                    <span>{error}</span>
                }
            </div>
        );
    }
}

export default connect(
    state => ({}),
    dispatch => ({
        showCropperDialog: modalProps => dispatch(showModal(modalTypes.CROPPER_EDITOR_MODAL, modalProps)),
        hideCropperDialog: modalProps => dispatch(hideModal(modalTypes.CROPPER_EDITOR_MODAL))
    })
)(ImageLoaderZone)
