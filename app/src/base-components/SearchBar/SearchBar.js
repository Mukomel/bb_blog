import React, {Component} from 'react';
import {string, node, func} from 'prop-types';
import cn from 'classnames';
import CloseIcon from '@material-ui/icons/Close';
import SearchIcon from '@material-ui/icons/Search';

import * as styles from './SearchBar.module.scss';

export default class SearchBar extends Component {
    static propTypes = {
        placeholder: string,
        searchIcon: node,
        className: string.isRequired,
        onSearch: func.isRequired
    };
    
    static defaultProps = {
        placeholder: 'Search',
        searchIcon: <SearchIcon/>,
    };
    
    state = {
        value: '',
        isSearchBarActive: false
    };
    
    handleChange = ({target: {value}}) => this.setState({value});

    handleCloseIconClick = () => this.setState({
        value: '',
        isSearchBarActive: false
    });

    handleInputFocus = () => this.setState({isSearchBarActive: true});

    handleInputBlur = () => {
        if (!this.state.value.trim().length > 0) {
            this.setState({isSearchBarActive: false})
        }
    };

    handleSearchSubmit = () => {
        this.props.onSearch(this.state.value);
        this.setState({
            value: '',
            isSearchBarActive: false
        })
    };

    render() {
        const {className, placeholder, searchIcon} = this.props;
        const {value, isSearchBarActive} = this.state;
        const searchBarClasses = cn([styles.searchBar, className]);
        const searchIconClasses = cn([styles.searchIcon, {
            [styles.active]: isSearchBarActive
        }]);
        const closeIconClasses = cn([styles.closeIcon, {
            [styles.active]: isSearchBarActive && value.length > 0
        }]);
        return (
            <div className={searchBarClasses}>
                <span className={searchIconClasses} onClick={this.handleSearchSubmit}>{searchIcon}</span>
                <input
                    type="text"
                    name="search"
                    placeholder={placeholder}
                    onChange={this.handleChange}
                    value={value}
                    onFocus={this.handleInputFocus}
                    onBlur={this.handleInputBlur}
                />
                <span className={closeIconClasses}>
                    <CloseIcon onClick={this.handleCloseIconClick}/>
                </span>
            </div>
        )
    }
}