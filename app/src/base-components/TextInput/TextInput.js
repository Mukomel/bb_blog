import React from 'react';
import * as styles  from './TextInput.module.scss';

const TextInput = ({ input, placeholder, meta: { touched, error } }) => (
    <div className={styles.container}>
        <input
            {...input}
            type='text'
            className={styles.input}
            placeholder={placeholder}
        />
        {touched && error && <div className={styles.errorMessage}>{error}</div>}
    </div>
);

export default TextInput;
