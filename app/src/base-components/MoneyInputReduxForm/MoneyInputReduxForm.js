import React from 'react';
import InputAdornment from '@material-ui/core/InputAdornment';
import {TextField} from 'redux-form-material-ui';
import NumberFormat from 'react-number-format';

import styles from './MoneyInputReduxForm.module.scss';

function NumberFormatCustom(props) {
    const {inputRef, onChange, ...other} = props;

    return (
        <NumberFormat
            {...other}
            ref={inputRef}
            decimalScale={2}
            fixedDecimalScale={true}
            allowNegative={false}
            onValueChange={values => {
                onChange(values.value);
            }}
            thousandSeparator
        />
    );
}

export default class MoneyInputReduxForm extends React.Component {
    render() {
        return (
            <TextField
                margin="normal"
                placeholder='0.00'
                type='tel'
                InputProps={{
                    startAdornment: <InputAdornment position="start"
                                                    className={styles.addon}>$</InputAdornment>,
                    inputComponent: NumberFormatCustom,
                    disableUnderline: true
                }}
                {...this.props}
            />
        )
    }
}
