import React from 'react';
import cn from 'classnames';
import { Field } from 'redux-form';

import * as styles from './FormTextField.module.scss';

const FormTextField = ({className, ...props}) => (
    <Field
        className={cn(styles.textField, className)}
        {...props}
    />
);

export default FormTextField