import React, {Component} from 'react';
import cn from 'classnames';
import Cropper from 'react-cropper';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';
import 'cropperjs/dist/cropper.css';


import * as styles from './CropperEditor.module.scss';

class CropEditor extends Component {
    static propTypes = {
        aspectRatio: PropTypes.number,
        circled: PropTypes.bool,
        accept: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
        image: PropTypes.node.isRequired,
        cropperHeight: PropTypes.number,
        cropperWidth: PropTypes.number
    };

    static defaultProps = {
        circled: false,
        aspectRatio: 4,
        accept: 'image/*',
    };

    state = { file: null };

    onFileSelect = (file) => {
        if (!this.state.file || !this.state.file.preview) {
            this.setState({file})
        }
    };

    handleCropImage = () => {
        this.cropperNode.cropper.getCroppedCanvas().toBlob(blob => {
            this.props.update(blob)
        });
    };

    renderCrop = () => {
        const {aspectRatio,cropperHeight, cropperWidth, circled, ...props} = this.props;

        return (
            <Cropper
                ref={node => this.cropperNode = node}
                className={circled ? styles.rounded : ''}
                src={this.state.file.preview}
                style={{
                    height: cropperHeight,
                    width: cropperWidth
                }}
                aspectRatio={aspectRatio}
                center
                crop={this.handleCropImage}
                {...props}
            />
        )
    };

    renderDropzone = () => {
        const {accept, image} = this.props;
        return (
            <Dropzone
                accept={accept}
                onDrop={files => this.onFileSelect(files[0])}
                multiple={false}
            >
                <div className={styles.innerDrop}>
                    <p>Select Photo</p>
                    {image}
                </div>
            </Dropzone>
        )
    };


    render() {
        const {file} = this.state;

        return (
            <div className={cn([styles.cropEditorContainer, {[styles.active]: file}])}>
                {
                    file && file.preview ? this.renderCrop() : this.renderDropzone()
                }
            </div>
        )
    }
}

export default CropEditor;