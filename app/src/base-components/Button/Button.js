import React from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';

import styles from './Button.module.scss';

const Button = ({className, children, type, onClick, variant, disabled}) => {
    const buttonClasses = cn(styles.button, className, {
        [styles[variant]]: variant
    });
    return (
        <button className={buttonClasses} type={type} onClick={onClick} disabled={disabled}>
            {children}
        </button>
    )
};

Button.propTypes = {
    type: PropTypes.string,
    className: PropTypes.string,
    variant: PropTypes.string,
    disabled: PropTypes.bool,
    onClick: PropTypes.func.isRequired,
};

Button.defaultProps = {
    type: 'button'
};

export default Button;
