import React from 'react';
import PropTypes from 'prop-types';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import Collapse from '@material-ui/core/Collapse'
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

import styles from './Sidebar.module.scss';

export default class Sidebar extends React.Component {
    state = { isOpenNestedMenu: false };

    handleMenuItemClick = value => {
        const { onClose, onItemClick } = this.props;
        onItemClick(value);
        onClose();
    };

    handleNestedMenuItemClick = () =>
        this.setState(prevState => ({ isOpenNestedMenu: !prevState.isOpenNestedMenu }));

    render() {
        const {isOpen, onClose, items} = this.props;
        const {isOpenNestedMenu} = this.state;

        return (
            <Drawer
                anchor="right"
                open={isOpen}
                onClose={onClose}
            >
                <div
                    tabIndex={0}
                    role="button"
                >
                    <List className={styles.list}>
                        {
                            items.map(item => {
                                const handleOnClick = item.children ?
                                    this.handleNestedMenuItemClick:
                                    () => this.handleMenuItemClick(item.value);
                                return (
                                    <React.Fragment>
                                        <ListItem
                                            button
                                            key={item.value}
                                            onClick={handleOnClick}>
                                            <ListItemText primary={item.label} />
                                            {
                                                item.children &&
                                                <React.Fragment>
                                                    {
                                                        isOpenNestedMenu ? <ExpandLess /> : <ExpandMore />
                                                    }
                                                </React.Fragment>
                                            }
                                        </ListItem>
                                        {
                                            item.children &&
                                            <Collapse
                                                in={isOpenNestedMenu}
                                                timeout="auto" unmountOnExit>
                                                <List component="div" disablePadding dense>
                                                    {
                                                        item.children.map(childrenItem => (
                                                            <ListItem
                                                                button
                                                                key={childrenItem.value}
                                                                onClick={() => this.handleMenuItemClick(childrenItem.value)}
                                                            >
                                                                <ListItemText
                                                                    inset
                                                                    primary={childrenItem.label}
                                                                />
                                                            </ListItem>
                                                        ))
                                                    }
                                                </List>
                                            </Collapse>
                                        }
                                    </React.Fragment>
                                )
                            })
                        }
                    </List>
                </div>
            </Drawer>
        )
    }
}

Sidebar.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    onItemClick: PropTypes.func.isRequired,
    items: PropTypes.arrayOf(PropTypes.shape({
        label: PropTypes.string.isRequired,
        value: PropTypes.string.isRequired
    }))
};
