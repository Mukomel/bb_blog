import React from 'react';
import { string, number, bool } from 'prop-types';
import cn from 'classnames';
import moment from'moment';
import * as styles from './PostComment.module.scss';

const PostComment = ({avatar, username, comment, createdData, likesCount, children, pointsCount, topFanIcon}) => {
    const a = moment();
    const b = moment(createdData);
    const dayPassed = a.diff(b, 'days') < 1;
    const timeComment = b.from(a);
    const userNameClasses = cn([styles.userName, styles[topFanIcon]]);
    return (
        <React.Fragment>
            {avatar ? <img
                src={`${avatar}`}
                className={styles.userAvatar}
                alt="avatar"
            /> : null}
            <div className={styles.textComment}>
                <p className={userNameClasses}>
                    {username}
                    {
                        pointsCount > 0 && <span className={styles.pointsCount}>{pointsCount}</span>
                    }
                </p>
                <p className={styles.definitelySharingT}>{comment}</p>
                <span className={styles.hour}>
                {dayPassed ? timeComment : moment(createdData).format('MMM DD, h:mm a')}
            </span>
                {
                    likesCount === 0 ? null : <span className={styles.replyLikes}>{likesCount} likes</span>
                }
                {children}
            </div>
        </React.Fragment>
    );
};

PostComment.propTypes = {
    avatar: string,
    pointsCount: number,
    username: string.isRequired,
    comment: string.isRequired,
    createdData: string.isRequired,
    likesCount: number.isRequired,
};

PostComment.defaultProps = {
    avatar: '',
    isTopFan: false,
    pointsCount: 0,
};

export default PostComment
