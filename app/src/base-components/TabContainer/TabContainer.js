import React from 'react';
import { node } from 'prop-types';
import Typography from '@material-ui/core/Typography';

const TabContainer = ({children}) => (
    <Typography component="div">
        {children}
    </Typography>
);

TabContainer.propTypes = {
    children: node.isRequired
};

export default TabContainer;
