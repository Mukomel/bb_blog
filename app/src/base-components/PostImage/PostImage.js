import React from 'react';
import { string, func } from 'prop-types';

import * as styles from './PostImage.module.scss';

const PostImage = ({imagePath, onImageClick}) => (
    <div className={styles.imageContainer}>
        <figure className={styles.figure}>
            <img src={imagePath} className={styles.image} onClick={onImageClick}/>
        </figure>
    </div>
);

PostImage.propTypes = {
    imagePath: string.isRequired,
    onImageClick: func.isRequired,
};

export default PostImage