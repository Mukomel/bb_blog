import React from 'react';
import moment from 'moment';
import cn from 'classnames';
import { string, func, bool, number } from 'prop-types';
// eslint-disable-next-line import/no-webpack-loader-syntax
import Like from '-!svg-react-loader!../../assets/images/like.svg';
// eslint-disable-next-line import/no-webpack-loader-syntax
import ViewIcon from '-!svg-react-loader!../../assets/images/eye.svg';

import * as styles from './PostContent.module.scss';

const PostContent = ({postDate, body, isPostLiked, onLike, views, likesCount, children}) => (
    <div className={styles.content}>
        <div className={styles.postDate}>{moment(postDate).format('MMM DD [at] h:mm a')}</div>
        <div className={styles.postStats}>
            <div className={styles.blockLikesCounter}>
                <Like className={cn(styles.like, {
                    [styles.likesActive]: isPostLiked
                })} onClick={onLike} />
                <span className={styles.counterLikes}>{likesCount}</span>
            </div>
            {views ? <div>
                <ViewIcon className={styles.view} />
                <span className={styles.counterLikes}>{views}</span>
            </div> : null}
        </div>
        <div className={styles.postBody}>{body}</div>
        {children}
    </div>
);

PostContent.propTypes = {
    postDate: string.isRequired,
    body: string,
    onLike: func.isRequired,
    isPostLiked: bool.isRequired,
    views: number,
    likesCount: number,
};

PostContent.defaultprops = {
    views: 0,
    likesCount: 0,
};

export default PostContent
