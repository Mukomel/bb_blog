import React from 'react';
import {object, string} from 'prop-types';
import cn from 'classnames';
import starIcon from '../../assets/images/star.png';

import * as styles from './UserDescription.module.scss';

const UserDescription = ({user: {avatar, username, points}, className}) => (
    <div className={cn(styles.user, className)}>
        <div className={styles.usernameWrapper}>
            <img src={`${avatar ? avatar : `https://api.adorable.io/avatars/50/${username}.png`}`} className={styles.avatar}/>
            <div className={styles.userName}>{username}</div>
        </div>
        <div className={styles.pointsWrapper}>
            <img src={starIcon} className={styles.starImage}/>{points}
        </div>
    </div>
);

UserDescription.propTypes = {
    user: object.isRequired,
    className: string
};

export default UserDescription
