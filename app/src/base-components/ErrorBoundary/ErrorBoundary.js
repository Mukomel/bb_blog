import React, {Component} from 'react';
import {connect} from 'react-redux';
import { sendErrorRequest} from "../../features/artist/actions";
import {showModal} from "../../features/ui/actions";
import {modalTypes} from "../../features/ui/constants";

class ErrorBoundary extends Component {
    state = { hasError: false };

    componentDidCatch(error, errorInfo) {
        const {showErrorBoundaryModal, sendError, user} = this.props;
        this.setState({ hasError: true }, () => {
            showErrorBoundaryModal();
            sendError(user ? user._id : "guest", error)
        })
    }

    render() {
        if (this.state.hasError) {
            return null
        }
        return this.props.children;
    }
}

export default connect(
    state => ({
        user: state.auth.user
    }),
    dispatch => ({
        sendError: (userId, error) => dispatch(sendErrorRequest(userId, error)),
        showErrorBoundaryModal: () => dispatch(showModal(modalTypes.ERROR_BOUNDARY_MODAL, {}))
    })
)(ErrorBoundary)

