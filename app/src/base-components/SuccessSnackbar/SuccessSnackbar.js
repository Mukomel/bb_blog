import React from 'react';
import {object} from 'prop-types';
import {connect} from 'react-redux';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import Fade from '@material-ui/core/Fade';
import {withStyles} from '@material-ui/core/styles'
import {hideSuccessMessage} from "../../features/ui/actions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

const styles = {
    root: {
        top: 30
    },

    snackbarContent: {
        background: '#3bc251',
        borderRadius: 10,
        maxWidth: '90vh'
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
};

const SuccessSnackbar = ({classes, isSuccessMessageVisible, hideSuccessMessage}) => {
    const snackbarPosition = {
        vertical: 'top',
        horizontal: 'center'
    };
    return (
        <Snackbar
            className={classes.root}
            anchorOrigin={snackbarPosition}
            open={isSuccessMessageVisible}
            autoHideDuration={6000}
            onClose={hideSuccessMessage}
        >
            <SnackbarContent
                className={classes.snackbarContent}
                aria-describedby="client-snackbar"
                message={
                    <span id="client-snackbar" className={classes.message}>
                    Your changes were saved
                </span>
                }
                action={[
                    <IconButton
                        key="close"
                        aria-label="Close"
                        color="inherit"
                        onClick={hideSuccessMessage}
                    >
                        <CloseIcon/>
                    </IconButton>
                ]}
            />
        </Snackbar>
    )
};

SuccessSnackbar.propTypes = {
    classes: object.isRequired
};

export default connect(
    state => ({
        isSuccessMessageVisible: state.ui.isSuccessMessageVisible
    }),
    dispatch => ({
        hideSuccessMessage: () => dispatch(hideSuccessMessage())
    })
)(withStyles(styles)(SuccessSnackbar))
