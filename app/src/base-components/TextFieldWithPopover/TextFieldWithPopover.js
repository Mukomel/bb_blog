import React from 'react';
import { func, bool, string, object } from 'prop-types';
import {TextField} from 'redux-form-material-ui';
import Popover from '@material-ui/core/Popover';
import CloseIcon from '@material-ui/icons/Close';
import Button from '@material-ui/core/Button'

import * as styles from './TextFieldWithPopover.module.scss';

const TextFieldWithPopover = ({isShowPopoverOnError, onCloseIconClick, errorClassName, anchorEl, meta: { warning }, ...rest}) => {
    return (
        <React.Fragment>
            {
                isShowPopoverOnError && warning ? <Popover
                    open={Boolean(anchorEl)}
                    anchorEl={anchorEl}
                    onClose={onCloseIconClick}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                    }}
                >
                    <p className={errorClassName}>{warning}</p>
                    <Button className={styles.closeButton} disableRipple>
                        <CloseIcon onClick={onCloseIconClick}/>
                    </Button>
                </Popover> : null
            }
            <TextField {...rest}/>
        </React.Fragment>
    )
};

TextFieldWithPopover.propTypes = {
    isShowPopoverOnError: bool.isRequired,
    onCloseIconClick: func.isRequired,
    errorClassName: string.isRequired,
    anchorEl: object.isRequired
};

export default TextFieldWithPopover