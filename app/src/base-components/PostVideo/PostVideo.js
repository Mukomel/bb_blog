import React from 'react';
import cn from 'classnames';
import { string, func, bool } from 'prop-types';

import * as styles from './PostVideo.module.scss';

const PostVideo = ({onPlayVideo, onPauseVideo, title, videoPath, isTitleActive, getRef}) => (
    <div className={styles.mediaContainer}>
        <div className={styles.videoWrapper}>
            <div className={styles.innerVideoContainer}>
                <video loop
                       controls
                       controlsList="nodownload"
                       onPlay={onPlayVideo}
                       onPause={onPauseVideo}
                       ref={node => getRef(node)}
                >
                    <source src={videoPath}/>
                </video>
            </div>
        </div>
        <div onClick={onPlayVideo} className={cn([styles.postTitle, styles.postVideoTitle, {
            [styles.activeVideoTitle]: isTitleActive
        }])}>{title}</div>
    </div>
);

PostVideo.propTypes = {
    videoPath: string.isRequired,
    onPlayVideo: func.isRequired,
    getRef: func.isRequired,
    onPauseVideo: func.isRequired,
    title: string.isRequired,
    isTitleActive: bool.isRequired,
};

export default PostVideo
