import React from 'react';
import { func, bool } from 'prop-types'
import styles from './Switch.module.scss'

const Switch = ({ onSwitch, defaultChecked }) => {
    
    const handleSwitch = ({ target }) => {
        onSwitch(target.checked)
    };
    
    return (
        <label className={styles.switch}>
            <input type="checkbox" onChange={handleSwitch} checked={defaultChecked}/>
            <span className={styles.slider} />
        </label>
    )
};

Switch.propTypes = {
    onSwitch: func.isRequired,
    defaultChecked: bool.isRequired
};

export default Switch