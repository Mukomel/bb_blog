import React from 'react';
import PropTypes from 'prop-types';

export default class BackgroundImage extends React.PureComponent {
    render() {
        const {url, className, size, attachment, onClick} = this.props;

        const styles = {
            backgroundImage: url ? `url(${encodeURI(url)})` : 'none',
            backgroundRepeat: 'no-repeat',
            backgroundSize: size,
            backgroundPosition: 'center center',
            backgroundAttachment: attachment
        };
        return <div style={styles} className={className} onClick={onClick}></div>
    }
}

BackgroundImage.propTypes = {
    url: PropTypes.string,
    className: PropTypes.string,
    size: PropTypes.oneOf(['cover', 'contain']),
    attachment: PropTypes.oneOf(['scroll', 'fixed']),
    onClick: PropTypes.func
};

BackgroundImage.defaultProps = {
    size: 'contain',
    attachment: 'scroll'
};
