import React from 'react';
import {Route, Redirect} from 'react-router-dom';
import SpinnerWithBackdrop from '../SpinnerWithBackdrop/SpinnerWithBackdrop';
import {userRoles} from '../../features/auth/constants';

const AuthenticatedAdminRoute = ({component: Component, componentProps, ...rest}) => {
    const route = props => {
        const {isLoggedIn, isFetchingUser, user} = componentProps;
        if (isLoggedIn && isFetchingUser) {
            return <SpinnerWithBackdrop />
        }
        if (!isLoggedIn || (user.role !== userRoles.ADMIN)) {
            return <Redirect to='/' />
        }
        if (isLoggedIn && (user.role === userRoles.ADMIN)) {
            return <Component {...props} {...componentProps}/>
        }
    };

    return <Route {...rest} render={route} />;
};


export default AuthenticatedAdminRoute;
