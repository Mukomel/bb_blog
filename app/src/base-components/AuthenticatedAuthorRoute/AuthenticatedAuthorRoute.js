import React from 'react';
import {Route, Redirect} from 'react-router-dom';
import SpinnerWithBackdrop from '../SpinnerWithBackdrop/SpinnerWithBackdrop';
import {userRoles} from '../../features/auth/constants';

const AuthenticatedAuthorRoute = ({component: Component, componentProps, ...rest}) => {
    const route = props => {
        const {isLoggedIn, isFetchingUser, user} = componentProps;
        if (isLoggedIn && isFetchingUser) {
            return <SpinnerWithBackdrop />
        }
        if (!isLoggedIn || ((user.role !== userRoles.ADMIN) && (user.role !== userRoles.AUTHOR))) {
            return <Redirect to='/' />
        }
        if (isLoggedIn && ((user.role === userRoles.ADMIN) || (user.role === userRoles.AUTHOR))) {
            return <Component {...props} {...componentProps}/>
        }
    };

    return <Route {...rest} render={route} />;
};


export default AuthenticatedAuthorRoute;
