import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import * as styles from './CloseFormIconButton.module.scss';

const CloseFormIconButton = ({onClick, ...rest}) => (
    <IconButton
        onClick={onClick}
        className={styles.closeButton}
        {...rest}
    >
        <CloseIcon/>
    </IconButton>
);

export default CloseFormIconButton