import React, {Component} from 'react';
import cn from 'classnames';
import {string, func, bool} from 'prop-types';
import LightTooltip from '../LightTooltip/LightTooltip';
import InfoIcon from '@material-ui/icons/Info';

import * as styles from './Checkbox.module.scss';

export default class Checkbox extends Component {
    constructor(props) {
        super(props);
        this.state = { checked: props.defaultChecked }
    }

    static propTypes = {
        onCheck: func.isRequired,
        name: string.isRequired,
        label: func,
        defaultChecked: bool
    };

    static defaultProps = { defaultChecked: false };

    handleCheck = ({target: {checked}}) => this.setState({checked}, () =>
        this.props.onCheck(this.state.checked));

    render() {
        const { className, name, label = '', tooltip} = this.props;
        return (
            <label htmlFor="checkbox" className={cn(styles.label, className)}>
                <input
                    className={styles.checkbox}
                    type="checkbox"
                    id="checkbox"
                    name={name}
                    defaultChecked={this.state.checked}
                    onChange={this.handleCheck}
                />
                <span>{label}</span>
                {
                    tooltip
                        && <span className={styles.tooltipContainer}>
                            <LightTooltip title={tooltip}>
                                <InfoIcon/>
                            </LightTooltip>
                        </span>
                }
            </label>
        );
    }
}
