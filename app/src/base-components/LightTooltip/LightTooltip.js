import React from 'react';
import {object} from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';

const styles = theme => ({
    lightTooltip: {
        background: theme.palette.common.white,
        color: theme.palette.text.primary,
        boxShadow: theme.shadows[1],
        fontSize: 14,
        position: 'relative',
        top: -90
    }
});

const LightTooltip = ({classes, children, ...props}) => {
    return (
        <Tooltip {...props} classes={{ tooltip: classes.lightTooltip }}>
            {children}
        </Tooltip>
    )
};

LightTooltip.propTypes = {
    classes: object.isRequired,
};

export default withStyles(styles)(LightTooltip);