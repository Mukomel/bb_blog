import React from 'react'
import {array, func, string, object} from 'prop-types'
import { withStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl'
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

const styles = theme => ({
    formControl: {
        marginRight: 48,
        minWidth: 120,
        maxWidth: 300,
    }
});

const CustomSelect = ({data, label, onSelectChange, value, classes, disabled}) => (
    <FormControl className={classes.formControl}>
        <InputLabel htmlFor="select">{label}</InputLabel>
        <Select
            value={value}
            onChange={onSelectChange}
            input={<Input id="select"/>}
            disabled={disabled}
        >
            {
                data && data.map(item => <MenuItem key={item} value={item}>{item}</MenuItem>)
            }
        </Select>
    </FormControl>
);

CustomSelect.propTypes = {
    classes: object.isRequired,
    data: array.isRequired,
    onSelectChange: func.isRequired,
    label: string.isRequired,
    value: string.isRequired,
};

export default withStyles(styles)(CustomSelect);
