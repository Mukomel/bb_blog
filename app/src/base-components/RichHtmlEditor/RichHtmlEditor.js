import React, {Component} from 'react';
import RichTextEditor from 'react-rte';
import {func, string, shape} from 'prop-types';

class RichHtmlEditor extends Component {
    static propTypes = {
        input: shape({
            onChange: func.isRequired,
            value: string
        }).isRequired
    };

    state = {value: null};

    componentDidMount() {
        const {input} = this.props;
        this.RichTextEditor = RichTextEditor;
        this.setState({
            value: input.value ?
                this.RichTextEditor.createValueFromString(input.value, 'html') :
                this.RichTextEditor.createEmptyValue()
        })
    }

    handleChange = value => {
        this.setState({value});
        let html = value.toString('html');
        this.props.input.onChange(html)
    };

    render() {
        const toolbarConfig = {
            display: [
                'INLINE_STYLE_BUTTONS',
                'BLOCK_TYPE_BUTTONS',
                'LINK_BUTTONS',
                'BLOCK_TYPE_DROPDOWN',
                'HISTORY_BUTTONS'],
            INLINE_STYLE_BUTTONS: [
                {label: 'Bold', style: 'BOLD', className: 'custom-css-class'},
                {label: 'Italic', style: 'ITALIC'},
                {label: 'Underline', style: 'UNDERLINE'}
            ],
            BLOCK_TYPE_DROPDOWN: [
                {label: 'Normal', style: 'unstyled'},
                {label: 'Heading Large', style: 'header-one'},
                {label: 'Heading Medium', style: 'header-two'},
                {label: 'Heading Small', style: 'header-three'}
            ],
            BLOCK_TYPE_BUTTONS: [
                {label: 'UL', style: 'unordered-list-item'},
                {label: 'OL', style: 'ordered-list-item'}
            ]
        };

        const {RichTextEditor, state: {value}, handleChange} = this;
        const { errorClassName, meta: { valid, error }} = this.props;

        return RichTextEditor ?
            <React.Fragment>
                {
                   !valid ? <p className={errorClassName}>{error}</p>: null
                }
                <RichTextEditor
                    {...this.props}
                    toolbarConfig={toolbarConfig}
                    value={value}
                    onChange={handleChange}
                />
            </React.Fragment> : null
    }
}

export default RichHtmlEditor