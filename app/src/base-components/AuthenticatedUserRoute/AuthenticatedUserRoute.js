import React from 'react';
import {Route, Redirect} from 'react-router-dom';
import SpinnerWithBackdrop from '../SpinnerWithBackdrop/SpinnerWithBackdrop';

const AuthenticatedUserRoute = ({component: Component, componentProps, ...rest}) => {
    const route = props => {
        const {isLoggedIn, isFetchingUser, user} = componentProps;
        if (isLoggedIn && isFetchingUser) {
            return <SpinnerWithBackdrop />
        }
        if (!isLoggedIn) {
            return <Redirect to='/' />
        }
        if (isLoggedIn && user) {
            return <Component {...props} {...componentProps}/>
        }
    };

    return <Route {...rest} render={route} />;
};


export default AuthenticatedUserRoute;