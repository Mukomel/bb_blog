export default {
    production: {
        API_DOMAIN: 'https://brandtsboys.com/api',
        FRONTEND_DOMAIN: 'https://brandtsboys.com',
        STRIPE_PUBLISHABLE_KEY: 'pk_live_clzhjpybZwB4qm0f4Qs5fgfS',
        // API_DOMAIN: 'http://localhost:3001',
        // STRIPE_PUBLISHABLE_KEY: 'pk_test_R0uuWc3MPH0IiJNvbVjBqdXp',
        STRIPE_ACCOUNT_COUNTRY_CODE: 'US',
        PERIOD_UNLOCK_PRICE_CENTS: 1999
    },
    staging: {
        API_DOMAIN: 'http://146.185.160.118:360/api',
        FRONTEND_DOMAIN: 'http://146.185.160.118:360',
        STRIPE_PUBLISHABLE_KEY: 'pk_test_R0uuWc3MPH0IiJNvbVjBqdXp',
        STRIPE_ACCOUNT_COUNTRY_CODE: 'US',
        PERIOD_UNLOCK_PRICE_CENTS: 1999
    },
    development: {
        API_DOMAIN: 'http://localhost:3001/api',
        FRONTEND_DOMAIN: 'http://localhost:3001',
        STRIPE_PUBLISHABLE_KEY: 'pk_test_R0uuWc3MPH0IiJNvbVjBqdXp',
        STRIPE_ACCOUNT_COUNTRY_CODE: 'US',
        PERIOD_UNLOCK_PRICE_CENTS: 1999
    },
    acceptedImageMIMETypes: ['image/png', 'image/jpeg', 'image/gif'],
    acceptedVideoMIMETypes: ['video/x-msvideo', 'video/avi', 'video/mp4', 'video/quicktime'],
}
